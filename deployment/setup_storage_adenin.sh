#!/usr/bin/env bash

# Output configuration
COLOR='\033[0;32m'
NC='\033[0m' # No Color

# Add user 'genwork:gen'
echo -e "${COLOR} - Add user 'genwork:gen'${NC}"
sudo groupadd -g 1010 gen
sudo useradd -u 1001 --create-home --shell /bin/bash genwork
# sudo usermod -a -G gen genwork
# group 'gen': Primary group
sudo usermod -g gen genwork
# Re-add default group 'genwork'
sudo usermod -a -G genwork genwork
sudo echo genwork ALL=NOPASSWD:ALL | sudo tee -a /etc/sudoers

# Installation: ZFS filesystem
echo -e "${COLOR} - Install ZFS filesystem${NC}"
# Install ZFS
sudo apt install zfsutils-linux zfs-initramfs
# sudo apt install zfs-dracut
# List and check Storage Devices
sudo fdisk -l
# ZFS-Configuration: Server with 48 Disks (RAID-Z1, 8 VDEVs à 6 discs) mounted to destination /srv/zfspool
zpool list
sudo zpool create -m /srv/zfspool zfspool \
  raidz1 /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh  \
  raidz1 /dev/sdi /dev/sdj /dev/sdk /dev/sdl /dev/sdm /dev/sdn \
  raidz1 /dev/sdo /dev/sdp /dev/sdq /dev/sdr /dev/sds /dev/sdt \
  raidz1 /dev/sdu /dev/sdv /dev/sdw /dev/sdx /dev/sdy /dev/sdz \
  raidz1 /dev/sdaa /dev/sdab /dev/sdac /dev/sdad /dev/sdae /dev/sdaf \
  raidz1 /dev/sdag /dev/sdah /dev/sdai /dev/sdaj /dev/sdak /dev/sdal \
  raidz1 /dev/sdam /dev/sdan /dev/sdao /dev/sdap /dev/sdaq /dev/sdar \
  raidz1 /dev/sdas /dev/sdat /dev/sdau /dev/sdav /dev/sdaw /dev/sdax
# Mount Pool 'zfspool' to other destination
# sudo zfs set mountpoint=/srv/zfspool zfspool
# Completely delete ZFS-Pool 'zfspool'
# sudo zpool destroy zfspool

# Check Pool status
sudo zpool status
zpool list
df -h
# Monitoring
# zpool iostat 2

# Get and Set LZ4 compression in ZFS pool
# zfs get all
# zfs get compression zfspool
# zfs get compressratio zfspool
sudo zfs set compression=lz4 zfspool

# ZIL, SLOG and Caching (L2ARC)
# Read: https://linuxhint.com/configuring-zfs-cache
# https://blog.programster.org/zfs-add-intent-log-device
sudo zpool add zfspool log /dev/sdb
# sudo zpool add zfspool log mirror /dev/sdb /dev/sdc
# sudo zpool add zfspool cache /dev/sdd

echo -e "${COLOR} - Install NFS server${NC}"
sudo apt-get install nfs-kernel-server watchdog
# sudo apt-get install nfs-kernel-server samba-common-bin
sudo chmod -R 777 /srv/zfspool
sudo chown nobody:nogroup /srv/zfspool
sudo nano /etc/exports
sudo systemctl restart nfs-kernel-server

echo -e "${COLOR} - Install NFS client${NC}"
sudo apt-get install -y nfs-common
(sudo echo "# Network-share (NFS at GWDG) ifs/genometation, NFSv3") | sudo tee -a /etc/fstab
(sudo echo "# Device                        Mountpoint               FStype  Options         Dump    Pass#") | sudo tee -a /etc/fstab
(sudo echo "10.153.18.1:/srv/zfspool        /nfs/zfs_thymin          nfs     rw,noatime      0       0") | sudo tee -a /etc/fstab
(sudo echo "134.76.13.205:/ifs/genometation /nfs/nfs_genometation    nfs     rw,noatime      0       0") | sudo tee -a /etc/fstab
sudo mkdir -p /nfs/zfs_thymin
sudo mkdir -p /nfs/nfs_genometation
sudo mount -a

echo -e "${COLOR} - Initial sync: Copy data from StorThymin${NC}"
# Initial sync: Copy data from StorThymin
screen -S rsync
sudo -i -u genwork
rsync -ahuv --delete --stats /nfs/zfs_thymin/* /srv/zfspool > rsync.log

echo -e "${COLOR} - Set firewall rules${NC}"
# Create firewall rules
# sudo ufw status
# Status: active
#
# To                         Action      From
# --                         ------      ----
# OpenSSH                    ALLOW       Anywhere
# OpenSSH (v6)               ALLOW       Anywhere (v6)
# 2049                       ALLOW       10.153.18.1
# 2049                       ALLOW       10.153.18.3
# 2049                       ALLOW       10.153.18.4
# 2049                       ALLOW       10.153.18.10
# 2049                       ALLOW       10.153.18.11
# 2049                       ALLOW       10.153.18.12
# 2049                       ALLOW       10.153.18.13
# ...
# 2049                       ALLOW       10.153.18.43
# 2049                       ALLOW       10.153.18.44
# 2049                       ALLOW       10.153.18.45
sudo ufw status numbered
sudo ufw enable
# sudo ufw delete 2
# sudo ufw insert 2 allow from 10.153.18.1 to any port nfs
sudo ufw allow OpenSSH
sudo ufw allow from 10.153.18.1 to any port nfs
sudo ufw allow from 10.153.18.3 to any port nfs
sudo ufw allow from 10.153.18.4 to any port nfs
sudo ufw allow from 10.153.18.10 to any port nfs
sudo ufw allow from 10.153.18.11 to any port nfs
sudo ufw allow from 10.153.18.12 to any port nfs
sudo ufw allow from 10.153.18.13 to any port nfs
sudo ufw allow from 10.153.18.14 to any port nfs
sudo ufw allow from 10.153.18.15 to any port nfs
sudo ufw allow from 10.153.18.16 to any port nfs
sudo ufw allow from 10.153.18.17 to any port nfs
sudo ufw allow from 10.153.18.18 to any port nfs
sudo ufw allow from 10.153.18.19 to any port nfs
sudo ufw allow from 10.153.18.20 to any port nfs
sudo ufw allow from 10.153.18.21 to any port nfs
sudo ufw allow from 10.153.18.22 to any port nfs
sudo ufw allow from 10.153.18.23 to any port nfs
sudo ufw allow from 10.153.18.24 to any port nfs
sudo ufw allow from 10.153.18.25 to any port nfs
sudo ufw allow from 10.153.18.26 to any port nfs
sudo ufw allow from 10.153.18.27 to any port nfs
sudo ufw allow from 10.153.18.28 to any port nfs
sudo ufw allow from 10.153.18.29 to any port nfs
sudo ufw allow from 10.153.18.30 to any port nfs
sudo ufw allow from 10.153.18.31 to any port nfs
sudo ufw allow from 10.153.18.32 to any port nfs
sudo ufw allow from 10.153.18.33 to any port nfs
sudo ufw allow from 10.153.18.34 to any port nfs
sudo ufw allow from 10.153.18.35 to any port nfs
sudo ufw allow from 10.153.18.36 to any port nfs
sudo ufw allow from 10.153.18.37 to any port nfs
sudo ufw allow from 10.153.18.38 to any port nfs
sudo ufw allow from 10.153.18.39 to any port nfs
sudo ufw allow from 10.153.18.40 to any port nfs
sudo ufw allow from 10.153.18.41 to any port nfs
sudo ufw allow from 10.153.18.42 to any port nfs
sudo ufw allow from 10.153.18.43 to any port nfs
sudo ufw allow from 10.153.18.44 to any port nfs
sudo ufw allow from 10.153.18.45 to any port nfs
