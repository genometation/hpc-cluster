#!/bin/bash

echo "=== Start of custom node setup ==="
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com
echo " - Change default gateway to bond0"
sudo ip route change default via 10.153.18.254 dev bond0 proto static
echo " - Mount NFS-filesystem to Node"
sudo apt-get install -y nfs-common
(sudo echo "# Network-share (NFS at GWDG) ifs/genometation, NFSv3") | sudo tee -a /etc/fstab
(sudo echo "# Device                        Mountpoint               FStype  Options         Dump    Pass#") | sudo tee -a /etc/fstab
(sudo echo "10.153.18.2:/srv/zfspool        /nfs/zfs_adenin          nfs     rw,noatime      0       0") | sudo tee -a /etc/fstab
sudo mkdir -p /nfs/zfs_adenin
sudo mount -a

echo " - Install additional packages"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
echo " - Install Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
(sudo echo '{
    "insecure-registries" : ["localhost:32000"]
}') | sudo tee /etc/docker/daemon.json
sudo systemctl restart docker

echo " - Disable Swapping"
# Kubernetes: Turn off SWAP
# Read: https://discuss.kubernetes.io/t/swap-off-why-is-it-necessary/6879
# https://www.howtoforge.com/tutorial/how-to-install-kubernetes-on-ubuntu/
sudo swapon -s
sudo swapoff -a
# Disable the SWAP permanently, make a comment on the SWAP partition type
sudo sed -e '/swap/ s/^#*/#/' -i /etc/fstab

echo " - Install MicroK8s"
sudo snap install microk8s --classic --channel=1.20/stable
echo " - Set Group and User permissions for MicroK8s"
sudo usermod -a -G microk8s ubuntu
sudo chown -f -R ubuntu ~/.kube
echo " - Modify MicroK8s (enable addons)"
# sg microk8s "microk8s enable ha-cluster"
sudo /snap/bin/microk8s enable ha-cluster
sudo /snap/bin/microk8s status --wait-ready
# echo " - Generate new certificates"
# sudo /snap/bin/microk8s refresh-certs
echo " - Show MicroK8s cluster-info"
# sg microk8s "microk8s kubectl cluster-info"
sudo /snap/bin/microk8s kubectl cluster-info
echo" - Set default CNI interface"
# Calico: IP-AutoDetection
# https://docs.projectcalico.org/reference/node/configuration#ip-autodetection-methods
# https://github.com/ubuntu/microk8s/issues/1554
sudo sed -re "s/(value: \"first-found\")/value: \"interface=bond.*\"/" -i /var/snap/microk8s/current/args/cni-network/cni.yaml
microk8s kubectl apply -f /var/snap/microk8s/current/args/cni-network/cni.yaml
microk8s status --wait-ready
microk8s kubectl get pods --all-namespaces
echo -e " - Install and upgrade microk8s addon (dashboard:v2.2.0)"
# Upgrade MicroK8s dashboard addon from 2.0.0 to 2.2.0
cp /snap/microk8s/current/actions/dashboard.yaml .
# Change line: "image: kubernetesui/dashboard:v2.0.0" to "image: kubernetesui/dashboard:v2.2.0"
sed -re "s/(image: kubernetesui\/dashboard:v2.0.0)/image: kubernetesui\/dashboard:v2.2.0/" -i dashboard.yaml
SNAP_DATA=/var/snap/microk8s/current
microk8s.kubectl --kubeconfig=$SNAP_DATA/credentials/client.config apply -f dashboard.yaml
# echo -e " - Enable microk8s addons (dns rbac storage metrics-server)"
# microk8s enable dns rbac storage metrics-server
# microk8s status --wait-ready

echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com

echo "=== End of custom node setup ==="
