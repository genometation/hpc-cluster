#!/bin/bash

echo "=== Start of custom node setup ==="
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com
echo " - Change default gateway to bond0"
sudo ip route change default via 10.153.18.254 dev bond0 proto static
echo " - Mount NFS-filesystem to Node"
sudo apt-get install -y nfs-common
(sudo echo "# Network-share (NFS at GWDG) ifs/genometation, NFSv3") | sudo tee -a /etc/fstab
(sudo echo "# Device                        Mountpoint               FStype  Options         Dump    Pass#") | sudo tee -a /etc/fstab
(sudo echo "10.153.18.2:/srv/zfspool        /nfs/zfs_adenin          nfs     rw,noatime      0       0") | sudo tee -a /etc/fstab
sudo mkdir -p /nfs/zfs_adenin
sudo mount -a

echo " - Append Cluster IP information to /etc/hosts"
cp /etc/hosts /home/ubuntu
cat /nfs/zfs_adenin/software/config/hosts >> /home/ubuntu/hosts
sudo cp /home/ubuntu/hosts /etc/hosts

echo " - Install additional packages"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
echo " - Install Docker"
# Option 1: Snap
sudo snap install docker --classic
docker login --username genometation --password Mnt6wRfstUFaBqDBF2Mey3x9
## Option 2: Community edition
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo add-apt-repository \
#   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
#   $(lsb_release -cs) \
#   stable"
#sudo apt-get update
#sudo apt-get install -y docker-ce docker-ce-cli containerd.io
#sudo mkdir -p /etc/docker
#(sudo echo '{
#    "insecure-registries" : ["10.153.18.10:32000"]
#}') | sudo tee /etc/docker/daemon.json
#sudo systemctl restart docker

echo " - Disable Swapping"
# Kubernetes: Turn off SWAP
# Read: https://discuss.kubernetes.io/t/swap-off-why-is-it-necessary/6879
# https://www.howtoforge.com/tutorial/how-to-install-kubernetes-on-ubuntu/
sudo swapon -s
sudo swapoff -a
# Disable the SWAP permanently, make a comment on the SWAP partition type
sudo sed -e '/swap/ s/^#*/#/' -i /etc/fstab

echo " - Install MicroK8s"
sudo snap install microk8s --classic --channel=1.24/stable
echo " - Set Group and User permissions for MicroK8s"
sudo usermod -a -G microk8s ubuntu
sudo chown -f -R ubuntu ~/.kube
echo " - Modify MicroK8s (enable addons)"
# sg microk8s "microk8s enable ha-cluster"
sudo /snap/bin/microk8s enable ha-cluster
sudo /snap/bin/microk8s status --wait-ready
# echo " - Generate new certificates"
# sudo /snap/bin/microk8s refresh-certs
echo " - Show MicroK8s cluster-info"
# sg microk8s "microk8s kubectl cluster-info"
sudo /snap/bin/microk8s kubectl cluster-info
echo " - Join MicroK8s cluster (token: valid until 2023-06-07)"
# sg microk8s "microk8s join 10.153.18.10:25000/9409e5b7f03e221fa74793a08e4fa97b/3deda5ebdc35"
sudo /snap/bin/microk8s join 10.153.18.10:25000/9409e5b7f03e221fa74793a08e4fa97b/3deda5ebdc35
# Use the '--worker' flag to join a node as a worker not running the control plane, eg:
# sudo /snap/bin/microk8s join 10.153.18.10:25000/d268227143994630be804c696b70664f/fe660fdda832 --worker
echo " - Show MicroK8s cluster-info"
# sg microk8s "microk8s kubectl cluster-info"
sudo /snap/bin/microk8s kubectl cluster-info
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com

echo "=== End of custom node setup ==="
