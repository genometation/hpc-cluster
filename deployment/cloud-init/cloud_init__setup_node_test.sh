#!/bin/bash

echo "=== Start of custom node setup ==="
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com
echo " - Change default gateway to bond0"
sudo ip route change default via 10.153.18.254 dev bond0 proto static
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com

echo "=== End of custom node setup ==="
