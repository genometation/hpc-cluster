#!/bin/bash

echo "=== Start of custom database setup ==="
echo " - Check internet connection"
ip a
ip route
ping -c3 10.153.18.10
ping -c3 134.76.10.46
ping -c3 ubuntu.com
echo " - Change default gateway to bond0"
sudo ip route change default via 10.153.18.254 dev bond0 proto static
echo " - Mount NFS-filesystem to Node"
sudo apt-get install -y nfs-common
(sudo echo "# Network-share (NFS at GWDG) ifs/genometation, NFSv3") | sudo tee -a /etc/fstab
(sudo echo "# Device                        Mountpoint               FStype  Options         Dump    Pass#") | sudo tee -a /etc/fstab
(sudo echo "10.153.18.2:/srv/zfspool        /nfs/zfs_adenin          nfs     rw,noatime      0       0") | sudo tee -a /etc/fstab
sudo mkdir -p /nfs/zfs_adenin
sudo mount -a
echo " - Install additional packages"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
echo " - Install Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
(sudo echo '{
    "insecure-registries" : ["localhost:32000"]
}') | sudo tee /etc/docker/daemon.json
sudo systemctl restart docker

echo " - Install database PostgreSQL"
export POSTGRES_VERSION=12
sudo apt-get install -y postgresql-$POSTGRES_VERSION postgresql-contrib-$POSTGRES_VERSION
# sudo apt-get install -y postgresql-client-$POSTGRES_VERSION
sudo apt-get install -y postgresql-server-dev-$POSTGRES_VERSION
# sudo /etc/init.d/postgresql start psql --command "ALTER USER postgres PASSWORD 'databix';"
sudo systemctl stop postgresql@$POSTGRES_VERSION-main.service
# Run the backup utility
mv /var/lib/postgresql/$POSTGRES_VERSION/main /var/lib/postgresql/$POSTGRES_VERSION/main_old
# sudo -u postgres pg_basebackup -h 10.153.18.3 -D /var/lib/postgresql/$POSTGRES_VERSION/main -U repuser -v -P 'databix' --xlog-method=stream
sudo -u postgres pg_basebackup -h 10.153.18.3 -D /var/lib/postgresql/$POSTGRES_VERSION/main -U repuser -v -P 'databix' --wal-method=stream

echo " - Configure PostgreSQL"
#  Switch user to postgres
sudo -i -u postgres
export POSTGRES_VERSION=12
# TODO: HA-Cluster-Mode with 2nd database server
# Read: https://cloud.google.com/community/tutorials/setting-up-postgres-hot-standby
sed -re "s/#hot_standby = on/hot_standby = on/" -i /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf
sed -re "s/#primary_conninfo = ''/primary_conninfo = 'host=10.153.18.3 port=5432 user=repuser password=databix'/" -i /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf
sed -re "s/#promote_trigger_file = ''/promote_trigger_file = '/tmp/postgresql.trigger.5432'/" -i /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf
# Create the recovery configuration file
# cp -avr /usr/share/postgresql/$POSTGRES_VERSION/recovery.conf.sample /var/lib/postgresql/$POSTGRES_VERSION/main/recovery.conf
# sed -re "s/#hot_standby = on/hot_standby = on/" -i /etc/postgresql/$POSTGRES_VERSION/main/recovery.conf
exit
# Switch to root-user 'ubuntu'
sudo systemctl start postgresql@$POSTGRES_VERSION-main.service

# echo " - Configure Firewall"
# sudo ufw status
# sudo ufw enable
# sudo ufw allow OpenSSH
# sudo ufw allow from 10.153.18.2 to any port nfs
# sudo ufw allow from 10.153.18.3 to any port nfs
# sudo ufw allow from 10.153.18.4 to any port nfs
# sudo ufw allow from 10.153.18.10 to any port nfs
# sudo ufw allow from 10.153.18.11 to any port nfs
# sudo ufw allow from 10.153.18.12 to any port nfs
# sudo ufw allow from 10.153.18.13 to any port nfs
# sudo ufw allow from 10.153.18.14 to any port nfs
# sudo ufw allow from 10.153.18.15 to any port nfs
# sudo ufw allow from 10.153.18.16 to any port nfs
# sudo ufw allow from 10.153.18.17 to any port nfs
# sudo ufw allow from 10.153.18.18 to any port nfs
# sudo ufw allow from 10.153.18.19 to any port nfs
# sudo ufw allow from 10.153.18.20 to any port nfs
# sudo ufw allow from 10.153.18.21 to any port nfs
# sudo ufw allow from 10.153.18.22 to any port nfs
# sudo ufw allow from 10.153.18.23 to any port nfs
# sudo ufw allow from 10.153.18.24 to any port nfs
# sudo ufw allow from 10.153.18.25 to any port nfs
# sudo ufw allow from 10.153.18.26 to any port nfs
# sudo ufw allow from 10.153.18.27 to any port nfs
# sudo ufw allow from 10.153.18.28 to any port nfs
# sudo ufw allow from 10.153.18.29 to any port nfs
# sudo ufw allow from 10.153.18.30 to any port nfs
# sudo ufw allow from 10.153.18.31 to any port nfs
# sudo ufw allow from 10.153.18.32 to any port nfs
# sudo ufw allow from 10.153.18.33 to any port nfs
# sudo ufw allow from 10.153.18.34 to any port nfs
# sudo ufw allow from 10.153.18.35 to any port nfs
# sudo ufw allow from 10.153.18.36 to any port nfs
# sudo ufw allow from 10.153.18.37 to any port nfs
# sudo ufw allow from 10.153.18.38 to any port nfs
# sudo ufw allow from 10.153.18.39 to any port nfs
# sudo ufw allow from 10.153.18.40 to any port nfs
# sudo ufw allow from 10.153.18.41 to any port nfs
# sudo ufw allow from 10.153.18.42 to any port nfs
# sudo ufw allow from 10.153.18.43 to any port nfs
# sudo ufw allow from 10.153.18.44 to any port nfs
# sudo ufw allow from 10.153.18.45 to any port nfs

# TODO: Activate to join Kubernetes cluster
# echo " - Install MicroK8s"
# sudo snap install microk8s --classic --channel=1.20/stable
# echo " - Set Group and User permissions for MicroK8s"
# sudo usermod -a -G microk8s ubuntu
# sudo chown -f -R ubuntu ~/.kube
# echo " - Modify MicroK8s (enable addons)"
# # sg microk8s "microk8s enable ha-cluster"
# sudo /snap/bin/microk8s enable ha-cluster
# echo " - Generate new certificates"
# sudo /snap/bin/microk8s refresh-certs
# echo " - Show MicroK8s cluster-info"
# # sg microk8s "microk8s kubectl cluster-info"
# sudo /snap/bin/microk8s kubectl cluster-info
# echo " - Join MicroK8s cluster (token: valid until 2022-03-12)"
# # sg microk8s "microk8s join 10.153.18.10:25000/a4d67e72ae2ffd2cbd346b4c67274ef3"
# sudo /snap/bin/microk8s join 10.153.18.10:25000/a4d67e72ae2ffd2cbd346b4c67274ef3
# echo " - Show MicroK8s cluster-info"
# # sg microk8s "microk8s kubectl cluster-info"
# sudo /snap/bin/microk8s kubectl cluster-info
# echo " - Check internet connection"
# ip a
# ip route
# ping -c3 10.153.18.10
# ping -c3 134.76.10.46
# ping -c3 ubuntu.com

echo "=== End of custom database setup ==="
