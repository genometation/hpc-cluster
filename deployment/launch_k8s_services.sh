#!/usr/bin/env bash

user_dir=/home/dsimm-a

# Setup Redis (working-queue)
cd ~/scripts/hpc-cluster/kubernetes/services/rediswq
# Geneate container: Redis-Master
kubectl apply -f redis-pod.yaml
kubectl apply -f redis-service.yaml
# Geneate container: Redis-Client
kubectl run -i --tty redis-client --image=redis -- /bin/bash
# kubectl run -i --tty redis-client --image=redis -- bash -c "exit"

# Push images to K8s registry
# - Augustus
cd ~/scripts/hpc-cluster/docker/augustus
./k8s-update-image.sh
# - Infernal
cd ~/scripts/hpc-cluster/docker/infernal
./k8s-update-image.sh
# - tRNAscanSE-wq
cd ~/scripts/hpc-cluster/docker/tRNAscanSE-wq
./k8s-update-image.sh
# - PyScipio3
cd ~/projects/pyscipio3
./docker-setup-k8s.sh
