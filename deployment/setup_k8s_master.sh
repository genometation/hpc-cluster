#!/usr/bin/env bash

# Output configuration
COLOR='\033[0;32m'
NC='\033[0m' # No Color

deployment_dir='deployment'
kubernetes_dir='kubernetes/rbac'

# IP tables
sudo apt-get install iptables-persistent
sudo iptables -P FORWARD ACCEPT
# Installation: Docker (https://docs.docker.com/engine/install/ubuntu/)
# sudo snap remove --purge docker
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get remove docker-ce docker-ce-cli containerd.io
# Option 1: Community edition
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
# docker login
# Use Docker-Account for higher pull quota (200 pulls per 6 hours)
# Read: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry
# cat ~/.docker/docker-credentials.txt | docker login --username genometation --password-stdin
docker login --username genometation --password Mnt6wRfstUFaBqDBF2Mey3x9
# Check DockerHub rate-limits
# user:anonymous
TOKEN=$(curl "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq -r .token)
curl --head -H "Authorization: Bearer $TOKEN" https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest
# user:genometation
TOKEN=$(curl --user 'genometation:Mnt6wRfstUFaBqDBF2Mey3x9' "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq -r .token)
curl --head -H "Authorization: Bearer $TOKEN" https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest
# Option 2: Snap
# sudo snap install docker

# Assume: group 'microk8s' already exists
# sudo usermod -a -G microk8s ubuntu
# sudo chown -f -R ubuntu ~/.kube

# Kubernetes: Turn off SWAP
# Read: https://discuss.kubernetes.io/t/swap-off-why-is-it-necessary/6879
# https://www.howtoforge.com/tutorial/how-to-install-kubernetes-on-ubuntu/
# sudo swapon -s
# sudo swapoff -a
# Disable the SWAP permanently, make a comment on the SWAP partition type
# sudo sed -e '/swap/ s/^#*/#/' -i /etc/fstab

# Installation: Docker (https://docs.docker.com/engine/install/ubuntu/)
echo -e "${COLOR} - Remove old MicroK8s${NC}"
# Stop all running pods
microk8s reset
sudo snap remove --purge microk8s
# echo -e "${COLOR} - Remove old MicroK8s snapshots${NC}"
# sudo snap saved
# sudo snap forget <snapshot-id>
# Remove Calico interfaces
# sudo ip link show | grep cali
# sudo ip link delete vxlan.calico
# sudo ip link delete cali...
echo -e "${COLOR} - Install new MicroK8s${NC}"
sudo snap install microk8s --classic --channel=1.24/stable
## Use Docker token for K8s (from docker login)
# Read: https://discuss.kubernetes.io/t/dockerhub-download-rate-limits/18729
# DockerHub rate-limit exceeded: Replace sources "image: docker.io -> quay.io"
# Read: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/#add-imagepullsecrets-to-a-service-account
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/dsimm-a/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/dsimm-a/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson \
    --namespace kube-system
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}' --namespace default
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}' --namespace kube-system
# kubectl get serviceaccounts default -o yaml --namespace default
# kubectl get serviceaccounts default -o yaml --namespace kube-system
microk8s start
microk8s status --wait-ready
# MicroK8s not working? Inspect K8s logs
sudo snap logs microk8s -f
# echo -e "${COLOR} - Set firewall rules${NC}"
# sudo iptables -P FORWARD ACCEPT
# sudo apt-get install iptables-persistent
# sudo ufw allow in on cni0 && sudo ufw allow out on cni0
# sudo ufw default allow routed
# sudo ufw status
echo -e "${COLOR} - Set default CNI interface${NC}"
# Calico: Set IP-AutoDetection to bond interfaces
# https://docs.projectcalico.org/reference/node/configuration#ip-autodetection-methods
# https://github.com/ubuntu/microk8s/issues/1554
sudo sed -re "s/(value: \"first-found\")/value: \"interface=bond.*\"/" -i /var/snap/microk8s/current/args/cni-network/cni.yaml
microk8s kubectl apply -f /var/snap/microk8s/current/args/cni-network/cni.yaml
microk8s status --wait-ready
microk8s kubectl get pods --all-namespaces
echo -e "${COLOR} - Enable microk8s addons (dashboard dns rbac hostpath-storage metrics-server)${NC}"
microk8s enable dashboard dns rbac hostpath-storage metrics-server
microk8s status --wait-ready
echo -e "${COLOR} - Enable microk8s addon (registry)${NC}"
microk8s enable registry:size=40Gi
microk8s status --wait-ready
# echo -e "${COLOR} - Enable microk8s addons (istio metallb)${NC}"
# microk8s enable istio
# microk8s status --wait-ready
# microk8s enable metallb
# microk8s status --wait-ready
# microk8s kubectl get pods --all-namespaces
echo -e "${COLOR} - Create K8s cluster admin user (RBAC)${NC}"
cd ../$kubernetes_dir && ./create_cluster_user_admin-user.sh
cd ../../$deployment_dir
echo -e "\n${COLOR} - Generate new certificates${NC}"
sudo microk8s refresh-certs --cert ca.crt
microk8s status --wait-ready
echo -e "\n${COLOR} - Generate cluster join token${NC}"
microk8s add-node --token-ttl 31536000
microk8s status --wait-ready
echo -e "\n${COLOR} - Mark the node as control-plane (master)${NC}"
# Read: https://kubernetes.io/docs/reference/setup-tools/kubeadm/implementation-details/
# By default, your cluster will not schedule Pods on the control-plane node for security reasons.
kubectl label nodes mcpmendel node-role.kubernetes.io/master=""
kubectl taint nodes mcpmendel node-role.kubernetes.io/master:NoSchedule
# Remove taint 'master' from node mcpmendel
# kubectl taint node mcpmendel node-role.kubernetes.io/master:NoSchedule-
# Remove taint 'master' from all nodes
# kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl get nodes -o wide
# Start Dashboard in background
nohup microk8s dashboard-proxy & echo $! > pid.k8s-dashboard.nohup

echo -e "\n${COLOR} - Multi-user MicroK8s${NC}"
# Read: https://microk8s.io/docs/multi-user
sudo snap remove --purge kubectl
sudo snap install kubectl --classic
# Extract current KubeConfig for other users
# genwork$ cp kubeconfig .kube/config
kubectl config view --flatten=true > ~/.kube/config
# kubectl config view --flatten=true | sed -re "s/127.0.0.1/10.153.18.10/"
