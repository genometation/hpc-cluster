#!/usr/bin/env bash

node=${1:-nodeala}

# Output configuration
COLOR='\033[0;32m'
NC='\033[0m' # No Color

echo -e "${COLOR}=== Start of Initial Setup of Node '${node}'  ===${NC}"
echo -e "${COLOR} - Mount NFS-filesystem to Node${NC}"
ssh ubuntu@${node} sudo apt-get install -y nfs-common
ssh ubuntu@${node} 'sudo echo "# Network-share (NFS at GWDG) ifs/genometation, NFSv3" | sudo tee -a /etc/fstab'
ssh ubuntu@${node} 'sudo echo "# Device                        Mountpoint               FStype  Options         Dump    Pass#"  | sudo tee -a /etc/fstab'
ssh ubuntu@${node} 'sudo echo "10.153.18.1:/srv/zfspool        /nfs/zfs_thymin          nfs     rw,noatime      0       0"  | sudo tee -a /etc/fstab'
ssh ubuntu@${node} sudo mkdir -p /nfs/zfs_thymin
ssh ubuntu@${node} sudo mount -a
echo -e "${COLOR} - Install additional packages${NC}"
ssh ubuntu@${node} sudo apt-get update
ssh ubuntu@${node} sudo apt-get upgrade -y
ssh ubuntu@${node} sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
echo -e "${COLOR} - Install Docker${NC}"
ssh ubuntu@${node} 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -'
ssh ubuntu@${node} 'sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"'
ssh ubuntu@${node} sudo apt-get update
ssh ubuntu@${node} sudo apt-get install -y docker-ce docker-ce-cli containerd.io
ssh ubuntu@${node} 'sudo echo { \
    "insecure-registries" : ["localhost:32000"] \
} | sudo tee -a /etc/docker/daemon.json'
ssh ubuntu@${node} sudo systemctl restart docker

echo -e "${COLOR} - Install MicroK8s${NC}"
ssh ubuntu@${node} sudo snap install microk8s --classic --channel=1.20/stable
echo -e "${COLOR} - Set Group and User permissions for MicroK8s${NC}"
ssh ubuntu@${node} sudo usermod -a -G microk8s ubuntu
ssh ubuntu@${node} sudo chown -f -R ubuntu ~/.kube
echo -e "${COLOR} - Modify MicroK8s (enable addons)${NC}"
ssh ubuntu@${node} /snap/bin/microk8s enable ha-cluster
echo -e "${COLOR} - Generate new certificates${NC}"
ssh ubuntu@${node} /snap/bin/microk8s refresh-certs
echo -e "${COLOR} - Show MicroK8s cluster-info${NC}"
ssh ubuntu@${node} /snap/bin/microk8s kubectl cluster-info
echo -e "${COLOR} - Join MicroK8s cluster (token: valid until 2022-03-12)${NC}"
ssh ubuntu@${node} /snap/bin/microk8s join 10.153.18.10:25000/709f0fad66b50d845fef816b19880048
echo -e "${COLOR} - Show MicroK8s cluster-info${NC}"
ssh ubuntu@${node} /snap/bin/microk8s kubectl cluster-info

echo -e "${COLOR}=== End of Initial Setup of Node '${node}' ===${NC}"
