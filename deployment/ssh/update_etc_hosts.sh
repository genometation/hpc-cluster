#!/usr/bin/env bash

node=${1:-nodeala}

# Output configuration
COLOR='\033[0;32m'
NC='\033[0m' # No Color

echo -e "${COLOR}=== Start of Update Node '${node}'  ===${NC}"
echo -e "${COLOR} - Append Cluster IP information to /etc/hosts${NC}"
ssh ubuntu@${node} cp /etc/hosts /home/ubuntu
ssh ubuntu@${node} cat /nfs/zfs_adenin/software/config/hosts >> /home/ubuntu/hosts
ssh ubuntu@${node} sudo cp /home/ubuntu/hosts /etc/hosts
echo -e "${COLOR}=== End of Update Node '${node}' ===${NC}"
