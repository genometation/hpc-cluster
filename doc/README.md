README
======

This directory contains two types of documentations:

  * **Project-Documentation with autodoc** (created with Sphinx)
    * Folders **source** and **build**
    * Index: ``build/html/index.html``
    * Refresh: `make html`
    * Refresh (detailed): `sphinx-build -b html . _build -vvvv`
    * Refresh Python Module Depencency: `pydeps ../../build/lib/python2.7/site-packages/scipio --max-bacon=4 --cluster --noshow -o src_scipio_clustered.svg`

Initial Setup of the Documentation:

```bash
sphinx-quickstart
sphinx-apidoc -f -o source/ ../src/scipio
make html
```

For further reading have a look at <https://docs-python2readthedocs.readthedocs.io/en/master/code-doc.html#submodules>.
Python Docstring Style (PEP484): <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>  
