.. HPC-Cluster documentation master file, created by
   sphinx-quickstart on Tue Jun  7 15:49:38 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HPC-Cluster's documentation!
=======================================

Collection of configuration files and scripts to setup and maintain the Genometation HPC-cluster.

* System configurations / Settings (Network, IPAM, NFS)
* Deployment


  - Cloud-Init
  - Curtin
  - SSH
  - General setup scripts
* Docker


  - Dockerfiles
  - docker-compose.yaml
  - docker-setup.sh (with K8s registry support)
  - K8s start und status scripts
* Kubernetes


  - Pods
  - Services
  - RBAC
* MAAS-CLI
* Scripts


  - RNA-Seq pipeline

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rnaseq_pipeline

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
