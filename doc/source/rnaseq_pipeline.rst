RNA-Seq pipeline
================

The RNA-Seq pipeline consists of several Bash scripts to process supporting
SRA data (`.fastq` files) for a specific genome annotation to produce
necessary resource files, such as

* read alignment to a respective genome (STAR)
* transcript assembly and quantitation of RNA-Seq data (StringTie)
* de novo transcriptome assembly (Trinity)
* ...


.. image:: _static/rnaseq_pipeline.drawio.png
   :alt: Pipeline diagram