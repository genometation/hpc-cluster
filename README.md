# HPC-Cluster Management

Collection of configuration files and scripts to setup and maintain the
Genometation HPC-cluster. This project contains

List of file-types
* Configurations / Settings (Network, IPAM, NFS)
* Deployment
  - Cloud-Init
  - Curtin
  - SSH
  - General setup scripts
* Docker
  - Dockerfiles
  - docker-compose.yaml
  - docker-setup.sh (with K8s registry support)
  - K8s start und status scripts
* Kubernetes
* MAAS-CLI
* Scripts

## Quick installation

    python3 -m ensurepip --default-pip --user
    python3 -m pip install --upgrade pip setuptools wheel virtualenv --user
    python3 -m virtualenv venv
    source ./venv/bin/activate
    python -m pip install --upgrade pip
    pip install sphinx sphinx_rtd_theme

