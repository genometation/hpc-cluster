127.0.0.1 localhost
127.0.1.1 mcpmendel

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

# Genometation Cluster: IPMI (OOB, FMZ)
10.153.19.1    storthymin.ipmi thymin.ipmi
10.153.19.2    storadenin.ipmi adenin.ipmi
10.153.19.3    dbcytosin.ipmi cytosin.ipmi
10.153.19.4    dbguanin.ipmi guanin.ipmi
10.153.19.10   mcpmendel.ipmi mendel.ipmi
10.153.19.11   mcplinne.ipmi linne.ipmi
10.153.19.12   mcpdarwin.ipmi darwin.ipmi
10.153.19.13   nodeala.ipmi ala.ipmi
10.153.19.14   nodearg.ipmi arg.ipmi
10.153.19.15   nodeasn.ipmi asn.ipmi
10.153.19.16   nodeasp.ipmi asp.ipmi
10.153.19.17   nodecit.ipmi cit.ipmi
10.153.19.18   nodecpg.ipmi cpg.ipmi
10.153.19.19   nodecys.ipmi cys.ipmi
10.153.19.20   nodegln.ipmi gln.ipmi
10.153.19.21   nodeglu.ipmi glu.ipmi
10.153.19.22   nodegly.ipmi gly.ipmi
10.153.19.23   nodehcy.ipmi hcy.ipmi
10.153.19.24   nodehis.ipmi his.ipmi
10.153.19.25   nodehse.ipmi hse.ipmi
10.153.19.26   nodehyp.ipmi hyp.ipmi
10.153.19.27   nodeile.ipmi ile.ipmi
10.153.19.28   nodeleu.ipmi leu.ipmi
10.153.19.29   nodelys.ipmi lys.ipmi
10.153.19.30   nodemet.ipmi met.ipmi
10.153.19.31   nodenle.ipmi nle.ipmi
10.153.19.32   nodenva.ipmi nva.ipmi
10.153.19.33   nodeorn.ipmi orn.ipmi
10.153.19.34   nodepen.ipmi pen.ipmi
10.153.19.35   nodephe.ipmi phe.ipmi
10.153.19.36   nodepra.ipmi pra.ipmi
10.153.19.37   nodepro.ipmi pro.ipmi
10.153.19.38   nodepyl.ipmi pyl.ipmi
10.153.19.39   nodesec.ipmi sec.ipmi
10.153.19.40   nodeser.ipmi ser.ipmi
10.153.19.41   nodethr.ipmi thr.ipmi
10.153.19.42   nodetic.ipmi tic.ipmi
10.153.19.43   nodetrp.ipmi trp.ipmi
10.153.19.44   nodetyr.ipmi tyr.ipmi
10.153.19.45   nodeval.ipmi val.ipmi

# Genometation Cluster: Provisioning (OOB, FMZ)
# No internet connection
10.153.20.1   storthymin.prov thymin.prov
10.153.20.2   storadenin.prov adenin.prov
10.153.20.3   dbcytosin.prov cytosin.prov
10.153.20.4   dbguanin.prov guanin.prov
10.153.20.10  mcpmendel.prov mendel.prov
10.153.20.11  mcplinne.prov linne.prov
10.153.20.12  mcpdarwin.prov darwin.prov
10.153.20.13  nodeala.prov ala.prov
10.153.20.14  nodearg.prov arg.prov
10.153.20.15  nodeasn.prov asn.prov
10.153.20.16  nodeasp.prov asp.prov
10.153.20.17  nodecit.prov cit.prov
10.153.20.18  nodecpg.prov cpg.prov
10.153.20.19  nodecys.prov cys.prov
10.153.20.20  nodegln.prov gln.prov
10.153.20.21  nodeglu.prov glu.prov
10.153.20.22  nodegly.prov gly.prov
10.153.20.23  nodehcy.prov hcy.prov
10.153.20.24  nodehis.prov his.prov
10.153.20.25  nodehse.prov hse.prov
10.153.20.26  nodehyp.prov hyp.prov
10.153.20.27  nodeile.prov ile.prov
10.153.20.28  nodeleu.prov leu.prov
10.153.20.29  nodelys.prov lys.prov
10.153.20.30  nodemet.prov met.prov
10.153.20.31  nodenle.prov nle.prov
10.153.20.32  nodenva.prov nva.prov
10.153.20.33  nodeorn.prov orn.prov
10.153.20.34  nodepen.prov pen.prov
10.153.20.35  nodephe.prov phe.prov
10.153.20.36  nodepra.prov pra.prov
10.153.20.37  nodepro.prov pro.prov
10.153.20.38  nodepyl.prov pyl.prov
10.153.20.39  nodesec.prov sec.prov
10.153.20.40  nodeser.prov ser.prov
10.153.20.41  nodethr.prov thr.prov
10.153.20.42  nodetic.prov tic.prov
10.153.20.43  nodetrp.prov trp.prov
10.153.20.44  nodetyr.prov tyr.prov
10.153.20.45  nodeval.prov val.prov

# Genometation Cluster: Network (SFP28, FMZ)
# https://www.genscript.com/Amino_Acid_Code.html
10.153.18.1    storthymin thymin
10.153.18.2    storadenin adenin
10.153.18.3    dbcytosin cytosin
10.153.18.4    dbguanin guanin
10.153.18.10   mcpmendel mendel
10.153.18.11   mcplinne linne
10.153.18.12   mcpdarwin darwin
10.153.18.13   nodeala
10.153.18.14   nodearg
10.153.18.15   nodeasn
10.153.18.16   nodeasp
10.153.18.17   nodecit
10.153.18.18   nodecpg
10.153.18.19   nodecys
10.153.18.20   nodegln
10.153.18.21   nodeglu
10.153.18.22   nodegly
10.153.18.23   nodehcy
10.153.18.24   nodehis
10.153.18.25   nodehse
10.153.18.26   nodehyp
10.153.18.27   nodeile
10.153.18.28   nodeleu
10.153.18.29   nodelys
10.153.18.30   nodemet
10.153.18.31   nodenle
10.153.18.32   nodenva
10.153.18.33   nodeorn
10.153.18.34   nodepen
10.153.18.35   nodephe
10.153.18.36   nodepra
10.153.18.37   nodepro
10.153.18.38   nodepyl
10.153.18.39   nodesec
10.153.18.40   nodeser
10.153.18.41   nodethr
10.153.18.42   nodetic
10.153.18.43   nodetrp
10.153.18.44   nodetyr
10.153.18.45   nodeval
