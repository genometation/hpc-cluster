# -*- coding: utf-8 -*-
""" Sub-package 'goenomics.yaml.parse': Collection of YAML helper methods. """

from argparse import Namespace
import yaml


def init_yaml_env():
    """ Initialize and configure YAML environment and add additional structure
    types (e.g. 'OrderedDict').

    Read: https://stackoverflow.com/a/8661021
    """

    def represent_dict_order(self, data):
        return self.represent_mapping('tag:yaml.org,2002:map', data.items())
    yaml.add_representer(OrderedDict, represent_dict_order)

    return yaml


def parse_yaml(yaml_str):
    """ Parse passed PyScipio-YAML string into Namespace object.

    Parameters:
        yaml_str (str): YAML in string representation

    Returns:
        Namespace obj
    """

    from yaml import load
    try:
        from yaml import CLoader as Loader, CDumper as Dumper
    except ImportError:
        from yaml import Loader, Dumper

    # Load passed PyScipio YAML-string
    obj = None
    data = load(yaml_str, Loader=Loader)
    for gene_id_k, gene_id_v in data.items():
        gene_meta_dict = gene_id_v[0][0]
        obj = Namespace(**gene_meta_dict)
        obj.HitID = gene_id_k
        obj.assembly = []
        assembly = gene_id_v[0][1]
        for hit_i, hit in enumerate(assembly):
            obj.assembly.append(Namespace(**hit))
    # Assumes YAML contains only one Gene
    return obj


# from collections import OrderedDict
# from yaml import load, scanner
# try:
#     from yaml import CLoader as Loader, CDumper as Dumper
# except ImportError:
#     from yaml import Loader, Dumper
# import yaml
#
# def setup_yaml():
#     """ Initialize and configure YAML environment and add additional structure
#     types or formatting rules (e.g. 'OrderedDict', 'quoted Strings').
#
#     Read: https://stackoverflow.com/a/8661021
#     """
#
#     # Add type 'OrderedDict'
#     def represent_ordered_dict(self, data):
#         return self.represent_mapping('tag:yaml.org,2002:map', data.items())
#     yaml.add_representer(OrderedDict, represent_ordered_dict)
#
#     # Quote all Strings with '"'
#     def represent_quoted_str(self, data):
#         return self.represent_scalar('tag:yaml.org,2002:str', data, style='"')
#     yaml.add_representer(str, represent_quoted_str)
#
#     return yaml
#
# yaml = setup_yaml()
# yaml_file = "bio_data/pyscipio/MagOryVsMagMQOP_allGenesPredictions.yaml"
# with open(yaml_file, 'r') as stream:
#     hits = load(stream, Loader=Loader)
# ohits = OrderedDict(sorted(hits.items(), key=lambda m: (m[1][0][0]['protein_query_name'], -m[1][0][0]['score'], -m[1][0][0]['identity_score'])))
# output = yaml.dump(ohits, sort_keys=False, default_flow_style=False, width=float("inf"))
# with open('bio_data/pyscipio/MagOryVsMagMQOP_allGenesPredictions_100_sorted.yaml', 'w') as f:
#     f.write(output)
