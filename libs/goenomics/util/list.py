# -*- coding: utf-8 -*-
""" Sub-package 'goenomics.yaml.parse': Collection of YAML helper methods. """

from argparse import Namespace
import yaml


def find_pair_overlap(list_of_pairs):
    hits = [(h.prot_start, h.prot_end) for h in g.assembly]
    contig_overlap = False
    # Find overlapping assembly-hits (contigs)
    for rtup in zip(hits, hits[1:]):
        if(rtup[0][1] >= rtup[1][0] and rtup[0][0] <= rtup[1][1]):
            contig_overlap = True
            break
    return contig_overlap
