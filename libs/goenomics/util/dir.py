# -*- coding: utf-8 -*-
""" Sub-package 'goenomics.util.dir': Collection of dir helper methods. """

import os


def makedirs(path):
    """ Helper method: Create directories for path

    Parameters:
        path (str): Path for directories
    """
    d = os.path.dirname(path)
    if not os.path.exists(d):
        os.makedirs(d)
