# -*- coding: utf-8 -*-
""" Sub-package 'goenomics.util.string': Collection of string helper methods. """

import hashlib


def hashstr(s):
    """ Generate SHA1-hash from string s.

    Args:
        s (str): String to be hashed

    Returns:
        str: Hashed string
    """
    return hashlib.sha1(str(s).encode('utf-8')).hexdigest()


def shorten(s, l):
    """ Shorten string s to length s by trimming """
    if len(s) < l:
        return s
    else:
        return s[0:int(l/2)+1] + s[-int(l/2)+1:]


def tr(s):
    """ Strip all non alpha-numeric symbols from passed string s. """
    # return s.translate(str.maketrans('', '', string.punctuation))
    # Replace all non alpha-numeric symbols in passed string s.
    return re.sub('[^0-9a-zA-Z]+', '_', s)
