# -*- coding: utf-8 -*-
""" Sub-package 'goenomics.util.file': Collection of file helper methods. """

import os


def file_get_contents(file_path):
    """ Read content of passed file 'file_path' into string

    Args:
        file_path (str): Filename

    Returns:
        str: File content
    """
    if os.path.exists(file_path):
        with open(file_path, 'r') as fh:
            return fh.read()  # Read file in one string
    return ""


def file_get_lastline(file_path):
    """ Read last line of passed file 'file_path' into string

    Args:
        file_path (str): Filename

    Returns:
        str: Content of last line
    """
    try:
        with open(file_path, 'rb') as fh:
            fh.seek(-2, 2)                    # Jump to the second last byte.
            while fh.read(1) != b"\n":        # Until EOL is found ...
                fh.seek(-2, 1)                # ... jump back, over the read byte plus one more.
            return fh.read().decode('ascii')  # Read all data from this point on; Convert binary to str
    except Exception as e:
        return ""
