#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Test: Package 'goenomics' """

from goenomics.util.file import file_get_contents
from goenomics.yaml.parse import parse_yaml
import csv
import pandas as pd
import sqlite3


# Prepare data
yaml_str = file_get_contents(
    "example_1__test_pyscipio_blast__example_query_pyscipio.fungi.default.conf__query_sequence_0_.yaml")
g = parse_yaml(yaml_str)
print(g)
# # CSV: File format (header)
# fieldnames = ['geneid',
#               'target.0',
#               'is_complete',
#               'start_codon',
#               'stop_codon',
#               'score',
#               'dna_start',
#               'dna_end',
#               'contigs']
#
#
# ### Main script ################################################################
# csv_rows = []
# for yaml_file in yaml_file_list:
#     # Prepare data
#     yaml_str = file_get_contents(yaml_file)
#     g = parse_yaml(yaml_str)
#     # Extract Scipio-YAML information and write to CSV (summary_file)
#     csv_row = [g.GeneID,
#                g.assembly[0].target,
#                g.is_complete,
#                g.start_codon,
#                g.stop_codon,
#                g.score,
#                g.assembly[0].dna_start,
#                g.assembly[-1].dna_end,
#                len(g.assembly)]
#     # hits = [(h.prot_start, h.prot_end) for h in g.assembly]
#     # contig_overlap = False
#     # # Find overlapping assembly-hits (contigs)
#     # for rtup in zip(hits, hits[1:]):
#     #     if(rtup[0][1] >= rtup[1][0] and rtup[0][0] <= rtup[1][1]):
#     #         contig_overlap = True
#     #         break
#     # csv_row.append(contig_overlap)
#     csv_rows.append(csv_row)
#
# # Write to CSV data
# with open('gene_annotation_summary.csv', 'w', newline='') as csvfile:
#     # writer = csv.writer(csvfile, delimiter=' ', quotechar='|')
#     # writer.writerow(csv_row)
#     writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=' ', quotechar='|')
#     writer.writeheader()
#     writer.writerow(dict(zip(fieldnames, csv_row)))
#
# # # Create in-memory database:
# # db = sqlite3.connect(':memory:')
# # Create a new database file:
# db = sqlite3.connect("genes.sqlite")
# # Load the CSV in chunks:
# for c in pd.read_csv("gene_annotation_summary.csv", chunksize=1000):
#     # Append all rows to a new database table, which we name 'genes':
#     c.to_sql("genes", db, if_exists="append")
# # Add an index on the 'geneid' column:
# # db.execute("CREATE INDEX geneid ON genes(geneid)")
# db.close()

# def get_voters_for_street(street_name):
#     conn = sqlite3.connect("genes.sqlite")
#     q = "SELECT * FROM genes WHERE street = ?"
#     values = (street_name,)
#     return pd.read_sql_query(q, conn, values)
