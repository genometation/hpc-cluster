#!/bin/sh

# Change these 3 values as required 
PROFILE=dsimm-a
API_KEY_FILE=/home/dsimm-a/scripts/maas-cli/api_key
API_SERVER=localhost

MAAS_URL=http://$API_SERVER:5240/MAAS/api/2.0

maas login $PROFILE $MAAS_URL - < $API_KEY_FILE

