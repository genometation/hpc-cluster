#!/usr/bin/env bash

# Generate and update Docker image in K8s registry
#docker build . -f Dockerfile -t localhost:32000/augustus:latest
#docker push localhost:32000/augustus:latest
#curl localhost:32000/v2/augustus/tags/list

# Create Pod for ...
# image: localhost:32000/augustus:latest
kubectl apply -f ./k8s_pod_spec.yaml
kubectl exec --stdin --tty augustus-interactive -- /bin/bash
# ... or with different name 'kws1'
sed -re "s/interactive/kws1/" k8s_pod_spec.yaml | microk8s.kubectl apply -f -
kubectl exec --stdin --tty augustus-kws1 -- /bin/bash
# list running pods
kubectl get pods -o wide
