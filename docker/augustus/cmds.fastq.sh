#!/usr/bin/env bash

# Convert FASTQ to FASTA
seqtk seq -a in.fastq > out.fa
# Filter and Sort FASTA for sequences with length geq 100
awk 'length($0) > 100' in.fa > out.geq100.fa
awk '{ print length(), $0 | "sort -n" }' out.geq100.fa > out.geq100.sorted.fa
