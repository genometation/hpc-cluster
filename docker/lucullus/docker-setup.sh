#!/usr/bin/env bash

# Build docker image of project with composer-preferences
docker-compose build
# # Build new image 'lucullus_standalone' from source (Dockerfile)
# docker build -t lucullus_standalone .    

# # Start image as container:
# #   Container autoremove; Publish exposed port to fixed host port 8080; 
# # 	Use user-defined network 'backend'
# #   Name container 'memcached'; daemonize
# docker run --rm -p 8080:8080 \
#            --network="backend" \
#            --mount source=fab8-tmp,target=/tmp \
#            --name fab8-lucullus -d lucullus_standalone 

# Start image as container via composer
docker-compose up -d