<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <script type="text/javascript" src="./js/lucullus.js"></script>
    <link type="text/css" href="./css/main.css" rel="stylesheet" />
    <link type="text/css" href="./css/lucullus.css" rel="stylesheet" />

    <title>Lucullus Tests</title>

    <script type="text/javascript">
        // var server - 'http://fab8:8080/api'
        var server = document.location.protocol + '//' + document.location.host + '/api/'
        var api = new Lucullus.Server(server, 'test')
        var res

        jQuery(function(){
            res = new Lucullus.Thing(api, 'SequenceResource')
            res.on_error(function(data){
                alert(data.error+"\n"+data.detail)
            })
        })
    </script>


</head>
<body>
</body>
</html>

