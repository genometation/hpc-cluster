<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <link type="text/css" href="./css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="/js/mlayer.js"></script>
    <script type="text/javascript" src="/js/lucullus.js"></script>
    <title>Lucullus 3DSpec Viewer</title>
</head>
<body style='overflow: hidden; margin:0'>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td></td>
            <td><div id='map_t' style=''></div></td>
            <td></td>
        </tr>
        <tr>
            <td><div id='map_l' style=''></div></td>
            <td><div id='map' style=''></div></td>
            <td><div id='map_r' style=''></div></td>
        </tr>
        <tr>
            <td></td>
            <td><div id='map_b' style=''></div></td>
            <td></td>
        </tr>
    </table></body>
</html>
<script type="text/javascript">
    // var server - 'http://fab8:8080/api'

    var getVars = {};
    (function () {
        var match,
            pl     = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1);

        while (match = search.exec(query))
           getVars[decode(match[1])] = decode(match[2]);
    })();

    var server = document.location.protocol + '//' + document.location.host + '/api/';
    var api = new Lucullus.Server(server, 'test');



    var points;
    var res, map;


    /* CrossDomain communication if this page is emdedded in an iframe. */

    var xsChannel;
    var xsOrigin;
    var xsDebug = false;
    var xsCache = [];

    function xsend(msg) {
        if(xsChannel) {
            xsChannel.postMessage(msg, xsOrigin);
        } else {
            xsCache.push(msg);
        }
    }

    window.addEventListener("message", function(event) {
      if(xsDebug || event.data.debug)
        console.log('Iframe got message:', event.data);

      if(!xsChannel && event.data.action === 'connect') {
        xsChannel = event.source;
        xsOrigin = event.origin;
        xsDebug = event.data.debug;
        xsend({action: 'connect', status:'OK'});
        while(xsCache.length > 0) {
            xsend(xsCache.shift());
        }
      }

      if (event.origin !== xsOrigin) return

      if (event.data.action === 'PING') {
        xsend({action: 'PONG'});
      }

      if (event.data.action === 'get_position') {
        xsend({action: 'get_position', viewport: map.viewport});
      }

      if (event.data.action === 'update') {
        d = event.data;
        if(d.level)
          res.query('setup', {level: d.level});
        if(d.x && d.y) {
          zoomTo(d.x.min, d.x.max, d.y.min, d.y.max);
        }
      }
    }, false);


    function logb(b, r) {
        return Math.log(r) / Math.log(b);
    }

    function zoomTo(xmin, xmax, ymin, ymax) {
        var ppm = res.state.ppm_res;
        var ppmmin = res.state.ppm_min;
        var ppmmax = res.state.ppm_max;
        // [0.16142578124999998, 0.16142578124999998]
        // [13.8, 13.8]
        // [179.1, 179.1]

        var x_offset = (ppmmax[0] - xmax) / ppm[0];
        var x_width  = (xmax - xmin) / ppm[0];

        var y_offset = (ymin - ppmmin[1]) / ppm[1];
        var y_width  = (ymax - ymin) / ppm[1];

        // Domain knowlege: calculate best zoom level
        var vp = map.viewport;
        var x_zoom = Math.floor(10 * logb(2, vp.width/x_width));
        var y_zoom = Math.floor(10 * logb(2, vp.height/y_width));
        var zoom = Math.min(x_zoom, y_zoom);
        map.zoom(zoom);
        var scale = Math.pow(2, -zoom/10)
        map.moveTo(x_offset/scale, y_offset/scale);
    }





    function start() {
        // Create 5 maps, one for each table field.
        map = new mlayer.Map('#map', {});
        var mapt = new mlayer.Map('#map_t', {});
        var mapl = new mlayer.Map('#map_l', {});
        var mapb = new mlayer.Map('#map_b', {});
        var mapr = new mlayer.Map('#map_r', {});

        // Make sure that the table fills the whole screen.
        jQuery(window).resize(function(){
            var w = jQuery(window).width()
            var h = jQuery(window).height()
            var ruler = 15;
            jQuery('#map').width(w-ruler-ruler).height(h-ruler-ruler)
            jQuery('#map_t,#map_b').width(w-ruler-ruler).height(ruler)
            jQuery('#map_l,#map_r').width(ruler).height(h-ruler-ruler)
            if(map) {
                map.resize();
                mapt.resize();
                mapl.resize();
                mapb.resize();
                mapr.resize();
            }
        })
        jQuery(window).resize()

        // Create the main server-side resource
        res = api.create('LevelResource');
        res.on_error(function(res, data){
            if(typeof data === 'undefined')
                alert("Hey, you found a (server side) error. Thanks, we will fix that soon!")
            else
                alert(data.error+"\n"+data.detail)
        })

        res.query('setup', {proc_level: 0.01, data_color: '#ff8000'});
        res.query('load_local', {
            data:   getVars.data,
            procs:  getVars.procs,
            proc2s: getVars.proc2s,
            poi:    getVars.poi}
        );

        // Add a level view to the center map and rulers to the border maps.
        map.addLayer(new mlayer.layer.LucullusView({view: res, buffer:1}));
        mapt.addLayer(new mlayer.layer.LucullusView({
            view: api.create('Ruler2View'), buffer:1, channel:'top'}))
        mapr.addLayer(new mlayer.layer.LucullusView({
            view: api.create('Ruler2View'), buffer:1, channel:'right'}))
        mapb.addLayer(new mlayer.layer.LucullusView({
            view: mapt.base.view, buffer:1, channel:'bottom'}))
        mapl.addLayer(new mlayer.layer.LucullusView({
            view: mapr.base.view, buffer:1, channel:'left'}))

        // Add move bridges so center map movements affect all maps
        map.addLayer(new mlayer.layer.MoveBridge({target: mapt, scaleY:0}));
        map.addLayer(new mlayer.layer.MoveBridge({target: mapr, scaleX:0}));
        map.addLayer(new mlayer.layer.MoveBridge({target: mapb, scaleY:0}));
        map.addLayer(new mlayer.layer.MoveBridge({target: mapl, scaleX:0}));

        // Add a point layer to the center map.
        points = map.addLayer(new mlayer.layer.Points());
        //canvas = map.addLayer(new mlayer.layer.Canvas());

        // Add a mouse control to the center map.
        var ctrl = new mlayer.layer.Controls()
        map.addLayer(ctrl);

        var update_number = 0;

        // Make sure that the rulers are always up to date.
        res.on_update(function() {
            if(res.state.ppm_res[0] == null) return;
            xsend({action: 'update', resource: res.state});
            update_number ++;
            if(update_number <= 1) { // TODO: Find a sane solution
                map.moveTo(999999999, 0);
            }

            var ppm_max  = res.state.ppm_max,
                ppm_min  = res.state.ppm_min,
                ppm_res  = res.state.ppm_res,
                data_res = res.state.data_res;

            mapt.base.view.query('setup', {
                unit: -(ppm_res[0] * data_res[0]),
                unit_offset: ppm_max[0]
            });
            mapr.base.view.query('setup', {
                unit: (ppm_res[1] * data_res[1]),
                unit_offset: ppm_min[1]
            });
        });

        // Fill the points layer with data.
        res.query('get_points', {}, function(res, result) {
            if(!result.result.points) return
            var ppm_max = res.state.ppm_max,
                ppm_min = res.state.ppm_min,
                ppm_res = res.state.ppm_res,
                data_res = res.state.data_res;

            jQuery.each(result.result.points, function(i, p) {
                p.name = p.name + ' [' + p.y + ', ' + p.x + ']';
                p.x = (ppm_max[0] - p.x) / (ppm_res[0] * data_res[0])
                p.y = (p.y - ppm_min[1]) / (ppm_res[1] * data_res[1])
                points.addPoint(p);
            });
        });

        map.show(); mapt.show(); mapr.show(); mapb.show(); mapl.show();

        // Install keyboard controls.
    	jQuery(document).keypress(function(e){
    		var step = 10
    		if     (e.which == 43) map.zoomIn(5)            // +
    		else if(e.which == 45) map.zoomOut(5)           // -
    		else if(e.which == 49) map.moveBy(-step, step)  // NUM 1
    		else if(e.which == 50) map.moveBy(0, step)      // NUM 2
    		else if(e.which == 51) map.moveBy(step, step)   // NUM 3
    		else if(e.which == 52) map.moveBy(-step, 0)     // NUM 4
    		//else if(e.which == 53) map.moveBy(5)
    		else if(e.which == 54) map.moveBy(step, 0)      // NUM 6
    		else if(e.which == 55) map.moveBy(-step, -step) // NUM 7
    		else if(e.which == 56) map.moveBy(0, -step)     // NUM 8
    		else if(e.which == 57) map.moveBy(step, -step)  // NUM 9
    	})
    }

    start();

</script>
