<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <link type="text/css" href="./css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="/js/mlayer.js"></script>
    <script type="text/javascript" src="/js/lucullus.js"></script>
    <title>Lucullus Sequence Viewer</title>
</head>
<body style='overflow: hidden'>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td></td>
            <td><div id='map_t' style=''></div></td>
        </tr>
        <tr>
            <td><div id='map_l' style=''></div></td>
            <td><div id='map' style=''></div></td>
        </tr>
    </table></body>
</html>
<script type="text/javascript">
    // var server - 'http://fab8:8080/api'

    var getVars = {};
    (function () {
        var match,
            pl     = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1);

        while (match = search.exec(query))
           getVars[decode(match[1])] = decode(match[2]);
    })();

    var server = document.location.protocol + '//' + document.location.host + '/api/';
    var api = new Lucullus.Server(server, 'test');

    var points;
    var res, map;


    /* CrossDomain communication if this page is emdedded in an iframe. */

    var xsChannel;
    var xsOrigin;
    var xsDebug = false;
    var xsCache = [];

    function xsend(msg) {
        if(xsChannel) {
            xsChannel.postMessage(msg, xsOrigin);
        } else {
            xsCache.push(msg);
        }
    }

    window.addEventListener("message", function(event) {  
      if(xsDebug || event.data.debug)
        console.log('Iframe got message:', event.data);

      if(!xsChannel && event.data.action === 'connect') {
        xsChannel = event.source;
        xsOrigin = event.origin;
        xsDebug = event.data.debug;
        xsend({action: 'connect', status:'OK'});
        while(xsCache.length > 0) {
            xsend(xsCache.shift());
        }
      }

      if (event.origin !== xsOrigin) return

      if (event.data.action === 'PING') {
        xsend({action: 'PONG'});
      }

      if (event.data.action === 'get_position') {
        xsend({action: 'get_position', viewport: map.viewport});
      }

      if (event.data.action === 'update') {
        d = event.data;
        if(d.level)
          res.query('setup', {level: d.level});
        if(d.x && d.y) {
          zoomTo(d.x.min, d.x.max, d.y.min, d.y.max);
        }
      }
    }, false);



    function logb(b, r) {
        return Math.log(r) / Math.log(b);
    }

    function zoomTo(xmin, xmax, ymin, ymax) {
        var ppm = res.state.ppm_resolution;
        var ppmmin = res.state.ppm_min;
        var ppmmax = res.state.ppm_max;
        // [0.16142578124999998, 0.16142578124999998]
        // [13.8, 13.8]
        // [179.1, 179.1]

        var x_offset = (ppmmax[0] - xmax) / ppm[0];
        var x_width  = (xmax - xmin) / ppm[0];

        var y_offset = (ymin - ppmmin[1]) / ppm[1];
        var y_width  = (ymax - ymin) / ppm[1];

        // Domain knowlege: calculate best zoom level
        var vp = map.viewport;
        var x_zoom = Math.floor(10 * logb(2, vp.width/x_width));
        var y_zoom = Math.floor(10 * logb(2, vp.height/y_width));
        var zoom = Math.min(x_zoom, y_zoom);
        map.zoom(zoom);
        var scale = Math.pow(2, -zoom/10)
        map.moveTo(x_offset/scale, y_offset/scale);
    }





    function start() {
        // Create 5 maps, one for each table field.
        map = new mlayer.Map('#map', {});
        var mapt = new mlayer.Map('#map_t', {});
        mapl = new mlayer.Map('#map_l', {});

        // Make sure that the table fills the whole screen.
        jQuery(window).resize(function(){
            var w = jQuery(window).width()
            var h = jQuery(window).height()
            var ruler = 15, index = 120;
            jQuery('#map').width(w-index).height(h-ruler)
            jQuery('#map_t').width(w-index).height(ruler)
            jQuery('#map_l').width(index).height(h-ruler)
            if(map) {
                map.resize();
                mapt.resize();
                mapl.resize();
            }
        })
        jQuery(window).resize()

        // Create the main server-side resource
        res = api.create('SequenceResource');
        res.on_error(function(res, data){
            if(typeof data === 'undefined')
                alert("Hey, you found a (server side) error. Thanks, we will fix that soon!")
            else
                alert(data.error+"\n"+data.detail)
        })

        idx = api.create('IndexView')
        res.query('load', {source: getVars.source});

        // Add a level view to the center map and rulers to the border maps.
        map.addLayer(new mlayer.layer.LucullusView({view: res, buffer:1}));
        mapt.addLayer(new mlayer.layer.LucullusView({
            view: api.create('Ruler2View'), buffer:1, channel:'top'}))
        mapl.addLayer(new mlayer.layer.LucullusView({view: idx, buffer:1}))

        // Add move bridges so center map movements affect all maps
        map.addLayer(new mlayer.layer.MoveBridge({target: mapt, scaleY:0}));
        map.addLayer(new mlayer.layer.MoveBridge({target: mapl, scaleX:0}));

        // Add a mouse control to the center map.
        var ctrl = new mlayer.layer.Controls()
        map.addLayer(ctrl);

        // Make sure that the rulers are always up to date.
        res.on_update(function() {
            idx.query('setup', {source: res.id, blocksize:12});
            mapt.base.view.query('setup', {
                unit: 1/12,
                unit_offset: 0
            });

            idx.query('setup', {source: res.id, blocksize:12});
            xsend({action: 'update', resource: res.state});
        });

        map.show(); mapt.show(); mapl.show();
    }

    start();

</script>
