/*
==========================
MLayer: OpenLayers light
==========================

MLayer is a tiny JavaScript library to display multi-layered dynamic maps in a
browser. It is heavily inspired by http://openlayers.org/ but aims to be
smaller, faster and less complicated.

Usage
======

::

    // <div id='map' style='width: 500px; height:400px;'/>
    var map = new mlayer.Map('#map', {});
    var layer = new mlayer.layer.Tiles({
        url: 'http://example.com/tiles/{size}/{x}-{y}-{z}.png'
    });
    map.addLayer(layer);
    map.show();

Licence
=======

    Copyright (c) 2011 Marcel Hellkamp

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

mlayer = {}
mlayer.layer = {}
mlayer.util = {}
mlayer.geometry = {}

mlayer.extend = function(baseclass, override) {
    /* Creates a new class with values from $override mixed into the prototype
       of $baseclass. */

    // Override the constructor, if requested.
    var constructor = baseclass
    if(override.constructor && override.constructor !== ({}).constructor)
        constructor = override.constructor

    // Create a new constructor function that calls the old one.
    var cls = function(){
        constructor.apply(this, arguments);
    };

    // Clone the baseclass prototype.
    var dummy = function(){}
    dummy.prototype = baseclass.prototype;
    cls.prototype = new dummy();

    // Copy (and overwrite) new prototype values.
    for (var name in override) {
        if(name === 'constructor') continue;
        cls.prototype[name] = override[name];
    }

    return cls;
};



mlayer.Map = function(node, config) {
    // The root DOM node for all map related nodes.
    this.dom = jQuery(node);
    this.dom.css({overflow:'hidden', position:'relative', top:'0px', left:'0px', padding:'0px'})
    this.dom.empty()

    // Container for map layers
    this.mapdom = jQuery('<div />');
    this.mapdom.css({position:'absolute', top:'0px', left:'0px', width:'1px', height:'1px'});
    this.dom.append(this.mapdom);
    
    // Container for HUD layers
    this.huddom = this.mapdom.clone();
    this.huddom.css('overflow', 'hidden')
    this.dom.append(this.huddom);

    // Lists of layers
    this.layers = [];

    // Base map layer (used for clipping and other stuff)
    this.base = null;

    /** A viewport defines the area of the map the user is able to see.
        It is a plain object with the following attributes:

        top: The X value of the top-most pixel row.
        left: The Y value of the left-most pixel column.
        width: The number of pixels in Y direction.
        height: The number of pixels in X direction.
        bottom: top+height
        right: left+width
        zoom: The zoom factor as specified by the user.
    */

    this.viewport = {top:    0, left:   0,
                     width:  0, height: 0,
                     bottom: 0, right:  0,
                     zoom:   0}

    // handle for the draw interval
    this._draw_interval = null;

    /*
     * Configuration 
     */

    this.autoclip = config.autoclip || true;
    this.draw_interval = config.draw_interval || 500;
    this.zindex = config.zindex || 100;
};

mlayer.Map.prototype = {
    start: function() {
        // Layout all layers and start the draw interval.
        if(this._draw_interval !== null) return
        this.layout()
        var self = this;
        this._draw_interval = window.setInterval(function(){
            self.draw();
        }, this.draw_interval)
    },
    stop: function() {
        // Clear the draw interval.
        if(this._draw_interval === null) return
        window.clearInterval(this._draw_interval);
        this._draw_interval = null;
    },
    show: function() {
        // Show the primary dom node and start rendering.
        this.dom.show();
        this.start();
    },
    hide: function() {
        // Hide the primary dom node and stop rendering.
        this.dom.hide();
        this.stop();
    },
    draw: function() {
        /* Draw each layer and check for a resized viewport.
            This method is called every draw_interval milliseconds and on
            events that drastically change the viewport (e.g. zoom).
        */
        var vp = this.viewport, layers = this.layers;
        for(var i=0, len=layers.length; i<len;) {layers[i++].onDraw(vp)}
    },
    resize: function() {
        /* This MUST be called whenever the root DOM node is resized. */
        var vp   = this.viewport,
            layers = this.layers,
            dom  = this.dom;
        if(vp.width != dom.innerWidth() || vp.height != dom.innerHeight()) {
            vp.width  = dom.innerWidth();
            vp.height = dom.innerHeight();
            vp.bottom = vp.top + vp.height;
            vp.right  = vp.left + vp.width;
            this.mapdom.width(vp.width).height(vp.height)
            this.huddom.width(vp.width).height(vp.height)
            for(var i=0, len=layers.length; i<len;) {layers[i++].onResize(vp)}
        }
    },
    layout: function() {
        /* Create HTML nodes for all layers. */

        var zindex = this.zindex;
        var mapdom = this.mapdom,
            huddom = this.huddom;

        // Do not layout an inactive map.
        if(this._draw_interval !== null) return;

        this.mapdom.empty();
        this.huddom.empty();
        jQuery.each(this.layers, function() {
            this.dom.css('z-index', zindex).empty();
            zindex += 1 + (this.onLayout(zindex) || 0);
            (this.is_hud?huddom:mapdom).append(this.dom)
        })

        this.max_zindex = zindex;
        this.resize();
    },
    addLayer: function(layer, base) {
        // Add a new layer object. If the second parameter is true, the layer
        // is used as the new base layer.
        layer.map = this
        if(base || layer.is_base || (!this.base && !layer.is_hud)) {
            if(this.base) this.base.is_base = false;
            layer.is_base = true;
            this.base = layer;
            this.layers.unshift(layer);
        } else {
            this.layers.push(layer);
        }
        this.layout();
        return layer
    },
    moveTo: function(x, y) {
        /*  Move the top-left corner viewport to a specific position. If
            either x or y are null, they default to the current position.

            Map movements are measured in browser pixels. The Map object has
            no knowlege about the scale or ratio of the data. Example: If each
            pixel represents n data units, and you want to move x data units
            to the left, you actually need to move x*n pixels to the left.
        */
        if(!this.base) return;
        var view = this.viewport, layers = this.layers;

        // Default to current position
        if(typeof x != 'number') x = view.left;
        if(typeof y != 'number') y = view.top;

        // Clip movements based on the current base-layer extent
        if(this.autoclip) {
            var box = this.base.getExtent(view.zoom);
            x = Math.max(box.left, Math.min(box.right-view.width, x));
            y = Math.max(box.top, Math.min(box.bottom-view.height, y));
        }

        // Calculate movement vector
        x = Math.round(x);
        y = Math.round(y);
        var dx = x - view.left;
        var dy = y - view.top;

        // No movement -> nothing to di.
        if(!dx && !dy) return;

        // Update viewport
        view.left   += dx;
        view.right  += dx;
        view.top    += dy;
        view.bottom += dy;

        // Trigger onMove callbacks for all maps.
        for(var i=0, len=layers.length; i<len;) {
            layers[i++].onMove(view)
        }

        // Actually move the primaty dom node.
        this.mapdom.css({left: -x+'px', top: -y+'px'});
    },
    moveBy: function(dx, dy) {
        // Move the viewport by a specific (pixel) distance.
        this.moveTo(this.viewport.left + dx, this.viewport.top + dy);
    },
    zoom: function(level) {
        // Change the zoom level but keep the focus on the same spot.
        // e.g. If the extent of the base layer changes, the map is moved to
        // the new position. This usually triggers a redraw.

        var view = this.viewport
        var old = view.zoom
        view.zoom = level

        // Trigger onZoom callbacks for all maps.
        for(var i=0, len=this.layers.length; i<len;) {this.layers[i++].onZoom(view)}

        // Get old and new extend
        var obox = this.base.getExtent(old);
        var nbox = this.base.getExtent(level);

        // Calculate center of current viewport in old extend
        var cx = view.left + view.width  / 2;
        var cy = view.top  + view.height / 2;

        // Calculate relative position of center within old extend
        var ox = (cx - obox.left) / (obox.right  - obox.left);
        var oy = (cy - obox.top)  / (obox.bottom - obox.top);

        // Calculate position of center in new extend
        var nx = nbox.left + ox * (nbox.right  - nbox.left);
        var ny = nbox.top  + oy * (nbox.bottom - nbox.top);

        // Move to new center.
        this.moveTo(nx - view.width/2, ny - view.height/2);

        return old;
    },
    zoomIn: function(amount) {
        this.zoom(this.viewport.zoom + (amount || 1))
    },
    zoomOut: function(amount) {
        this.zoom(this.viewport.zoom - (amount || 1))
    },
    click: function(x, y) {
        var view = this.viewport;
        for(var i=0, len=this.layers.length; i<len;) {this.layers[i++].onClick(view, x, y)}
    }
};

mlayer.layer.BaseLayer = function(config) {
    this.config = config || {}
    this.dom = jQuery('<div />')
    this.dom.css({ position: 'absolute', top: '0px', left: '0px',
                   width: '1px', height: '1px' })
    this.map = null;
    this.init(config||{})
};

mlayer.layer.BaseLayer.prototype = {
    is_base: false,
    init: function(config) {
        /* Called by the constructor. */
    },
    getExtent: function(zoom) {
        /* Called by the Map to get the extend of the base layer. The extent
           is an object with the following attributes:

           top: Minimum x value.
           left: Minimum y value.
           bottom: Maximum x value.
           right: Maximum y value.

           The extend should be calculated according to the specified zoom
           level. If a zoom level is not supported, return null. The returned
           object must not change at runtime.
        */
        return {top:0, left:0, right:0, bottom:0};
    },
    onLayout: function(zIndex) {
        // Called by the Map as soon as the layer is is added to the dom tree
        //  and on subsequent Map.layout() calls. This should create all
        //  dom nodes required to display the data and add them to this.dom.
        // @param zIndex CSS z-index of this.dom.
        // @return The number of additional z-indices used by this Layer.
        return 0
    },
    onDraw: function(viewport) {
        /* Called every Map.draw_interval milliseconds, even if the viewport
         * did not change. Should update visuals according to new viewport.
         */
    },
    onResize: function(viewport) {
        /* Called on viewport resize events, just before the draw() call.
         * @param viewport Current viewport.
        */
    },
    onZoom: function(viewport) {
        /* Called on zoom events.
         * @param viewport Current viewport.
        */
    },
    onMove: function(viewport) {
        /* Called on every single move event. Do only fast calculations here.
         */
    },
    onClick: function(x, y) {
        /* Called on click events. */
    }
};


mlayer.layer.Canvas = mlayer.extend(mlayer.layer.BaseLayer, {
    init: function(config) {
        // Position and number of currently displayed tiles
        this.points = [];
        if(config.points) {
            var self = this
            jQuery.each(config.points, function(index, p) {
                self.addPoint(p);
            });
        }
        this.needs_refresh = true;
        this.extent = {top:0, bottom:0, left:0, right:0}
    },
    addPoint: function(p) {
        this.extent.top = Math.min(this.extent.top, p.x);
        this.extent.bottom = Math.max(this.extent.bottom, p.x);
        this.extent.left = Math.min(this.extent.left, p.y);
        this.extent.right = Math.max(this.extent.right, p.y);
        this.points.push({x:p.x, y:p.y, name:p.name, color:p.color});
        this.points.sort(function(a, b){
            return b.y - a.y
        })
        this.needs_refresh = true;
    },
    getExtend: function(zoom) {
        var scale = Math.pow(2, zoom/10)
        return { top: this.extent.top * scale,
                 left: this.extent.left * scale,
                 bottom: this.extent.bottom * scale,
                 right: this.extent.right * scale}
    },
    onZoom: function() {
        this.needs_refresh = true;
    },
    onResize: function(vp) {
        ctx = this.canvas[0].getContext('2d')
        ctx.canvas.width = vp.width
        ctx.canvas.height = vp.height
        this.needs_refresh = true;
    },
    onLayout: function(zindex) {
        this.canvas = jQuery('<canvas />').appendTo(this.dom)
        this.needs_refresh = true;
        return 1
    },
    onMove: function(vp) {
        this.needs_refresh = true;
    },
    onDraw: function(vp) {
        if(! this.needs_refresh) return;
        this.needs_refresh = false;
        var scale = Math.pow(2, vp.zoom/10)
        this.scale = scale
        this.canvas.css({top:vp.top, left:vp.left})
        ctx.canvas.width = vp.width
        ctx.canvas.height = vp.height
        ctx = this.canvas[0].getContext('2d')
        ctx.clearRect(0, 0, vp.width, vp.height)
        ctx.save()
        //ctx.translate(vp.left, vp.top);
        var self = this;
        jQuery.each(this.points, function(index, p) {
            ctx.fillStyle = p.color
            ctx.fillRect(p.x*scale-2, p.y*scale-2, 4, 4)
        })
        ctx.restore()
    }
});

/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */

(function($) {

var types = ['DOMMouseScroll', 'mousewheel'];

if ($.event.fixHooks) {
    for ( var i=types.length; i; ) {
        $.event.fixHooks[ types[--i] ] = $.event.mouseHooks;
    }
}

$.event.special.mousewheel = {
    setup: function() {
        if ( this.addEventListener ) {
            for ( var i=types.length; i; ) {
                this.addEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = handler;
        }
    },
    
    teardown: function() {
        if ( this.removeEventListener ) {
            for ( var i=types.length; i; ) {
                this.removeEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = null;
        }
    }
};

$.fn.extend({
    mousewheel: function(fn) {
        return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
    },
    
    unmousewheel: function(fn) {
        return this.unbind("mousewheel", fn);
    }
});


function handler(event) {
    var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
    event = $.event.fix(orgEvent);
    event.type = "mousewheel";
    
    // Old school scrollwheel delta
    if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta/120; }
    if ( orgEvent.detail     ) { delta = -orgEvent.detail/3; }
    
    // New school multidimensional scroll (touchpads) deltas
    deltaY = delta;
    
    // Gecko
    if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
        deltaY = 0;
        deltaX = -1*delta;
    }
    
    // Webkit
    if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
    if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = -1*orgEvent.wheelDeltaX/120; }
    
    // Add event and delta to the front of the arguments
    args.unshift(event, delta, deltaX, deltaY);
    
    return ($.event.dispatch || $.event.handle).apply(this, args);
}

})(jQuery);

/* This object recognizes drag-move-drop gestures starting from a specific
   HTML node.
   
   Three callbacks are available: onDrag, onMove and onDrop. Each gets the
   DragDrop instance as its only argument.

*/

mlayer.util.distance = function(x, y) {
    return Math.sqrt(Math.pow(y[0]-x[0], 2) + Math.pow(y[1]-x[1], 2))
};

mlayer.util.DragDrop = function(config) {
    config          = config || {}
    this.shield     = null;
    this.dragging   = false;
    this.posStart   = [0, 0];
    this.posCurrent = [0, 0];
    this.posLast    = [0, 0];
    this.lastClick  = null;
    this.onDrag     = config.onDrag   || function(){}
    this.onMove     = config.onMove   || function(){}
    this.onDrop     = config.onDrop   || function(){}
    this.onWheel    = config.onWheel  || function(){}
    this.onClick    = config.onClick  || function(){}
    this.onDClick   = config.onDClick || function(){}
    if(config.dom) {
        this.setDom(config.dom)
    };
};

mlayer.util.DragDrop.prototype = {
    setDom: function(dom) {
        this.dom = jQuery(dom);
        var self = this;
        var mouseDown = function(e) {
            if(e.button != 0) return
            e.preventDefault();
            e.stopPropagation();
            // We missed a mouseUp event ??
            if(self.dragging) { mouseUp(e); }
            self.posStart   = [e.pageX, e.pageY];
            self.posLast    = [e.pageX, e.pageY];
            self.posCurrent = [e.pageX, e.pageY];
            self.dragging = new Date();
            if(!self.shield) {
                self.shield = jQuery('<div />').css({
                    position: 'absolute',
                    visibility: 'hidden',
                    width: '100%',
                    height: '100%',
                    zIndex: '99999'
                }).prependTo('body:first');
            }
            self.shield.css({cursor:'move', visibility: "visible"});
            jQuery(document).bind('mousemove', mouseMove);
            jQuery(document).bind('mouseup', mouseUp);
            self.onDrag(self);
            return true
        }
        var mouseMove = function(e) {
            if(!self.dragging) return;
            self.posLast = self.posCurrent;
            self.posCurrent = [e.pageX, e.pageY];
            self.onMove(self);
        }
        var mouseUp = function(e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery(document).unbind('mousemove', mouseMove);
            jQuery(document).unbind('mouseup', mouseUp);
            if(!self.dragging) return;
            self.posLast = self.posCurrent;
            self.posCurrent = [e.pageX, e.pageY];
            var elapsed = (new Date()).getTime() - self.dragging.getTime();
            self.dragging = false;
            self.shield.css({cursor:'', visibility: "hidden"});
            self.onDrop();
            // The shield is a combo breaker. Normal click events won't work.
            if(elapsed < 500 && 3 > mlayer.util.distance(self.posStart, self.posCurrent)) {
                self.onClick(e.pageX, e.pageY); // This is fake, but works.
                var now = new Date()
                if(self.lastClick && now - self.lastClick < 500)
                    self.onDClick(e.pageX, e.pageY);
                self.lastClick = now;
            }
        }
        var mouseWheel = function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            event.stopPropagation();
            self.onWheel(delta, deltaX, deltaY);
        }
        this.dom.bind('mousedown', mouseDown);
        this.dom.bind('mousewheel', mouseWheel);
    }
};

mlayer.layer.Controls = mlayer.extend(mlayer.layer.BaseLayer, {
    is_hud: true,
    init: function(config) {
        // Position and number of currently displayed tiles
        this.status = jQuery('<span />').css({
            position: 'absolute', width: '1px'
        }).appendTo(this.dom)
        this.dom.css({width: '100%', height:'100%'});
    },
    onLayout: function(zindex) {
        var map = this.map;
        var self = this;
        this.drag_control = new mlayer.util.DragDrop({
            dom: this.dom,
            onMove: function(c) {
                var dx = c.posCurrent[0] - c.posLast[0];
                var dy = c.posCurrent[1] - c.posLast[1];
                map.moveBy(-dx, -dy);
            },
            onWheel: function(delta, deltaX, deltaY) {
                if(delta > 0) map.zoomIn(1);
                else if(delta < 0) map.zoomOut(1);
            },
            onClick: function(x, y) {
                var o = map.dom.offset()
                map.click(x-o.left, y-o.top)
            },
            onDClick: function(p) {
            }
        })

        this.joystick = jQuery('<div />').css({
            'background-image': 'url(/img/joystick.png)',
            'background-position': 'center center',
            position: 'absolute', top: '10px', left: '10px',
            width:'48px', height:'48px'}).appendTo(this.dom)

        this.zoom_plus = jQuery('<div />').css({
            'background-image': 'url(http://maps.gstatic.com/intl/de_ALL/mapfiles/szc.png)',
            'background-position': 'top center',
            position: 'absolute', top: '63px', left: '26px',
            width:'17px', height:'17px'}).appendTo(this.dom)

        this.zoom_minus = jQuery('<div />').css({
            'background-image': 'url(http://maps.gstatic.com/intl/de_ALL/mapfiles/szc.png)',
            'background-position': 'bottom center',
            position: 'absolute', top: '83px', left: '26px',
            width:'17px', height:'17px'}).appendTo(this.dom)

        this.zoom_plus.on('mousedown', function(event) {
            event.preventDefault();
            event.stopPropagation();
            map.zoomIn(5)
        })

        this.zoom_minus.on('mousedown', function(event) {
            event.preventDefault();
            event.stopPropagation();
            map.zoomOut(5)
        })

        this.joystick_control = new mlayer.util.DragDrop({
            dom: this.joystick,
            onDrag: function(c) {
                self._joystick_interval = window.setInterval(function(){
                    var dx = Math.ceil((c.posCurrent[0] - c.posStart[0])/10);
                    var dy = Math.ceil((c.posCurrent[1] - c.posStart[1])/10);
                    var px = (!dx) ? 'center' : ((dx > 0) ? 'right' : 'left')
                    var py = (!dy) ? 'center' : ((dy > 0) ? 'bottom' : 'top')
                    self.joystick.css('background-position', px+' '+py)
                    map.moveBy(dx, dy);
                }, 50)
            },
            onDrop: function(c) {
                window.clearInterval(self._joystick_interval);
                self._joystick_interval = null;
                self.joystick.css('background-position', 'center center')
            }
        })

        this.status.css({top: '5px', left: (map.viewport.width-50)+'px'})
        if(this.config.hide_controls){
            self.joystick.hide()
            self.zoom_plus.hide()
            self.zoom_minus.hide()
        }
        return 0;
    },
    onResize: function(vp) {
        this.status.css({top: '5px', left: (vp.width-50)+'px'})
    },
    onMove: function(vp) {
        this.status.html('x:'+vp.left+' y:'+vp.top)
    }
});


mlayer.layer.Tiles = mlayer.extend(mlayer.layer.BaseLayer, {
    init: function(config) {
        // Position and number of currently displayed tiles
        this.covered = {top:0, left:0, bottom:0, right:0, zoom:0}
        this.tilesize = config.tilesize || 256;
        this.offset = config.offset || {top:0, left:0}
        this.size = config.size || {width:0, height:0}
        
        // Number of tiles to draw in advance
        this.buffer = config.buffer || 0;
        if(typeof config.url == 'string') {
            var url = config.url
            config.url = function(x, y, z, ts) {
                return url.replace(/\{x\*size\}/g, x*ts)
                          .replace(/\{y\*size\}/g, y*ts)
                          .replace(/\{x\}/g, x)
                          .replace(/\{y\}/g, y)
                          .replace(/\{z\}/g, z)
                          .replace(/\{size\}/g, ts);
            }
        }
        if(typeof config.url == 'function')
            this.url = config.url;
    },
    getExtent: function(zoom) {
        var scale = Math.pow(2, zoom/10)
        return {
            top: this.offset.top * scale,
            left: this.offset.left * scale,
            bottom: (this.offset.top + this.size.height) * scale,
            right: (this.offset.left + this.size.width) * scale,
        }
    },
    onDraw: function(view) {
        if(!this.url) return;

        var cov = this.covered;
        var ts  = this.tilesize;
        var ext = this.getExtent(view.zoom);
        var off = this.offset;

        // Calculate visible tiles (including border)
        var vtop    = Math.floor(view.top    / ts) - this.buffer;
        var vleft   = Math.floor(view.left   / ts) - this.buffer;
        var vbottom = Math.ceil( view.bottom / ts) + this.buffer;
        var vright  = Math.ceil( view.right  / ts) + this.buffer;

        // Clip visible tiles on extent
        vtop    = Math.max(vtop,    Math.floor(ext.top    / ts));
        vleft   = Math.max(vleft,   Math.floor(ext.left   / ts));
        vbottom = Math.min(vbottom, Math.ceil(ext.bottom / ts));
        vright  = Math.min(vright,  Math.ceil(ext.right  / ts));

        this.dom.hide()
        if(vtop == vbottom || vleft == vright) return;
        if(cov.top != vtop || cov.left != vleft || cov.bottom != vbottom || cov.right != vright
        || cov.zoom != view.zoom) {
            var html = '';
            var z = view.zoom;
            for(var x=vleft; x<vright; x++) {
                for(var y=vtop; y<vbottom; y++) {
                    html += '<span style="position: absolute;'+
                            ' width:'+ts+'px; height:'+ts+'px;'+
                            ' top:'+(y*ts)+'px; left:'+(x*ts)+'px;'+
                            ' background: transparent url(\''+this.url(x,y,z,ts)+'\')" no-repeat top left" />';
                }
            }
            this.dom.html(html);
            cov.top = vtop;
            cov.left = vleft;
            cov.right = vright;
            cov.bottom = vbottom;
            cov.zoom = view.zoom;
        }
        this.dom.show()
    },
    onZoom: function(view) {
        this.covered = {top:0, left:0, bottom:0, right:0, zoom:0}
        this.dom.hide()
    },
    onLayout: function() {
        this.covered = {top:0, left:0, bottom:0, right:0, zoom:0}
    }
});

mlayer.layer.LucullusView = mlayer.extend(mlayer.layer.Tiles, {
    init: function(config) {
        var self = this;
        config.url = function(x, y, z, ts) {
            return self.view.image(self.channel, x*ts, y*ts, z, ts, ts, 'png')
        }
        mlayer.layer.Tiles.prototype.init.call(this, config);

        this.channel = config.channel || 'default'
        this.view = config.view;
        this.view.on('change', function(view) {
            this.covered = {top:0, left:0, bottom:0, right:0};
            this.offset.top = this.view.state.offset[0]
            this.offset.left = this.view.state.offset[1]
            this.size.width = this.view.state.size[0]
            this.size.height = this.view.state.size[1]
            if(this.map) this.map.moveBy(0,0);
        }, this)
    }
});
/* This layer does not draw anything, but relays movements to a different map.
   It is used to synchronize two or more maps.
*/

mlayer.layer.MoveBridge = mlayer.extend(mlayer.layer.BaseLayer, {
    init: function(config) {
        this.target = config.target
        this.scaleX = typeof config.scaleX == 'number' ? config.scaleX : 1;
        this.scaleY = typeof config.scaleY == 'number' ? config.scaleY : 1;
        this.offsetX = config.offsetX || 0;
        this.offsetY = config.offsetY || 0;
    },
    onMove: function(vp) {
        this.target.moveTo(this.offsetX + this.scaleX * vp.left,
                           this.offsetY + this.scaleY * vp.top)
    },
    onZoom: function(vp) {
        this.target.zoom(vp.zoom)
    },
    onClick: function(vp) {
        
    }
});
/* Draw labeled points and show the label if the mouse is near the point. */

mlayer.img = {}
mlayer.img.greenmark = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAFRQTFRF////AAAAAAAAAAAAAAAAAA4AAAwAAAwAAAAAAAAAAAsAABUAAEAAAEoAAAAAAFIAAFwAAHAAAHoAAI8AAJkAAKMAACMAACMAACEAACYAACMAAKIA3Cc20AAAABt0Uk5TAAIIDxASFRYXGBgYGBgZGRkZGRkZGZSa4ODi6/J+SAAAAGhJREFUCNdlj1sSgCAIRcnSMjMTtOf+91nYVJOeH7gwwAXgQjRKq1rATSWN8+RdL6skOxsiE4aOC9LGh0Fesya8OhgBzRQ/XA0tcrJsMwdUMBIn+7FyIF308/l8f36/8Jf8I+Hj///fCZaWC+tWhEW+AAAAAElFTkSuQmCC'
mlayer.img.greenmark = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAACFSURBVAiZLc69DQFhAADQ97loxFV+cjoqrZohLgawyC1hBxKb0F+UCjoGEByJT+Fb4OWFGKOwDR1MMMIV57iKj2BjgAUuuKHAGPsWpqg1So2jRoka0xYKuZu2yk5fWyX/C5mlnrevl6eZmY+1rxOyYGOIOc4pM0q5Q0jbbkoUKXWJq3j/AQHkK6RM64heAAAAAElFTkSuQmCC'

mlayer.layer.Points = mlayer.extend(mlayer.layer.BaseLayer, {
    init: function(config) {
        // Position and number of currently displayed tiles
        this.points = [];
        if(config.points) {
            var self = this
            jQuery.each(config.points, function(index, p) {
                self.addPoint(p);
            });
        }
        this.needs_refresh = true;
        this.extent = {top:0, bottom:0, left:0, right:0}
    },
    addPoint: function(p) {
        this.extent.top = Math.min(this.extent.top, p.x);
        this.extent.bottom = Math.max(this.extent.bottom, p.x);
        this.extent.left = Math.min(this.extent.left, p.y);
        this.extent.right = Math.max(this.extent.right, p.y);
        this.points.push({x:p.x, y:p.y, name:p.name, color:p.color, opacity:p.opacity});
        this.points.sort(function(a, b){
            return b.y - a.y
        })
        this.needs_refresh = true;
    },
    getExtend: function(zoom) {
        var scale = Math.pow(2, zoom/10)
        return { top: this.extent.top * scale,
                 left: this.extent.left * scale,
                 bottom: this.extent.bottom * scale,
                 right: this.extent.right * scale}
    },
    onZoom: function() {
        this.needs_refresh = true;
        this.dom.hide()
    },
    onLayout: function(zindex) {
        this.label_zindex = zindex+1;
        return 1
    },
    onDraw: function(vp) {
        var scale = Math.pow(2, vp.zoom/10)
        this.scale = scale
        if(! this.points) return;
        if(! this.needs_refresh) return;
        this.needs_refresh = false;
        var html = '';
        var img = jQuery(
            '<span style="position: absolute; width:2px; height:2px; border: 1px solid green; border-radius: 3px;" />');
        var self = this;
        this.dom.empty()
        this.dom.hide()
        jQuery.each(this.points, function(index, p) {
            var node = img.clone()
            node.attr('desc', p.name);
            node.css({
                left: Math.round(p.x*scale) - 2 + 'px',
                top:  Math.round(p.y*scale) - 2 + 'px',
                borderColor: p.color,
                opacity: (p.opacity || 1.0)
            })
            self.dom.append(node)
            p.node = node
        })
        this.list = jQuery('<div />')
        this.list.css({
            position: 'absolute',
            top: vp.top + 60 + 'px',
            left: vp.left + 10 +'px',
            display: 'hidden',
            whiteSpace: 'nowrap',
            fontSize: '10px',
            backgroundColor: 'white',
            border: '1px solid grey',
        }).hide().appendTo(this.dom)
        this.dom.show()
        return this
    },
    onClick: function(vp, x, y) {
        var text = '', radius = 5, scale = this.scale;
        var self = this;
        var sx = (vp.left+x)/scale, sy = (vp.top+y)/scale;
        radius = radius / scale
        var match = false;
        var n=0;
        jQuery.each(self.points, function(index, p) {
            if(p.txt) {
                p.txt.remove();
                p.txt = null;
                p.node.css('borderColor', p.color)
            }
            var dx = this.x-sx, dy = this.y-sy
            if(-radius < dx && dx < radius && -radius < dy && dy < radius) {
                n+=1;
                p.node.css('borderColor', 'red')
                p.txt = jQuery('<div />')
                p.txt.css({ borderLeft: '3px solid '+p.color,
                            padding: '0 2px 0 2px',
                            fontFamily: 'monospace',
                            //paddingLeft: radius*2-dx,
                            //marginLeft:  radius*2+dx
                          })
                var name = p.name;
                if(p.opacity)
                    name = name + '('+p.opacity+')';
                p.txt.text(name).prependTo(self.list)
                match = true
            }
        })
        if(match) {
            var w = this.list.outerWidth(),
                h = this.list.outerHeight();
            var top  = vp.top + y + radius + 5;
            var left = vp.left + x + radius + 5;
            if(top+h > vp.bottom) top = vp.top + y - h - radius - 5;
            if(left+w > vp.right) left = vp.left + x - w - radius - 5;
            this.list.css({top:top, left:left}).show()
        } else {
            this.list.hide()
        }
    }
});

mlayer.layer.Sprite = mlayer.extend(mlayer.layer.BaseLayer, {
    is_hud: true,
    init: function(config) {
        // Position and number of currently displayed tiles
        this.offset = config.offset || [0,0]
        this.size = config.size || [50,30]
        this.domid = config.domid || ''
    },
    onLayout: function() {
        this.dom.css({top: this.offset[1], left: this.offset[0], width: this.size[0] + 'px', height: this.size[1] + 'px'})
        this.dom.attr('id', this.domid)
    }
});

