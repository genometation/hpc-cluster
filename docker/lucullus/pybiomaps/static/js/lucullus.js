
Lucullus = {}


Lucullus.Server = function(url, key) {
    if (!(this instanceof arguments.callee))
        throw new Error("Constructor called as a function");

    this.url = url
    this.key = key
    this.rc  = 0
}

Lucullus.Server.prototype.create = function(type) {
    return new Lucullus.Thing(this, type)
}






Lucullus.Thing = function(api, type) {
    if (!(this instanceof arguments.callee))
        throw new Error("Constructor called as a function");

    this.id = null      // Resource ID
    this.type = type    // Resource class name.
    this.api = api
    this.state = {}
    this.queue = []

    this.events = {
        create: jQuery.Callbacks(),
        update: jQuery.Callbacks(),
        answer: jQuery.Callbacks(),
        error:  jQuery.Callbacks()
    }

    var self = this
    this.on_create = function(cb) {self.events.create.add(cb)}
    this.on_update = function(cb) {self.events.update.add(cb)}
    this.on_answer = function(cb) {self.events.answer.add(cb)}
    this.on_error  = function(cb) {self.events.error.add(cb)}

    this.query('create', {type:type})
}

Lucullus.Thing.prototype.query = function(action, data, success, error) {
    var def = jQuery.Deferred()
    var self = this
    def.query = {action:action, data: data}
    def.always(function(){
        var old = self.queue.shift()
        if(old !== def) alert('Queue out of order exception')
        if(self.queue.length) {
            self.send_query()
        }
    })
    if(success) def.done(success)
    if(error) def.fail(error)
    this.queue.push(def)
    if(this.queue.length == 1) {
        self.send_query()
    }
    return def.promise()
}

Lucullus.Thing.prototype.send_query = function() {
    var self = this,
        deferred = this.queue[0],
        uri = this.api.url,
        action = deferred.query.action,
        data   = deferred.query.data || {};

    if(this.id && action) uri = uri + 'r' + this.id + '/' + action
    else if(this.id)  uri = uri + 'r' + this.id
    else if(action) uri = uri + action

    data.apikey = this.api.key

    var request = jQuery.ajax(uri, {
        cache: false,
        //data: JSON.stringify(data),
        data: data,
        dataType: 'json',
        type: 'POST'
    })

    this.last_query = request

    request.done(function(data){
        if(typeof data == 'undefined' || data.error) {
            self.events.error.fireWith(self, [self, data])
            deferred.reject([self, data])
            return
        }
        var old_mtime = self.state.mtime || 0
        var old_id = self.id
        if(data.id) self.id = data.id
        if(data.type) self.type = data.type
        if(data.actions) self.actions = data.methods
        if(data.state) self.state = data.state
        // Fire more specific events
        if(self.id && self.old_id === null) {
            self.events.create.fireWith(self)
            self.events.create.lock()
        }
        if(old_mtime < self.state.mtime)
            self.events.update.fireWith(self, [self])
        if(data.result)
            self.last_answer = request
            self.events.answer.fireWith(self, [self])
        deferred.resolve(self, data)
    })

    request.fail(function(data) {
        self.events.error.fireWith(self, [self, undefined])
        deferred.reject(self, undefined)
    })

}

Lucullus.Thing.prototype.has_action = function(name) {
    return jQuery.inArray(this.actions, name) > -1
}

Lucullus.Thing.prototype.get_image = function(channel, x, y, z, w, h, format) {
    if(x === undefined) x = 0
    if(y === undefined) y = 0
    if(z === undefined) z = 0
    if(w === undefined) w = 256
    if(h === undefined) h = 256
    if(format === undefined) format = 'png'
    if(w === 'full') w = Math.ceil(this.state.size[0] * Math.pow(2, z/10))
    if(h === 'full') h = Math.ceil(this.state.size[1] * Math.pow(2, z/10))
    if(z === 'fit') {
        var z1 = Math.log(Math.pow(w/this.state.size[0], 10)) / Math.LN2
        var z2 = Math.log(Math.pow(h/this.state.size[1], 10)) / Math.LN2
        z = Math.ceil(Math.min(z1, z2))
    }
    if(z === NaN || w === NaN || h === NaN) return

    return this.api.url+'r'+this.id+'/'+channel+'-x'+x+'y'+y+'z'+z+
           'w'+w+'h'+h+'.'+format+'?rev='+this.state.rev
}






mlayer.layer.LucullusView = mlayer.extend(mlayer.layer.Tiles, {
    init: function(config) {
        var self = this;
        config.url = function(x, y, z, ts) {
            return self.view.get_image(self.channel, x*ts, y*ts, z, ts, ts, 'png')
        }
        mlayer.layer.Tiles.prototype.init.call(this, config);

        this.channel = config.channel || 'default'
        this.view = config.view;
        this.view.on_update(function(view) {
            self.covered = {top:0, left:0, bottom:0, right:0};
            self.offset.top = view.state.offset[0]
            self.offset.left = view.state.offset[1]
            self.size.width = view.state.size[0]
            self.size.height = view.state.size[1]
            if(self.map) self.map.moveBy(0,0);
        })
    }
});

