#!/usr/bin/env python
# encoding: utf-8
"""
plugin_seq.py

Created by Marcel Hellkamp on 2009-01-20.
Copyright (c) 2008 Marcel Hellkamp. All rights reserved.
"""

import cairo
import math
import random
import os
import sys
import urllib2
from StringIO import StringIO
from fnmatch import fnmatch
from itertools import izip
import array, struct

from pybiomaps.resource import BaseView, ResourceQueryError
from pybiomaps.render.color import pick as color_picker
from Bio import SeqIO, Seq, SeqRecord

import logging
log = logging.getLogger(__name__)


def calc_digits(f, err=0):
    ''' Return a sane number of digits to display. '''
    d = 0
    while -1 < f*10**d < 1:
        d += 1
    return d



class ProcFile(object):
    def __init__(self, filename):
        self.data = {}
        with open(filename, 'r') as fp:
            for line in fp:
                if not line.startswith('##$'): continue
                key, value = [x.strip() for x in line[3:].split("=", 1)]
                self.data[key] = value
        self.dim = int(self.data['SI'])
        self.scale = 2 ** float(self.data['NC_proc'])
        self.sweep = float(self.data['SW_p'])
        self.sfreq = float(self.data['SF'])
        self.ppm_max = float(self.data['OFFSET'])
        self.ppm_min = self.ppm_max - (self.sweep / self.sfreq)
        self.ppm_width = self.ppm_max - self.ppm_min
        self.ppm_resolution = self.ppm_width / self.dim
        self.byteorder = 'b' if 'BYTORDP' in self.data else 'l'


try:
    import matplotlib._cntr as mplctr
    import numpy
except ImportError:
    mplctr = None


class ContourTracer(object):
    def __init__(self, narray):
        if narray.ndim != 2: raise TypeError("Need 2d array")
        self.data = narray
        self.width, self.height = narray.shape
        self._cntr = None
        self._tcache = {}

    def __getstate__(self):
        d = self.__dict__.copy()
        d.update(_cntr=None, _tcache={})
        return d

    @classmethod
    def load(cls, fname, width, height, format='<i4'):
        with open(fname, "rb") as fp:
            data = numpy.fromfile(fp, format, -1)
            data = numpy.ndarray(shape=(width, height), buffer=data, dtype=format)
            return cls(data)

    def trace(self, level):
        ''' Calculate a contour on a specific level. Return an iterator that
            yields closed paths. Each path is a list of points. Each point is
            a (x, y) tuple. '''
        if not self._cntr:
            if not mplctr: raise ImportError('matplitlib._cntr not found.')
            ax, ay = numpy.meshgrid(numpy.arange(self.height),
                                    numpy.arange(self.width))
            self._cntr = mplctr.Cntr(ax, ay, self.data)
        if level not in self._tcache:
            self._tcache[level] = self._cntr.trace(level)
        return self._tcache[level]





class LevelResource(BaseView):
    def prepare(self):
        #: Raw 2rr data (unscaled)
        self.data = array.array('i')

        # Meta values for the raw data array
        #: Value scale (raw_value * scale = intensity)
        self.scale = None

        #: Intensity level of first contour
        self.level = None

        #: Lowest and highest value in dataset
        self.data_min   = None
        self.data_max   = None

        #: Covered spectrum on the two axis (parts per million)
        self.ppm_min = None, None
        self.ppm_max = None, None
        #: Data desolution (datapoints per ppm) on the two axis
        self.ppm_res = None, None

        #: 2d dimension of data array
        self.data_dim = None, None
        #: data points per image pixel
        self.data_res = None, None

        #: Image size is independant of data size
        self.img_size = 1024, 1024

        # Caches
        self.contour_tracer = None

        self.grid = None
        self.color = {
            'bg': (1,1,1,1), 'grid': (0,0,0,0.2), 'data': (0,0,0,1),
            'points': (1,0,0,1)
        }

        self.points = []
        self._color_cache = {}

    def size(self): return self.img_size

    def getstate(self):
        s = super(LevelResource, self).getstate()
        s['scale'] = self.scale
        s['level'] = self.level
        s['ppm_min'] = self.ppm_min
        s['ppm_max'] = self.ppm_max
        s['ppm_res'] = s['ppm_res'] = self.ppm_res
        s['data_dim'] = self.data_dim
        s['data_res'] = self.data_res
        s['img_size'] = self.img_size
        return s

    def setup(self, **options):
        if 'level' in options:
            self.level = float(options.get('level'))
            self.update()

        #self.grid = int(options.get('grid') or self.grid)
        for key in options:
            if key.endswith("_color"):
                self.color[key[:-6]] = color_picker(options[key])

        self.touch()

    def update(self):
        # If the level is between 0 and 1, it represents the fraction of data
        # points above the actual level. We (re)calculate this here.
        if self.level is None:
            self.level = 0.01
        if 0 < self.level < 1 and self.data:
            sarray = sorted(self.data)
            index = int((len(sarray)-1)*(1.0-self.level))
            self.level = int(sarray[index] * self.scale)

        # Calculate raw data points per pixel
        if self.data:
            self.data_res = (float(self.data_dim[0]) / self.img_size[0],
                             float(self.data_dim[1]) / self.img_size[1])

    def api_load_bruker(self, data, procs, proc2s):
        fmask = '/tmp/webpeakr/peakr_uploaded_file_%d'

        # Import procs/proc2s meta files.

        if procs.isdigit() and proc2s.isdigit():
            procfiles = fmask % int(procs), fmask % int(proc2s)
        elif procs.startswith('http://') and proc2s.startswith('http://'):
            raise ResourceQueryError("HTTP import not implemented.")
        else:
            raise ResourceQueryError("Unsupported import protocol for procs/proc2s.")

        try:
            which = 'procs'
            p1 = ProcFile(procfiles[0])
            which = 'proc2s'
            p2 = ProcFile(procfiles[1])
        except IOError:
            raise ResourceQueryError('Could not load "%s" file.' % which)
        except KeyError:
                raise ResourceQueryError('Missing parameters in "%s" file.' % which)
        except TypeError:
                raise ResourceQueryError('Malformed data in "%s" file.' % which)

        if p1.scale != p2.scale:
            raise ResourceQueryError('procs/proc2s: Scale values do not match.')
        if p1.byteorder != p2.byteorder:
            raise ResourceQueryError('procs/proc2s: Byte order does not match.')

        # Import 2rr data file.

        if data.isdigit():
            datafile = fmask % int(data)
        elif data.startswith('http'):
            raise ResourceQueryError("HTTP import not implemented.")
        else:
            raise ResourceQueryError("Unsupported import protocol for 2rr datafile.")

        try:
            with open(datafile, 'rb') as fp:
                data = array.array('i')
                data.fromfile(fp, p1.dim * p2.dim)
        except IOError, e:
            raise ResourceQueryError('Could not load 2rr data file.')

        if p1.byteorder != 'b':
            data.byteswap()

        self.data      = data
        self.procs     = p1
        self.proc2s    = p2
        self.data_dim  = p1.dim, p2.dim
        self.data_min  = min(self.data)
        self.data_max  = max(self.data)
        self.ppm_min   = (p1.ppm_min, p2.ppm_min)
        self.ppm_max   = (p1.ppm_max, p2.ppm_max)
        self.ppm_res   = (p1.ppm_resolution, p2.ppm_resolution)
        self.ppm_ratio = p1.ppm_resolution / p2.ppm_resolution
        self.scale     = p1.scale
        self.contour_tracer   = None
        self.touch()
        self.update()

    def api_load_poi(self, poi, color='green'):
        fmask = '/tmp/webpeakr/peakr_uploaded_file_%d'

        if poi.isdigit():
            fname = fmask % int(poi)
        elif poi.startswith('http://'):
            raise ResourceQueryError("HTTP import not implemented.")
        else:
            raise ResourceQueryError("Unsupported import protocol for poi datafile.")

        try:
            with open(fname, 'r') as fp:
                lines = list(fp)
        except IOError:
            raise ResourceQueryError('Could not load poi data.')

        points = []
        for lineno, line in enumerate(lines):
            line = line.strip()
            if not line or line[0] in '#%': continue
            parts = line.split(None, 3)
            try:
                x = float(parts[1])
                y = float(parts[0])
                name = parts[2]
                flags = dict(f.split(':', 1) for f in parts[3:] if ':' in f)
            except ValueError:
                raise ResourceQueryError('Syntax error in line %d' % lineno)
            point = dict(x=x, y=y, name=name, color=flags.get('color',color))
            point.update(flags)
            point['ccolor'] = color_picker(color, False)
            if 'opacity' in flags:
                r,g,b,a = point['ccolor']
                a *= float(flags['opacity'])
                if not a: continue # Ignore invisible POIs
                point['ccolor'] = (r,g,b,a)
            points.append(point)

        self.points.extend(points)
        # We have no resolution data. This scales the points up to 1024px
        if not any(self.ppm_res):
            self.ppm_min   = a = (min(p['x'] for p in self.points)-1,
                                  min(p['y'] for p in self.points)-1)
            self.ppm_max   = b = (max(p['x'] for p in self.points)+1,
                                  max(p['y'] for p in self.points)+1)
            self.ppm_res   = (float(b[0]-a[0])/1024, float(b[1]-a[1])/1024)
            self.ppm_ratio = 1.0, 1.0
            self.data_dim  = 1024, 1024
            self.data_res  = 1.0, 1.0
            self.data_min  = 0
            self.data_max  = 100
            self.img_size  = 1024, 1024
            self.scale     = 1.0

        self.touch()
        self.update()

    def api_load_local(self, data=None, procs=None, proc2s=None, poi=None):
        if data and procs and proc2s:
            self.api_load_bruker(data, procs, proc2s)
        if poi:
            print poi
            if isinstance(poi, basestring): poi = [poi]
            for pl in poi:
                for p in pl.split(','):
                    if ':' in p:
                        p, _, color = p.partition(':')
                    else:
                        color = 'green'
                    self.api_load_poi(p, color)

    def api_get_points(self):
        return {'points': self.points}

    def api_cross(self, x, y):
        pass
        #data = numpy.flipud(numpy.ndarray(shape=self.dim, buffer=self.data, dtype='<i4'))
        #x, y = int(x), int(y)
        #column = [int(p) for p in data[x,:]]
        #row    = [int(p) for p in data[:,y]]
        #return {'row': row, 'column': column, 'value': int(data[x,y])}

    def get_highmap_paths(self, level, bbox):
        if not self.data: return
        rx, ry = self.data_res # data points per pixel
        level = int(level / self.scale)

        if not self.contour_tracer:
            data = numpy.ndarray(shape=self.data_dim[::-1], dtype='<i4',
                                 buffer=self.data)
            data = numpy.flipud(data)
            self.contour_tracer = ContourTracer(data)

        if bbox:
            # Turn bbox from pixel into data coordinates and return only the
            # interesting points
            bmin, bmax = bbox
            bmin = bmin[0]*rx, bmin[1]*ry
            bmax = bmax[0]*rx, bmax[1]*ry
            for path in self.contour_tracer.trace(level):
                # path is a 2xN numpy array.
                if (path.max(axis=0)<bmin).any(): continue
                if (path.min(axis=0)>bmax).any(): continue
                yield path / (rx,ry)
        else:
            for path in self.contour_tracer.trace(level):
                yield path / (rx,ry)

    def render(self, rc):
        rc.clear(self.color['bg'])
        self.render_levels(rc)
        self.render_grid(rc)
        if 'poi' in rc.channel:
            self.render_points(rc)
            self.render_ruler(rc)
        return self

    def render_levels(self, rc):
        if not self.data: return
        c = rc.context
        bbox = rc.area.area[:2], rc.area.area[2:]

        levels = []
        r,g,b,a = self.color['data']
        numlevel = 10

        for i in xrange(numlevel):
            level = self.level * 2 ** i
            if level > self.data_max * self.scale: break
            levels.append(level)

        c.set_line_width(1.0 * rc.scale)
        c.set_source_rgba(r, g, b, a)
        line_to, move_to, close_path = c.line_to, c.move_to, c.close_path
        for level in levels:
            c.new_path()
            for path in self.get_highmap_paths(level, bbox):
                move_to(*path[0])
                for point in path[1:]:
                    line_to(*point)
                close_path()
            c.stroke()

    def calc_grid(self, rc, axis=0):
        ''' Return low boundary and width of grid lines (in pixel coordinates). '''
        px = rc.scale

        data_per_pixel  = self.data_res[axis]
        pixel_per_data  = 1.0 / data_per_pixel
        units_per_data  = self.ppm_res[axis]
        pixel_per_unit  = pixel_per_data / units_per_data
        # The grid defaults to one unit per pixel
        grid = pixel_per_unit

        # Adjust the grid until it makes sense to the viewer,
        # but keep nice unit values.
        while grid/px > 1000: grid /= 10
        while grid/px < 100:  grid *= 10

        # Calculate pixel position of first visible grid line
        # or in other words: Pixel offset of grid relative to axis
        if axis == 0:
            offset = (self.ppm_max[axis] * -pixel_per_unit) % grid
        else:
            offset = (self.ppm_min[axis] * pixel_per_unit) % grid

        return offset, grid

    def render_grid(self, rc):
        if not all(self.data_res): return

        c = rc.context
        area = rc.area
        px = rc.scale

        offset_x, width = self.calc_grid(rc, 0)
        left   = area.left - (area.left % width)
        right  = area.right + width - (area.right % width)

        offset_y, height = self.calc_grid(rc, 1)
        top     = area.top - (area.top % height)
        bottom  = area.bottom + height - (area.bottom % height)

        width, height = float(width), float(height)

        # Draw thin lines
        c.set_source_rgba(*self.color['grid'])
        c.set_line_width(1*px)
        c.set_dash([1*px,1*px])

        mark = left-width-offset_x
        while mark < right+width:
            c.move_to(mark-px, top)
            c.line_to(mark-px, bottom)
            mark += width/10.0
        mark = top - height-offset_y
        while mark < bottom+height:
            c.move_to(left, mark-px)
            c.line_to(right, mark-px)
            mark += height/10.0
        c.stroke()

        # Draw thick lines (each 10 grid lines)
        c.set_dash([])
        mark = left-width-offset_x
        while mark < right+width:
            c.move_to(mark-px, top)
            c.line_to(mark-px, bottom)
            mark += width
        mark = top - height-offset_y
        while mark < bottom+height:
            c.move_to(left, mark-px)
            c.line_to(right, mark-px)
            mark += height
        c.stroke()

    def render_ruler(self, rc):
        c = rc.context
        area = rc.area
        px = rc.scale
        fontsize = 10 * px

        offset_x, width = self.calc_grid(rc, 0)
        left   = area.left - (area.left % width)
        right  = area.right + width - (area.right % width)

        offset_y, height = self.calc_grid(rc, 1)
        top     = area.top - (area.top % height)
        bottom  = area.bottom + height - (area.bottom % height)

        width, height = float(width), float(height)

        c.set_source_rgba(0,0,0,1)
        fo = cairo.FontOptions()
        fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
        c.set_font_options(fo)
        c.set_font_size(fontsize)
        font_extends = c.font_extents()
        font_height = font_extends[3]

        digits = calc_digits(width)
        format = '%%.%df' % digits if digits else '%.0f'

        units_offset    = self.ppm_max[0]
        data_per_pixel  = self.data_res[0]
        units_per_data  = self.ppm_res[0]
        units_per_pixel = units_per_data * data_per_pixel * -1
        mark = left-width-offset_x
        while mark < right+width:
            value = units_offset + mark * units_per_pixel
            text = format % value
            text_width, text_height = c.text_extents(text)[2:4]
            c.move_to(mark - text_width/2.0, area.top+text_height+px)
            c.show_text(text)
            mark += width

        digits = calc_digits(height)
        format = '%%.%df' % digits if digits else '%.0f'

        units_offset    = self.ppm_min[1]
        data_per_pixel  = self.data_res[1]
        units_per_data  = self.ppm_res[1]
        units_per_pixel = units_per_data * data_per_pixel
        mark = top-height-offset_y
        while mark < bottom+height:
            value = units_offset + mark * units_per_pixel
            text = format % value
            text_width, text_height = c.text_extents(text)[2:4]
            c.move_to(area.left+px, mark+text_height/2)
            c.show_text(text)
            mark += height

    def ppm_to_px(self, x, y):
        x = (self.ppm_max[0] - x) / (self.ppm_res[0] * self.data_res[0])
        y = (y - self.ppm_min[1]) / (self.ppm_res[1] * self.data_res[1])
        return x, y

    def render_points(self, rc):
        px = rc.scale
        c = rc.context
        c.set_line_width(1*px)
        for p in self.points:
            x, y = self.ppm_to_px(p['x'], p['y'])
            c.set_source_rgba(*p['ccolor'])
            c.arc(x-1, y-1, 2.0*px, 0, 2*math.pi);
            c.stroke()





