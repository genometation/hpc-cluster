# Python Issue #5853
import mimetypes
mimetypes.init()

import logging, sys, bottle

if len(sys.argv) < 2:
    print 'Usage: %s port [debug]' % __file__
    sys.exit()

port = int(sys.argv[1])
debug = 'debug' in sys.argv
quiet = 'quiet' in sys.argv

# Pimp my logging
COLOR_SEQ = "\033[1;%dm"
format="%(asctime)s $BOLD%(levelname)s$RESET [%(name)s] %(filename)s (%(lineno)d) - $C%(message)s$RESET\n"
format = format.replace('$RESET', "\033[0m")
format = format.replace('$BOLD', "\033[1m")
format = format.replace('$WHITE', "\033[1;37m")
format = format.replace('$C', "\033[1;32m")
logging.basicConfig(level=logging.DEBUG if debug else logging.WARNING, stream=sys.stderr, format=format)
logging.getLogger('paste').setLevel(logging.WARNING)
logging.getLogger('wsgi').setLevel(logging.WARNING)

import pybiomaps.server
app = pybiomaps.server.app

if debug:
    from time import time
    timelog = logging.getLogger('pybiomaps.timeit')
    class Debugger(object):
        api=2
        def apply(self, func, route):
            def timeit(*a, **ka):
                ts = time()
                result = func(*a, **ka)
                td = time()-ts
                if td > 0.02:
                    timelog.info('%s %.3fs' % (bottle.request.path, td))
                return result
            return timeit

        def setup(self, app):
            import werkzeug
            app.wsgi = werkzeug.DebuggedApplication(app.wsgi, evalex=True)
            app.catchall = False
    pybiomaps.server.app.install(Debugger())

pybiomaps.server.config(path_db='/tmp/pybiomaps')
pybiomaps.server.config(api_keys=['test'])
pybiomaps.server.config(debug=debug)

# Plugins
pybiomaps.server.load_plugin('pybiomaps.resource.common')
pybiomaps.server.load_plugin('pybiomaps.plugins.seq')
pybiomaps.server.load_plugin('pybiomaps.plugins.newick')
pybiomaps.server.load_plugin('pybiomaps.plugins.level')
pybiomaps.server.load_plugin('pybiomaps.plugins.ruler')

if __name__ == '__main__':
    pybiomaps.server.start(server='wsgiref', host='0.0.0.0', port=port, reloader=debug, quiet=quiet)
