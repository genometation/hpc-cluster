# Reference Genome AACU03
cd /mnt/customer/self/target_AACU03000000
samtools faidx AACU03000000.fasta
jbrowse add-assembly AACU03000000.fasta --out /var/www/html/jbrowse2 --load copy
jbrowse text-index --out /var/www/html/jbrowse2
# PyScipio: First, complete
cd /mnt/customer/self/prediction_AACU03000000
sed -i -E 's/^([^ ]+)[[:space:]].*(PyScipio.*)/\1\t\2/' allGenesFirstPrediction.complete.gff   # Delete Chromosome names
sed -i '/##.*/d' allGenesFirstPrediction.complete.gff  # Delete comments
gt gff3 -sortlines -tidy -retainids allGenesFirstPrediction.complete.gff > PyScipio_allGenesFirstPrediction.sorted.gff
bgzip PyScipio_allGenesFirstPrediction.sorted.gff
tabix PyScipio_allGenesFirstPrediction.sorted.gff.gz
jbrowse add-track PyScipio_allGenesFirstPrediction.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# PyScipio: all
# sed -i -E 's/^(##sequence-region[[:space:]])([^ ]+)[[:space:]].*([0-9]+[[:space:]][0-9]+)/\1\2 \3/' allGenesFirstPrediction.partial.gff # Trim Chromosome names in comments
sed -i -E 's/^([^ ]+)[[:space:]].*(PyScipio.*)/\1\t\2/' allGenesFirstPrediction.partial.gff   # Trim Chromosome names
sed -i '/##.*/d' allGenesFirstPrediction.partial.gff  # Delete comments
gff3sort/gff3sort.pl allGenesFirstPrediction.partial.gff > PyScipio_allGenesFirstPrediction.partial.sorted.gff
bgzip PyScipio_allGenesFirstPrediction.partial.sorted.gff
tabix PyScipio_allGenesFirstPrediction.partial.sorted.gff.gz
jbrowse add-track PyScipio_allGenesFirstPrediction.partial.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# AUGUSTUS
gt gff3 -sortlines -tidy -retainids aug.3rd.autoAug.genome.result.gff > Augustus.Profile.AACU03.sorted.gff
bgzip Augustus.Profile.AACU03.sorted.gff
tabix Augustus.Profile.AACU03.sorted.gff.gz
jbrowse add-track Augustus.Profile.AACU03.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# StringTie
gt gff3 -sortlines -tidy -retainids transcripts.gtf > StringTie.sorted.gff
bgzip StringTie.sorted.gff
tabix StringTie.sorted.gff.gz
jbrowse add-track StringTie.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# StringTie: conf01
gt gff3 -sortlines -tidy -retainids transcripts.conf01.gtf > StringTie.conf01.sorted.gff
bgzip StringTie.conf01.sorted.gff
tabix StringTie.conf01.sorted.gff.gz
jbrowse add-track StringTie.conf01.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# StringTie: conf02 (Augustus reference)
gt gff3 -sortlines -tidy -retainids transcripts.conf02.gtf > StringTie.conf02.sorted.gff
bgzip StringTie.conf02.sorted.gff
tabix StringTie.conf02.sorted.gff.gz
jbrowse add-track StringTie.conf02.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP:
gt gff3 -sortlines -tidy -retainids gmap_transcript_trinity.gff > gmap_transcript_trinity.sorted.gff
bgzip gmap_transcript_trinity.sorted.gff
tabix gmap_transcript_trinity.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_00
gff3sort/gff3sort.pl gmap_transcript_trinity_conf00.gff > gmap_transcript_trinity_conf00.sorted.gff
bgzip gmap_transcript_trinity_conf00.sorted.gff
tabix gmap_transcript_trinity_conf00.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf00.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_00a
gff3sort/gff3sort.pl gmap_transcript_trinity_conf00a.gff > gmap_transcript_trinity_conf00a.sorted.gff
bgzip gmap_transcript_trinity_conf00a.sorted.gff
tabix gmap_transcript_trinity_conf00a.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf00a.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_01
gff3sort/gff3sort.pl gmap_transcript_trinity_conf01.gff > gmap_transcript_trinity_conf01.sorted.gff
bgzip gmap_transcript_trinity_conf01.sorted.gff
tabix gmap_transcript_trinity_conf01.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf01.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_03
gff3sort/gff3sort.pl gmap_transcript_trinity_conf03.gff > gmap_transcript_trinity_conf03.sorted.gff
bgzip gmap_transcript_trinity_conf03.sorted.gff
tabix gmap_transcript_trinity_conf03.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf03.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_04
gff3sort/gff3sort.pl gmap_transcript_trinity_conf04.gff > gmap_transcript_trinity_conf04.sorted.gff
bgzip gmap_transcript_trinity_conf04.sorted.gff
tabix gmap_transcript_trinity_conf04.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf04.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# Trinity/GMAP: conf_05
gff3sort/gff3sort.pl gmap_transcript_trinity_conf05.gff > gmap_transcript_trinity_conf05.sorted.gff
bgzip gmap_transcript_trinity_conf05.sorted.gff
tabix gmap_transcript_trinity_conf05.sorted.gff.gz
jbrowse add-track gmap_transcript_trinity_conf05.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force

# Index tracks for gene name search
jbrowse text-index --out /var/www/html/jbrowse2
