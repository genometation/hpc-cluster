#!/usr/bin/env bash

# Run interactively
docker exec -it jbrowse2_jbrowse_1 /bin/bash
# jBrowse2
# Read: https://jbrowse.org/jb2/docs/superquickstart_web/
cd /mnt/test/jbrowse
## Create indexed FASTA file and load it using the add-assembly command
## Add your genome assembly to the config at /var/www/html/jbrowse2/config.json
## Also copies the files to this directory
samtools faidx Magnaporthe_oryzae.MG8.dna.toplevel.fa
jbrowse add-assembly Magnaporthe_oryzae.MG8.dna.toplevel.fa --out /var/www/html/jbrowse2 --load copy
jbrowse text-index --out /var/www/html/jbrowse2

## load gene annotations from a GFF, using "GenomeTools" (gt) to sort the gff
## and tabix to index the GFF3
# Official reference annotation: Magnaporthe Oryzae
gt gff3 -sortlines -tidy -retainids Magnaporthe_oryzae.MG8.51.gff > Magnaporthe_oryzae.MG8.51.sorted.gff
bgzip Magnaporthe_oryzae.MG8.51.sorted.gff
tabix Magnaporthe_oryzae.MG8.51.sorted.gff.gz
jbrowse add-track Magnaporthe_oryzae.MG8.51.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# AUGUSTUS
gt gff3 -sortlines -tidy -retainids augustus_magnapg_test.gff3 > Augustus.Profile.Magnaporthe_grisea.sorted.gff
bgzip Augustus.Profile.Magnaporthe_grisea.sorted.gff
tabix Augustus.Profile.Magnaporthe_grisea.sorted.gff.gz
jbrowse add-track Augustus.Profile.Magnaporthe_grisea.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# AUGUSTUS
gt gff3 -sortlines -tidy -retainids augustus_pyscipio_test.gff3 > Augustus.Profile.PyScipio.train.sorted.gff
bgzip Augustus.Profile.PyScipio.train.sorted.gff
tabix Augustus.Profile.PyScipio.train.sorted.gff.gz
jbrowse add-track Augustus.Profile.PyScipio.train.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# PyScipio annotation (first predictions)
gt gff3 -sortlines -tidy -retainids PyScipio_allGenesFirstPrediction.gff > PyScipio_allGenesFirstPrediction.sorted.gff
bgzip PyScipio_allGenesFirstPrediction.sorted.gff
tabix PyScipio_allGenesFirstPrediction.sorted.gff.gz
jbrowse add-track PyScipio_allGenesFirstPrediction.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# PyScipio annotation (first predictions)
sed -i -E 's/^([0-9]+).*dna.*REF(.*)/\1\2/' allGenesFirstPrediction.complete.gff  # Delete
sed -i '/##.*/d' allGenesFirstPrediction.complete.gff
gt gff3 -sortlines -tidy -retainids allGenesFirstPrediction.complete.gff > allGenesFirstPrediction.complete.sorted.gff
bgzip allGenesFirstPrediction.complete.sorted.gff
tabix allGenesFirstPrediction.complete.sorted.gff.gz
jbrowse add-track allGenesFirstPrediction.complete.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# PyScipio annotation (first predictions)
sed -i -E 's/^([0-9]+).*dna.*REF(.*)/\1\2/' allGenesFirstPrediction.partial.gff  # Delete Chromosome names
sed -i '/##.*/d' allGenesFirstPrediction.partial.gff  # Delete comments
gt gff3 -sortlines -tidy -retainids allGenesFirstPrediction.partial.gff > allGenesFirstPrediction.partial.sorted.gff
bgzip allGenesFirstPrediction.partial.sorted.gff
tabix allGenesFirstPrediction.partial.sorted.gff.gz
jbrowse add-track allGenesFirstPrediction.partial.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy

# PyScipio AACU03
sed -i -E 's/^([^ ]+)[[:space:]].*(PyScipio.*)/\1\t\2/' allGenesFirstPrediction.complete.gff   # Delete Chromosome names
sed -i '/##.*/d' allGenesFirstPrediction.complete.gff  # Delete comments
gt gff3 -sortlines -tidy -retainids allGenesFirstPrediction.complete.gff > PyScipio_allGenesFirstPrediction.sorted.gff
bgzip allGenesFirstPrediction.complete.sorted.gff
tabix allGenesFirstPrediction.complete.sorted.gff.gz
jbrowse add-track allGenesFirstPrediction.complete.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy --force
# StringTie
gt gff3 -sortlines -tidy -retainids transcripts.gtf > StringTie.sorted.gff
bgzip StringTie.sorted.gff
tabix StringTie.sorted.gff.gz
jbrowse add-track StringTie.sorted.gff.gz --out /var/www/html/jbrowse2 --load copy
# Index tracks for gene name search
jbrowse text-index --out /var/www/html/jbrowse2
# Open in Browse http://mcpmendel:5000/jbrowse2
