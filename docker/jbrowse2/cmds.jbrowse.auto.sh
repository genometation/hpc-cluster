#!/usr/bin/env bash
#
# NOTE:
# working_dir = prediction_XXX

accession=$(basename $(pwd) | sed -re "s/prediction_//")
genomefile="../target_${accession}/${accession}.min.fasta"
# Load Reference sequence track
samtools faidx ${genomefile}
jbrowse add-assembly ${genomefile} --name ${accession}.min --out /var/www/html/jbrowse2 --load copy --force
# # Reduce genes to intron with max. length 10k
# for f in jbrowse/*.gff; do
#   gffread -g ${genomefile} -i 10000 -o jbrowse_10k/10k_$(basename ${f}) -FO --keep-genes --force-exons --keep-exon-attrs --merge --sort-alpha ${f};
# done
# # Clean up (jbrowse files)
# rm -f jbrowse_10k/*.gff.*
# Load tracks for single GFFs
for f in jbrowse/*.gff; do
  rm ${f}.*
  [ ! -f ${f}.sorted.gff ] && gff3sort.pl ${f} > ${f}.sorted.gff
  [ ! -f ${f}.sorted.gff.gz ] && bgzip ${f}.sorted.gff
  [ ! -f ${f}.sorted.gff.gz.tbi ] && tabix ${f}.sorted.gff.gz
  mv ${f}.sorted.gff.gz ${f}.gz && mv ${f}.sorted.gff.gz.tbi ${f}.gz.tbi
  jbrowse add-track ${f}.gz --assemblyNames ${accession}.min --out /var/www/html/jbrowse2 --load copy --force
done
# Load BAM file (RNAseq / STAR)
# f="/mnt/customer/KWS/pilot/prediction_KWS/star/SRR13213749/Aligned.sortedByCoord.out.bam"
samtools index ${f}
jbrowse add-track ${f} --assemblyNames ${accession}.min --out /var/www/html/jbrowse2/ --load copy --force
# Index tracks for gene name search
jbrowse text-index --out /var/www/html/jbrowse2
