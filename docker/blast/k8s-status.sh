#!/usr/bin/env bash

# Request pods information
# kubectl get pods --namespace=default
# Get detailed pod information with node-assignment
kubectl get pods -o wide --namespace=default

# Request job status (JobGroup)
kubectl get jobs -l jobgroup=blast-jobgroup
# kubectl describe job blast-job-00
# kubectl describe job trnascan-job-01
# ...
# kubectl describe job trnascan-job-15

# View logs
kubectl logs job/blast-job-00 | tail
kubectl logs job/blast-job-01 | tail
kubectl logs job/blast-job-02 | tail
kubectl logs job/blast-job-03 | tail
kubectl logs job/blast-job-04 | tail
kubectl logs job/blast-job-05 | tail
kubectl logs job/blast-job-06 | tail
kubectl logs job/blast-job-07 | tail

# Request node information
node='mcpdarwin'
# kubectl describe node ${node}
# ssh ubuntu@${node} kubectl cluster-info
# Node: Check ressource utilization (cpus, memory, load, threads, threads:running)
ssh ubuntu@${node} -t htop
