#!/usr/bin/env bash

# Prepare data files with FASTA-paths
# Source file: 'blastp-calls.txt'

num_jobs=8
# Generate K8s-jobs-YAML for 8 jobs
# ... running BLAST calls consecutive using 124 threads each
./k8s_render_blast_jobs.py.sh ${num_jobs} blastp-calls.txt < k8s_blast_job.tmpl.yaml.jinja2 > k8s_blast_jobs.yaml

# Create jobs in K8s
# microk8s.kubectl delete job -l jobgroup=blast-jobgroup
microk8s.kubectl create -f k8s_blast_jobs.yaml

# Request JobGroup status
microk8s.kubectl get jobs -l jobgroup=blast-jobgroup
