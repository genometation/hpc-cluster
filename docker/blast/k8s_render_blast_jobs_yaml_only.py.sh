#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Kubernetes Jobs creator (YAMLs)

K8s-YAML-creator takes (BLAST)-commands (as they are) from passed jobs file and 
divides them equally in $num_jobs of batches. The batches of (BLAST)-commands will
then be directly written into the jobs of the final K8s-YAML.

The generated YAML does not need any additional files (except for the data files
needed for the command to run).

Warning:
If $num_jobs is set to small, then too many commands get written into one K8s-job,
which will results in an K8s execution ERROR ('argument list is too long').

Params:
  argv[1]: Number of jobs
  argv[2]: Name of file with BLAST jobs
  stdin:   Stream of Jinja2-YAML-template

Call script:
  ./k8s_render_blast_jobs_yaml_only.py.sh 8 blastp-calls.txt < k8s_blast_job_yaml_only.tmpl.yaml.jinja2

"""

from itertools import accumulate
from jinja2 import Template
import sys
import os

# Get CLI args
num_jobs = int(sys.argv[1]) if sys.argv[1] else sys.exit(1)
job_file = sys.argv[2]

# Generate K8s-YAML, if job-file exists
if job_file and os.path.exists(job_file):
    with open(job_file, newline='') as fp:
        blast_calls = [l.strip() for l in fp.readlines()]

    # Calculate chunks
    num_calls = len(blast_calls)
    chunk_size = num_calls // num_jobs
    chunks = [chunk_size] * (num_jobs - 1) + [chunk_size + (num_calls % num_jobs)]
    # Divide jobs into batches
    batch = []; job_batches = []
    for i, call in enumerate(blast_calls):
        batch.append(call)
        if i+1 in list(accumulate(chunks)):
            job_batches.append(batch)
            batch = []

    # # Control
    # print('Total num. of BLAST calls: %d' % num_calls)
    # print('Num. of BLAST jobs: %d' % num_jobs)
    # print('Batch-Size per Job: %d' % chunk_size)
    # print('Batches: %r' % list(accumulate(chunks)))

    # Render Jinja2-YAML from STDIN
    print(Template(sys.stdin.read()).render(
        num_jobs=num_jobs,
        blast_calls=job_batches)
    )
