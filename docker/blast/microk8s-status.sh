#!/usr/bin/env bash

# Request pods information
# microk8s.kubectl get pods --namespace=default
# Get detailed pod information with node-assignment
microk8s.kubectl get pods -o wide --namespace=default

# Request job status (JobGroup)
microk8s.kubectl get jobs -l jobgroup=blast-jobgroup
# microk8s.kubectl describe job blast-job-00
# microk8s.kubectl describe job trnascan-job-01
# ...
# microk8s.kubectl describe job trnascan-job-15

# View logs
microk8s.kubectl logs job/blast-job-00 | tail
microk8s.kubectl logs job/blast-job-01 | tail
microk8s.kubectl logs job/blast-job-02 | tail
microk8s.kubectl logs job/blast-job-03 | tail
microk8s.kubectl logs job/blast-job-04 | tail
microk8s.kubectl logs job/blast-job-05 | tail
microk8s.kubectl logs job/blast-job-06 | tail
microk8s.kubectl logs job/blast-job-07 | tail

# Request node information
node='mcpdarwin'
# microk8s.kubectl describe node ${node}
# ssh ubuntu@${node} microk8s.kubectl cluster-info
# Node: Check ressource utilization (cpus, memory, load, threads, threads:running)
ssh ubuntu@${node} -t htop
