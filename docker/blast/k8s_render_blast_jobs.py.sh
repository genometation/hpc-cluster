#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Kubernetes Jobs creator (YAMLs)

K8s-YAML-creator takes (BLAST)-commands (as they are) from passed jobs file and
divides them equally in $num_jobs of batches. The batches of (BLAST)-commands will
then be written into single BASH scripts, that are called in jobs of the K8s-YAML.

Note:
The generated YAML needs additional source files (BASH batch-scripts), that have
to be stored in the YAML referenced location.

Params:
  argv[1]: Number of jobs
  argv[2]: Name of file with BLAST jobs
  stdin:   Stream of Jinja2-YAML-template

Call script:
  ./k8s_render_blast_jobs.py.sh 8 blastp-calls.txt < k8s_blast_job.tmpl.yaml.jinja2

"""

from itertools import accumulate
from jinja2 import Template
import sys
import os

# Get CLI args
num_jobs = int(sys.argv[1]) if sys.argv[1] else sys.exit(1)
job_file = sys.argv[2]
fn_wo_ext = str(os.path.splitext(job_file)[0])

# Generate K8s-YAML, if job-file exists
if job_file and os.path.exists(job_file):
    with open(job_file, newline='') as fp:
        blast_calls = [l.strip() for l in fp.readlines()]

    # Calculate chunks
    num_calls = len(blast_calls)
    chunk_size = num_calls // num_jobs
    chunks = [chunk_size] * (num_jobs - 1) + [chunk_size + (num_calls % num_jobs)]
    # Divide jobs into batches
    batch = []
    job_batches = []
    for i, call in enumerate(blast_calls):
        batch.append(call)
        if i+1 in list(accumulate(chunks)):
            job_batches.append(batch)
            batch = []

    # Write batches into files
    job_batch_filenames = []
    for i in range(num_jobs):
        batch_fn = fn_wo_ext + "_c%d_%02d.sh" % (num_jobs, i)
        job_batch_filenames.append(batch_fn)
        with open(batch_fn, 'w') as f:
            # Script data
            f.write("#!/usr/bin/env bash\n\n")
            for j, job in enumerate(job_batches[i]):
                f.write("echo 'Running job: %d/%d'\n" % (j+1, len(job_batches[i])))
                f.write(job + "\n")
            f.write("\n")

    # # Control
    # print('Total num. of BLAST calls: %d' % num_calls)
    # print('Num. of BLAST jobs: %d' % num_jobs)
    # print('Batch-Size per Job: %d' % chunk_size)
    # print('Batches: %r' % list(accumulate(chunks)))

    # Render Jinja2-YAML from STDIN
    print(Template(sys.stdin.read()).render(
        num_jobs=num_jobs,
        blast_jobs=job_batch_filenames)
    )
