#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Project: pyscipio3 
Author:  Dominic Simm <dominic.simm@cs.uni-goettingen.de>
Date:    2021-07-29

usage:  splitfile.py file size delim
file:   file containing information
chunksize: Available options
        - real filesize abbreviated by: nK = n*10^3, nM = n*10^6, nG = ...
        - number of entries: n
delimiter: Delimiting character or string at beginning of line e.g.
        - FASTA: '>'
        - YAML: '---'
output: files filename_NNNN.ext
        everytime size or number of entries is exceeded, a new file
        is opened for the coming sequences
"""

import re, os, sys


def write_to_file(chunk, filename, file_ext, filecnt):
    """ Helper method """
    with open("%s_%04d%s" % (filename, filecnt, file_ext), "w+") as fw:
        fw.write(chunk)
        print("Writing to %s_%04d%s...%d characters" % (filename, filecnt, file_ext, len(chunk)))

# CLI arguments
if len(sys.argv) != 4:
    print("Usage: splitfile.py file chunksize delimiter")
    exit(-1)
[splitfile, chunksize, delimiter] = sys.argv[1:4]

# File vars
filename, file_ext = os.path.splitext(splitfile)
file_cnt, entry_cnt, chunk = 0, 0, ''
# Size vars
sizes = {'K': 10**3, 'M': 10**6, 'G': 10**9}
realsize = sizes.get(chunksize[-1].upper())
if not realsize:
    # Mode: Split by 'Number of entries'
    maxsize = int(chunksize)
else:
    # Mode: Split by 'Real filesize'
    num = chunksize[0:-1]
    maxsize = int(num) * realsize

# Split file
with open(splitfile, 'r') as readfile:
    for line in readfile:
        if line.startswith(delimiter):
            if not realsize and entry_cnt >= maxsize or realsize and len(chunk) > maxsize:
                # Store chunk information
                write_to_file(chunk, filename, file_ext, file_cnt)
                chunk, entry_cnt = '', 0
                file_cnt += 1
            entry_cnt += 1
        chunk += line
    # Store last chunk
    write_to_file(chunk, filename, file_ext, file_cnt)
