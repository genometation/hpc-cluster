#!/usr/bin/env bash

# Build the image
docker build -t job-trnascan-wq .
# Tag and push it to registry
docker tag job-trnascan-wq localhost:32000/job-trnascan-wq
docker push localhost:32000/job-trnascan-wq

# Test image interactively
# kubectl run -i --tty trnascan-wq --image=localhost:32000/job-trnascan-wq -- /bin/bash
