#!/usr/bin/env bash

# Test: Genomefiles
# src='/mnt/genomes/9bc93e4c-4dbd-4c39-ad95-28bac37e0bca/1e447e7a-1ac6-476c-bfbe-7f65cae22635/1e447e7a-1ac6-476c-bfbe-7f65cae22635.fasta'
# Magnaporthe Oryzae AACU
# src='/mnt/genomes/cc5af340-05f9-49e6-b134-ed2cffae1867/e513abf8-5984-4b4d-8031-df2f1176351d/e513abf8-5984-4b4d-8031-df2f1176351d.fasta'

# Get params
src=${1:-""}
if [ -z "${src}" ]; then
  echo "Missing genomefile parameter."
  exit 1
fi
# Set vars
gdir=$(dirname "${src}")
gfile=$(basename "${src}")

# Skip, if tRNA for genomefile processed
[ -d ${gdir}/trna ] && [ -s ${gdir}/trna/${gfile}.output ] && exit 0

# Get data
printf "\n$ cp ${src} ."
time cp ${src} .

# Split genomefile in chunks, if exceeds the size of 100Mb
time python3 splitfile.py ${gfile} 100M ">"
# Run over chunk files
bf=$(basename "${gfile}" | cut -d. -f1)
ext=$(basename "${gfile}" | cut -d. -f2)
for cfile in ${bf}_*.${ext};
do
  # tRNAScanSE
  printf "\n$ time tRNAscan-SE --forceow --len 400 --detail -H -o ${cfile}.output -f ${cfile}.struct -s ${cfile}.isospecific -m ...\n"
  time tRNAscan-SE --forceow --len 400 --detail -H -o ${cfile}.output -f ${cfile}.struct -s ${cfile}.isospecific -m ${cfile}.stats -l ${cfile}.log ${cfile}
done
# Join results of chunk files
head -3 ${bf}_0000.${ext}.output > ${gfile}.output
# tail -n +4 ${bf}_*.output >> ${gfile}.output
for f in ${bf}_*.output; do tail -n +4 "${f}" >> ${gfile}.output; done
cat *.struct >> ${gfile}.struct
head -1 ${bf}_0000.${ext}.isospecific > ${gfile}.isospecific
# tail -n +2 ${bf}_*.isospecific >> ${gfile}.isospecific
for f in ${bf}_*.isospecific; do tail -n +2 "${f}" >> ${gfile}.isospecific; done
cat *.stats >> ${gfile}.stats
cat *.log >> ${gfile}.log

# Copy back results
printf "\n Copy back results.\n"
mkdir -p ${gdir}/trna
cp ${gfile}.* ${gdir}/trna
# Cleanup all genome associated files
rm ${bf}*

# Return code
exit 0
