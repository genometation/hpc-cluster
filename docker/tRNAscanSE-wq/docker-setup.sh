#!/usr/bin/env bash

# # Cache Dfam source for Docker installation
# cp /mnt/databases/dfam/3.3/Dfam.h5 .

# Build docker image of project with composer-preferences
# docker-compose build --no-cache
docker-compose build
# Start image as container via composer
docker-compose up -d
# docker-compose up -d --force-recreate

# Login into running Docker-Container
#docker exec -it trnascanwq_trnascanwq_1 /bin/bash
