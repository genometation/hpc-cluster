#!/usr/bin/env bash

# Requirements:
# - Redis service is running
# # Set up Redis service
# cd kubernetes/services/rediswq
# kubectl apply -f redis-pod.yaml
# kubectl apply -f redis-service.yaml

# Start a temporary interactive pod for running the Redis CLI
kubectl run -i --tty redis-client --image=redis -- /bin/bash
kubectl cp ./genomefiles_trnascan_queue_undone.txt redis-client:/data
kubectl attach redis-client -c redis-client -i -t
# => Manually fill up queue with jobs
# redis-cli -h redis
# redis:6379> rpush trnascanwq "/mnt/genomes/cf1e7fb0-3e82-4b1b-a0b4-9d67fad7a1c8/e6fdfeaa-8d94-4058-a910-9836a4859107/e6fdfeaa-8d94-4058-a910-9836a4859107.fasta"
# redis:6379> rpush trnascanwq "/mnt/genomes/37491fca-1e03-4fbc-8213-2ad16d1f4a41/7faef575-162f-4914-a144-8897ecf4443e/7faef575-162f-4914-a144-8897ecf4443e.fasta"
# redis:6379> rpush trnascanwq "/mnt/genomes/92b270f7-ecc7-4e44-aef1-e5e6d96725db/f3d03524-68b6-4f3f-b198-29be88c8c2f6/f3d03524-68b6-4f3f-b198-29be88c8c2f6.fasta"
# redis:6379> lrange trnascanwq 0 -1
# redis:6379> DEL trnascanwq  # Delete list trnascanwq
# redis:6379> FLUSHDB  # Erase all entries from current DB
# Add all files to working queue (genomefiles_queue_all.txt)
# while read line
# do
#     echo rpush trnascanwq "${line}" | redis-cli -h redis
# done < genomefiles_queue_all.txt
root@temp:/data# cat genomefiles_trnascan_queue_undone.txt | awk '{printf "RPUSH trnascanwq %s \n", $1}' | redis-cli -h redis > import.log
root@temp:/data# echo "lrange trnascanwq 0 -1" | redis-cli -h redis | wc -l

# Run directly from kubectl
kubectl exec redis-client -- bash -c "echo 'DEL trnascanwq' | redis-cli -h redis | wc -l"
kubectl cp ./genomefiles_trnascan_queue_undone.txt temp:/data
kubectl exec redis-client -- ls .
kubectl exec redis-client -- bash -c "cat genomefiles_trnascan_queue_undone.txt | awk '{printf \"RPUSH trnascanwq %s \n\", \$1}' | redis-cli -h redis > import.log"
kubectl exec redis-client -- bash -c "echo 'lrange trnascanwq 0 -1' | redis-cli -h redis | wc -l"
kubectl exec redis-client -- bash -c "echo 'lrange trnascanwq 0 -1' | redis-cli -h redis"
kubectl exec redis-client -- cat import.log

# Build the image
docker build -t job-trnascan-wq .
# Tag and push it to registry
docker tag job-trnascan-wq localhost:32000/job-trnascan-wq
docker push localhost:32000/job-trnascan-wq
# Test image interactively
# kubectl run -i --tty wmtemp --image=localhost:32000/job-trnascan-wq -- /bin/bash

# Run the job
kubectl apply -f ./job.yaml
kubectl describe jobs.batch job-trnascan-wq
kubectl get pods
kubectl logs jobs/job-trnascan-wq
# kubectl logs job-trnascan-wq-...
kubectl exec job-trnascan-wq-bpt47 -i -t -- /bin/bash
# Delete the job
kubectl delete jobs.batch job-trnascan-wq

# Control created tRNAScanSE .output and .struct files
# Newly modified files since YYYY-MM-DD
find -L /mnt/genomes -type f -name "*.log" -exec [[ tail -n1 {} == End time* ]] \;
find -L /mnt/genomes -type f -name "*.log" -exec grep -H "End time" {} \;
# grep --include=\*.{log} -rnw '/mnt/genomes/' -e "End time"
find -L /mnt/genomes -type f -name "*.output" -newermt 2021-09-07
# Number of files
find -L /mnt/genomes -type f -name "*.output" | wc -l
# Number of lines per file
find -L /mnt/genomes -type f -name "*.output" -exec wc -l {} \;
# Sum numbers of lines per file
find -L /mnt/genomes -type f -name "*.output" -exec wc -l {} \; | cut -d" " -f1 | paste -sd+ | bc
find -L /mnt/genomes -type f -name "*.struct" | wc -l
