#!/usr/bin/env bash

# Build docker image of project with composer-preferences
# docker-compose build --no-cache
docker-compose build
# Start image as container via composer
docker-compose up -d
# docker-compose up -d --force-recreate

# Login into running Docker-Container
#docker exec -it augustus_augustus_1 /bin/bash

# Generate and update Docker image in K8s registry
docker build . -f Dockerfile -t localhost:32000/augustus:latest
docker push localhost:32000/augustus:latest
curl localhost:32000/v2/augustus/tags/list
