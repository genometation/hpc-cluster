#!/usr/bin/env bash

# Build the Docker image
docker build -t augustus .
# Tag and push it to registry
docker tag augustus localhost:32000/augustus:latest
docker push localhost:32000/augustus:latest
# Check image registry state
curl localhost:32000/v2/augustus/tags/list

# Test image interactively
# kubectl run -i --tty augustustemp --image=localhost:32000/augustus -- /bin/bash
