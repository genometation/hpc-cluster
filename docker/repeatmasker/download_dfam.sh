#!/bin/bash

# Download DFAM sources
DFAM_VERSION='3.3'

declare -a files=("Dfam.embl.gz"
                  "Dfam.h5.gz"
                  "Dfam.hmm.gz"
                  "Dfam_curatedonly.embl.gz"
                  "Dfam_curatedonly.h5.gz"
                  "Dfam_curatedonly.hmm.gz "
                 )

# Get node stats
for file in "${files[@]}"
do
  wget -O ${DFAM_VERSION}/${file} https://www.dfam.org/releases/Dfam_${DFAM_VERSION}/families/${file}
  # gunzip -k ${DFAM_VERSION}/${file}
done
