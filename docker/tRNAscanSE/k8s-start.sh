#!/usr/bin/env bash

# Prepare data files with FASTA-paths
# Source file: 'genomefiles_trnascan_queue.txt'

num_jobs=20
# Generate chunk files for 16 Jobs (genomefiles_trnascan_queue_c{$chunks}_*.txt)
./generate_chunks.sh ${num_jobs}
# Copy chunk files to folder /mnt/genomes
# mv jobs/genomefiles_trnascan_queue_c*.txt /mnt/genomes

# TODO: Workaround as dsimm-a (Permission denied)
mv jobs/genomefiles_trnascan_queue_c*.txt /nfs/zfs_adenin
# Copy to change ownership
sudo -i -u genwork
cp /nfs/zfs_adenin/genomefiles_trnascan_queue_c*.txt /nfs/zfs_adenin/genomes
rm -f /nfs/zfs_adenin/genomefiles_trnascan_queue_c*.txt
exit

# Generate K8s-jobs-YAML for 16 Jobs, each with 64 parallel threads
./k8s_render_trnascan_jobs.py.sh ${num_jobs} < k8s_trnascan_job.tmpl.yaml.jinja2 > k8s_trnascan_jobs.yaml

# Create jobs in K8s
# microk8s.kubectl delete job -l jobgroup=trnascan-jobgroup
microk8s.kubectl create -f k8s_trnascan_jobs.yaml

# Request JobGroup status
microk8s.kubectl get jobs -l jobgroup=trnascan-jobgroup
