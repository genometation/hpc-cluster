#!/usr/bin/env bash

# Split file='genomefiles_trnascan_queue.txt' into chunks-times files
file='genomefiles_trnascan_queue.txt'
# Get chunks from CLI parameter 1 (default: '8')
chunks=${1:-8}
num_lines=$(grep -c -E '\n' $file)
chunk_lines=$((num_lines / chunks))
# csplit file.txt <num lines> "{repetitions}"
csplit -f "genomefiles_trnascan_queue_c${chunks}_" -b "%0${#chunks}d.txt" -k -s -z $file $chunk_lines "{$((chunks-1))}"
# Append remaining lines to last chunk-file (chunks-1)
cat "genomefiles_trnascan_queue_c${chunks}_${chunks}.txt" >> "genomefiles_trnascan_queue_c${chunks}_$((chunks-1)).txt"
rm "genomefiles_trnascan_queue_c${chunks}_${chunks}.txt"
# Move files to jobs folder
mv genomefiles_trnascan_queue_c"${chunks}"_* jobs
