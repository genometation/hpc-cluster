#!/usr/bin/env bash

# Request pods information
# microk8s.kubectl get pods --namespace=default
# Get detailed pod information with node-assignment
microk8s.kubectl get pods -o wide --namespace=default
# microk8s.kubectl describe pod trnascan-job-01-tbb6t

# Request job status (JobGroup)
microk8s.kubectl get jobs -l jobgroup=trnascan-jobgroup
# microk8s.kubectl describe job trnascan-job-00
# microk8s.kubectl describe job trnascan-job-01
# ...
# microk8s.kubectl describe job trnascan-job-15

# View logs
microk8s.kubectl logs job/trnascan-job-00
# microk8s.kubectl logs job/trnascan-job-01
# microk8s.kubectl logs job/trnascan-job-02
# microk8s.kubectl logs job/trnascan-job-03

# Request node information
node='nodemet'
# microk8s.kubectl describe node ${node}
# ssh ubuntu@${node} microk8s.kubectl cluster-info
# Node: Check ressource utilization (cpus, memory, load, threads, threads:running)
ssh ubuntu@${node} -t htop
