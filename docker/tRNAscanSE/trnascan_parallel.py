#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Module `Parallel tRNAscan`: Run tRNAscan jobs distributed and in parallel by
multiple threads in ThreadPool. """

import csv
import glob
from multiprocessing.pool import ThreadPool
from multiprocessing import cpu_count
from optparse import OptionParser, OptionGroup
import os
from pathlib import Path
from subprocess import Popen
import shutil
import string
import sys
# # Use /dev/null for subprocesses
# try:
#     from subprocess import DEVNULL  # Python 3
# except ImportError:
#     DEVNULL = open(os.devnull, 'wb')


def file_get_lines(fname):
    """ Helper method: Read content from file

    Parameters:
        fname (str): Path to file

    Returns:
        list
    """
    data = []
    with open(fname, newline='') as csvfile:
        reader = list(csv.reader(csvfile, delimiter=' ', quotechar='|'))
        for row in reader:
            data.append(''.join(row))
    return data


def readlastline(f):
    """ Helper method: Read last line from file

    Parameters:
        fname (str): Path to file

    Returns:
        str
    """
    try:
        f.seek(-2, 2)                    # Jump to the second last byte.
        while f.read(1) != b"\n":        # Until EOL is found ...
            f.seek(-2, 1)                # ... jump back, over the read byte plus one more.
        return f.read().decode('ascii')  # Read all data from this point on; Convert binary to str
    except Exception as e:
        return ""


def makedirs(path):
    """ Helper method: Create directories for path

    Parameters:
        path (str): Path for directories
    """
    if not os.path.exists(path):
        os.makedirs(path)


def minmax(value, low, high):
    """ `Module function`: Take value or extremes of range(min, max), if value exceeds

    Case 01: value < min < max
             value: -100; min: 0: max: 150.
             => return  0 (min)
    Case 02: min < value < max
             value:  100; min: 0: max: 150
             => return 100 (value)
    Case 03: min < max < value
             value:  250; min: 0: max: 150
             => return 150 (max)

    Args:
        value (int):
        low (int):
        high (int):

    Returns:
        int:
    """

    assert low <= high
    return min(max(low, value), high)


def init_log(opt):
    import logging

    logging.basicConfig(
        format='\033[1m%(levelname) -8s\033[0m [+%(relativeCreated)dms] <%(name)s> %(filename)s:%(lineno)d in %(funcName)s()\n%(message)s\n'
    )
    root_logger = logging.getLogger(__name__)

    level = logging.ERROR - opt.verbose * 10
    level = minmax(level, low=logging.DEBUG, high=logging.ERROR)
    root_logger.setLevel(level)
    return root_logger


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


# Global variables
finished_jobs = 0


def schedule_pool(options):
    """ Manage and start tRNAscan jobs as concurrent subprocesses as threads in pool.
        ThreadPool determines automatically number of CPUs and distributes jobs equally.
    """

    result_list = []

    def log_result(result):
        # This is called whenever work(job_id, cmd) returns a result.
        # result_list is modified only by the main process, not the pool workers.
        job_id = result['job_id']
        options = result['options']
        result_list.append(job_id)

        # Update Progress Bar
        global finished_jobs
        finished_jobs += 1
        printProgressBar(finished_jobs, options.jobs_max,
                         prefix='Progress:', suffix='Complete', length=50)

    def work(job_id, cmd, options):
        makedirs(job_id)
        subprocess_worker = Popen(
            cmd,
            stdout=open(".stdout", 'w+'),
            stderr=open(".err.log", 'w+')
        )
        subprocess_worker.wait()
        return {'job_id': job_id, 'options': options}

    # Prepare jobs
    f_list = file_get_lines(options.filelist)
    # file = 'genomes/genomes_jgi/Zymoseptoria_tritici_IPO323_v2_chromosome.fasta'
    # tRNAscan-SE --forceow --len 400 --detail -H -o $file.trna.o.log -f $file.trna.f.log -s $file.trna.s.log -l $file.trna.l.log $file
    jobs_dict = dict()
    for genomefile in f_list:
        trna_base = os.path.join(os.path.dirname(genomefile), 'trna')
        trna_file = str(os.path.join(trna_base, os.path.basename(genomefile)))
        trna_log = trna_file + '.log'
        job = {
            trna_base:
            ['tRNAscan-SE',
             '--forceow',
             '--len 400',
             '--detail',
             '-H',
             '-o %s.output' % trna_file,
             '-f %s.struct' % trna_file,
             '-s %s.isospecific' % trna_file,
             '-m %s.stats' % trna_file,
             '-l %s.log' % trna_file,
             '%s' % genomefile]
        }
        if os.path.exists(trna_log):
            with open(trna_log, 'rb') as file:
                # Read last line of log file
                log_lastline = readlastline(file)
                if not log_lastline.startswith('End Time'):
                    # Restart unfinished tRNAscan jobs
                    jobs_dict.update(job)
        else:
            # Collect unprocessed tRNAscan jobs
            jobs_dict.update(job)

    # Set to the number of workers (None defaults to cpu count of machine)
    # num = None
    # Set number of worker threads to use
    num_threads = options.procs if 2 < options.procs <= cpu_count() else cpu_count()
    reserve_for_os = 0
    num = num_threads - reserve_for_os
    tpool = ThreadPool(num)
    log.info(
        "- Number of available threads: %d (CPUs: %d)",
        cpu_count(),
        cpu_count() / 2)
    log.info("- Number of concurrent workers (threads): %d", num)

    # Configure progressBar
    options.jobs_max = len(jobs_dict)

    # Schedule jobs as threads
    log.info("Starting tRNAscan jobs: %d" % options.jobs_max)
    jobs_submitted = False
    for job_id, cmd in jobs_dict.items():
        if not jobs_submitted:
            print("Example job:")
            print("Start job %s" % job_id)
            print("%s\n" % " ".join(cmd))
            # print("%s\n" % cmd)
            # Initial call to print 0% progress
            printProgressBar(0, options.jobs_max, prefix='Progress:', suffix='Complete', length=50)
        tpool.apply_async(work, args=(job_id, cmd, options), callback=log_result)
        jobs_submitted = True

    # Cleanup
    tpool.close()
    tpool.join()


def main(args):
    """ ``Main method:`` Manage and start tRNAscan runs ... """

    # Parse arguments
    parser = make_parser()
    (options, args) = parser.parse_args(args)
    # Check input
    if len(args) == 2:
        options.filelist = args
    if not options.filelist:
        parser.error("Missing file listing")
    # Initiate Logger
    global log
    log = init_log(options)

    # Output passed arguments
    log.info("List of genome files: %s", options.filelist)

    # Parallel processing
    schedule_pool(options)


def make_parser():
    parser = OptionParser(usage="usage: %prog [options] filelist",
                          version="%prog 0.1")
    parser.set_defaults(filelist=None)
    opt = parser.add_option
    opt('-v', '--verbose', action='count', default=0,
        help='show verbose information (including progress)')
    opt('--procs', metavar='n', type="int", default=16,
        help='number of processes to run in parallel, defaults to "16"')

    group = OptionGroup(parser, "Data source")
    opt = group.add_option
    opt('-f', '--filelist', metavar='file',
        help='List of Genome files (absolute paths, CSV compatible)')
    parser.add_option_group(group)

    return parser


if __name__ == '__main__':
    main(sys.argv[1:])
