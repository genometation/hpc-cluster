#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Kubernetes Jobs creator (YAMLs)

Call script:
  ./k8s_render_trnascan_jobs.py.sh 20 < k8s_trnascan_job.tmpl.yaml.jinja2

"""

from jinja2 import Template
import sys

# Set number of K8s-jobs
num_jobs = int(sys.argv[1]) if sys.argv[1] else 4
# Render Jinja2-YAML from STDIN
print(Template(sys.stdin.read()).render(num_jobs=num_jobs))
