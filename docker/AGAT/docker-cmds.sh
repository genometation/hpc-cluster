#!/usr/bin/env bash

# Read: https://github.com/NBISweden/AGAT

# Download and test Docker container 'AGAT'
# get the chosen AGAT container version
docker pull quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0
# use an AGAT's tool e.g. agat_convert_sp_gxf2gxf.pl
docker run quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0 agat_convert_sp_gxf2gxf.pl --help

# Mount ZFS test directory (with write permissions) into Docker container
docker run quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0 agat_sp_merge_annotations.pl --help
docker run quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0 agat_sp_merge_annotations.pl --gff allGenesFirstPrediction.complete.gff --gff augustus_pyscipio_test.gff3 --out AGAT_merge_test.gff
docker run -it -v /mnt/test:/mnt/test quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0 agat_sp_merge_annotations.pl --gff /mnt/test/STAR/allGenesFirstPrediction.complete.gff --gff /mnt/test/STAR/augustus_pyscipio_test.gff3 --out /mnt/test/STAR/AGAT/AGAT_merge_test.gff
# Keep AGAT container running
# docker exec -it quay.io/biocontainers/agat:0.8.0--pl5262hdfd78af_0 /bin/bash
