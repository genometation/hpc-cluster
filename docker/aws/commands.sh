#!/usr/bin/env bash

# Read:
# https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-docker.html
# https://docs.aws.amazon.com/AmazonS3/latest/userguide/setup-aws-cli.html

# AWS CLI Access (No AWS account required)
docker run --rm -it amazon/aws-cli s3 ls s3://sra-pub-src-2/ --no-sign-request
