#!/usr/bin/env python

import time
from subprocess import Popen
import os
import rediswq


host = "redis"
# Uncomment next two lines if you do not have Kube-DNS working.
# import os
# host = os.getenv("REDIS_SERVICE_HOST")
work_queue = "windowmasker"


def run_windowmasker(genomefile_path):
    """ Run WindowMasker shell script """
    # Step 1: Copy data from NFS to container
    # Step 2: Run WindowMasker tasks
    # Step 3: Copy result files back
    # Step 4: Cleanup
    print("Run WindowMasker shell script")
    try:
        job_id = os.path.basename(genomefile_path)
        log_file_base = "%s" % (job_id)
        cmd = ['/bin/bash', 'windowmasker_run.sh', '%s' % genomefile_path]
        print("Start subprocess: %r" % ' '.join(cmd))
        subprocess_worker = Popen(
            cmd,
            stdout=open(log_file_base + ".stdout", 'w+'),
            stderr=open(log_file_base + ".err.log", 'w+')
        )
        print("Wait for subprocess")
        subprocess_worker.wait()
        print("Subprocess finished")
    except Exception as e:
        print("Error while processing %s " + itemstr)
        return False
    return True


q = rediswq.RedisWQ(name=work_queue, host=host)
print("Worker with sessionID: " + q.sessionID())
print("Initial queue state: empty=" + str(q.empty()))
while not q.empty():
    item = q.lease(lease_secs=10, block=True, timeout=2)
    if item is not None:
        itemstr = item.decode("utf-8")
        print("Working on " + itemstr)
        # time.sleep(10)  # Put your actual work here instead of sleep.
        run_windowmasker(itemstr)
        q.complete(item)
    else:
        print("Waiting for work")
print("Queue empty, exiting")
