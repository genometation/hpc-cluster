#!/usr/bin/env bash

# Requirements:
# - Redis service is running
# # Set up Redis service
# kubectl apply -f redis-pod.yaml
# kubectl apply -f redis-service.yaml

# Start a temporary interactive pod for running the Redis CLI
kubectl run -i --tty temp --image=redis -- /bin/bash
kubectl cp ./genomefiles_queue_all.txt temp:/data
# kubectl attach temp -c temp -i -t
# => Manually fill up queue with jobs
# redis-cli -h redis
# redis:6379> rpush windowmasker "/mnt/genomes/cf1e7fb0-3e82-4b1b-a0b4-9d67fad7a1c8/e6fdfeaa-8d94-4058-a910-9836a4859107/e6fdfeaa-8d94-4058-a910-9836a4859107.fasta"
# redis:6379> rpush windowmasker "/mnt/genomes/37491fca-1e03-4fbc-8213-2ad16d1f4a41/7faef575-162f-4914-a144-8897ecf4443e/7faef575-162f-4914-a144-8897ecf4443e.fasta"
# redis:6379> rpush windowmasker "/mnt/genomes/92b270f7-ecc7-4e44-aef1-e5e6d96725db/f3d03524-68b6-4f3f-b198-29be88c8c2f6/f3d03524-68b6-4f3f-b198-29be88c8c2f6.fasta"
# redis:6379> lrange windowmasker 0 -1
while read line
do
    echo rpush windowmasker "${line}" | redis-cli -h redis
done < genomefiles_queue_all.txt
cat genomefiles_queue_all.txt | awk '{printf "RPUSH windowmasker %s \n", $1}' | redis-cli -h redis > import.log

# Build the image
docker build -t job-windowmasker-wq .
# Tag and push it to registry
docker tag job-windowmasker-wq localhost:32000/job-windowmasker-wq
docker push localhost:32000/job-windowmasker-wq
# Test image interactively
# kubectl run -i --tty wmtemp --image=localhost:32000/job-windowmasker-wq -- /bin/bash

# Run the job
kubectl apply -f ./job.yaml
kubectl describe jobs.batch job-windowmasker-wq
kubectl get pods
kubectl logs jobs/job-windowmasker-wq
# kubectl logs job-windowmasker-wq-...
kubectl exec job-windowmasker-wq-bpt47 -i -t -- /bin/bash
