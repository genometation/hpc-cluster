################################################################################
# Docker Container - Basic Ubuntu system with WindowMasker and required software
#
# Dockerfile for https://docs.docker.com/examples/
FROM    ubuntu:20.04

# Optionally set a maintainer name to let people know who made this image.
MAINTAINER Dominic Simm <dominic.simm@cs.uni-goettingen.de>

# Set timezone in tzdata
RUN     apt-get update --fix-missing && \
        DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin" \
        apt-get install -y tzdata
RUN     echo "Europe/Berlin" > /etc/timezone && \
        ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
        dpkg-reconfigure -f noninteractive tzdata

### Begin: BASE IMAGE SECTION

# Install basic system requirements (nano, rsync, screen ...)
ARG     DEBIAN_FRONTEND="noninteractive"
RUN     apt-get update --fix-missing && \
        apt-get install -y apt-utils build-essential coreutils && \
        apt-get install -y curl git less nano sudo wget
RUN     apt-get install -y autoconf
RUN     apt-get update --fix-missing && apt-get install -y python3 python3-pip
RUN     pip install redis
# Note: The official Debian and Ubuntu images automatically 'apt-get clean'
# after each 'apt-get'

### End: BASE IMAGE SECTION

# Set environment variables
ENV     TMP /tmp
ENV     INSTALL /usr/local

### Begin: RepeatMasker specific packages
# Read: https://www.repeatmasker.org/RepeatMasker/

# Install RepeatMasker
ENV     WINDOWMASKER 4.1.2
RUN     cd $INSTALL && mkdir windowmasker && cd windowmasker && \
        curl ftp://ftp.ncbi.nlm.nih.gov/pub/agarwala/windowmasker/windowmasker --output windowmasker && \
        chmod +x windowmasker

# Compatibility: Cluster environment user (uid, gid)
# Create user 'genwork' and grant sudoers permissions
RUN     groupadd -g 1010 gen
RUN     useradd -u 1001 --create-home --shell /bin/bash genwork
RUN     usermod -g gen genwork
RUN     usermod -a -G genwork genwork
RUN     echo genwork ALL=NOPASSWD:ALL > /etc/sudoers
ENV     HOME /home/genwork
WORKDIR	$HOME
# Switch to user 'genwork'
USER    genwork
COPY    windowmasker_run.sh .
COPY    rediswq.py .
COPY    worker.py .
RUN     sudo chown -R genwork:gen *
# Set ENVs
ENV     PATH="${PATH}:${INSTALL}/windowmasker"

# Set the default command to run when starting the container
# CMD     python3 worker.py
