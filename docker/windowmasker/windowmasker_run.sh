#!/usr/bin/env bash

# Test: Genomefiles
# src='/mnt/genomes/9bc93e4c-4dbd-4c39-ad95-28bac37e0bca/1e447e7a-1ac6-476c-bfbe-7f65cae22635/1e447e7a-1ac6-476c-bfbe-7f65cae22635.fasta'
# Magnaporthe Oryzae AACU
# src='/mnt/genomes/cc5af340-05f9-49e6-b134-ed2cffae1867/e513abf8-5984-4b4d-8031-df2f1176351d/e513abf8-5984-4b4d-8031-df2f1176351d.fasta'

# Get params
src=${1:-""}
if [ -z "${src}" ]; then
  echo "Missing genomefile parameter."
  exit 1
fi
# Set vars
gdir=$(dirname "${src}")
gfile=$(basename "${src}")

# Get data
printf "\n$ cp ${src} ."
time cp ${src} .
# Start Masking
printf '\n$ windowmasker -mk_counts -fa_list false -in ${gfile} -out ${gfile}.wm_counts\n'
time windowmasker -mk_counts -fa_list false -in ${gfile} -out ${gfile}.wm_counts
printf '\n$ windowmasker -ustat ${gfile}.wm_counts -in ${gfile} -outfmt fasta -out ${gfile}.wm'
time windowmasker -ustat ${gfile}.wm_counts -in ${gfile} -outfmt fasta -out ${gfile}.wm
# Convert lowercase letters into N-regions
# time cat ${gfile}.wm | tr [a-z] 'N' | ${gfile}.wm.n
printf '\n$ sed -i -e "/^[A-Za-z]/ y/a/N/" ... ${gfile}.wm'
time sed -i -e "/^[A-Za-z]/ y/a/N/" \
            -e "/^[A-Za-z]/ y/c/N/" \
            -e "/^[A-Za-z]/ y/g/N/" \
            -e "/^[A-Za-z]/ y/t/N/" \
            ${gfile}.wm
# Measure masked region
a=$(tr -cd 'A' < ${gfile}.wm | wc -c)
c=$(tr -cd 'C' < ${gfile}.wm | wc -c)
g=$(tr -cd 'G' < ${gfile}.wm | wc -c)
t=$(tr -cd 'T' < ${gfile}.wm | wc -c)
n=$(tr -cd 'N' < ${gfile}.wm | wc -c)
sum=$((a+c+g+t+n))
printf '\nMasked percentage (N/[ACGNT]): %d/%d (%0.2f)\n' "${n}" "${sum}" "$((n/sum))"
# Copy back results
cp ${gfile}.wm_counts ${gdir}
cp ${gfile}.wm ${gdir}
# Cleanup
rm ${gfile}
rm ${gfile}.*

# Return code
exit 0
