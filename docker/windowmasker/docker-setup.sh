#!/usr/bin/env bash

# # Cache Dfam source for Docker installation
# cp /mnt/databases/dfam/3.3/Dfam.h5 .

# Build docker image of project with composer-preferences
# docker-compose build --no-cache
docker-compose build
# Start image as container via composer
docker-compose up -d
# docker-compose up -d --force-recreate

# Login into running Docker-Container
#docker exec -it repeatmasker_repeatmasker_1 /bin/bash

# # Cleanup: Cache Dfam source
# rm Dfam.h5

# # Generate and update Docker image in K8s registry
# docker build . -f Dockerfile -t localhost:32000/repeatmasker:registry
# docker push localhost:32000/repeatmasker
# curl localhost:32000/v2/repeatmasker/tags/list

# # Run RepeatMasker (initial tests)
# time RepeatMasker -pa 32 -qq -species fungi Magnaporthe_oryzae.MG8.dna.toplevel.fa
# time RepeatMasker -pa 32 -s -species fungi Magnaporthe_oryzae.MG8.dna.toplevel.fa
# time RepeatMasker -pa 32 -s -species viridiplantae Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
# time RepeatMasker -pa 32 -s -species arabidopsis Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
# time RepeatMasker -pa 32 -s -species "homo sapiens" Magnaporthe_oryzae.MG8.dna.toplevel.fa
# time RepeatMasker -pa 64 -s -species "homo sapiens" Magnaporthe_oryzae.MG8.dna.toplevel.fa
# time RepeatMasker -pa 64 -s -species "homo sapiens" Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
