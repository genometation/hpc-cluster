#!/usr/bin/env bash

# # Cache Dfam source for Docker installation
# cp /mnt/databases/dfam/3.3/Dfam.h5 .

# Build docker image of project with composer-preferences
# docker-compose build --no-cache
docker-compose build
# Start image as container via composer
docker-compose up -d
# docker-compose up -d --force-recreate

# Login into running Docker-Container
# docker exec -it infernal_infernal_1 /bin/bash

# # Generate and update Docker image in K8s registry
# docker build . -f Dockerfile -t localhost:32000/repeatmasker:registry
# docker push localhost:32000/repeatmasker
# curl localhost:32000/v2/repeatmasker/tags/list

# # Run Infernal (initial tests)
# cmfetch [options] <cmfile> <key> (retrieves CM named <key>)
# cmfetch -f [options] <cmfile> <keyfile> (retrieves all CMs listed in <keyfile>)
# cmfetch Rfam.cm RF00003 > RF00003.cm
# cmfetch -f Rfam.cm Rfam_subset.txt > Rfam_subset.cm
# time cmsearch --nohmmonly --rfam --cut_ga -o AACU03000000.fasta.cmsearch.out --tblout AACU03000000.fasta.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset.cm AACU03000000.fasta
# time cmsearch --nohmmonly --rfam --cut_ga -o OETA01000000.fasta.cmsearch.out --tblout OETA01000000.fasta.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset.cm OETA01000000.fasta
# cmsearch --nohmmonly --rfam --cut_ga -o my.cmsearch.out --tblout my.cmsearch.tblout RF0003.cm my.fa
# cmscan --nohmmonly --rfam --cut_ga --fmt 2 --oclan --oskip --clanin Rfam.clanin -o my.cmscan.out --tblout my.cmscan.tblout Rfam.cm my.fa
