#!/usr/bin/env bash

# Build the image
docker build -t job-infernal-wq .
# Tag and push it to registry
docker tag job-infernal-wq localhost:32000/job-infernal-wq
docker push localhost:32000/job-infernal-wq

# Test image interactively
# kubectl run -i --tty infernalwqtemp --image=localhost:32000/job-infernal-wq -- /bin/bash
