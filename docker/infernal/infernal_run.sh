#!/usr/bin/env bash

# Test: Genomefiles
# src='/mnt/genomes/9bc93e4c-4dbd-4c39-ad95-28bac37e0bca/1e447e7a-1ac6-476c-bfbe-7f65cae22635/1e447e7a-1ac6-476c-bfbe-7f65cae22635.fasta'
# Magnaporthe Oryzae AACU
# src='/mnt/genomes/cc5af340-05f9-49e6-b134-ed2cffae1867/e513abf8-5984-4b4d-8031-df2f1176351d/e513abf8-5984-4b4d-8031-df2f1176351d.fasta'

# Get params
src=${1:-""}
if [ -z "${src}" ]; then
  echo "Missing genomefile parameter."
  exit 1
fi
# Set vars
gdir=$(dirname "${src}")
gfile=$(basename "${src}")

# Get data
printf "\n$ cp ${src} ."
time cp ${src} .

# INFERNAL
printf "\n$ time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.spliceosome.cmsearch.out --tblout ${gfile}.spliceosome.cmsearch.tblout /mnt ...\n"
time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.spliceosome.cmsearch.out --tblout ${gfile}.spliceosome.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset_spliceosome.cm ${gfile}
printf "\n$ time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.ribosome.cmsearch.out --tblout ${gfile}.ribosome.cmsearch.tblout /mnt ...\n"
time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.ribosome.cmsearch.out --tblout ${gfile}.ribosome.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset_ribosome.cm ${gfile}
printf "\n$ time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.ribonuclease_P.cmsearch.out --tblout ${gfile}.ribonuclease_P.cmsearch.tblout /mnt ...\n"
time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.ribonuclease_P.cmsearch.out --tblout ${gfile}.ribonuclease_P.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset_ribonuclease_P.cm ${gfile}
printf "\n$ time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.snoRNA_U3.cmsearch.out --tblout ${gfile}.snoRNA_U3.cmsearch.tblout /mnt ...\n"
time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.snoRNA_U3.cmsearch.out --tblout ${gfile}.snoRNA_U3.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset_snoRNA_U3.cm ${gfile}
printf "\n$ time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.telomerase.cmsearch.out --tblout ${gfile}.telomerase.cmsearch.tblout /mnt ...\n"
time cmsearch --nohmmonly --rfam --cut_ga -o ${gfile}.telomerase.cmsearch.out --tblout ${gfile}.telomerase.cmsearch.tblout /mnt/databases/rfam/14.6/Rfam_subset_telomerase.cm ${gfile}

# Copy back results
printf "\n Copy back results.\n"
cp *.cmsearch.out ${gdir}
cp *.cmsearch.tblout ${gdir}
# Cleanup all genome associated files
rm ${gfile}
rm ${gfile}.*

# Return code
exit 0
