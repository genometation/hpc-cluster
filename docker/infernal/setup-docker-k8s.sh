#!/usr/bin/env bash

# Requirements:
# - Redis service is running
# # Set up Redis service
# kubectl apply -f redis-pod.yaml
# kubectl apply -f redis-service.yaml

# Start a temporary interactive pod for running the Redis CLI
kubectl run -i --tty redis-client --image=redis -- /bin/bash
kubectl cp ./genomefiles_queue_all.txt redis-client:/data
kubectl attach redis-client -c redis-client -i -t
# => Manually fill up queue with jobs
# redis-cli -h redis
# redis:6379> rpush infernal "/mnt/genomes/cf1e7fb0-3e82-4b1b-a0b4-9d67fad7a1c8/e6fdfeaa-8d94-4058-a910-9836a4859107/e6fdfeaa-8d94-4058-a910-9836a4859107.fasta"
# redis:6379> rpush infernal "/mnt/genomes/37491fca-1e03-4fbc-8213-2ad16d1f4a41/7faef575-162f-4914-a144-8897ecf4443e/7faef575-162f-4914-a144-8897ecf4443e.fasta"
# redis:6379> rpush infernal "/mnt/genomes/92b270f7-ecc7-4e44-aef1-e5e6d96725db/f3d03524-68b6-4f3f-b198-29be88c8c2f6/f3d03524-68b6-4f3f-b198-29be88c8c2f6.fasta"
# redis:6379> lrange infernal 0 -1
# redis:6379> FLUSHDB  # Erase all entries from current DB
# Add all files to working queue (genomefiles_queue_all.txt)
# while read line
# do
#     echo rpush infernal "${line}" | redis-cli -h redis
# done < genomefiles_queue_all.txt
root@temp:/data# cat genomefiles_queue_all.txt | awk '{printf "RPUSH infernal %s \n", $1}' | redis-cli -h redis > import.log
root@temp:/data# echo "lrange infernal 0 -1" | redis-cli -h redis | wc -l

# Build the image
docker build -t job-infernal-wq .
# Tag and push it to registry
docker tag job-infernal-wq localhost:32000/job-infernal-wq
docker push localhost:32000/job-infernal-wq
# Test image interactively
# kubectl run -i --tty wmtemp --image=localhost:32000/job-infernal-wq -- /bin/bash

# Run the job
kubectl apply -f ./job.yaml
kubectl describe jobs.batch job-infernal-wq
kubectl get pods
kubectl logs jobs/job-infernal-wq
# kubectl logs job-infernal-wq-...
kubectl exec job-infernal-wq-bpt47 -i -t -- /bin/bash
# Delete the job
kubectl delete jobs.batch job-infernal-wq

# Control created cmsearch.out and .tblout files
find -L /mnt/genomes -type f -name "*.spliceosome.cmsearch.out" | wc -l
find -L /mnt/genomes -type f -name "*.ribosome.cmsearch.out" | wc -l
find -L /mnt/genomes -type f -name "*.ribonuclease_P.cmsearch.out" | wc -l
find -L /mnt/genomes -type f -name "*.snoRNA_U3.cmsearch.out" | wc -l
find -L /mnt/genomes -type f -name "*.telomerase.cmsearch.out" | wc -l
