#!/usr/bin/env bash

# Source of Jupyter
# Install Jupyter Notebook Data Science Stack
# https://github.com/jupyter/docker-stacks

# Build docker image of project with composer-preferences
docker-compose -f docker-compose.yml build --force-rm
# docker-compose -f docker-compose.yml build --no-cache
# docker-compose -f docker-compose.yml build

# Create volumes
docker volume create --name jupyter_data

# Start image as container:
#   Container autoremove; Publish exposed port to fixed host port 8888;
# 	Use user-defined network 'frontend'
#   Mount volumes at container-destination;
#   Name container 'jupyter'; daemonize
# docker run --rm -p 8888:8888 \
#            --network="frontend" \
#            --name jupyter -d jupyter/datascience-notebook

# Start image as container via composer
docker-compose -f docker-compose.yml up > jupyter_docker.log &
# docker-compose -f docker-compose.yml up -d
