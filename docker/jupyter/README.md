README
======

Jupyter is configured to be accessed with a Token. A connection can be established by visiting the following URL:
http://jupyter.fab8-vm/?token=e965a8712969d6df7f128fd3a72516f5e2f824baf4fb8bc3

Save and Store
--------------
The Jupyter-service is running as a Docker container. All the notebooks or data that shall be stored to persist,
have to located under the store folder in the root directory.
