#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Kubernetes Jobs creator (YAMLs) """

from jinja2 import Environment, FileSystemLoader, select_autoescape
import glob
# import pathlib
import json
from optparse import OptionParser, OptionGroup
import os.path
import string
import sys


def tr(s):
    return s.translate(str.maketrans('', '', string.punctuation))


def render_template(opt):
    # Remember base directory
    basedir = os.getcwd()
    # Switch to genome annotation location
    os.chdir(opt.wd)
    query_files = glob.glob('/'.join([opt.query_base, '*.fas']))
    query_items = [{"job": tr(os.path.basename(f)), "filename": os.path.basename(
        f), "path": f} for f in query_files]

    # # Template data
    # data = {
    #     'working_directory': opt.wd,
    #     'target': opt.target,
    #     'query_files': json.dumps(query_items),
    # }
    # Render template
    os.chdir(basedir)
    env = Environment(
        loader=FileSystemLoader('.'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    print(env.get_template('pyscipio3_job.tmpl.yaml.jinja2').render(
        working_directory=opt.docker_path,
        target=opt.target,
        query_base=opt.query_base,
        query_files=query_items)
    )


def main(args):
    """ Main method """

    # Parse arguments
    parser = parse_commandline_input()
    (options, args) = parser.parse_args(args)
    # Check input
    if len(args) == 3:
        options.wd, options.docker_path, options.target, options.query_base = args
    if not (options.wd and options.target and options.query_base):
        parser.error("Missing target file and/or query-base folder")

    # log.info("Target genome: %s", options.target)
    # log.info("Query genes: %s", options.query_base)
    # Populate template with data
    render_template(options)


def parse_commandline_input():
    parser = OptionParser(usage="usage: %prog [options] working_directory target query_base",
                          description="%prog -w /mnt/test/pyscipio/target_magnap_grisea_query_magnap_oryzae -d test_data/pyscipio/target_magnap_grisea_query_magnap_oryzae -t Magnaporthe_grisea_70_15_v3_chromosome.fasta -q q_genes",
                          version="%prog 0.1")
    parser.set_defaults(wd=None, target=None, query_base=None)
    # Add options
    group = OptionGroup(parser, "Data sources")
    opt = group.add_option
    opt('-w', '--wd', metavar='dir',
        help='Working directory for Job')
    opt('-d', '--docker_path', metavar='dir',
        help='Docker: Working directory for Job')
    opt('-t', '--target', metavar='file',
        help='Target file')
    opt('-q', '--query_base', metavar='dir',
        help='Folder containing query FASTA files (genes)')
    parser.add_option_group(group)

    return parser


if __name__ == '__main__':
    main(sys.argv[1:])
