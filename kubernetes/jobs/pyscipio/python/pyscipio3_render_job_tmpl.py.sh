#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Kubernetes Jobs creator (YAMLs) """

from jinja2 import Template
import sys

print(Template(sys.stdin.read()).render())
