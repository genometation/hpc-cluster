#!/bin/bash

python3 -m ensurepip --default-pip --user
python3 -m pip install --upgrade pip setuptools wheel virtualenv --user
########################################################################################################################
## Create and activate virtual python environment  ``venv`` in project folder:
python3 -m virtualenv venv
source ./venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
