#!/bin/bash

# Input parameters
#query="$1"
#query_base=$(basename "$query" | cut -d. -f1)
#target="$2"
#target_base=$(basename "$target" | cut -d. -f1)
#test_dir=${3:-test}

#annotation_path="$1"
annotation_path=/mnt/test/pyscipio/target_magnap_grisea_query_magnap_oryzae/q_genes_100

# Expand the template into multiple files, one for each item to be processed.
mkdir ./manifests
for f in $annotation_path/*.fas;
do
  bf=$(basename "$f" | cut -d. -f1 | tr "[:punct:]" -)
  cat pyscipio3_job.tmpl.yaml | sed "s/\$ITEM/$bf/" > ./manifests/job-$bf.yaml
done
