#!/usr/bin/env bash

# Kubernetes nodes
declare -a nodes=("mcpmendel"
                  "mcplinne"
                  "mcpdarwin"
                  "nodeala"
                  "nodearg"
                  "nodeasn"
                  "nodeasp"
                  "nodecit"
                  "nodecpg"
                  "nodecys"
                  "nodegln"
                  "nodeglu"
                  "nodegly"
                  "nodehcy"
                  "nodehis"
                  "nodehse"
                  "nodehyp"
                  "nodeile"
                  "nodeleu"
                  "nodelys"
                  "nodemet"
                  "nodenle"
                  "nodenva"
                  "nodeorn"
                  "nodepen"
                  "nodephe"
                  "nodepra"
                  "nodepro"
                  "nodepyl"
                  "nodesec"
                  "nodeser"
                  "nodethr"
                  "nodetic"
                  "nodetrp"
                  "nodetyr"
                  "nodeval"
                )

# Request pods information with node-assignment
microk8s.kubectl get pods -o wide --namespace=default
# Get node stats
for node in "${nodes[@]}"
do
    microk8s.kubectl top node ${node}
done
