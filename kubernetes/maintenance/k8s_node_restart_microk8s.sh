#!/usr/bin/env bash

# Kubernetes nodes
node=${1:-nodeala}

# Run commands for node
ssh ubuntu@${node} hostname
ssh ubuntu@${node} microk8s stop
ssh ubuntu@${node} microk8s start
