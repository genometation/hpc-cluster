#!/usr/bin/env bash

# Kubernetes nodes
# declare -a nodes=("mcpmendel"
declare -a nodes=("mcplinne"
                  "mcpdarwin"
                  "nodeala"
                  "nodearg"
                  "nodeasn"
                  "nodeasp"
                  "nodecit"
                  "nodecpg"
                  "nodecys"
                  "nodegln"
                  "nodeglu"
                  "nodegly"
                  "nodehcy"
                  "nodehis"
                  "nodehse"
                  "nodehyp"
                  "nodeile"
                  "nodeleu"
                  "nodelys"
                  "nodemet"
                  "nodenle"
                  "nodenva"
                  "nodeorn"
                  "nodepen"
                  "nodephe"
                  "nodepra"
                  "nodepro"
                  "nodepyl"
                  "nodesec"
                  "nodeser"
                  "nodethr"
                  "nodetic"
                  "nodetrp"
                  "nodetyr"
                  "nodeval"
                )

# Get node stats
for node in "${nodes[@]}"
do
  ssh ubuntu@${node} 'cp /etc/hosts /home/ubuntu'
  ssh ubuntu@${node} 'cat /nfs/zfs_adenin/software/config/hosts >> /home/ubuntu/hosts'
  ssh ubuntu@${node} 'sudo cp /home/ubuntu/hosts /etc/hosts'
done
