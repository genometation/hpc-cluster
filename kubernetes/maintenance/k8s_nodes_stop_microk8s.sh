#!/usr/bin/env bash

# Kubernetes nodes
# declare -a nodes=("mcpmendel"
#                   "mcplinne"
#                   "mcpdarwin"
declare -a nodes=("nodeala"
                  "nodearg"
                  "nodeasn"
                  "nodeasp"
                  "nodecit"
                  "nodecpg"
                  "nodecys"
                  "nodegln"
                  "nodeglu"
                  "nodegly"
                  "nodehcy"
                  "nodehis"
                  "nodehse"
                  "nodehyp"
                  "nodeile"
                  "nodeleu"
                  "nodelys"
                  "nodemet"
                  "nodenle"
                  "nodenva"
                  "nodeorn"
                  "nodepen"
                  "nodephe"
                  "nodepra"
                  "nodepro"
                  "nodepyl"
                  "nodesec"
                  "nodeser"
                  "nodethr"
                  "nodetic"
                  "nodetrp"
                  "nodetyr"
                  "nodeval"
                )

# Get node stats
for node in "${nodes[@]}"
do
    ssh ubuntu@${node} hostname
    ssh ubuntu@${node} microk8s stop
done
