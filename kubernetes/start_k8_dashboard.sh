#!/bin/bash

# Run Dashboard addon
token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s kubectl -n kube-system describe secret $token
# ... from remote (http://10.153.18.10:10443)
microk8s kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443 --address 10.153.18.10
