#!/usr/bin/env bash

# Set up Redis service
kubectl apply -f redis-pod.yaml
kubectl apply -f redis-service.yaml
# kubectl apply -f redis.yaml

# Start a temporary interactive pod for running the Redis CLI
# to fill up queue
kubectl run -i --tty temp --image=redis -- /bin/bash
# kubectl attach temp -c temp -i -t

# Build the image
docker build -t job-wq-2 .
# Tag and push it to registry
docker tag job-wq-2 localhost:32000/job-wq-2
docker push localhost:32000/job-wq-2

# Run the job
kubectl apply -f ./job.yaml
kubectl describe jobs.batch job-wq-2
kubectl get pods
kubectl logs jobs/job-wq-2
kubectl logs job-wq-2-gknrd
