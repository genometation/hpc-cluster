#!/usr/bin/env bash

# Terminal configuration: Highlighting
COLOR='\033[0;32m'
NC='\033[0m' # No Color

# Get K8s username from argv[1]
k8s_user=${1}
if [ -n "$1" ];
then
  # Create ClusterUser for Namespace 'default'
  # Read: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md
  echo -e "${COLOR} - Add and bind cluster user: '${k8s_user}'${NC}"
  microk8s kubectl apply -f service-account-${k8s_user}.yaml
  microk8s kubectl apply -f bind-user-${k8s_user}.yaml
  # Generate Bearer Token
  echo -e "${COLOR} - Generate Bearer Token for cluster user: '${k8s_user}'${NC}"
  ref_k8s_user=k8s_user
  microk8s kubectl -n default get secret $(microk8s kubectl -n default get sa/${!ref_k8s_user} -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
else
  echo -e "${COLOR} - Abort K8s user creation: No username found ...${NC}"
fi
