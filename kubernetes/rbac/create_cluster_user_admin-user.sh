#!/usr/bin/env bash

# Output configuration
COLOR='\033[0;32m'
NC='\033[0m' # No Color

# Create ClusterUser for Namespace 'default'
# Read: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md
echo -e "${COLOR} - Add and bind cluster user: 'admin-user'${NC}"
microk8s kubectl apply -f service-account-admin-user.yaml
microk8s kubectl apply -f bind-user-admin-user.yaml
# Generate Bearer Token
echo -e "${COLOR} - Generate Bearer Token for cluster user: 'admin-user'${NC}"
# Deprecated from K8s > v.1.24+
# microk8s kubectl -n default get secret $(microk8s kubectl -n default get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
kubectl create token admin-user --duration=10000h

# kubectl -n default delete serviceaccount admin-user
# kubectl -n default delete clusterrolebinding admin-user
