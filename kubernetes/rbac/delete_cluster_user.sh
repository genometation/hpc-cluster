#!/usr/bin/env bash

# Terminal configuration: Highlighting
COLOR='\033[0;32m'
NC='\033[0m' # No Color

# Get K8s username from argv[1]
k8s_user=${1}
if [ -n "$1" ];
then
  # Remove ClusterUser for Namespace 'default'
  # Read: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md
  echo -e "${COLOR} - Remove cluster user: '${k8s_user}'${NC}"
  microk8s kubectl -n default delete clusterrolebinding ${k8s_user}
  microk8s kubectl -n default delete serviceaccount ${k8s_user}
else
  echo -e "${COLOR} - Abort K8s user creation: No username found ...${NC}"
fi
