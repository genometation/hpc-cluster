#!/bin/bash

# ##  Download dumps
# wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump_archive/taxdmp_2020-10-01.zip

for f in *.zip;
do
    bf=$(basename "$f" | cut -d. -f1)
    #  Unzip
    unzip "$f" -d "$bf"
    ##  Prepare TaxNames .dmp-files and convert in CSVs
    cut -f1-4 -d"|" "$bf"/names.dmp > "$bf"/names_cols.csv
    sed 's/"/""/g' "$bf"/names_cols.csv | tr '\t' '"' | sed 's/"//' > "$bf"/names_cols_trimmed.csv
    ##  Prepare TaxNodes .dmp-files and convert in CSVs
    cut -f1-12 -d"|" "$bf"/nodes.dmp > "$bf"/nodes_cols.csv
    tr -d "\t" < "$bf"/nodes_cols.csv > "$bf"/nodes_cols_trimmed.csv
    cut -f1-2 -d"|" "$bf"/merged.dmp > "$bf"/merged_cols.csv
    tr -d "\t" < "$bf"/merged_cols.csv > "$bf"/merged_cols_trimmed.csv
    cut -f1 -d"|" "$bf"/delnodes.dmp > "$bf"/delnodes_cols.csv
    tr -d "\t" < "$bf"/delnodes_cols.csv > "$bf"/delnodes_cols_trimmed.csv
done
