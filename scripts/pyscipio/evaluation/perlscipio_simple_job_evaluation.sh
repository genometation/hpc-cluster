#!/usr/bin/env bash

result_dir=${1:-"."}
yaml_header_size=32

# Get numbers
num_genes_total=$(find $result_dir -type f -name '*.stdout' | wc -l)
num_hits_total=$(find $result_dir -type f -name '*.yaml' | wc -l)
num_hits_compl=$(find $result_dir -name "*.yaml" -print0 | xargs -0 head -n$yaml_header_size | grep -c "status: auto")
num_hits_compl_alt=$(find $result_dir -name "*.yaml" ! -name '*_0_.yaml' -print0 | xargs -0 head -n$yaml_header_size | grep -c "status: true")
num_hits_compl_all=$((${num_hits_compl}+${num_hits_compl_alt}))
num_hits_part=$(find $result_dir -name "*.yaml" -print0 | xargs -0 head -n$yaml_header_size | grep -c "status: incomplete")
num_hits_part_alt=$(find $result_dir -name "*.yaml" ! -name '*_0_.yaml' -print0 | xargs -0 head -n$yaml_header_size | grep -c "status: false")
num_hits_part_all=$((${num_hits_part}+${num_hits_part_alt}))
num_hits_total_alt=$((${num_hits_compl_alt}+${num_hits_part_alt}))
num_hits_found=$((${num_hits_compl}+${num_hits_part}))
num_genes_missed=$((${num_genes_total}-${num_hits_found}))
cap_line="================;========;=======;======;====="
sec_line="----------------;--------;-------;------;-----"
# Fill table with data
printf -v table '%s\n' \
"No. of ...;Complete;Partial;Missed;Total" \
"${cap_line}" \
"genes;-;-;${num_genes_missed};${num_genes_total}" \
"${sec_line}" \
"hits;${num_hits_compl};${num_hits_part};-;${num_hits_found}" \
"alternative hits;${num_hits_compl_alt};${num_hits_part_alt};-;${num_hits_total_alt}" \
"${sec_line}" \
# Evaluate scores
for ((i=9;i>=5;i--));
do
  bin=$(find $result_dir -name "*.yaml" -print0 | xargs -0 head -n$yaml_header_size | grep -c "\"score\": 0.${i}")
  printf -v data '%s\n' "alt. hits > 0.${i};-;${bin};-;-"
  table+=$data
done
printf -v data '%s\n' "${cap_line}" " ;${num_hits_compl_all};${num_hits_part_all};-;${num_hits_total}"
table+=$data
# Print table data
echo "${table}" | column -t -s';'
