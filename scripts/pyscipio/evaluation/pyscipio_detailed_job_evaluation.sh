#!/usr/bin/env bash

run_folder=${1:-"."}

# Total number of query genes found in target genome
total_queries=$(ls -l ${run_folder} | grep -c ^d)
#total_queries=$(find ${run_folder} -maxdepth 0 -type d | wc -l)
query_no_result=$(find ${run_folder} -maxdepth 1 -type d -empty | wc -l)
echo "###################################################"
echo ""
echo "Total queries: ${total_queries}"
echo "Genes not found: ${query_no_result}"
echo ""
echo "###################################################"

find ${run_folder} -maxdepth 2 -type f -name '*.yaml' > ${run_folder}/file_list.txt
while IFS= read -r line || [[ -n $line ]]
  do
    query_gene=$(echo "${line}" | sed -n -E 's/.*\/(gene[^\/]*fas)\//\1/p')
    query_gene_nr=$(echo "${line}" | sed -n -E 's/.*_([0-9]*)_[.]yaml$/\1/p')
    completeness=$(head -16 "${line}" | sed -n -E 's/[[:space:]]*is_complete:[[:space:]]([a-z]*)$/\1/p')
    score=$(head -16 "${line}" | sed -n -E 's/[[:space:]]*score:[[:space:]]([0-9.]*)$/\1/p')
    target_nr=$(grep -c "GeneBlockID" "${line}")
    target_name=""
    dna_start=""
    dna_end=""
    if [[ ${target_nr} == "1" ]]; then
      target_name=$(head -30 "${line}" | sed -n -E 's/[[:space:]]*target:.*([A-Z]{2,6}[0-9]{2}S?[0-9]{4,8}[.][0-9]{1,2})[|].*/\1/p')
      dna_start=$(head -30 "${line}" | sed -n -E 's/[[:space:]]*dna_start:[[:space:]]([0-9]*)$/\1/p')
      dna_end=$(head -30 "${line}" | sed -n -E 's/[[:space:]]*dna_end:[[:space:]]([0-9]*)$/\1/p')
    fi
    echo ${query_gene} ";" ${query_gene_nr} ";" ${completeness} ";" ${score} ";" ${target_name} ";" ${dna_start} ";" ${dna_end} >> ${run_folder}/gene_summary.txt
done < ${run_folder}/file_list.txt
sed -i -E 's/[[:space:]];[[:space:]]/;/g' ${run_folder}/gene_summary.txt
sed -i -E 's/;[[:space:]]/;/g' ${run_folder}/gene_summary.txt

total_nr_yaml=$(cat ${run_folder}/gene_summary.txt | wc -l)
hits_complete=$(cut -d ';' -f 2-3 ${run_folder}/gene_summary.txt | grep -c -E "0;true")
alt_hits_complete=$(cut -d ';' -f 2-3 ${run_folder}/gene_summary.txt | grep -c -E "[^0];true")
hits_incomplete=$(cut -d ';' -f 2-3 ${run_folder}/gene_summary.txt | grep -c -E "0;false")
alt_hits_incomplete=$(cut -d ';' -f 2-3 ${run_folder}/gene_summary.txt | grep -c -E "[^0];false")

# echo "###################################################"
echo ""
echo "Total number yaml-files: ${total_nr_yaml}"
echo ""
if [[ $((hits_complete+alt_hits_complete+hits_incomplete+alt_hits_incomplete)) != "${total_nr_yaml}" ]]; then
  echo "Something is wrong with file and hit counting!"
fi
if [[ ${#hits_complete} -gt 5 ]] || [[ ${#hits_incomplete} -gt 5 ]];
  then
    if [[ ${#hits_complete} -gt ${#hits_incomplete} ]];
      then
        nr=$((${#hits_complete}+7))
        spaces=$(for ((i=0; i<nr; i++)); do echo -n " "; done)
        space1=" "
        space2=$((space1+${#hits_complete}-${#hits_incomplete}))
      else
        nr=$((${#hits_incomplete}+7))
        spaces=$(for ((i=0; i<nr; i++)); do echo -n " "; done)
        space2=" "
        space1=$((space2+${#hits_incomplete}-${#hits_complete}))
    fi
  else
    spaces=$(for ((i=0; i<12; i++)); do echo -n " "; done)
    len=$((5-${#hits_complete}))
    space1=$(for ((i=0; i<len; i++)); do echo -n " "; done)
    len=$((5-${#hits_incomplete}))
    space2=$(for ((i=0; i<len; i++)); do echo -n " "; done)
fi
echo "${spaces}hits | alternative hits"
echo "Complete  :${space1}${hits_complete} | ${alt_hits_complete}"
echo "Incomplete:${space2}${hits_incomplete} | ${alt_hits_incomplete}"
echo ""
nr_1target=$(cut -d ";" -f 5 ${run_folder}/gene_summary.txt | grep -c -E "^[A-Z0-9.]*$")
nr_multipletarget=$(cut -d ";" -f 5 ${run_folder}/gene_summary.txt | grep -c -E "^$")
echo "Nr genes on single target   : ${nr_1target}"
echo "Nr genes on multiple targets: ${nr_multipletarget}"
echo ""

echo "Score > ###: hits | alternative hits"
for ((i=9;i>=5;i--))
  do
    score_hits=$(cut -d ';' -f 2,4 ${run_folder}/gene_summary.txt | grep -c -E "0;0.${i}")
    score_alt_hits=$(cut -d ';' -f 2,4 ${run_folder}/gene_summary.txt | grep -c -E "[^0];0.${i}")
    echo "Score 0.${i}  : ${score_hits} | ${score_alt_hits}"
done
echo ""
echo "###################################################"

while IFS= read -r line || [[ -n $line ]]
  do
    if [[ $(echo ${line} | cut -d ";" -f 2) != "0" ]] && [[ $(echo ${line} | cut -d ";" -f 5) != "" ]];
      then
        ref_target=$(echo ${line} | cut -d ";" -f 5)
        ref_gene=$(echo ${line} | cut -d ";" -f 1)
        ref_start=$(echo ${line} | cut -d ";" -f 6)
        ref_end=$(echo ${line} | cut -d ";" -f 7)
        grep -E "${ref_target}" ${run_folder}/gene_summary.txt > ${run_folder}/temp.txt
        sed -i -E "/${line}/d" ${run_folder}/temp.txt
        sed -i '/^$/d' ${run_folder}/temp.txt
        cut -d ";" -f 2,6,7 ${run_folder}/temp.txt | sort -n -t ';' -k2,2 -k3,3 > ${run_folder}/temp_sorted.txt

        if [[ $(echo ${run_folder}/temp_sorted.txt | cut -d ";" -f 1 | grep "0") == "0" ]];then
            overlap_query_gene="true"
          else
            overlap_query_gene="false"
        fi

        declare -i identical_hits=0
        declare -i overlapping_hits=0
        while IFS= read -r line || [[ -n $line ]]
          do
            fer_start=$(echo ${line} | cut -d ";" -f 2)
            fer_end=$(echo ${line} | cut -d ";" -f 3)
            if [[ ${ref_start} -eq ${fer_start} ]] && [[ ${ref_end} -eq ${fer_end} ]]; then
              (( ++identical_hits ))
            fi
            if { [[ ${ref_start} -le ${fer_start} ]] && [[ ${ref_end} -gt ${fer_start} ]]; } || { [[ ${ref_start} -gt ${fer_start} ]] && [[ ${ref_end} -le ${fer_end} ]]; }; then
              (( ++overlapping_hits ))
            fi
        done < ${run_folder}/temp_sorted.txt
        echo ${ref_gene} ";" ${ref_target} ";" ${overlap_query_gene} ";" ${identical_hits} ";" ${overlapping_hits} >> ${run_folder}/overlap_summary.txt
    fi

done < ${run_folder}/gene_summary.txt

sed -i -E 's/[[:space:]];[[:space:]]/;/g' ${run_folder}/overlap_summary.txt
sed -i -E 's/;[[:space:]]/;/g' ${run_folder}/overlap_summary.txt

echo ""
echo "###################################################"
echo ""
echo "Analysis alternative hits"
echo ""
total_query_gene_overlap=$(grep -c -E ";true;" ${run_folder}/overlap_summary.txt)
echo "Overlapping any query gene: ${total_query_gene_overlap}"
identical_to_other_hits=$(cut -d ";" -f 4 ${run_folder}/overlap_summary.txt | grep -c -E "[^0]")
echo "Identical to other hits   : ${identical_to_other_hits}"
overlapping_other_hits=$(cut -d ";" -f 5 ${run_folder}/overlap_summary.txt | grep -c -E "[^0]")
echo "Overlapping any other hit : ${overlapping_other_hits}"
