#!/usr/bin/env bash

# Documentation:
# Only for local testing in Docker image of PyScipio job folders
# => under /mnt/test/pyscipio/job-...
#
# Example usage:
# cd /mnt/test/pyscipio/test_target_magnap_oryzae_query_magnap_oryzae;
# ./perlscipio_parallel_test.sh target/Magnaporthe_oryzae.MG8.dna.toplevel.fa q_genes conf/pyscipio.fungi.strict.conf;
# cd /mnt/test/pyscipio/job_target_magnap_job_MQOP01000000_query_magnap_oryzae
# time ./perlscipio_parallel_job.sh target/MQOP01000000.fasta q_genes_10 conf/perlscipio.fungi.default.conf 4

# Define a timestamp function
timestamp() {
  date +"%s"  # current time
}

### Params and Vars ############################################################

# Get params
target=${1}
geneset=${2:-q_genes_10}
config=${3}
max_parallel=${4:-1}
# Set variables
config_base=$(basename "$config" | cut -d. -f2-3 | tr -c '[:alnum:]_' '_')
result_dir="run__perlscipio__${geneset}__${config_base}_$(timestamp)"
mkdir -p ${result_dir}

# Load Scipio config file as one-line string
ARGS=$(cat ${config} | tr "\n" " ")
# ARGS="--fast_nw --accepted_intron_penalty=1.0 --exhaust_align_size=15000 --exhaust_gap_size=21 --gap_to_close=6 --max_assemble_size=75000 --max_mismatch=7 --max_move_exon=6 --min_coverage=60 --min_identity=90 --min_intron_len=22 --min_score=0.3 --region_size=2000 --show_exon=type,nucl_start,nucl_end,dna_start,dna_end,prot_start,prot_end,seqshifts,mismatchlist,translation,seq,undeterminedlist,overlap,inframe_stopcodons --blat_score=15 --blat_identity=81 --blat_tilesize=7"

### Run job ####################################################################
printf "\n- Run job\n"
# Start PerlScipio with specified target/query and config files

# Option 1: Parallel runs in N-process batches
# active_procs=0
# for fastafile in ${geneset}/*.fas; do
#   base_fastafile=$(basename "$fastafile" | cut -d. -f1)
#   printf '%s\n' "scipio.pl ${ARGS} --verbose ${target} ${fastafile} > ${result_dir}/${base_fastafile}.yaml 2> ${result_dir}/${base_fastafile}.stdout &"
#   scipio.pl ${ARGS} --verbose ${target} ${fastafile} > ${result_dir}/${base_fastafile}.yaml 2> ${result_dir}/${base_fastafile}.stdout &
#   # Manage batches of parallel processes
#   active_procs=$((active_procs+1))
#   if [ $active_procs -ge $max_parallel ]
#   then
#     echo "Wait and reset"
#     active_procs=0
#     wait
#   fi
# done
# # Wait for last batch of processes
# wait

# Option 2: Parallel run upto N-processes
for file in ${geneset}/*.fas; do
    (
      # Execution block
      base_file=$(basename "$file" | cut -d. -f1)
      printf '%s\n' "scipio.pl ${ARGS} --verbose ${target} ${file} > ${result_dir}/${base_file}.yaml 2> ${result_dir}/${base_file}.stdout"
      scipio.pl ${ARGS} --verbose ${target} ${file} > ${result_dir}/${base_file}.yaml 2> ${result_dir}/${base_file}.stdout
    ) &
    # Wait here for any job to be finished,
    # so there is a place to start next one.
    if [[ $(jobs -r -p | wc -l) -ge $max_parallel ]]; then wait -n; fi
done
# Wait for last batch of processes
wait

# # Option 3: GNU parallel
# # https://www.gnu.org/software/parallel/sem.html
# for file in ${geneset}/*.fas; do
#   base_file=$(basename "$file" | cut -d. -f1)
#   printf '%s\n' "scipio.pl ${ARGS} --verbose ${target} ${file} > ${result_dir}/${base_file}.yaml 2> ${result_dir}/${base_file}.stdout"
#   sem -j $max_parallel "scipio.pl ${ARGS} --verbose ${target} ${file} > ${result_dir}/${base_file}.yaml 2> ${result_dir}/${base_file}.stdout"; echo done
# done
# sem --wait

### Evaluation #################################################################
printf "\n- Short result evaluation\n\n"
time ./perlscipio_simple_job_evaluation.sh ${result_dir}

### Cleanup ####################################################################
printf "\n- Cleanup files\n"
# Remove stdout files and empty error logs
printf "\nCleanup empty and duplicate error logs:\n"
find ${result_dir} -name '*.stdout' -type f -delete
find ${result_dir} -name '*.log' -type f -empty -delete
# Remove duplicates error log and show errors
shopt -s nullglob
if [[ -n $(echo ${result_dir}/*.log) ]]
then
  mkdir -p ${result_dir}/logs
  mv ${result_dir}/*.log ${result_dir}/logs
  cd ${result_dir}/logs
  # Remove identical error log files
  declare -A arr
  shopt -s globstar
  for file in **; do
    [[ -f "${file}" ]] || continue
    read cksm _ < <(md5sum "${file}")
    if ((arr[${cksm}]++)); then
      rm -f ${file}
    fi
  done
  # List error logs
  printf "\nShow errors:\n"
  tail -n +1 *.err.log
fi

### Push back results to NFS-storage ###########################################
printf "\n- Push back results to NFS-storage\n"

### Prepare results locally
# Use tar archive
archive="full_scipio_results.tar"
tar --sort=name -cf ${archive} ${result_dir} --remove-files && mv ${archive} ${result_dir}.tar
