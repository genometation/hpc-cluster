#!/bin/bash

# Note:
# Script needs to be run in environment with same NCBI-BLAST version (makeblastdb)
# as installed in pipeline Docker container (e.g. PyScipio3)

## Set vars and directory names
# _base_dir="/nfs/zfs_adenin/test/pyscipio"
_base_dir="/mnt/test/pyscipio"
_query="query_magnap_oryzae"
_target="target_magnap_oryzae"

## List of Target FASTA files
declare -a target_files=(
    "/mnt/ncbi_assemblies/AACU00000000/AACU03000000/AACU03000000.fasta"
)
job_dirs=""
## Create job directory
for target_file_path in "${target_files[@]}"
do
    ## Define vars
    tar_dir=$(dirname "$target_file_path")
    tar_bf=$(basename "$target_file_path" | cut -d. -f1)
    # echo "${tar_dir}/${tar_bf}"
    printf "\n\n - Create new job directory:\n"

    ## Query directory
    query_folder="${_query}"
    # echo "Make dir: '${query_folder}'"
    # mkdir -p ${query_folder}/q_genes && cd ${query_folder}
    # cp ${query_file_path} .
    # # Split Query FASTA $file into single files 'gene_XX.fas' (divided by '>')
    # file=$(basename "$query_file_path")
    # num=$(grep -c -E '^>.*$' $file)
    # csplit -f "gene_" -b "%0${#num}d.fas" -k -s -z $file '/>/' "{$((num-1))}"
    # # Copy single files into batch folders
    # mv gene_* q_genes

    ## Target directory
    target_folder="${_target}_${tar_bf}"
    echo "Make dir: '${target_folder}'"
    mkdir ${target_folder} && cd ${target_folder}
    cp ${target_file_path} .
    makeblastdb -dbtype nucl -in "${tar_bf}.fasta"

    ## Job directory
    job_folder="job_${target_folder}_${query_folder}"
    job_dirs+="\n${job_folder}"
    echo "Make dir: '${job_folder}'"
    cd ${_base_dir}
    mkdir ${job_folder} && cd ${job_folder}
    ln -s ../pyscipio_conf conf
    ln -s ../pyscipio_parallel.py pyscipio_parallel.py
    ln -s ../pyscipio_parallel_job.sh pyscipio_parallel_job.sh
    ln -s ../pyscipio_eval_params.sh pyscipio_eval_params.sh
    ln -s ../pyscipio_simple_job_evaluation.sh pyscipio_simple_job_evaluation.sh
    ln -s ../pyscipio_detailed_job_evaluation.sh pyscipio_detailed_job_evaluation.sh
    ln -s ../pyscipio_prefilter_results.py pyscipio_prefilter_results.py
    ln -s ../perlscipio_parallel_job.sh perlscipio_parallel_job.sh
    ln -s ../perlscipio_simple_job_evaluation.sh perlscipio_simple_job_evaluation.sh
    ln -s ../${query_folder}/q_genes q_genes
    ln -s ../${query_folder}/q_genes_10 q_genes_10
    ln -s ../${query_folder}/q_genes_100 q_genes_100
    ln -s ../${query_folder}/q_genes_1k q_genes_1k
    ln -s ../${query_folder}/q_genes_10k q_genes_10k
    ln -s ../${target_folder} target

    ## Back to base dir
    cd ${_base_dir}
done
printf "\n\n- Created job directories:\n"
printf ${job_dirs}
