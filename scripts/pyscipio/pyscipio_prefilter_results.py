#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" PyScipio gene prediction prefilter """

import csv
import os
import pandas as pd
import sqlite3
import sys
import re
from scipio.export import parse_yaml


### Helper methods #############################################################
def file_get_contents(file_path):
    """ Read content of passed file 'file_path' into string

    Args:
        file_path (str): Filename

    Returns:
        str: File content
    """
    if os.path.exists(file_path):
        with open(file_path, 'r') as fh:
            return fh.read()  # Read file in one string
    return None


### Vars and params ############################################################
job = os.path.basename(sys.argv[2])
result_path = os.path.dirname(sys.argv[1])
yaml_file_list = file_get_contents(sys.argv[1]).splitlines()
if not yaml_file_list:
    print('Error: YAML list could not be read.')
    sys.exit(-1)
filename = 'prefilter_scipio_results'
csv_file = os.path.join(result_path, filename + '.csv')
sqlite_file = os.path.join(result_path, filename + '.sqlite')

# CSV: File format (header)
fieldnames = ['job',
              'filepath',
              'gene_nr',
              'hit_nr',
              'target',
              'is_complete',
              'start_codon',
              'stop_codon',
              'score',
              'identity_score',
              'exon_intron_ratio',
              'exon_num',
              'intron_max',
              'inframe_stopcodons',
              'dna_start',
              'dna_end',
              'contigs']


### Data preparation ###########################################################
#
csv_rows = []
for yaml_file in yaml_file_list:
    # Load and parse YAML-data
    yaml_str = file_get_contents(yaml_file)
    try:
        g = parse_yaml(yaml_str)
    except Exception as e:
        print("Error: Parsing YAML-file '%s'\n\n%s)" % (yaml_file, e))
        continue
    # Relative path from
    sub_path = yaml_file.replace('%s/' % result_path, '')
    # Gene number (e.g. gene_00024_fas => 24)
    gene_path = os.path.dirname(yaml_file)
    m = re.search('.+gene_([0-9]+)_fas', gene_path)
    gene_nr = int(m.group(1)) if m else -1
    # Hit number (e.g. query_sequence_(2) => 2)
    m = re.search('.+\(([0-9]+)\)', g.HitID)
    hit_nr = m.group(1) if m else g.HitID
    # Target ID gb|accession (e.g. AACU03000000)
    m = re.search('.+([A-Z]{4,6}[0-9]{8,11}).+', g.assembly[0].target)
    target_id = m.group(1) if m else g.assembly[0].target

    # Extract Scipio-YAML information and write to CSV (summary_file)
    csv_row = [job,
               sub_path,
               gene_nr,
               hit_nr,
               target_id,
               g.is_complete,
               g.start_codon,
               g.stop_codon,
               g.score,
               g.identity_score,
               g.exon_intron_ratio,
               g.exon_num,
               g.intron_max,
               g.inframe_stopcodons,
               g.assembly[0].dna_start,
               g.assembly[-1].dna_end,
               len(g.assembly)]
    csv_rows.append(csv_row)

# Write to CSV data
with open(csv_file, 'w', newline='') as csv_fp:
    writer = csv.DictWriter(csv_fp, fieldnames=fieldnames, delimiter=' ', quotechar='|')
    #                         , quoting=csv.QUOTE_ALL)
    writer.writeheader()
    for row in csv_rows:
        writer.writerow(dict(zip(fieldnames, row)))

# Create a new database file:
db = sqlite3.connect(sqlite_file)
# Load the CSV in chunks:
for c in pd.read_csv(csv_file, chunksize=1000):
    # Append all rows to a new database table, which we name 'genes':
    c.to_sql('genes', db, if_exists='replace')
db.close()

# ### Result pre-filtering #######################################################
# #
# conn = sqlite3.connect(sqlite_file)
# Delete all YAMLs matching the criteria:
# -
# sql_query = pd.read_sql_query('''
# SELECT *, abs(dna_end-dna_start) AS dna_len FROM genes
# WHERE hit_nr > 0
# ORDER BY dna_start ASC;''', conn)
# df = pd.DataFrame(sql_query)
#
# # Remove filtered YAML-files
# for yaml in df:
#     filtered_yaml_file = os.path.join(result_path, df.filepath)
#     os.remove(filtered_yaml_file)
