#!/usr/bin/env bash

chunk_size=1000
file=MagOryVsMagMQOP_allGenesPredictions.yaml
max=$(grep -c "### end of PyScipio output" $file)
num=$(($max / $chunk_size))
cp -f $file rest.yaml

for ((i=1;i<=$num;i++));
do
  csplit -n ${#chunk_size} -s -k rest.yaml '/### end of PyScipio output/+1' {$((chunk_size-1))}
  mv -f "xx$chunk_size" rest.yaml
  cat xx* > "chunk_$((i*chunk_size)).yaml"
  rm -f xx*
done

# csplit MagOryVsMagMQOP_allGenesPredictions_100.yaml '/### end of PyScipio output/+1' {9}
# mv xx10 > rest.yaml
# cat xx* > MagOryVsMagMQOP_allGenesPredictions_10.yaml
# rm -f xx*
