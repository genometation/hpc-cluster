### Test-Set: Magnaporthe oryzae (Pyricularia oryzae) ##########################
#
# Query-Protein-Set:  Magnaporthe oryzae (Pyricularia oryzae)
# Targets: Magnaporthe Genome
#
/mnt/ncbi_assemblies/AACU00000000/AACU03000000/AACU03000000.fasta
/mnt/ncbi_assemblies/AACU00000000/AACU03000000/JH165175_JH165220.fasta
/mnt/ncbi_assemblies/VCMT00000000/VCMT01000000/VCMT01000000.fasta
/mnt/ncbi_assemblies/MQOR00000000/MQOR01000000/MQOR01000000.fasta
/mnt/ncbi_assemblies/MQOP00000000/MQOP01000000/MQOP01000000.fasta
/mnt/ncbi_assemblies/JAAXMU000000000/JAAXMU010000000/JAAXMU010000000.fasta
/mnt/ncbi_assemblies/JAATWF000000000/JAATWF010000000/JAATWF010000000.fasta
/mnt/ncbi_assemblies/JAATWG000000000/JAATWG010000000/JAATWG010000000.fasta
/mnt/ncbi_assemblies/JGVY00000000/JGVY01000000/JGVY01000000.fasta
/mnt/ncbi_assemblies/MQRB00000000/MQRB01000000/MQRB01000000.fasta
/mnt/ncbi_assemblies/PJZD00000000/PJZD01000000/PJZD01000000.fasta
/mnt/ncbi_assemblies/NKQG00000000/NKQG01000000/NKQG01000000.fasta
/mnt/ncbi_assemblies/LXOQ00000000/LXOQ01000000/LXOQ01000000.fasta
/mnt/ncbi_assemblies/AZSW00000000/AZSW01000000/AZSW01000000.fasta
/mnt/ncbi_assemblies/JMQU00000000/JMQU01000000/JMQU01000000.fasta
/mnt/ncbi_assemblies/AXDJ00000000/AXDJ01000000/AXDJ01000000.fasta
/mnt/ncbi_assemblies/PQBJ00000000/PQBJ01000000/PQBJ01000000.fasta
/mnt/ncbi_assemblies/ANOW00000000/ANOW01000000/ANOW01000000.fasta
/mnt/ncbi_assemblies/PYSQ00000000/PYSQ01000000/PYSQ01000000.fasta
/mnt/ncbi_assemblies/RRCK00000000/RRCK01000000/RRCK01000000.fasta
/mnt/ncbi_assemblies/LOFB00000000/LOFB01000000/LOFB01000000.fasta
/mnt/ncbi_assemblies/ADBL00000000/ADBL01000000/ADBL01000000.fasta
/mnt/ncbi_assemblies/LNTO00000000/LNTO01000000/LNTO01000000.fasta
/mnt/ncbi_assemblies/PJYT00000000/PJYT01000000/PJYT01000000.fasta

# pyscipio_make_job_dir.sh
declare -a target_files=(
    "/mnt/ncbi_assemblies/AACU00000000/AACU03000000/AACU03000000.fasta"
    "/mnt/ncbi_assemblies/AACU00000000/AACU03000000/JH165175_JH165220.fasta"
    "/mnt/ncbi_assemblies/VCMT00000000/VCMT01000000/VCMT01000000.fasta"
    # ...
)
# pyscipio3_jobs.tmpl.yaml.jinja2
{%- set job_set = "magnaporyzae" %}
{%- set params = [{"job_dir": "job_target_magnap_AACU03000000_query_magnap_oryzae", "target_file": "AACU03000000.fasta", },
                  {"job_dir": "job_target_magnap_ADBL01000000_query_magnap_oryzae", "target_file": "ADBL01000000.fasta", },
                  {"job_dir": "job_target_magnap_ANOW01000000_query_magnap_oryzae", "target_file": "ANOW01000000.fasta", },
                  {"job_dir": "job_target_magnap_AXDJ01000000_query_magnap_oryzae", "target_file": "AXDJ01000000.fasta", },
                  {"job_dir": "job_target_magnap_AZSW01000000_query_magnap_oryzae", "target_file": "AZSW01000000.fasta", },
                  {"job_dir": "job_target_magnap_JAATWF010000000_query_magnap_oryzae", "target_file": "JAATWF010000000.fasta", },
                  {"job_dir": "job_target_magnap_JAATWG010000000_query_magnap_oryzae", "target_file": "JAATWG010000000.fasta", },
                  {"job_dir": "job_target_magnap_JAAXMU010000000_query_magnap_oryzae", "target_file": "JAAXMU010000000.fasta", },
                  {"job_dir": "job_target_magnap_JGVY01000000_query_magnap_oryzae", "target_file": "JGVY01000000.fasta", },
                  {"job_dir": "job_target_magnap_JH165175_JH165220_query_magnap_oryzae", "target_file": "JH165175_JH165220.fasta", },
                  {"job_dir": "job_target_magnap_JMQU01000000_query_magnap_oryzae", "target_file": "JMQU01000000.fasta", },
                  {"job_dir": "job_target_magnap_LNTO01000000_query_magnap_oryzae", "target_file": "LNTO01000000.fasta", },
                  {"job_dir": "job_target_magnap_LOFB01000000_query_magnap_oryzae", "target_file": "LOFB01000000.fasta", },
                  {"job_dir": "job_target_magnap_LXOQ01000000_query_magnap_oryzae", "target_file": "LXOQ01000000.fasta", },
                  {"job_dir": "job_target_magnap_MQOP01000000_query_magnap_oryzae", "target_file": "MQOP01000000.fasta", },
                  {"job_dir": "job_target_magnap_MQOR01000000_query_magnap_oryzae", "target_file": "MQOR01000000.fasta", },
                  {"job_dir": "job_target_magnap_MQRB01000000_query_magnap_oryzae", "target_file": "MQRB01000000.fasta", },
                  {"job_dir": "job_target_magnap_NKQG01000000_query_magnap_oryzae", "target_file": "NKQG01000000.fasta", },
                  {"job_dir": "job_target_magnap_PJYT01000000_query_magnap_oryzae", "target_file": "PJYT01000000.fasta", },
                  {"job_dir": "job_target_magnap_PJZD01000000_query_magnap_oryzae", "target_file": "PJZD01000000.fasta", },
                  {"job_dir": "job_target_magnap_PQBJ01000000_query_magnap_oryzae", "target_file": "PQBJ01000000.fasta", },
                  {"job_dir": "job_target_magnap_PYSQ01000000_query_magnap_oryzae", "target_file": "PYSQ01000000.fasta", },
                  {"job_dir": "job_target_magnap_RRCK01000000_query_magnap_oryzae", "target_file": "RRCK01000000.fasta", },
                  {"job_dir": "job_target_magnap_VCMT01000000_query_magnap_oryzae", "target_file": "VCMT01000000.fasta", },]
%}

### Test-Set: Arabidopsis thaliana #############################################
#
# Query-Protein-Set:  Arabidopsis thaliana
# Targets: Arabidopsis Genome
/mnt/ncbi_assemblies/JSAD00000000/JSAD01000000/JSAD01000000.fasta
/mnt/ncbi_assemblies/OMOL00000000/OMOL01000000/OMOL01000000.fasta
/mnt/ncbi_assemblies/OFEF00000000/OFEF01000000/OFEF01000000.fasta
/mnt/ncbi_assemblies/AFNB00000000/AFNB01000000/AFNB01000000.fasta
/mnt/ncbi_assemblies/LUHQ00000000/LUHQ01000000/LUHQ01000000.fasta
/mnt/ncbi_assemblies/CACSHJ000000000/CACSHJ010000000/CACSHJ010000000.fasta
/mnt/ncbi_assemblies/LXSY00000000/LXSY01000000/LXSY01000000.fasta
/mnt/ncbi_assemblies/CAEFZF000000000/CAEFZF010000000/CAEFZF010000000.fasta
/mnt/ncbi_assemblies/MJMM00000000/MJMM01000000/MJMM01000000.fasta
/mnt/ncbi_assemblies/FJVB00000000/FJVB01000000/FJVB01000000.fasta
/mnt/ncbi_assemblies/OANL00000000/OANL01000000/OANL01000000.fasta
/mnt/ncbi_assemblies/CADHRU000000000/CADHRU010000000/CADHRU010000000.fasta
/mnt/ncbi_assemblies/RCNM00000000/RCNM01000000/RCNM01000000.fasta
/mnt/ncbi_assemblies/JAAEDN000000000/JAAEDN010000000/JAAEDN010000000.fasta
/mnt/ncbi_assemblies/BASP00000000/BASP01000000/BASP01000000.fasta
/mnt/ncbi_assemblies/ADBK00000000/ADBK01000000/ADBK01000000.fasta

# pyscipio_make_job_dir.sh
declare -a target_files=(
    "/mnt/ncbi_assemblies/JSAD00000000/JSAD01000000/JSAD01000000.fasta"
    "/mnt/ncbi_assemblies/OMOL00000000/OMOL01000000/OMOL01000000.fasta"
    "/mnt/ncbi_assemblies/OFEF00000000/OFEF01000000/OFEF01000000.fasta"
    "/mnt/ncbi_assemblies/AFNB00000000/AFNB01000000/AFNB01000000.fasta"
    "/mnt/ncbi_assemblies/LUHQ00000000/LUHQ01000000/LUHQ01000000.fasta"
    "/mnt/ncbi_assemblies/CACSHJ000000000/CACSHJ010000000/CACSHJ010000000.fasta"
    "/mnt/ncbi_assemblies/LXSY00000000/LXSY01000000/LXSY01000000.fasta"
    "/mnt/ncbi_assemblies/CAEFZF000000000/CAEFZF010000000/CAEFZF010000000.fasta"
    "/mnt/ncbi_assemblies/MJMM00000000/MJMM01000000/MJMM01000000.fasta"
    "/mnt/ncbi_assemblies/FJVB00000000/FJVB01000000/FJVB01000000.fasta"
    "/mnt/ncbi_assemblies/OANL00000000/OANL01000000/OANL01000000.fasta"
    "/mnt/ncbi_assemblies/CADHRU000000000/CADHRU010000000/CADHRU010000000.fasta"
    "/mnt/ncbi_assemblies/RCNM00000000/RCNM01000000/RCNM01000000.fasta"
    "/mnt/ncbi_assemblies/JAAEDN000000000/JAAEDN010000000/JAAEDN010000000.fasta"
    "/mnt/ncbi_assemblies/BASP00000000/BASP01000000/BASP01000000.fasta"
    "/mnt/ncbi_assemblies/ADBK00000000/ADBK01000000/ADBK01000000.fasta"
)
# pyscipio3_jobs.tmpl.yaml.jinja2
{%- set job_set = "arabidthaliana" %}
{%- set params = [
      {"job_dir": "job_target_arabid_JSAD01000000_query_arabid_thaliana", "target_file": "JSAD01000000.fasta", },
      {"job_dir": "job_target_arabid_OMOL01000000_query_arabid_thaliana", "target_file": "OMOL01000000.fasta", },
      {"job_dir": "job_target_arabid_OFEF01000000_query_arabid_thaliana", "target_file": "OFEF01000000.fasta", },
      {"job_dir": "job_target_arabid_AFNB01000000_query_arabid_thaliana", "target_file": "AFNB01000000.fasta", },
      {"job_dir": "job_target_arabid_LUHQ01000000_query_arabid_thaliana", "target_file": "LUHQ01000000.fasta", },
      {"job_dir": "job_target_arabid_CACSHJ010000000_query_arabid_thaliana", "target_file": "CACSHJ010000000.fasta", },
      {"job_dir": "job_target_arabid_LXSY01000000_query_arabid_thaliana", "target_file": "LXSY01000000.fasta", },
      {"job_dir": "job_target_arabid_CAEFZF010000000_query_arabid_thaliana", "target_file": "CAEFZF010000000.fasta", },
      {"job_dir": "job_target_arabid_MJMM01000000_query_arabid_thaliana", "target_file": "MJMM01000000.fasta", },
      {"job_dir": "job_target_arabid_FJVB01000000_query_arabid_thaliana", "target_file": "FJVB01000000.fasta", },
      {"job_dir": "job_target_arabid_OANL01000000_query_arabid_thaliana", "target_file": "OANL01000000.fasta", },
      {"job_dir": "job_target_arabid_CADHRU010000000_query_arabid_thaliana", "target_file": "CADHRU010000000.fasta", },
      {"job_dir": "job_target_arabid_RCNM01000000_query_arabid_thaliana", "target_file": "RCNM01000000.fasta", },
      {"job_dir": "job_target_arabid_JAAEDN010000000_query_arabid_thaliana", "target_file": "JAAEDN010000000.fasta", },
      {"job_dir": "job_target_arabid_BASP01000000_query_arabid_thaliana", "target_file": "BASP01000000.fasta", },
      {"job_dir": "job_target_arabid_ADBK01000000_query_arabid_thaliana", "target_file": "ADBK01000000.fasta", },
    ]
%}
