#!/usr/bin/env bash

# Benchmark
t_start=$(date -d "now" '+%s')

# Test: PyScipio
# python -m  scipio.cmd --target=Magnaporthe_grisea_70_15_v3_chromosome.fasta --query=q_genes_10/gene_00000.fas
# Test: Parallel processing
./pyscipio_parallel.py --target=Magnaporthe_grisea_70_15_v3_chromosome.fasta --query=q_genes_10
./pyscipio_parallel.py --target=Magnaporthe_grisea_70_15_v3_chromosome.fasta --query=q_genes_100
# ./pyscipio_parallel.py --target=Magnaporthe_grisea_70_15_v3_chromosome.fasta --query=q_genes_1k
# ./pyscipio_parallel.py --target=Magnaporthe_grisea_70_15_v3_chromosome.fasta --query=q_genes_10k

# Benchmark
t_end=$(date -d "now" '+%s')
t_diff=$((t_end - t_start))
echo "Time spent: ${t_diff} sec."
