#!/usr/bin/env bash

# Benchmark
t_start=$(date -d "now" '+%s')

make clean

# # Run docker commands
# docker-compose up -d --scale pyscipio=8
# docker exec <container> bash -c "command1 ; command2 ; command3"
for f in q_genes/*.fas;
do
    bf=$(basename "$f" | cut -d. -f1)
    docker exec pyscipio3_pyscipio_1 /bin/bash -c " \
        cd test_data/pyscipio/target_magnap_grisae_query_magnap_oryzae; \
        source /home/genwork/.bashrc && make test_${bf}"
done

# Benchmark
t_end=$(date -d "now" '+%s')
t_diff=$((t_end - t_start))
echo "Time spent: ${t_diff} sec."
