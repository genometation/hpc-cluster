#!/usr/bin/env bash

# Note:
# Script needs to be run in environment with same NCBI-BLAST version (makeblastdb)
# as installed in pipeline Docker container (e.g. PyScipio3)

# Documentation: Example usage
# source pyscipio3/venv/bin/activate;
# cd test_data/pyscipio/job_target_magnap_job_AACU03000000_query_magnap_oryzae;
# ./pyscipio_eval_params.sh target/AACU03000000.fasta q_genes_10

# Define a timestamp function
timestamp() {
  date +"%s" # current time
}

### Params and Vars ############################################################

# Get params
target=${1}
geneset=${2:-q_genes_10}
config="conf"

## Set vars and directory names
user_dir="/home/genwork"
pyscipio_conf_dir="/home/genwork/pyscipio3/tests/_pyscipio_conf"
# pyscipio_conf_dir="/home/genwork/pyscipio3/tests/_pyscipio_conf/autogen"
result_dir="run__$(timestamp)"
temp="/home/genwork/tmp"

## List of config files
declare -a config_files=(
    # 'pyscipio.no_args.conf'
    # 'pyscipio.no_args.blat.conf'
    # 'pyscipio.default.conf'
    # 'pyscipio.default.blat.conf'
    'pyscipio.fungi.default.conf'
    'pyscipio.fungi.default.blat.conf'
    # 'pyscipio.fungi.default.hicut.conf'
    # 'pyscipio.fungi.default.locut.conf'
    # 'pyscipio.fungi.strict.conf'
    # 'pyscipio.fungi.strict.blat.conf'
    # 'pyscipio.fungi.weak.conf'
    # 'pyscipio.fungi.weak.blat.conf'
)

### Pull job files into Docker #####################################################
# printf "\n- Pull job files into Docker\n"
# Make job files local available on node
# - Script is called from shared NFS mount directory '/mnt/test/pyscipio/job_target_...'
# - Copy content of current folder to Node's user directory (Docker entry directory)
nfs_job_dir=$(pwd)
nfs_job_dir_base=$(basename "${nfs_job_dir}")
node_job_dir="${user_dir}/run_local_${nfs_job_dir_base}"
declare -a src_files=(
    "${config}"
    "pyscipio_*"
    "${geneset}"
    "target"
)
rm -rf ${node_job_dir}
mkdir -p ${node_job_dir}
for src_file in "${src_files[@]}"
do
    # Variant 1: Use cp
    # -L: Copy directory tree that symlink ${src_file} points to
    cp -RL ${nfs_job_dir}/${src_file} ${node_job_dir}
done

# Switch to local job directory
cd ${node_job_dir}

### Run parameter tests from array #############################################
for config_file in "${config_files[@]}"
do
    config="${pyscipio_conf_dir}/${config_file}"
### Run parameter tests from autogen directory #################################
# for config in ${pyscipio_conf_dir}/*.conf;
# do
#     config_file=$(basename ${config})
    ### Run PyScipio with specified target/query and config files ##############
    printf "\n- Run config: ${config_file}\n\n"
    # printf '%s\n' "./pyscipio_parallel.py --procs=96 -vvvv --mode=multi_thread --temp=${temp} " \
    # "--target=${target} --query=${geneset} --config=${config} --results=${result_dir}"
    time ./pyscipio_parallel.py --procs=96 --mode=multi_thread --temp=${temp} \
     --target=${target} --query=${geneset} --config=${config} --results=${result_dir}

    ### Evaluation #############################################################
    # printf "\n- Short result evaluation\n\n"
    printf "\n"
    # Show table
    ./pyscipio_simple_job_evaluation.sh ${result_dir}
    # Write CSV
    csv_header="config;genes;predictions;complete;incomplete;missing;alt_complete;alt_incomplete"
    [ ! -f "parameter_optimization.csv" ] && echo ${csv_header} > parameter_optimization.csv
    ./pyscipio_simple_job_evaluation.sh ${result_dir} "csv" ${config_file} >> parameter_optimization.csv

    ### Cleanup ################################################################
    # printf "\n- Cleanup files\n"
    printf "\n"
    rm -rf ${result_dir}
    rm -rf ${temp}
done

### Result #####################################################################
printf "\n- Tested parameter set results (CSV) \n\n"
cat parameter_optimization.csv
printf "\n"
