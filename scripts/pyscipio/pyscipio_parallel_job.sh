#!/usr/bin/env bash

# Documentation: Example usage
# source pyscipio3/venv/bin/activate;
# cd test_data/pyscipio/test_target_magnap_oryzae_query_magnap_oryzae;
# ./pyscipio_parallel_job.sh target/Magnaporthe_oryzae.MG8.dna.toplevel.fa q_genes pyscipio.default.conf
# ./pyscipio_parallel_job.sh target/AACU03000000.fasta q_genes_10 pyscipio.default.fungi.conf

# Define a timestamp function
timestamp() {
  date +"%s" # current time
}

### Params and Vars ############################################################

# Get params
target=${1}
geneset=${2:-q_genes_10}
config_file=${3:-"pyscipio.default.conf"}

## Set vars and directory names
user_dir="/home/genwork"
pyscipio_conf_dir="/home/genwork/pyscipio3/tests/_pyscipio_conf"
config_base=$(basename "$config_file" | cut -d. -f2-3 | tr -c '[:alnum:]_' '_')
result_dir="run__${geneset}__${config_base}_$(timestamp)"
temp="/home/genwork/tmp"

### Pull job files to Node #####################################################
printf "\n- Pull job files to Node\n"
# Make job files local available on node
# - Script is called from shared NFS mount directory '/mnt/test/pyscipio/job_target_...'
# - Copy content of current folder to Node's user directory (Docker entry directory)
nfs_job_dir=$(pwd)
nfs_job_dir_base=$(basename "${nfs_job_dir}")
node_job_dir="${user_dir}/run_local_${nfs_job_dir_base}"
declare -a src_files=(
    "${config_file}"
    "pyscipio_*"
    "${geneset}.tar"
    "target"
)
# Remove old local folder
rm -rf ${temp}
rm -rf ${node_job_dir}
# Create new local folder
mkdir -p ${node_job_dir}
time for src_file in "${src_files[@]}"
do
    # Variant 1: Use cp
    # -L: Copy directory tree that symlink ${src_file} points to
    cp -RL ${nfs_job_dir}/${src_file} ${node_job_dir}
    # Variant 2: Use cp and tar
    # [ -f "${nfs_job_dir}/${src_file}" ] && cp -RL ${nfs_job_dir}/${src_file} ${node_job_dir}
    # # Use tar to transfer and copy
    # [ -d "${nfs_job_dir}/${src_file}" ] && tar -C ${nfs_job_dir} -hcf - ${src_file} \
    # | tar -C ${node_job_dir} -xf -
done

# Extract in local job directory
cd ${node_job_dir}
tar xf "${geneset}.tar"

### Run job ####################################################################
printf "\n- Run job\n"
# Start PyScipio with specified target/query and config files
config=${pyscipio_conf_dir}/${config_file}
printf '%s\n' "./pyscipio_parallel.py --procs=96 -vvvv --mode=multi_thread --temp=${temp} " \
"--target=${target} --query=${geneset} --config=${config} --results=${result_dir}"
time ./pyscipio_parallel.py --procs=96 -vvvv --mode=multi_thread --temp=${temp} \
   --target=${target} --query=${geneset} --config=${config} --results=${result_dir}

### Evaluation #################################################################
printf "\n- Short result evaluation\n\n"
time ./pyscipio_simple_job_evaluation.sh ${result_dir}

### Cleanup ####################################################################
printf "\n- Cleanup files\n"
# Remove stdout files and empty error logs
printf "\nCleanup empty and duplicate error logs:\n"
find ${result_dir} -name '*.stdout' -type f -delete
find ${result_dir} -name '*.log' -type f -empty -delete
# Remove duplicates error log and show errors
shopt -s nullglob
if [[ -n $(echo ${result_dir}/*.log) ]]
then
  mkdir -p ${result_dir}/logs
  mv ${result_dir}/*.log ${result_dir}/logs
  cd ${result_dir}/logs
  # Remove identical error log files
  declare -A arr
  shopt -s globstar
  for file in **; do
    [[ -f "${file}" ]] || continue
    read cksm _ < <(md5sum "${file}")
    if ((arr[${cksm}]++)); then
      rm -f ${file}
    fi
  done
  # List error logs
  printf "\nShow errors:\n"
  tail -n +1 *.err.log
fi

### Prefilter Scipio results ###################################################
cd ${node_job_dir}
# Folder for prepared results to
prepared_dir="copy_to_nfs"
mkdir -p ${prepared_dir}
#
printf "\n- Prefilter PyScipio results\n"
find ${result_dir} -maxdepth 2 -type f -name '*.yaml' > yamls_unsorted.txt
sort yamls_unsorted.txt > ${prepared_dir}/yamls.txt
find ${result_dir} -maxdepth 2 -type f -name '*_0_.yaml' > /yamls_first_unsorted.txt
sort yamls_first_unsorted.txt > ${prepared_dir}/yamls_first.txt
time python -W ignore pyscipio_prefilter_results.py ${prepared_dir}/yamls.txt ${nfs_job_dir}

### Push back results to NFS-storage ###########################################
printf "\n- Push back results to NFS-storage\n"

### Prepare results locally
# Switch to local job directory on Node
cd ${node_job_dir}
# # Use tar archive
# archive="full_scipio_results.tar"
# tar --sort=name -cf ${archive} ${result_dir}
## Compose different result sets (YAMLs)
# - All Genes predictions
set_01='allGenesPredictions'
# # Option 1: Extract from tar
# tar -O -xf ${archive} --wildcards "run__*/gene_*/*.yaml" > "${set_01}.yaml"
# Option 2: Use file-list
while read F; do cat $F >> "${set_01}.yaml"; done < yamls.txt
# Remove lines '---' from concatenated YAML
# sed -i '8,$ {/---/d}' "${set_01}.yaml"
tar -czf "${set_01}.yaml.tar.gz" "${set_01}.yaml"
# - All Genes first prediction
set_02='allGenesFirstPrediction'
# # Option 1: Extract from tar
# tar -O -xf ${archive} --wildcards "run__*/gene_*/*_0_.*" > "${set_02}.yaml"
# Option 2: Use file-list
while read F; do cat $F >> "${set_02}.yaml"; done < yamls.txt
# sed -i '8,$ {/---/d}' "${set_02}.yaml"
tar -czf "${set_02}.yaml.tar.gz" "${set_02}.yaml"
# Copy all to result_dir
# mv ${archive} ${prepared_dir}
mv ${set_01}.* ${prepared_dir}
mv ${set_02}.* ${prepared_dir}
mv ${prepared_dir} ${result_dir}

### Transfer data from node to NFS location
mkdir -p ${nfs_job_dir}/${result_dir}
# Copy result directory back to shared NFS job folder
time cp ${result_dir}/${prepared_dir}/* ${nfs_job_dir}/${result_dir}
# Copy PyScipio error logs for debugging
[ -d "${result_dir}/logs" ] && cp -R ${result_dir}/logs ${nfs_job_dir}/${result_dir}

# ### Evaluation (detailed) ######################################################
# cd ${node_job_dir}
# printf "\n- Detailed result evaluation\n"
# ./pyscipio_detailed_job_evaluation.sh $result_dir

### Final ######################################################################
printf "\n- Job Done\n"
