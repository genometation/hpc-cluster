#!/bin/bash

sudo du -h --max-depth=0 /boot
sudo du -h --max-depth=0 /cdrom
sudo du -h --max-depth=0 /dev
sudo du -h --max-depth=0 /etc
sudo du -h --max-depth=0 /home
sudo du -h --max-depth=0 /media
# sudo du -h --max-depth=0 /mnt
# sudo du -h --max-depth=0 /nfs
sudo du -h --max-depth=0 /opt
sudo du -h --max-depth=0 /proc
sudo du -h --max-depth=0 /root
sudo du -h --max-depth=0 /run
sudo du -h --max-depth=0 /snap
sudo du -h --max-depth=0 /srv
sudo du -h --max-depth=0 /swap.img
sudo du -h --max-depth=0 /sys
sudo du -h --max-depth=0 /tmp
sudo du -h --max-depth=0 /Users
sudo du -h --max-depth=0 /usr
sudo du -h --max-depth=0 /var
