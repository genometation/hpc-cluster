#!/bin/bash

## Backup: Postgres databases (keep versions of last 7 days)

_dir="/home/backup"
_offsite="/nfs/zfs_adenin/backup/databases"
_dow="$(date +'%w')"
_woy="$(date +'%W')"

## Full backups: Databases to backup
declare -a dbs=("airflow"
                "maasdb"
                "genometation_db_default"
                "genometation_db_import"
                "genometation_db_valid"
                "genometation_db_live"
                )
for db in "${dbs[@]}"
do
   echo "Backup $db"
   pg_dump -U postgres ${db} | gzip -9 > ${_dir}/${db}.daily.${_dow}.sql.gz
   cp ${_dir}/${db}.daily.${_dow}.sql.gz ${_dir}/${db}.weekly.${_woy}.sql.gz
   cp ${_dir}/${db}.daily.*.sql.gz ${_offsite}
   cp ${_dir}/${db}.weekly.*.sql.gz ${_offsite}
   rm -f ${_dir}/${db}.daily.latest.sql.gz && ln -s ${_dir}/${db}.daily.${_dow}.sql.gz ${_dir}/${db}.daily.latest.sql.gz
done

## Data-only backups: Databases to backup
declare -a data_dbs=("genometation_db_default"
                     "genometation_db_import"
                     "genometation_db_valid"
                     "genometation_db_live"
                     )
for db in "${data_dbs[@]}"
do
   echo "Backup $db"
   pg_dump -U postgres -a -T 'django_*' ${db} | gzip -9 > ${_dir}/${db}.data_only_wo_django.daily.${_dow}.sql.gz
   cp ${_dir}/${db}.data_only_wo_django.daily.${_dow}.sql.gz ${_dir}/${db}.data_only_wo_django.weekly.${_woy}.sql.gz
   cp ${_dir}/${db}.data_only_wo_django.daily.*.sql.gz ${_offsite}
   cp ${_dir}/${db}.data_only_wo_django.weekly.*.sql.gz ${_offsite}
   rm -f ${_dir}/${db}.data_only_wo_django.daily.latest.sql.gz && ln -s ${_dir}/${db}.data_only_wo_django.daily.${_dow}.sql.gz ${_dir}/${db}.data_only_wo_django.daily.latest.sql.gz
done

## Remove weekly backups _dir to save storage
rm -f ${_dir}/*.weekly.*.sql.gz
