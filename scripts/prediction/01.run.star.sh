#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

unset FASTQ1
unset FASTQ2
unset FORCEOVERWRITE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 01.run.star.sh -x all -g [id] -i [path] <optional -j [path]> <optional -o [path] -f>

-x [index | align | all]  Mode of execution, either index (generate genome index), align (align reads) or all (both)
-g [id]                   genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]                 path to SRR input file, e.g. /mnt/ncbi_sra/SRRxxxxx/SRRxxxxx.fastq
-j [path]                 path to SRR input file 2 in case of paired-end data, e.g. /mnt/ncbi_sra/SRRxxxxx/SRRxxxxx_2.fastq
-o [path]                 basic output data directory (will be the current directory if option not set)
                          Results will be written into basic_output_data_dir/prediction/star
-f                        force overwrite

example:
./01.run.star.sh -x all -g JAATWF010000000 -i /mnt/ncbi_sra/SRR15506173/SRR15506173_1.fastq -j /mnt/ncbi_sra/SRR15506173/SRR15506173_2.fastq -o /mnt/customer/self
./01.run.star.sh -x index -g JAATWF010000000 -o /mnt/customer/self

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp

OPTIND=1
while getopts ":g:fi:j:o:x:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      FASTQ1=$OPTARG
      ;;
    j )
      FASTQ2=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    x )
      EXECMODE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

ALLOWEDEXECMODE="index align all"
ALLOWEDEXECMODE=$( tr '[:upper:]' '[:lower:]' <<<"$ALLOWEDEXECMODE" )
[[ -n ${EXECMODE} ]] && EXECMODE=$( tr '[:upper:]' '[:lower:]' <<<"${EXECMODE}" )
if [[ -n ${EXECMODE} ]] && ! [[ ${ALLOWEDEXECMODE} =~ (^|[[:space:]])$EXECMODE($|[[:space:]]) ]]; then
  echo "Please provide an allowed argument for mode of execution!"
  exit 1
fi
# Invoking the config-script only if star-script is not run from "multi-STAR".
if [ -z "${MULTI_STAR}" ]; then
  # The config-script will set all PATH-variables, check for presence or mkdir subfolders.
  # Turn debugging on within the config-script.
  SCRIPT="star"
  . config.sh

  STATUSFILE="01.run.star"_"${DATE_SAVED}".out
  LOGFILE="01.run.star"_"${DATE_SAVED}".log

#  METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#  [ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"
#
  STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
  LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
  ### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
  exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)
fi

echo "#####################################################################################"
echo ""
echo "01.run.star.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

# STAR --option1-name option1-value(s) --option2-name option2-value(s) ...
# --runThreadN           number of threads
# --runMode              genomeGenerate mode
# --genomeDir            /path/to/store/genome_indices
# --genomeFastaFiles     /path/to/FASTA_file
# --genomeSAindexNbases  int: length (bases) of the SA pre-indexing string. Typically between 10 and 15.
#                        Longer strings will use much more memory, but allow faster searches. For small
#                        genomes, the parameter –genomeSAindexNbases must be scaled down to
#                        min(14, log2(GenomeLength)/2 - 1). default: 14

# Step 1: Creating a genome index
if [[ ${EXECMODE} == "index" ]] || [[ ${EXECMODE} == "all" ]]; then
  if [ ! -d "${GENOME_DIR}"/star_genome_index ] || [ -n "${FORCEOVERWRITE}" ]; then
    # Compute length of the SA pre-indexing string (as param for genomeSAindexNbases)
    LOG2_NRBASES=$( echo 'l(NRBASES)/l(2)' | bc -l )
    INDEX_NBASES=$( echo "scale=2; $LOG2_NRBASES/2 - 1" | bc )
    ROUNDED_NBASES=$( printf "%.0f\n" "${INDEX_NBASES}" )

    mkdir -p "${GENOME_DIR}"/star_genome_index
    echo "Creating genome index ..."
    echo ""
    # For debugging
    # echo "STAR --runThreadN 6 --runMode genomeGenerate --genomeDir ${GENOME_DIR}/genome_index_${GENOMEFILEID} \
    # --genomeFastaFiles ${GENOME_DIR}/${GENOMEFILEID}.min.fasta --genomeSAindexNbases ${ROUNDED_NBASES}"
    STAR --runThreadN 6 \
    --runMode genomeGenerate \
    --genomeDir "${GENOME_DIR}"/star_genome_index \
    --genomeFastaFiles "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
    --genomeSAindexNbases "${ROUNDED_NBASES}"
  else
    echo "Genome index already created, moving on to STAR alignment ..."
    echo ""
  fi
fi
  # --outFileNamePrefix "${OUT_DIR}"/"${SRAACC}"/star_aligned \

# Step 2: Aligning reads
# --readFilesIn         default: Read1 Read2
#                       string(s): paths to files that contain input read1 (and, if needed, read2)
# --outFileNamePrefix   output files name prefix (including full or relative path) (default: ./)
# --limitBAMsortRAM     default: 0
#                       int>=0: maximum available RAM (bytes) for sorting BAM. If =0, it will be set
#                       to the genome index size. 0 value can only be used with –genomeLoadNoSharedMemory option.
# --outSAMtype          default: SAM
#                       strings: type of SAM/BAM output
#                       1st word: BAM (output BAM without sorting); SAM (output SAM without sorting); None (no SAM/BAM output)
#                       2nd, 3rd: Unsorted (standard unsorted); SortedByCoordinate (sorted by coordinate. This option
#                       will allocate extra memory for sorting which can be specified by –limitBAMsortRAM.). Both
#                       "Unsorted SortedByCoordinate" will output both unsorted and sorted files.
# --outSAMstrandField   default: None
#                       None (not used); intronMotif (strand derived from the intron motif. This option changes the output
#                       alignments: reads with inconsistent and/or non-canonical introns are filtered out.)
#                       intronMotif is required for StringTie
# --outSAMunmapped      default: None
#                       string(s): output of unmapped reads in the SAM format
#                       1st word: None (no output); Within (output unmapped reads within the main SAM file (i.e.Aligned.out.sam))
#                       2nd word: KeepPairs (record unmapped mate for each alignment, and, in case of unsorted
#                       output, keep it adjacent to its mapped mate. Only affects multi-mapping reads.)
# --outWigType          default: None
#                       string(s): type of signal output, e.g. ”bedGraph” OR ”bedGraph read1 5p”.
#                       Requires sorted BAM: –outSAMtype BAM SortedByCoordinate .
#                       1st word: None (no signal output); bedGraph (bedGraph format); wiggle (wiggle format)
#                       2nd word: read1 5p (signal from only 5’ of the 1st read, useful for CAGE/RAMPAGE etc); read2 (signal from only 2nd read)
# --outWigStrand        default: Stranded
#                       string: strandedness of wiggle/bedGraph output
#                       Stranded (separate strands, str1 and str2); Unstranded (collapsed strands)
# --alignIntronMin      default: 21
#                       minimum intron size: genomic gap is considered intron if its length>=alignIntronMin, otherwise it is considered Deletion
# --alignIntronMax      default: 0    (Encode standard: 1000000)
#                       maximum intron size; if 0, max intron size will be determined by (2ˆwinBinNbits)*winAnchorDistNbins (defaults to 589824)
# --outSJfilterIntronMaxVsReadN  default: 50000 100000 200000
#                       N integers>=0: maximum gap allowed for junctions supported by 1,2,3,,,N reads
#                       i.e. by default junctions supported by 1 read can have gaps <=50000b, by 2 reads: <=100000b,
#                       by 3 reads: <=200000. by >=4 reads any gap <=alignIntronMax
#                       does not apply to annotated junctions
# --winBinNbits         default: 16
#                       int>0: =log2(winBin), where winBin is the size of the bin for the
#                       windows/clustering, each window will occupy an integer number of bins.
# --winAnchorDistNbins  default: 9
#                       int>0: max number of bins between two anchors that allows aggregation of anchors into one window
# --outFilterMismatchNmax  default: 10
#                       int: alignment will be output only if it has no more mismatches than this value.

if [[ ${EXECMODE} == "align" ]] || [[ ${EXECMODE} == "all" ]]; then
  FASTQ_FILE_NAME=$( basename "${FASTQ1}" )
  # BASE_DIR=$( dirname "$(dirname "${fastq1_path}")" )
  SRAACC=$( echo "${FASTQ_FILE_NAME}" | sed -n -E 's/^(SRR[0-9]{8})(|_[12])\.fastq$/\1/p' )

  # Break if STAR results already available
  if [ -f "${OUT_DIR}"/"${SRAACC}"/Aligned.sortedByCoord.out.bam ] && [ -z "${FORCEOVERWRITE}" ]; then
    echo "SRR data ${SRAACC} have already been aligned to ${GENOMEFILEID}.min.fasta with STAR."
    exit 1
  fi

  # Set params for outSJfilterIntronMaxVsReadN  (default: 50000 100000 200000)
  SJFILTER=$( echo "$((INTRONMAX/8))" "$((INTRONMAX/4))" "$((INTRONMAX/2))" )
  echo "The following max intron lengths will be set depending on number of matching reads:"
  echo " 1 read   $((INTRONMAX/8))"
  echo " 2 read   $((INTRONMAX/4))"
  echo " 3 read   $((INTRONMAX/2))"
  echo ">4 read   ${INTRONMAX}    (more than 4 reads)"
  echo ""

  echo "Aligning reads with STAR ..."
  echo ""
  # For debugging
  #echo "STAR --genomeDir ${BASE_DIR}/genome_index_${GENOMEFILEID}/ --runThreadN 64 --readFilesIn ${FASTQ1} ${FASTQ2} \
  #--outFileNamePrefix ${BASE_DIR}/${OUT_DIR}/star/${SRAACC}/star_aligned --limitBAMsortRAM 1468827010 \
  #--outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outSAMunmapped Within --outSAMattributes Standard \
  #--outWigType wiggle --outWigStrand Unstranded --alignIntronMax ${INTRONMAX} --outSJfilterIntronMaxVsReadN ${SJFILTER}"

  STAR --genomeDir "${GENOME_DIR}"/star_genome_index \
  --runThreadN 64 \
  --readFilesIn "${FASTQ1}" "${FASTQ2}" \
  --outFileNamePrefix "${OUT_DIR}"/"${SRAACC}"/ \
  --limitBAMsortRAM 1468827010 \
  --outSAMtype BAM SortedByCoordinate \
  --outSAMstrandField intronMotif \
  --outSAMunmapped Within \
  --outSAMattributes Standard \
  --outWigType wiggle \
  --outWigStrand Unstranded \
  --alignIntronMax "${INTRONMAX}" \
  --outSJfilterIntronMaxVsReadN "${SJFILTER}" \
  --outFilterMismatchNmax 5

  if [ -d "${OUT_DIR}"/"${SRAACC}"/star_aligned_STARtmp ] || [ -d "${OUT_DIR}"/"${SRAACC}"/_STARtmp ];then
    echo "Cleaning empty temporary STAR directories ..."
    echo ""
    find "${OUT_DIR}"/"${SRAACC}" -depth -type d -empty -delete
  fi
fi
# Options not used
# --sjdbGTFfile         /path/to/GTF_file
#                       Chromosome names in the GTF file have to match the
#                       chromosome names in the FASTA genome sequence file
# --sjdbOverhang        readlength-1  (default=100)
# --outSAMattributes    default: Standard
#                       string: a string of desired SAM attributes, in the order desired for the output
#                       SAM. Tags can be listed in any combination/order.
#                       ***Presets:
#                       None (no attributes); Standard (NH HI AS nM); All (NH HI AS nM NM MD jM jI MC ch)
#                       ***Alignment:
#                       NH  number of loci the reads maps to: =1 for unique mappers, >1 for
#                           multimappers. Standard SAM tag.
#                       HI  multiple alignment index, starts with –outSAMattrIHstart (=1 by
#                           default). Standard SAM tag.
#                       AS  local alignment score, +1/-1 for matches/mismateches, score*
#                           penalties for indels and gaps. For PE reads, total score for two mates. Stadnard SAM tag.
#                       nM  number of mismatches. For PE reads, sum over two mates.
# --outSJfilterOverhangMin  default: 30 12 12 12
#                       4 integers: minimum overhang length for splice junctions on both sides for: (1)
#                       non-canonical motifs, (2) GT/AG and CT/AC motif, (3) GC/AG and CT/GC
#                       motif, (4) AT/AC and GT/AT motif. -1 means no output for that motif
#                       does not apply to annotated junctions

end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"