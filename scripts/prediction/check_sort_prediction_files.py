#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports
from optparse import OptionParser
import os
import sys

query_path = r"/mnt/c/Users/marti/hpc-cluster"


def get_genome_group_folders(path):
    genome_group_folders = [f.path for f in os.scandir(path) if f.is_dir()]
    return genome_group_folders


def get_genome_assembly_folders(path):
    genome_assembly_folders = []
    genome_group_dirs = get_genome_group_folders(path)
    for ggdir in genome_group_dirs:
        genome_assembly_folder = [f.path for f in os.scandir(ggdir) if f.is_dir()]
        for folder in genome_assembly_folder:
            genome_assembly_folders.append(folder)
    return genome_assembly_folders


def organize_rrna_data():
    # genome_group_dirs = get_genome_folders(query_path)
    # for ggdir in genome_group_dirs:
    #     genome_assembly_dirs = get_genome_folders(ggdir)
    #     for gadir in genome_assembly_dirs:
    #         print(dir)
    gaf = get_genome_assembly_folders(query_path)
    for d in gaf:
        print(d)


def main(args):
    """ ``Main:`` . """

    # Parse arguments
    parser = make_parser()
    (options, args) = parser.parse_args(args)
    if len(sys.argv[1:]) < 1:
        parser.print_help()
        parser.error("Not sufficient arguments given!")
    # print(options)

    if options.rrna_data:
        # (Re-)organize rRNA data into subdirectory, check rRNA data for completeness
        organize_rrna_data()


def make_parser():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 0.1415926")
    opt = parser.add_option
    opt('-v', '--verbose', action='count', default=0,
        help='Show verbose information (including progress)')
    opt('-r', '--rrna_data', action="store_true", dest="rrna_data",
        help='(Re-)organize rRNA data into subdirectory, check rRNA data for completeness')
    opt('-t', '--trna_data', action="store_true", dest="trna_data",
        help='(Re-)organize tRNA data into subdirectory, check tRNA data for completeness')
    opt('-s', '--stats_data', action="store_true", dest="stats_data",
        help='(Re-)organize genome stats data into subdirectory, check stats data for completeness')
    opt('-d', '--data_dir', metavar='dir',
        help='(Re-)organize rRNA data into subdirectory, check rRNA data for completeness')

    return parser


if __name__ == '__main__':
    main(sys.argv[1:])
