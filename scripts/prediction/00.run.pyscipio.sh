#!/usr/bin/env bash

# Species of Magnaporthe oryzae
time pyscipio_parallel_test.sh target/AACU03000000.fasta q_genes pyscipio.default.all.conf | tee pyscipio.default.all.log
time pyscipio_parallel_test.sh target/AACU03000000.fasta q_genes pyscipio.default.all.conf > pyscipio.default.all.log 2>&1 &
time pyscipio_parallel_test.sh target/JAATWF010000000.fasta q_genes pyscipio.default.all.conf | tee pyscipio.default.all.log
time pyscipio_parallel_test.sh target/JAATWF010000000.fasta q_genes pyscipio.default.all.conf > pyscipio.default.all.log 2>&1 &

# Arabidopsis in Beta vulgaris
# job_target_PCNB00000000_query_arabid_thaliana
time pyscipio_parallel_test.sh target/PCNB00000000.fasta q_genes pyscipio.plant.default.all.conf | tee pyscipio.plant.default.all.log
# Beta vulgaris subsp. vulgaris in PCNB00000000
# job_target_PCNB00000000_query_beta_vulagris
time pyscipio_parallel_test.sh target/PCNB00000000.fasta q_genes pyscipio.plant.default.all.conf > pyscipio.plant.default.all.log 2>&1 &
