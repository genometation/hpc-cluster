#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

ext_start=$(date +%s)
ext_start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 01b.run_multiple.star.sh -g [id] -i [path] <optional -o [path] -f>

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]        path to input file SRAdata.lst that lists the SRA data directories, one per row, e.g.
                      /mnt/ncbi_sra/SRRxxxxx
                      /mnt/ncbi_sra/SRRyyyyy
-o [path]        basic output data directory (will be the current directory if option not set)
                 Results will be written into basic_output_data_dir/prediction/star
-f               force overwrite

-h   Show this help

example:
./01b.run_multiple.star.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/SRAdata.lst -o /mnt/customer/self

EOF
exit 0
}

#-f               full, get all SRA-files in the input directory
#-r [first-last]  range of SRA-files (format SRR14672191-SRR14672195)

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fi:o:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      INPUTFILE=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

# Setting "multi-STAR" variable so that config-script is not invoked twice.
MULTI_STAR="true"
# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="star"
. config.sh

if [ -n "${FORCEOVERWRITE}" ]; then
  FORCEOVERWRITEOPTION=" -f"
else
  FORCEOVERWRITEOPTION=""
fi

STATUSFILE="01b.run_multiple.star"_"${DATE_SAVED}".out
LOGFILE="01b.run_multiple.star"_"${DATE_SAVED}".log

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

echo "#####################################################################################"
echo ""
echo "01b.run_multiple.star.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $ext_start_date"
echo ""
# Step 1: Get the paths of all raw data files
# done in config.sh script
# sets ${INPUTFILEFOLDER}

# Step 2: Determine, whether the raw data consists of single or paired-end reads
# done in config.sh script
# returns sra_file_list_unique_"${DATE_SAVED}".txt

# Step 3: Generate genome index (once per genome)
# 01.run.star will check whether index already exists
# echo "./01.run.star.sh ${FORCEOVERWRITEOPTION}-x index -g ${GENOMEFILEID} -o ${BASE_DIR}"
. ./01.run.star.sh -x index -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -o "${BASE_DIR}"

# Step 4: Run 01.run.star.sh on all data (in parallel)
MAX_PARALLEL=5
echo "Max ${MAX_PARALLEL} jobs in parallel ..."
echo ""
# The following line might better be de-commented when script is run manually. The line
# ensures that all processes started within the loop will be killed if the script is
# terminated prematurely. This is not needed within the pipeline, where it might lead
# to an unwanted EXIT because of an error within one of the executed scripts in the loop.
# trap 'echo killing all children; kill 0' EXIT

while IFS= read -r line || [[ -n $line ]]
  do
    sleep 10   # Important! Otherwise subprocesses might run out of tracking.
    SRA_FILE=$( echo "${line}" | sed -n -E 's/(^SRR[0-9]{8})[[:space:]][sp]/\1/p' )
    if [[ "${line}" =~ " s" ]]; then
      # echo "./01.run.star.sh -g ${GENOMEFILEID} -o ${BASE_DIR} -i ${INPUTFILEFOLDER}/${SRA_FILE}/${SRA_FILE}.fastq"
      . ./01.run.star.sh -x align -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -o "${BASE_DIR}" -i "${INPUTFILEFOLDER}"/"${SRA_FILE}"/"${SRA_FILE}".fastq &
    else
      # echo "./01.run.star.sh -g ${GENOMEFILEID} -o ${BASE_DIR} -i ${INPUTFILEFOLDER}/${SRA_FILE}/${SRA_FILE}_1.fastq -j ${INPUTFILEFOLDER}/${SRA_FILE}/${SRA_FILE}_2.fastq"
      . ./01.run.star.sh -x align -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -o "${BASE_DIR}" -i "${INPUTFILEFOLDER}"/"${SRA_FILE}"/"${SRA_FILE}"_1.fastq -j "${INPUTFILEFOLDER}"/"${SRA_FILE}"/"${SRA_FILE}"_2.fastq &
    fi
    # track job ids to wait for later
    pids[${SRAACC}]=$!
    # Wait here for any job to be finished, so there is a place to start next one.
    if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi
done < "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt

# wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done


ext_end=$(date +%s)
ext_end_date=$(date +%T)
runtime=$((ext_end-ext_start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $ext_end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"