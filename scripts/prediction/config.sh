#!/bin/bash

# Bash settings
# set -e
# set -u
# set -o pipefail
# Repeat executed commands
# set -x

#####################################################################################
#
#  General variables for use in prediction pipeline scripts
#

echo "#####################################################################################"
echo ""
echo "Setting environment variables ..."
echo ""

#GENOMEFILEFORMAT="^[A-Za-z0-9_]*\.fasta$"
#if [ -z "${GENOMEFILEID}" ] || [[ ! "${GENOMEFILEID}" =~ $G{ENOMEFILEFORMAT} ]]; then
#  echo "The genome file $GENOMEFILEID does not exist or has a wrong format, please provide a correct file and path!"
#  exit 1
#fi
if [ -z "${GENOMEFILEID}" ]; then
  echo "Please provide a genome file ID."
  exit 1  # die with error code 1
fi

if [ -n "${BASE_DIR}" ] && [ ! -d "${BASE_DIR}" ]; then
  echo "Provided output directory ${BASE_DIR} DOES NOT exist, please provide a correct path!"
  exit 1  # die with error code 1
fi
if [ -z "${BASE_DIR}" ]; then
  BASE_DIR=$(pwd)
fi
HOME="/home/genwork"
# GENOME_DIR="${BASE_DIR}"/target_"${GENOMEFILEID}"
# MAIN_OUT_DIR="${BASE_DIR}"/prediction_"${GENOMEFILEID}"
GENOME_DIR="${BASE_DIR}"/target
MAIN_OUT_DIR="${BASE_DIR}"/prediction
OUT_DIR="${MAIN_OUT_DIR}"/"${SCRIPT}"
[ ! -d "${GENOME_DIR}" ] && mkdir -p "${GENOME_DIR}"
echo "All data will be written into ${OUT_DIR} ..."
echo ""
[ ! -d "${OUT_DIR}" ] && mkdir -p "${OUT_DIR}"

METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"


################################################################################
# optional: Simplify fasta headers of target genome
if [ ! -f "${BASE_DIR}"/"${GENOMEFILEID}".fasta ]; then
  echo "Either target genome not available or wrong genome file ID."
  exit 1  # die with error code 1
elif [ -f "${BASE_DIR}"/"${GENOMEFILEID}".fasta ] && [ ! -f "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta ]; then
  echo "Simplifying fasta headers of target genome ${BASE_DIR}/${GENOMEFILEID}.fasta ..."
  echo ""
  sed -E 's/^>[a-z0-9|]*([A-Z]{4,6}[0-9]{8,11})\.[0-9].*/>\1/' "${BASE_DIR}"/"${GENOMEFILEID}".fasta > "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta
else
  echo "Fasta headers of target genome ${GENOME_DIR}/${GENOMEFILEID}.fasta already simplified ..."
  echo ""
fi

################################################################################
# optional: Check for presence of STAR results
if [ "${SCRIPT}" = "stringtie" ] || [ "${SCRIPT}" = "trinity" ]; then
  if [ ! -d "${MAIN_OUT_DIR}"/star ]; then
    echo "The STAR-output directory ${MAIN_OUT_DIR}/star does not exist, please run STAR-aligner first!"
    exit 1
  elif [ -d "${MAIN_OUT_DIR}"/star ] &&  ! compgen -G "${MAIN_OUT_DIR}"/star/SRR* > /dev/null; then
    echo "The STAR-output directory ${MAIN_OUT_DIR}/star does not contain any SRRnnn directories, please run STAR-aligner first!"
    exit 1
  fi
fi

################################################################################
# optional: Generate list of SRA-files to align (STAR) or assemble (Trinity)
if [ "${MULTI_STAR}" = "true" ] || [ "${MULTI_TRINITY}" = "true" ]; then
  if [[ ! -f "${INPUTFILE}" ]]; then
    echo "The input file ${INPUTFILE} does not exist, please provide a correct path!"
    exit 1
  fi
  # The SRR raw data is expected to be in folders  /mnt/ncbi_sra/SRRxxxxx, /mnt/ncbi_sra/SRRyyyyy, etc
  FIRSTSRA=$( head -1 "${INPUTFILE}" )
  INPUTFILEFOLDER=$( dirname "${FIRSTSRA}" )

  # Step 1: Get the paths of all raw data files
  if [[ -f "${INPUTFILE}" ]]; then
    echo "Getting the list of sra-files to map ..."
    echo ""
    [ ! -f "${METADATAFOLDER}"/sra_file_list_"${DATE_SAVED}".txt ] && touch "${METADATAFOLDER}"/sra_file_list_"${DATE_SAVED}".txt
    while IFS= read -r line || [[ -n $line ]]
      do
        find -L "${line}" -maxdepth 1 -mindepth 1 -type f -name 'SRR*.fastq' | sort -n >> "${METADATAFOLDER}"/sra_file_list_"${DATE_SAVED}".txt
    done < "${INPUTFILE}"
  fi

  # Step 2: Determine, whether the raw data consists of single or paired-end reads
  # The structure of the file will look like this. p means paired-end, s means single-read
  # SRR17166871 p
  # SRR14672190 s
  # SRR15205249 p
  # SRR15205262 p
  # SRR15205267 p
  # SRR15205263 p
  # SRR15205266 p
  [ ! -f "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt ] && touch "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt
  while IFS= read -r line || [[ -n $line ]]
    do
      CHECK_FILE_NAME=$( basename "${line}" )
      SRA_FILE_FORMAT="^SRR[0-9]{8}(|_[12])\.fastq$"
      if [[ ! "$CHECK_FILE_NAME" =~ $SRA_FILE_FORMAT ]]; then
        continue
      else
        SRAACC=$( basename "${line}" | sed -n -E 's/^(SRR[0-9]{8})(|_[12])\.fastq$/\1/p' )
        if ( grep -q "$SRAACC" "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt ); then
          sed -i "s/$SRAACC s/$SRAACC p/" "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt
        else
          echo "${SRAACC} s" >> "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt
        fi
      fi
  done < "$METADATAFOLDER"/sra_file_list_"${DATE_SAVED}".txt
fi

################################################################################
# optional: Compute max intron length from genome size
if [ "${SCRIPT}" = "star" ] || [ "${SCRIPT}" = "trinity" ] || [ "${SCRIPT}" = "gmap" ]; then
  # correct number of bases, excluding fasta headers and new-line characters
  NRBASES_W_EOL=$( sed 's/>.*//' "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta | wc -m )
  NREOL=$( wc -l "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta | cut -d" " -f1 )
  NRBASES=$((NRBASES_W_EOL-NREOL))
  # NRBASES=$( wc -m < "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta )
  echo "Length of target genome is about $NRBASES bp."
  echo ""
  # Compute max intron length (STAR default is 589824)
  # Linear increase of max-intron-length by 666 for each genome-length of 10000
  # => yeast         800 (12 kbp)
  # => sugar beat  36666 (500 kbp)
  # => human      206666 (3100 kbp)
  INTRONMAX=$((NRBASES/15000))
  echo "Max intron length is set to $INTRONMAX bp."
  echo ""
fi

#if [ -f "${BASE_DIR}"/"${out_dir}"/pyscipio/allGenesFirstPrediction.complete.yaml ]; then
#  # PyScipio: Estimate max intron size based on YAML
#  echo "-------------------------------------------------------------------------------------"
#  echo ""
#  echo "Estimate max intron size based on PyScipio-YAML ..."
#  echo ""
#  # Get number of genes with intron_max (other genes might be single-exon genes)
#  num=$(grep "intron_max" "${BASE_DIR}"/"${out_dir}"/pyscipio/allGenesFirstPrediction.complete.yaml | cut -d: -f2 | tr -d ' ' | sed '/^0$/d' | sort -n | wc -l)
#  # Remove last 0.5% from list to exclude outliers
#  offset=$(python -c "from math import ceil; print(ceil($num*0.005))")
#  # Get the intron_max from the 99.95% entry
#  INTRONMAX=$(grep "intron_max" "${BASE_DIR}"/"${out_dir}"/pyscipio/allGenesFirstPrediction.complete.yaml | cut -d: -f2 | tr -d ' ' | sed '/^0$/d' | sort -n | head -n-$offset | tail -n1)
#fi

################################################################################
# Check for correct translation table
ALLOWEDTRANSTABLES="1 2 3 4 5 6 9 10 11 12 13 14 16 21 22 23 24 25 26 27 28 29 30 31 33"
if [[ -n ${TRANSTABLE} ]] && ! [[ $ALLOWEDTRANSTABLES =~ (^|[[:space:]])$TRANSTABLE($|[[:space:]]) ]]; then
  echo "Please provide an allowed genetic code number!"
  exit 1
fi
if [[ -z ${TRANSTABLE} ]]; then
  TRANSTABLE="1"
fi
