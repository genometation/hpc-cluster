#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Project: PyScipio
Author:  Dominic Simm <simm@goenomics.com>
Date:    2022-05-11
Version: 1.0.1

This file is part of the software package "PyScipio", and is released
under the "CC BY-NC-ND 4.0" license agreement. Please see the LICENSE
file that should have been included as part of this package.
"""

__author__ = 'Dominic Simm'
__copyright__ = 'Copyright 2022, GOENOMICS GmbH. All rights reserved.'
__credits__ = ["Martin Kollmar", "Dominic Simm"]
__license__ = 'CC BY-NC-ND 4.0'
__version__ = '1.0.1'
__maintainer__ = 'Dominic Simm'
__email__ = 'simm@goenomics.com'
__status__ = 'Production'


# Imports
import logging
from optparse import OptionParser, OptionGroup
import os
import pandas as pd
from sqlalchemy import create_engine, Column, Integer, String, Text, Float, ForeignKey
from sqlalchemy.orm import Session, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
import sqlite3
import sys


# Configuration: Log ###################################################################################################
logging.basicConfig(
    level=logging.DEBUG, format='%(asctime)s|%(levelname)s|%(message)s')


# Class and Table definitions (SQLAlchemy) #############################################################################
Base = declarative_base()


class Gene(Base):
    __tablename__ = "gene"
    gene_id = Column(Integer, primary_key=True)
    seqname = Column(String)
    source = Column(String)
    feature = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    score = Column(String)
    strand = Column(String)
    frame = Column(String)
    coverage = Column(Float)
    start_codon = Column(String)
    start_codon_pos = Column(Integer)
    stop_codon = Column(String)
    stop_codon_pos = Column(Integer)
    attribute = Column(Text)
    gff_id = Column(String)
    gff_parent_id = Column(String)
    mrnas = relationship("Mrna", back_populates="gene")

    def __str__(self):
        return f"Gene: {self.gene_id} {self.source} {self.seqname} \
                       {self.feature} {self.start}-{self.end}"


class Mrna(Base):
    __tablename__ = "mrna"
    mrna_id = Column(Integer, primary_key=True)
    gene_id = Column(Integer, ForeignKey("gene.gene_id"))
    seqname = Column(String)
    source = Column(String)
    feature = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    score = Column(String)
    strand = Column(String)
    frame = Column(String)
    coverage = Column(Float)
    attribute = Column(Text)
    gff_id = Column(String)
    gff_parent_id = Column(String)
    gene = relationship("Gene", back_populates="mrnas")
    exons = relationship("Exon", back_populates="mrna")
    utrs = relationship("Utr", back_populates="mrna")
    cds = relationship("Cds", back_populates="mrna")

    def __str__(self):
        return f"mRNA: {self.mrna_id} {self.source} {self.seqname} \
                       {self.feature} {self.start}-{self.end}"


class Exon(Base):
    __tablename__ = "exon"
    exon_id = Column(Integer, primary_key=True)
    mrna_id = Column(Integer, ForeignKey("mrna.mrna_id"))
    seqname = Column(String)
    source = Column(String)
    feature = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    score = Column(String)
    strand = Column(String)
    frame = Column(String)
    coverage = Column(Float)
    attribute = Column(Text)
    gff_id = Column(String)
    gff_parent_id = Column(String)
    mrna = relationship("Mrna", back_populates="exons")

    def __len__(self):
        return abs(self.start - self.end)

    def __str__(self):
        return f"Exon: {self.exon_id} {self.source} {self.seqname} \
                       {self.feature} {self.start}-{self.end}"


class Cds(Base):
    __tablename__ = "cds"
    cds_id = Column(Integer, primary_key=True)
    mrna_id = Column(Integer, ForeignKey("mrna.mrna_id"))
    seqname = Column(String)
    source = Column(String)
    feature = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    score = Column(String)
    strand = Column(String)
    frame = Column(String)
    coverage = Column(Float)
    attribute = Column(Text)
    gff_id = Column(String)
    gff_parent_id = Column(String)
    mrna = relationship("Mrna", back_populates="cds")

    def __len__(self):
        return abs(self.start - self.end)

    def __str__(self):
        return f"Cds: {self.cds_id} {self.source} {self.seqname} \
                       {self.feature} {self.start}-{self.end}"


class Utr(Base):
    __tablename__ = "utr"
    utr_id = Column(Integer, primary_key=True)
    mrna_id = Column(Integer, ForeignKey("mrna.mrna_id"))
    seqname = Column(String)
    source = Column(String)
    feature = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    score = Column(String)
    strand = Column(String)
    frame = Column(String)
    coverage = Column(Float)
    attribute = Column(Text)
    gff_id = Column(String)
    gff_parent_id = Column(String)
    mrna = relationship("Mrna", back_populates="utrs")

    def __len__(self):
        return abs(self.start - self.end)

    def __str__(self):
        return f"Utr: {self.utr_id} {self.source} {self.seqname} \
                       {self.feature} {self.start}-{self.end} \
                 mRNA: {self.mrna_id}"


# Many-to-many relationship: Link identical/matching genes
class GeneMap(Base):
    __tablename__ = 'gene_gene'
    gene_left_id = Column(Integer, ForeignKey('gene.gene_id'), primary_key=True)
    gene_right_id = Column(Integer, ForeignKey('gene.gene_id'), primary_key=True)


def create_orm_database(options):
    """ Create the basic SQLite database with SQLAlchemy ORM models.

    :param options:  General basic configuration
    :return: None
    """
    # Database file
    if os.path.exists(options.database):
        os.remove(options.database)
    # Create tables for above defined Classes
    Base.metadata.create_all(options.engine)


def read_gff(file_path, cols=None):
    """ Read and parse GTF file content into Pandas Dataframe

    Read: https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md
          http://gmod.org/wiki/GFF3
    GTF file description: http://uswest.ensembl.org/info/website/upload/gff.html

    Common fields:
    ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']

    Args:
        file_path (str): Filename
        cols (list):     Column names of GFF

    Returns:
        dataframe:
    """
    # GFF columns
    if cols is None:
        cols = ['seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame', 'attribute']
    logging.info(f'Reading {file_path} ...')
    df = pd.read_csv(file_path, header=None, names=cols, sep='\t', comment='#', dtype=str)
    # not sure why it could end with dtype 'O', enforce them to be int
    for c in ['start', 'end']:
        df[c] = df[c].astype(int)
    return df


def read_gff_into_df(options):
    """ Helper to read and parse all passed GFF-files into Pandas DataFrames. """
    gff_dfs = {os.path.basename(options.base_gff): read_gff(options.base_gff)}
    with open(options.extra_gffs) as fp:
        gffs = fp.read().splitlines()
        for gff_file in gffs:
            filename = os.path.basename(gff_file)
            gff_dfs[filename] = read_gff(gff_file)
    return gff_dfs


# a = "ID=J11198017.mrna1;Parent=J11198017.path1;Name=J11198017;Dir=indeterminate;coverage=100.0;identity=99.7
# ;matches=288;mismatches=1;indels=0;unknowns=0;Target=J11198017 289 1 .;CDS_Target=J11198017 287 3 ."
def parse_attribute(s):
    """ Parse attributes field of GFF, separated by the ';' symbol. """
    return dict(item.strip().replace('"', '').split("=") for item in filter(lambda x: "=" in x, s.split(";")))


# b = "MGG_01963T0 pep chromosome:MG8:1:59812:60835:1 gene:MGG_01963 transcript:MGG_01963T0
# gene_biotype:protein_coding transcript_biotype:protein_coding description:Putative uncharacterized protein
# %255BSource:UniProtKB/TrEMBL%253BAcc:G4MLZ6%255D"
def parse_description(s):
    """ Parse description field within attributes, separated by """
    import re
    s = re.sub(r"%[0-9]+[A-Z]", " ", s)  # Remove unicodes
    s = re.sub(r"( [A-Z]?[a-z_]+:)", r"#CUT#\1", s)  # Set split-marks before each key-value pair
    return dict(item.strip().replace('"', '').split(":", 1) for item in filter(lambda x: ":" in x, s.split("#CUT#")))


def import_gff_structured(session, df_gff, features=None, corrections=None, flag_parse_attribute=False,
                          filter_terminal_exons=False):
    """ Load GFF data as Gene-Exon structure in SQLite
    Read: https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md

    Args:
        session (Session):            Instance of SQLAlchemy session
        df_gff (DataFrame):           Parsed GFF input data
        features (dict):              Feature filter to find gene/exon rows
                                      features = {'gene': ['gene'], 'exon': ['CDS', 'exon']}
        corrections (dict):           Operations to adjust field values on import
                                      {'gene_feature': 'gene', 'gene_start': [3, '-'], 'gene_end': [-3, '+']}
        flag_parse_attribute (bool):  Parse attribute field to dict
        filter_terminal_exons (bool): Remove terminal exons from input genes

    Returns:
        None
    """
    # Set default parameters
    if not features:
        features = {'gene': ['gene'], 'mRNA': ['mRNA', 'transcript'], 'exon': ['exon'], 'CDS': ['CDS']}
    if corrections:  # None to omit
        c = corrections
    else:
        c = None
    if not parse_attribute:
        attribute = {'cov': None}
    min_exon_length = 15

    # Import into database
    for index, row in df_gff.iterrows():
        # print(row)  # Debug
        if flag_parse_attribute:  # Parse GFF column 'attribute'
            attribute = parse_attribute(row.get('attribute'))
        if row['feature'] in features['gene']:
            # Persist pending cascading objects (ORM) at once
            if index > 0:  # begin after first gene line passed
                # Work-around: Add start- and stop-codon to terminal exons
                if 'PyScipio' in geneObj.source:
                    for mrna in geneObj.mrnas:
                        mrna.exons.sort(key=lambda x: x.start)
                        # mrna.cds[0].start_codon_pos = mrna.cds[0].start
                        # mrna.cds[-1].stop_codon_pos = mrna.cds[-1].end-2
                        geneObj.start_codon_pos = mrna.cds[0].start
                        geneObj.stop_codon_pos = mrna.cds[-1].end-2
                # Filter terminal exons by specific length (min_exon_length)
                if filter_terminal_exons:
                    for mrna in geneObj.mrnas:
                        mrna.exons.sort(key=lambda x: x.start)
                        while len(mrna.exons[0]) < min_exon_length:
                            mrna.exons.pop(0)
                        while len(mrna.exons[-1]) < min_exon_length:
                            mrna.exons.pop(-1)
                        mrna.start = mrna.exons[0].start
                        mrna.end = mrna.exons[-1].end
                # Persist (non-empty genes)
                if len(geneObj.mrnas) > 0:
                    session.commit()
            # Create new ORM object cascade
            geneObj = Gene(
                seqname=row['seqname'],
                source=row['source'],
                feature=c['gene_feature'] if c and c.get('gene_feature') else row['feature'],
                start=row['start'] + c.get('gene_start')[0] if c and c.get('gene_start') and row['strand'] ==
                                                               c.get('gene_start')[1] else row['start'],
                end=row['end'] + c.get('gene_end')[0] if c and c.get('gene_end') and row['strand'] == c.get('gene_end')[
                    1] else row['end'],
                score=row['score'],
                strand=row['strand'],
                frame=row['frame'],
                coverage=attribute.get('coverage'),
                attribute=row['attribute'],
                gff_id=attribute.get('ID'),
                gff_parent_id=attribute.get('Parent'),
            )
            session.add(geneObj)
        if row['feature'] in ['start_codon']:
            geneObj.start_codon_pos = row['start']
        if row['feature'] in ['stop_codon']:
            geneObj.stop_codon_pos = row['start']
        if row['feature'] in features['mRNA']:
            mrnaObj = Mrna(
                seqname=row['seqname'],
                source=row['source'],
                feature='mRNA',
                start=row['start'],
                end=row['end'],
                score=row['score'],
                strand=row['strand'],
                frame=row['frame'],
                coverage=attribute.get('coverage'),
                attribute=row['attribute'],
                gff_id=attribute.get('ID'),
                gff_parent_id=attribute.get('Parent'),
            )
            geneObj.mrnas.append(mrnaObj)
        if row['feature'] in features['exon']:
            exonObj = Exon(
                seqname=row['seqname'],
                source=row['source'],
                feature='exon',
                start=row['start'],
                end=row['end'],
                score=row['score'],
                strand=row['strand'],
                frame=row['frame'],
                coverage=attribute.get('coverage'),
                attribute=row['attribute'],
                gff_id=attribute.get('ID'),
                gff_parent_id=attribute.get('Parent'),
            )
            mrnaObj.exons.append(exonObj)
        if row['feature'] in features['CDS']:
            cdsObj = Cds(
                seqname=row['seqname'],
                source=row['source'],
                feature='cds',
                start=row['start'],
                end=row['end'],
                score=row['score'],
                strand=row['strand'],
                frame=row['frame'],
                coverage=attribute.get('coverage'),
                attribute=row['attribute'],
                gff_id=attribute.get('ID'),
                gff_parent_id=attribute.get('Parent'),
            )
            mrnaObj.cds.append(cdsObj)

    # Persist last pending cascading objects (ORM) at once
    session.commit()


# Definitions
def_strand = {'+': +1, '-': -1}
def_type = {'cds': 'CDS', 'CDS': 'CDS', 'exon': 'exon', 'utr': 'UTR', '5-utr': 'five_prime_UTR', '3-utr': 'three_prime_UTR'}


def prepare_gff3(gene_list):
    """ Transform Gene objects in BioPython SeqRecords with Features

    Args:
        gene_list (list): List of Gene objects
    Returns:
        list: List of SeqRecords
    """

    from Bio.Seq import Seq
    from Bio.SeqRecord import SeqRecord
    from Bio.SeqFeature import SeqFeature, FeatureLocation

    # Custom vars
    package = 'PyScipio'
    vendor = 'GNX'

    # 3-level representation: Gene - mRNA - CDS/Exons/UTRs
    records = []
    for idx, gene in enumerate(gene_list):
        attributes = parse_attribute(gene.attribute)
        if attributes.get('other'):
            other = parse_description(attributes.get('other'))
            attributes.update(other)
        # Gene level
        # e.g. ID=gene:MGG_01947;biotype=protein_coding;description=Cytochrome P450 3A19 ...
        header = gene.seqname  # attributes.get('ID')
        gene_assembly_record = SeqRecord(Seq(''), header, description=header)
        gene_assembly_qualifiers = {
            "source": package,
            "score": gene.score,
            "ID": "%s_gene%d" % (vendor, gene.gene_id),
            "Note": attributes.get('description'),  # PyScipio query information
        }
        coords = sorted([gene.start, gene.end])
        # BioPython: FeatureLocation is 0-based, thus imported GFF.start coordinates need to be corrected
        gene_assembly_feature = SeqFeature(
            FeatureLocation(coords[0]-1, coords[1]),
            type="gene",
            strand=def_strand.get(gene.strand),
            qualifiers=gene_assembly_qualifiers
        )
        gene_assembly_feature.sub_features = []

        # mRNA level (transcript)
        # e.g. ID=transcript:MGG_01947T0;Parent=gene:MGG_01947;biotype=protein_coding;transcript_id=MGG_01947T0
        for id_mrna, mrna in enumerate(gene.mrnas):
            mrna_qualifiers = {
                "source": package,
                "ID": "%s_transcript%d" % (vendor, mrna.mrna_id),
            }
            mrna_coords = sorted([mrna.start, mrna.end])
            mrna_feature = SeqFeature(
                FeatureLocation(mrna_coords[0]-1, mrna_coords[1]),
                type="mRNA",
                strand=def_strand.get(mrna.strand),
                qualifiers=mrna_qualifiers
            )
            mrna_feature.sub_features = []
            # Optional: if information available
            if gene.start_codon_pos is not None or gene.stop_codon_pos is not None:
                seq_block_qualifiers = {
                    "source": package,
                }
                gene_assembly_start = SeqFeature(
                    FeatureLocation(gene.start_codon_pos-1, gene.start_codon_pos-1 + 3),
                    type="start_codon",
                    strand=def_strand.get(gene.strand),
                    qualifiers=seq_block_qualifiers
                )
                mrna_feature.sub_features.append(gene_assembly_start)
                gene_assembly_stop = SeqFeature(
                    FeatureLocation(gene.stop_codon_pos-1, gene.stop_codon_pos-1 + 3),
                    type="stop_codon",
                    strand=def_strand.get(gene.strand),
                    qualifiers=seq_block_qualifiers
                )
                mrna_feature.sub_features.append(gene_assembly_stop)

            # CDS/Exon/UTR level
            # CDS:  ID=CDS:MGG_01947T0;Parent=transcript:MGG_01947T0;protein_id=MGG_01947T0
            # exon: Parent=transcript:MGG_01947T0;Name=MGG_01947-E5;constitutive=1;ensembl_end_phase=-1; ...
            # UTR:  Parent=transcript:MGG_01947T0
            # mrna_blocks = sorted(mrna.cds + mrna.exons + mrna.utrs, key=lambda m: m.start)
            mrna_blocks = sorted(mrna.cds + mrna.utrs, key=lambda m: m.start)
            for i, mrna_block in enumerate(mrna_blocks):
                seq_block_qualifiers = {
                    "source": package,
                    "phase": mrna_block.frame,
                    # "ID": "%s_%d.%d.%d" % (vendor, gene.gene_id, mrna.mrna_id, i),
                    "coverage": mrna_block.coverage,
                }
                seq_coords = sorted([mrna_block.start, mrna_block.end])
                mrna_feature.sub_features.append(
                    SeqFeature(
                        FeatureLocation(seq_coords[0]-1, seq_coords[1]),
                        type=def_type.get(mrna_block.feature),
                        strand=def_strand.get(mrna_block.strand),
                        qualifiers=seq_block_qualifiers
                    ),
                )
            # Set block order in GFF
            mrna_feature.sub_features = sorted(mrna_feature.sub_features, key=lambda m: m.location.start)
            gene_assembly_feature.sub_features.append(mrna_feature)
        gene_assembly_record.features = [gene_assembly_feature]
        records.append(gene_assembly_record)
    return records


def render_gff3(gene_dict, options):
    """ Render PyScipio results as GFF3

    Options:
        1: Print into STDOUT
        2: Print into file

    Args:
        gene_dict (OrderedDict): Results as dictionary
        options (object): PyScipio options object
    Returns:
        None
    """

    from BCBio import GFF
    from io import StringIO

    # Output as GFF3:
    records = prepare_gff3(gene_dict)
    if options.outfile == 'STDOUT':
        out_handle = StringIO()
        GFF.write(records, out_handle, include_fasta=False)
        gff_output = out_handle.getvalue()
        print(gff_output)
    else:
        out_file = os.path.join(options.working_dir, os.path.basename(options.outfile) + '.gff')
        print("File: %s" % out_file)
        with open(out_file, "w") as out_handle:
            GFF.write(records, out_handle, include_fasta=False)


def load_gff_df_in_db(dfs, options):
    """ Load GFF into database. """
    features = {'gene': ['gene'], 'mRNA': ['mRNA', 'transcript'], 'exon': ['exon'], 'CDS': ['CDS']}
    with Session(options.engine) as session:
        for gff_filename, gff_df in dfs.items():
            # Load GFF data
            logging.info(f'Importing {gff_filename} ...')
            import_gff_structured(session, gff_df, features, corrections=None, flag_parse_attribute=True,
                                  filter_terminal_exons=True)


def update_cds_coverage_information(options):
    """ Update PyScipio CDS with coverage information. """
    logging.info(f'Update PyScipio CDS with coverage information ...')
    conn = sqlite3.connect(options.database)
    sql_query = pd.read_sql_query('''
    SELECT g1.mrna_id, g1.cds_id, g1.seqname, g1.source, g1.feature, g1.start, g1.end, g1.score, g1.strand, 
           g2.mrna_id as rnaseq_mrna_id, g2.cds_id as rnaseq_cds_id, 
           g2.seqname as rnaseq_seqname, g2.source as rnaseq_source, g2.feature as rnaseq_feature, g2.start as 
           rnaseq_start, 
           g2.end as rnaseq_end, g2.score as rnaseq_score, g2.strand as rnaseq_strand, g2.frame as rnaseq_frame, 
           g3.coverage as rnaseq_mrna_coverage
       FROM
      (SELECT * FROM cds WHERE source LIKE '%PyScipio%') g1
       INNER JOIN (SELECT * FROM cds WHERE source NOT LIKE '%PyScipio%') g2 
       INNER JOIN (SELECT * FROM mrna) g3 
       ON
         g2.seqname = g1.seqname and
         g2.strand = g1.strand and
         g2.start = g1.start and 
         g2.end = g1.end and
         g2.mrna_id = g3.mrna_id
    ;''', conn)
    df = pd.DataFrame(sql_query)
    conn.close()
    # Update PyScipio CDS with coverage information
    with Session(options.engine) as session:
        for cds_entry in df.loc[df.groupby('cds_id').rnaseq_mrna_coverage.idxmax()].itertuples():
            cdsObj = session.query(Cds).get(cds_entry.cds_id)
            cdsObj.coverage = cds_entry.rnaseq_mrna_coverage
            # Save
            session.add(cdsObj)
            session.commit()


def add_utr_information(options):
    """ Add UTR information to genes with supporting RNAseq data. """
    conn = sqlite3.connect(options.database)
    sql_query = pd.read_sql_query('''
    SELECT g1.gene_id, g1.mrna_id, g1.seqname, g1.source, g1.feature, g1.start, g1.end, g1.score, g1.strand, 
           g2.gene_id as rnaseq_gene_id, g2.mrna_id as rnaseq_mrna_id, g2.seqname as rnaseq_seqname, g2.source as rnaseq_source, 
           g2.feature as rnaseq_feature, g2.start as rnaseq_start, g2.end as rnaseq_end, g2.score as rnaseq_score, g2.strand as rnaseq_strand, g2.frame as rnaseq_frame, g2.coverage as rnaseq_coverage 
    FROM
      (SELECT * FROM mrna WHERE source LIKE '%PyScipio%') g1
       INNER JOIN (SELECT * FROM mrna WHERE source NOT LIKE '%PyScipio%') g2 
       ON
         g2.seqname = g1.seqname and
         g2.strand = g1.strand and
        ((g2.start < g1.start and g2.end > g1.start) or (g2.start < g1.end and g2.end > g1.end))
         and g2.coverage > 85.0
    ORDER BY g1.seqname, g1.start ASC;''', conn)
    df = pd.DataFrame(sql_query)
    conn.close()

    # Add beginning UTR regions
    logging.info(f'Add UTR regions (beginning) ...')
    with Session(options.engine) as session:
        for start_utr in df.loc[df.groupby('gene_id').rnaseq_start.idxmin()].query('rnaseq_start < start').itertuples():
            mrnaObj = session.query(Mrna).get(start_utr.mrna_id)
            first_cds = sorted(mrnaObj.cds, key=lambda x: x.start)[0]
            feat = '5-utr' if mrnaObj.strand == '+' else '3-utr'
            rnaseqMrnaObj = session.query(Mrna).get(start_utr.rnaseq_mrna_id)
            for rnaseq_exon in sorted(rnaseqMrnaObj.exons, key=lambda x: x.start):
                if rnaseq_exon.start < first_cds.start:
                    # Copy missing exons as exons and UTRs
                    utrObj = Utr(
                        start=rnaseq_exon.start,
                        end=rnaseq_exon.end,
                        mrna_id=mrnaObj.mrna_id,
                        seqname=rnaseq_exon.seqname,
                        source=rnaseq_exon.source,
                        feature=feat,
                        score=rnaseq_exon.score,
                        strand=rnaseq_exon.strand,
                        frame=rnaseq_exon.frame,
                        coverage=start_utr.rnaseq_coverage,
                        attribute=rnaseq_exon.attribute,
                    )
                    if rnaseq_exon.end > first_cds.start:
                        utrObj.end = first_cds.start-1
                    mrnaObj.utrs.append(utrObj)
                    exonObj = Exon(
                        start=utrObj.start,
                        end=utrObj.end,
                        mrna_id=utrObj.mrna_id,
                        seqname=utrObj.seqname,
                        source=utrObj.source,
                        feature='exon',
                        score=utrObj.score,
                        strand=utrObj.strand,
                        frame=utrObj.frame,
                        coverage=utrObj.coverage,
                        attribute=utrObj.attribute,
                    )
                    mrnaObj.exons.append(exonObj)
            mrnaObj.start = start_utr.rnaseq_start
            mrnaObj.gene.start = start_utr.rnaseq_start
            # Save
            session.add(mrnaObj)
            session.commit()
        # Add trailing UTR regions
        logging.info(f'Add UTR regions (trailing) ...')
        for end_utr in df.loc[df.groupby('gene_id').rnaseq_end.idxmax()].query('rnaseq_end > end').itertuples():
            mrnaObj = session.query(Mrna).get(end_utr.mrna_id)
            last_cds = sorted(mrnaObj.cds, key=lambda x: x.start)[-1]
            feat = '5-utr' if mrnaObj.strand == '-' else '3-utr'
            rnaseqMrnaObj = session.query(Mrna).get(end_utr.rnaseq_mrna_id)
            for rnaseq_exon in sorted(rnaseqMrnaObj.exons, key=lambda x: x.start):
                if rnaseq_exon.end > last_cds.end:
                    # Copy missing exons as exons and UTRs
                    utrObj = Utr(
                        start=rnaseq_exon.start,
                        end=rnaseq_exon.end,
                        mrna_id=mrnaObj.mrna_id,
                        seqname=rnaseq_exon.seqname,
                        source=rnaseq_exon.source,
                        feature=feat,
                        score=rnaseq_exon.score,
                        strand=rnaseq_exon.strand,
                        frame=rnaseq_exon.frame,
                        coverage=end_utr.rnaseq_coverage,
                        attribute=rnaseq_exon.attribute,
                    )
                    if rnaseq_exon.start < last_cds.end:
                        utrObj.start = last_cds.end+1
                    mrnaObj.utrs.append(utrObj)
                    exonObj = Exon(
                        start=utrObj.start,
                        end=utrObj.end,
                        mrna_id=utrObj.mrna_id,
                        seqname=utrObj.seqname,
                        source=utrObj.source,
                        feature='exon',
                        score=utrObj.score,
                        strand=utrObj.strand,
                        frame=utrObj.frame,
                        coverage=utrObj.coverage,
                        attribute=utrObj.attribute,
                    )
                    mrnaObj.exons.append(exonObj)
            mrnaObj.end = end_utr.rnaseq_end
            mrnaObj.gene.end = end_utr.rnaseq_end
            # Save
            session.add(mrnaObj)
            session.commit()


def export_merged_gff(options):
    """ Export merged GFF file. """
    logging.info(f'Export merged GFF file ...')
    # Select genes for GFF: Find all genes with 3'-UTR and 5'-UTR
    if options.select == 'utr':
        conn = sqlite3.connect(options.database)
        sql_query = pd.read_sql_query('''
        SELECT g1.mrna_id 
        FROM (SELECT * FROM utr WHERE feature = '3-utr') g1
           INNER JOIN (SELECT * FROM utr WHERE feature = '5-utr') g2 
           ON
             g2.mrna_id = g1.mrna_id''', conn)
        df = pd.DataFrame(sql_query)
        utr_gene_inds = list(df['mrna_id'])
        conn.close()
    # Get matching gene objects and export GFF
    with Session(options.engine) as session:
        if options.select == 'utr':
            gene_list = session.query(Gene).where(Gene.gene_id.in_(utr_gene_inds)).order_by(Gene.seqname, Gene.start).all()
        else:
            gene_list = session.query(Gene).where(Gene.source.like('%PyScipio%')).order_by(Gene.seqname, Gene.start).all()
        # Plot GFF
        if options.export_preview:
            options.outfile = 'STDOUT'
            render_gff3(gene_list[:10], options)
        else:
            render_gff3(gene_list, options)


def main(args):
    """ ``Main:`` Manage merging of PyScipio GFF with additional GFF sources. """

    # Parse arguments
    parser = make_parser()
    (options, args) = parser.parse_args(args)
    if len(sys.argv[1:]) < 5:
        parser.print_help()
        parser.error("Not sufficient arguments given!")
    # Store additional configuration
    options.database = os.path.join(options.working_dir, 'gff-orm.plain.sqlite')
    options.engine = create_engine(f"sqlite:///{options.database}")  # , echo=True)
    print(options)

    if not options.export_only:
        # Read GFF contents with additional information
        gff_dfs = read_gff_into_df(options)
        # Reset and create ORM database from scratch
        create_orm_database(options)
        # Fill database
        load_gff_df_in_db(gff_dfs, options)
        # Update coverage information
        update_cds_coverage_information(options)
        # Add UTR information
        add_utr_information(options)
    # Export merged GFF information
    export_merged_gff(options)


def make_parser():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 0.1415926")
    opt = parser.add_option
    opt('-v', '--verbose', action='count', default=0,
        help='Show verbose information (including progress)')
    opt('-e', '--export_only', action='store_true',
        help='Run only GFF export')
    opt('-p', '--export_preview', action='store_true',
        help='Show preview of GFF export')
    opt('-s', '--select',
        metavar='name',
        type='choice',
        choices=['all', 'utr', ],
        default='all',
        help='Gene selection for GFF export (all, utr, ...')

    group = OptionGroup(parser, "Data source")
    opt = group.add_option
    opt('-w', '--working_dir', metavar='dir',
        help='Set the working directory')
    opt('-f', '--base_gff', metavar='file',
        help='Base GFF file for merge (PyScipio)')
    opt('-x', '--extra_gffs', metavar='file',
        help='File listing with additional GFF files (one file per line)')
    opt('-o', '--outfile', metavar='file',
        help='Final GFF file with merged information')
    parser.add_option_group(group)

    return parser


if __name__ == '__main__':
    main(sys.argv[1:])
