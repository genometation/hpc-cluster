#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE
unset TRANSTABLE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 06.run.gmap.sh -g [id] -i [file] <optional -o [path] -t [num] -f>

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [file]        input data directory/file (where the TSA-files to be mapped are located)
-o [path]        basic output data directory (will be the current directory if option not set)
                 If genome-file-id and basic-output-data-dir are given, results will be written
                 into basic_output_data_dir/prediction/gmap
-t [num]         genetic code translation table (default: 1; numbers as of NCBI list)
-f               force overwrite

-h   Show this help

example:
06.run.gmap.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/trinity -o /mnt/customer/self

EOF
exit 0
}

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fi:o:t:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      INPUTFILE=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    t )
      TRANSTABLE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))
# Invoking the config-script only if star-script is not run from "multi-STAR".
if [ -z "${MULTI_GMAP}" ]; then
  # The config-script will set all PATH-variables, check for presence or mkdir subfolders.
  # Turn debugging on within the config-script.
  SCRIPT="gmap"
  . config.sh

  STATUSFILE="06.run.gmap"_"${DATE_SAVED}".out
  LOGFILE="06.run.gmap"_"${DATE_SAVED}".log

#  METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#  [ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"
#
  STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
  LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
  ### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
  exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)
fi


if [ ! -f "${INPUTFILE}" ]; then
  echo "The input data file ${INPUTFILE} does not exist, please provide a correct path!"
  exit 1
fi

if [ -z "${TRANSTABLE}" ]; then
  TRANSTABLE=1
fi

# Directory for gmap_build - Tool for genome database creation for GMAP or GSNAP
# ../target_"${GENOMEFILEID}"/"${GENOMEGMAP_DIR}"/
GENOMEFILEIDMIN=$( head -1 "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta | sed 's/>//' )
GENOMEGMAP_DIR="${GENOMEFILEIDMIN}".gmap
# Directory for iit_store data
GENOMEMAPS_DIR="${GENOMEGMAP_DIR}"/"${GENOMEGMAP_DIR}".maps


echo "#####################################################################################"
echo ""
echo "06.run.gmap.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

echo "-------------------------------------------------------------------------------------"
echo ""
echo "Estimate total intron size based on target genome length ..."
echo ""
TOTALINTRON=$((INTRONMAX*2))
echo "Total intron length is set to $TOTALINTRON bp."
echo ""

echo "-------------------------------------------------------------------------------------"
echo ""
echo "Building the database genome index for GMAP ..."
echo ""

if { [ ! -s "${GENOME_DIR}"/"${GENOMEGMAP_DIR}"/"${GENOMEFILEIDMIN}".gmap.chromosome ] && \
   [ ! -s "${GENOME_DIR}"/"${GENOMEGMAP_DIR}"/"${GENOMEFILEIDMIN}".gmap.genomecomp ]; } || [ -n "${FORCEOVERWRITE}" ]; then
  gmap_build -D "${GENOME_DIR}" -d "${GENOMEGMAP_DIR}" -k 13 "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta >&2
else
  echo "Genome index for ${GENOMEFILEID}.min.fasta already created ..."
  echo ""
fi

if [ ! -d "${GENOMEMAPS_DIR}" ]; then
  mkdir -p "${GENOME_DIR}"/"${GENOMEMAPS_DIR}"
fi

################################################################################
# Running GMAP

INPUTFILENAME=$( basename "${INPUTFILE}" )
INPUTBASENAME=${INPUTFILENAME%.fasta}
if compgen -G "${OUT_DIR}"/"${INPUTBASENAME}"_gmap.gff > /dev/null && [ -z "${FORCEOVERWRITE}" ]; then
  echo "-------------------------------------------------------------------------------------"
  echo ""
  echo "GMAP has already been run, nothing to do ..."
  exit 1
else
  SRAACC=$( basename "${INPUTFILE}" | sed -n -E 's/^(SRR[0-9]{8}).*\.fasta$/\1/p' )
  INTRON_INPUTFILENAME="${MAIN_OUT_DIR}"/stringtie/"${SRAACC}"_stringtie.gtf
  echo "-------------------------------------------------------------------------------------"
  echo ""
  echo "Building intron and splice site map files for GMAP (from StringTie data) ..."
  echo ""
  # From the GMAP package documentation:
  # This package includes an implementation of interval index trees (IITs), which permits efficient
  # lookup of interval information. The gmap program also allows you (with its -m flag) to look up pre-mapped
  # annotation information that overlaps your query cDNA sequence. These interval index trees (or map files)
  # are built using the iit_store program included in this package.
  # Usage: iit_store [OPTIONS...] -o outputfile inputfile, or
  #        cat inputfile | iit_store [OPTIONS...] -o outputfile
  #
  #  inputfile is in either FASTA or GFF3 format
  #
  #  -o, --output=STRING       Name of output iit file (.iit will be added as a suffix if necessary)
  #  -G, --gff                 Parse input file in gff3 format
  #  -l, --label=STRING        For gff input, the feature attribute to use (default is ID)

  gtf_introns < "${INTRON_INPUTFILENAME}" | iit_store -o "${GENOME_DIR}"/"${GENOMEMAPS_DIR}"/"${SRAACC}"_introns.iit
  gtf_splicesites < "${INTRON_INPUTFILENAME}" | iit_store -o "${GENOME_DIR}"/"${GENOMEMAPS_DIR}"/"${SRAACC}"_splicesites.iit

  echo "-------------------------------------------------------------------------------------"
  echo ""
  echo "Mapping ${INPUTBASENAME} data against ${GENOMEFILEID}.min.fasta with GMAP ..."
  echo ""
  # gmap - Genomic Mapping and Alignment Program
  # gmap [OPTIONS...] <FASTA files...>
  # Input options (must include -d or -g)
  # -D, --dir=directory          Genome directory. Default is /var/cache/gmap
  # -d, --db=STRING              Genome database. If argument is '?' (with the quotes), this command lists available databases.
  # -t, --nthreads=INT           Number of worker threads
  # --max-deletionlength=INT     Max length for a deletion (default 100). Above this size, a genomic gap will be
  #                              considered an intron rather than a deletion. If the genomic gap is less than
  #                              --max-deletionlength and greater than --min-intronlength, a known splice site or
  #                              splice site probabilities of 0.80 on both sides will be reported as an intron.
  # --min-intronlength=INT       Min length for one internal intron (default 9). Below this size, a genomic gap
  #                              will be considered a deletion rather than an intron.
  # --max-intronlength-middle=INT  Max length for one internal intron (default 500000). Note: for backward compatibility,
  #                              the -K or --intronlength flag will set both --max-intronlength-middle and
  #                              --max-intronlength-ends.  Also see --split-large-introns below.
  # --max-intronlength-ends=INT  Max length for first or last intron (default 10000).  Note: for backward compatibility,
  #                              the -K or --intronlength flag will set both --max-intronlength-middle and --max-intronlength-ends.
  # --trim-end-exons=INT         Trim end exons with fewer than given number of matches (in nt, default 12)
  # -L, --totallength=INT        Max total intron length (default 2400000)
  # --cross-species              Use a more sensitive search for canonical splicing, which helps especially for
  #                              cross-species alignments and other difficult cases
  # --allow-close-indels=INT     Allow an insertion and deletion close to each other (0=no, 1=yes (default), 2=only
  #                              for high-quality alignments)
  # -n, --npaths=INT             Maximum number of paths to show (default 5). If set to 1, GMAP will not report
  #                              chimeric alignments, since those imply two paths. If you want a single alignment
  #                              plus chimeric alignments, then set this to be 0.
  # --translation-code=INT       Genetic code used for translating codons to amino acids and computing CDS Integer
  #                              value (default=1) corresponds to an available code at
  #                              http://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
  # -m, --map=iitfile            Map file. If argument is '?' (with the quotes), this lists available map files.
  # -B, --batch=INT              Batch mode (default = 2)
  #                                      Mode     Offsets       Positions       Genome
  #                                        0      see note      mmap            mmap
  #                                        1      see note      mmap & preload  mmap
  #                              (default) 2      see note      mmap & preload  mmap & preload
  #                                        3      see note      allocate        mmap & preload
  #                                        4      see note      allocate        allocate
  #                                        5      expand        allocate        allocate
  # -x, --chimera-margin=INT     Amount of unaligned sequence that triggers search for the remaining sequence
  #                              (default 30). Enables alignment of chimeric reads, and may help with some
  #                              non-chimeric reads. To turn off, set to zero.
  # -f, --format=INT             Other format for output (also note the -A and -S options and other options listed
  #                              under Output types):
  #                                 psl (or 1) = PSL (BLAT) format,
  #                                 gff3_gene (or 2) = GFF3 gene format
  #                                 gff3_match_cdna (or 3) = GFF3 cDNA_match format (used by process_GMAP_alignments_gff3_chimeras_ok.pl)
  #                                 .... and many more

  gmap -D ${GENOME_DIR} -d ${GENOMEGMAP_DIR} ${INPUTFILE} \
  -f 2 -n 1 -x 50 -t 2 -B 5 -A --translation-code=${TRANSTABLE} --allow-close-indels=2 \
  --max-deletionlength=30 --max-intronlength-middle=${INTRONMAX} --max-intronlength-end=${INTRONMAX} --totallength=${TOTALINTRON} \
  --map=${SRAACC}_introns.iit \
  > "${OUT_DIR}"/"${INPUTBASENAME}"_gmap.gff
fi

# GMAP: Align Trinity transcripts to reference genome
# process_GMAP_alignments_gff3_chimeras_ok.pl  (util to run GMAP from the Trinity package)
# => step 1: gmap_build -D $genomeBaseDir -d $genomeBaseDir/$genomeDir -k 13 $genome >&2
# => step 2: gmap -D $genomeBaseDir -d $genomeDir $transcriptDB -f $format -n $num_gmap_top_hits -x 50 -t $CPU -B 5
# => optional: --intronlength=$max_intron -m ref_${splice_assist}.iit
#  Required:
#  --genome <string>           target genome to align to
#  --transcripts <string>      cdna sequences to align
#
#  Optional:
#  -N <int>                    number of top hits (default: 1)
#  -I <int>                    max intron length (Max length for one internal intron (default 1000000))
#  --CPU <int>                 number of threads (default: 2; => -t)
#  --no_chimera                do not report chimeric alignments (=> $num_gmap_top_hits = 1)
#  --SAM                       output in SAM format (gff3_gene (or 2) = GFF3 gene format)
#  --gtf <string>              gene structure annotations in gtf format (for genome building)
#  --splice_assist <string>    splice assist mode (introns or splicesites)

#if [ -f "$INPUTFILE" ]; then
#  INPUTFILENAME=$( basename "$INPUTFILE" )
#  INPUTBASENAME=${INPUTFILENAME%.fasta}
#  echo "Mapping ${INPUTBASENAME} ..."
#  echo ""
#  /software/trinityrnaseq-v2.13.2/util/misc/process_GMAP_alignments_gff3_chimeras_ok.pl \
#  --genome "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
#  --transcripts "${INPUTFILE}" -I "${INTRONMAX}" > "${OUT_DIR}"/"${INPUTBASENAME}"_gmap_"${DATE_SAVED}".gff
#elif [ -d "$INPUTFILE" ]; then
#  find -L "$INPUTFILE" -maxdepth 1 -mindepth 1 -type f -name '*.fasta' > "${OUT_DIR}"/tsa_file_list_"${DATE_SAVED}".txt
#  while IFS= read -r line || [[ -n $line ]]
#    do
#      INPUTFILENAME=$( basename "$line" )
#      INPUTBASENAME=${INPUTFILENAME%.fasta}
#      echo "Mapping ${INPUTBASENAME} ..."
#      echo ""
#      /software/trinityrnaseq-v2.13.2/util/misc/process_GMAP_alignments_gff3_chimeras_ok.pl \
#      --genome "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
#      --transcripts "${line}" -I "${INTRONMAX}" > "${OUT_DIR}"/"${INPUTBASENAME}"_gmap_"${DATE_SAVED}".gff
#  done < "${OUT_DIR}"/tsa_file_list_"${DATE_SAVED}".txt
#fi

# Postprocessing: Adjust headers to minify
# Extract NCBI accession numbers
# sed -E 's/^[a-z0-9|]*([A-Z]{4,6}[0-9]{8,11})\.[0-9].*(gmap.*)/\1\t\2/' "${BASE_DIR}"/"${out_dir}"/gmap/gmap_transcript_trinity_single_"${DATE_SAVED}".gff > "${BASE_DIR}"/"${out_dir}"/gmap/gmap_transcript_trinity_single_"${DATE_SAVED}".min.gff

end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"