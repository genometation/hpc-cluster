#!/usr/bin/env bash
################################################################################
# NOTE:
# This will add missing genetic codes to AUGUSTUS source code
################################################################################

showHelp() {
cat << EOF

Usage: geneticcode2augustus.sh -i [file] -j [file]

-i [file]         path-to-augustus-genetic-code-file (e.g. /software/augustus/src/geneticcode.cc)
-j [file]         path-to-missing-genetic-code-file (e.g. /mnt/software/hpc-cluster/scripts/prediction/genetic_codes.txt)

EOF
exit 0
}

OPTIND=1
while getopts ":i:j:h" opt; do
  case ${opt} in
    i )
      AUGFILE=$OPTARG
      ;;
    j )
      GNXFILE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

PATHTOAUGFILE=$( dirname "${AUGFILE}" )
sudo mv "${PATHTOAUGFILE}"/geneticcode.cc "${PATHTOAUGFILE}"/geneticcode.cc.original
GENETICCODE="${GNXFILE}"
AUGGENETICCODE="${PATHTOAUGFILE}/geneticcode.cc.original"

GCLINES=$( sed -n -E '/^\ "KN.*24$/,/^.*FLF"\,\}\ \/\/\ 33$/{ p }' "${GENETICCODE}" )
STARTLINES=$( sed -n -E '/^\ "--.*24$/,/^.*-M-"\,\}\ \/\/\ 33$/{ p }' "${GENETICCODE}" )

#sed -n -E "s/^\ \"KN.*FLF\"\}\;\/\/\ 24/$GCLINES/p" genetic_codes.txt
while IFS= read -r line || [[ -n $line ]]; do
  regex="^\s\"KN.*FLF\"\}\;\/\/\ 24$"
  regex2="^\s\"--.*-M-\"\}\;\s\/\/\ 24$"
  if [[ "${line}" =~ $regex ]]; then
    echo "${GCLINES}" >> "${PATHTOAUGFILE}"/geneticcode.cc
  elif [[ "${line}" =~ $regex2 ]]; then
    echo "${STARTLINES}" >> "${PATHTOAUGFILE}"/geneticcode.cc
  else
    echo "${line}" >> "${PATHTOAUGFILE}"/geneticcode.cc
  fi

done < "${AUGGENETICCODE}"

