#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

ext_start=$(date +%s)
ext_start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 04b.run.trinity_on_multiple_datasets.sh -g [id] -i [path] <optional -o [path] -f>

Note: Trinity genome-guided-mode depends on STAR-aligned SRA-data. The needed bam-file data will automatically
      be detected in the directory basic_output_data_dir/prediction/star (within the 04.run.trinity.sh script).

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]        path to input file SRAdata.lst that lists the SRA data directories, one per row, e.g.
                      /mnt/ncbi_sra/SRRxxxxx
                      /mnt/ncbi_sra/SRRyyyyy
-o [path]        basic output data directory (will be the current directory if option not set)
                 Results will be written into (basic_output_data_dir/)prediction/trinity
-f               force overwrite

-h   Show this help

example:
./04b.run.trinity_on_multiple_datasets.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/SRAdata.lst -o /mnt/customer/self
./04b.run.trinity_on_multiple_datasets.sh -g KWS -i /mnt/customer/KWS/pilot/prediction_KWS/SRAdata.lst -o /mnt/customer/KWS/pilot

EOF
exit 0
}

#-i [path]        input data directory (where the sra-files are located, e.g. /mnt/ncbi_sra)
#-f               full, get all SRA-files in the input directory
#-r [first-last]  range of SRA-files (format SRR14672191-SRR14672195)

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fi:o:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      INPUTFILE=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))


# Setting "multi-TRINITY" variable so that config-script is not invoked twice.
MULTI_TRINITY="true"
# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="trinity"
. config.sh

if [ -n "${FORCEOVERWRITE}" ]; then
  FORCEOVERWRITEOPTION=" -f"
else
  FORCEOVERWRITEOPTION=""
fi

#    f )
#      FULLFOLDER="true"
#      ;;
#    r )
#      INPUTFILERANGE=$OPTARG
#      SELECTEDFOLDER="true"
#      ;;
#if { [[ -n "${FULLFOLDER}" ]] && [[ -n "${SELECTEDFOLDER}" ]]; } || { [[ -z "${FULLFOLDER}" ]] && [[ -z "${SELECTEDFOLDER}" ]]; }; then
#  echo "Wrong option! Please choose either -f (full folder) oder -r (range of SRR-data) ..."
#  exit 1
#fi

STATUSFILE="04b.run.trinity_on_multiple_datasets"_"${DATE_SAVED}".out
LOGFILE="04b.run.trinity_on_multiple_datasets"_"${DATE_SAVED}".log

#METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

echo "#####################################################################################"
echo ""
echo "04b.run.trinity_on_multiple_datasets.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $ext_start_date"
echo ""


#####################################################################################################
# NOTE: The following code is correct, but Trinity does not run with multiple SRR-input files.
#       Trinity stops with an unclear error. The error seems to be related to available RAM, although
#       enough RAM should be available. Maybe the error is related to normalization problems across
#       multiple data sets.
#echo "-------------------------------------------------------------------------------------"
#echo ""
#echo "Combining left- and right-sra-files for Trinity ..."
#echo ""
#
#while IFS= read -r line || [[ -n $line ]]
#  do
#    SRAACC=$( echo "${line}" | sed -n -E 's/(^SRR[0-9]{8})[[:space:]][sp]/\1/p' )
#    if [[ "${line}" =~ " s" ]]; then
#      SINGLE_FILES_TMP+="${INPUTFILEFOLDER}/${SRAACC}.fastq,"
#    else
#      LEFT_FILES_TMP+="${INPUTFILEFOLDER}/${SRAACC}_1.fastq,"
#      RIGHT_FILES_TMP+="${INPUTFILEFOLDER}/${SRAACC}_2.fastq,"
#    fi
#done < "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt
#
#SINGLE_FILES="${SINGLE_FILES_TMP%,}"
#LEFT_FILES="${LEFT_FILES_TMP%,}"
#RIGHT_FILES="${RIGHT_FILES_TMP%,}"
#if [ -n "${SINGLE_FILES}" ]; then
#  ./04.run.trinity.sh -g "${GENOMEFILEID}" -l "${SINGLE_FILES}"
#fi
#if [ -n "${LEFT_FILES}" ] && [ -n "${RIGHT_FILES}" ]; then
#  ./04.run.trinity.sh -g "${GENOMEFILEID}" -l "${LEFT_FILES}" -r "${RIGHT_FILES}"
#fi
#####################################################################################################

#echo "-------------------------------------------------------------------------------------"
#echo ""
#echo "Starting Trinity ..."
#echo ""
#
#while IFS= read -r line || [[ -n $line ]]
#  do
#    SRAACC=$( echo "${line}" | sed -n -E 's/(^SRR[0-9]{8})[[:space:]][sp]/\1/p' )
#    if [[ "${line}" =~ " s" ]]; then
#      ./04.run.trinity.sh -g "${GENOMEFILEID}" -l "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}".fastq -o "${BASE_DIR}"
#    else
#      ./04.run.trinity.sh -g "${GENOMEFILEID}" -l "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}"_1.fastq -r "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}"_2.fastq -o "${BASE_DIR}"
#    fi
#done < "$METADATAFOLDER"/sra_file_list_unique_"${DATE_SAVED}".txt

# Step 1: Get the paths of all raw data files
# done in config.sh script
# sets ${INPUTFILEFOLDER}

# Step 2: Determine, whether the raw data consists of single or paired-end reads
# done in config.sh script
# returns sra_file_list_unique_"${DATE_SAVED}".txt

# Step 3: Run 04.run.trinity.sh on all data (in parallel)
echo "-------------------------------------------------------------------------------------"
echo ""
echo "Starting Trinity in parallel ..."
echo ""

MAX_PARALLEL=2
# The following line might better be de-commented when script is run manually. The line
# ensures that all processes started within the loop will be killed if the script is
# terminated prematurely. This is not needed within the pipeline, where it might lead
# to an unwanted EXIT because of an error within one of the executed scripts in the loop.
trap 'echo killing all children; kill 0' EXIT

while IFS= read -r line || [[ -n $line ]]
  do
    # sleep 10   # Important! Otherwise subprocesses might run out of tracking.
    SRAACC=$( echo "${line}" | sed -n -E 's/(^SRR[0-9]{8})[[:space:]][sp]/\1/p' )
    if [[ "${line}" =~ " s" ]]; then
      . ./04.run.trinity.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -l "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}".fastq -o "${BASE_DIR}" &
    else
      . ./04.run.trinity.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -l "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}"_1.fastq -r "${INPUTFILEFOLDER}"/"${SRAACC}"/"${SRAACC}"_2.fastq -o "${BASE_DIR}" &
    fi
    # track job ids to wait for later
    pids[${SRAACC}]=$!
    # Wait here for any job to be finished, so there is a place to start next one.
    # The following line does not work with Trinity, which starts too many further subprocesses
    # if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi
    until [[ $(jobs -r -p | wc -l) -lt ${MAX_PARALLEL} ]]
    do
      # check every 60 sec for jobs to be completed
      sleep 60
    done


done < "${METADATAFOLDER}"/sra_file_list_unique_"${DATE_SAVED}".txt

# wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done

ext_end=$(date +%s)
ext_end_date=$(date +%T)
runtime=$((ext_end-ext_start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $ext_end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"