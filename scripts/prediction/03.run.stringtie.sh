#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE
unset REF_ANNOT

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 03.run.stringtie.sh -g [id] -i [path] <optional -o [path] -r [path] -f>

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]        path to STAR-aligned SRR directory, e.g. /mnt/customer/self/prediction/star/SRRxxxxx
-o [path]        basic output data directory (will be the current directory if option not set)
                 Results will be written into basic_output_data_dir/prediction/stringtie
-r [path]        reference annotation (optional; full path)
-f               force overwrite

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp

OPTIND=1
while getopts ":g:fi:o:r:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      BAM_DIR=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    r )
      REF_ANNOT=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

SRAACC=$( basename "${BAM_DIR}" )
if [ ! -d "${MAIN_OUT_DIR}"/star/"${SRAACC}" ]; then
  echo "The STAR-output directory ${MAIN_OUT_DIR}/star/${SRAACC} does not exist, please run STAR-aligner first!"
  exit 1
elif [ ! -f "${MAIN_OUT_DIR}"/star/"${SRAACC}"/Aligned.sortedByCoord.out.bam ]; then
  echo "The required BAM-file in the STAR-output directory ${MAIN_OUT_DIR}/star/${SRAACC} does not exist, please run STAR-aligner first!"
  exit 1
fi
# Invoking the config-script only if stringtie-script is not run from "multi-STRINGTIE".
if [ -z "${MULTI_STRINGTIE}" ]; then
  # The config-script will set all PATH-variables, check for presence or mkdir subfolders.
  # Turn debugging on within the config-script.
  SCRIPT="stringtie"
  . config.sh

  STATUSFILE="03.run.stringtie"_"${DATE_SAVED}".out
  LOGFILE="03.run.stringtie"_"${DATE_SAVED}".log

#  METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#  [ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

  STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
  LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
  ### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
  exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)
fi

if [[ -n ${REF_ANNOT} ]] && [ ! -f "$REF_ANNOT" ]; then
  echo "The reference annotation $REF_ANNOT cannot be found, please provide a correct path!"
  exit 1
fi
if [[ -n ${REF_ANNOT} ]]; then
  USE_REF_ANNOT=" -G ${REF_ANNOT}"
else
  USE_REF_ANNOT=""
fi

echo "#####################################################################################"
echo ""
echo "03.run.stringtie.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

# Stringtie is extremely fast (few sec. to few min per sample), therefore a re-run might be affordable.
if compgen -G "${OUT_DIR}/${SRAACC}_stringtie.gtf" > /dev/null && [ -z "${FORCEOVERWRITE}" ]; then
  echo "Stringtie has already been run on STAR-aligned SRA data ${SRAACC}, moving on to next data ..."
  exit 1
fi

# stringtie [-o <output.gtf>] [other_options] <read_alignments.bam>

# -f <0.0-1.0>      Sets the minimum isoform abundance of the predicted transcripts as a fraction of the most abundant
#                   transcript assembled at a given locus. Lower abundance transcripts are often artifacts of incompletely
#                   spliced precursors of processed transcripts. Default: 0.01
# -G <ref_ann.gff>	Use a reference annotation file (in GTF or GFF3 format) to guide the assembly process. The output
#                   will include expressed reference transcripts as well as any novel transcripts that are assembled.
#                   This option is required by options -B, -b, -e, -C.
# -m <int>	        Sets the minimum length allowed for the predicted transcripts. Default: 200
# -c <float>	      Sets the minimum read coverage allowed for the predicted transcripts. A transcript with a lower
#                   coverage than this value is not shown in the output. Default: 1
# -s <float>	      Sets the minimum read coverage allowed for single-exon transcripts. Default: 4.75
# --conservative  	Assembles transcripts in a conservative mode. Same as -t -c 1.5 -f 0.05

# StringTie: Generate Transcriptome assembly from STAR aligned reads
echo "stringtie -c 2.0 -f 0.05${USE_REF_ANNOT} -m 30 -o ${OUT_DIR}/${SRAACC}_stringtie.gtf ${BAM_DIR}/Aligned.sortedByCoord.out.bam"
stringtie -c 2.0 -f 0.05"${USE_REF_ANNOT}" -m 30 -o "${OUT_DIR}"/"${SRAACC}"_stringtie.gtf "${BAM_DIR}"/Aligned.sortedByCoord.out.bam


end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"