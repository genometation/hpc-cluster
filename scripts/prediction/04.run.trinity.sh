#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

unset RNASEQ_LEFT
unset RNASEQ_RIGHT
unset FORCEOVERWRITE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 04.run.trinity.sh -g [id] -l [path] <optional -r [path] -o [path] -f>

Note:         Script needs to be run with absolute PATH or from basic-output-data directory outside of prediction-folder.
Prerequisite: Trinity genome-guided-mode depends on STAR-aligned SRA-data.

-g [id]       genome file id (id of genome file = old structure, might be replaced by uuid )
-l [path]     path to SRR (left) input file, e.g. /mnt/ncbi_sra/SRRxxxxx/SRRxxxxx(_1).fastq
-r [path]     path to SRR right input file in case of paired-end data, e.g. /mnt/ncbi_sra/SRRxxxxx/SRRxxxxx_2.fastq
-o [path]     basic output data directory (will be the current directory if option not set)
              If genome-file-id and basic-output-data-dir are given, results will be written
              into basic_output_data_dir/prediction/trinity
-f            force overwrite

example:
./04.run.trinity.sh -g PCNB00000000 -l /mnt/ncbi_sra/SRR15205249/SRR15205249_1.fastq -r /mnt/ncbi_sra/SRR15205249/SRR15205249_2.fastq -o /mnt/customer/self

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fl:o:r:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    l )
      RNASEQ_LEFT=$OPTARG
      ;;
    r )
      RNASEQ_RIGHT=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

SRAACC=$( basename "${RNASEQ_LEFT}" | sed -n -E 's/^(SRR[0-9]{8})(|_[12])\.fastq$/\1/p' )

# Invoking the config-script only if trinity-script is not run from "multi-TRINITY".
if [ -z "${MULTI_TRINITY}" ]; then
  # The config-script will set all PATH-variables, check for presence or mkdir subfolders.
  # Turn debugging on within the config-script.
  SCRIPT="trinity"
  . config.sh

  STATUSFILE="04.run.trinity"_"${DATE_SAVED}".out
  LOGFILE="04.run.trinity"_"${DATE_SAVED}".log

#  METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#  [ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"
#
  STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
  LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
  ### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
  exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)
fi

# Check existence of files
if [ ! -f "${RNASEQ_LEFT}" ] || { [ -n "${RNASEQ_RIGHT}" ] && [ ! -f "${RNASEQ_RIGHT}" ]; }; then
  echo "RNA-Seq file ${RNASEQ_LEFT} or ${RNASEQ_RIGHT} DOES NOT exist."
  exit 1  # die with error code 1
fi

echo "#####################################################################################"
echo ""
echo "04.run.trinity.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

shopt -s globstar
#if compgen -G "${MAIN_OUT_DIR}/**/${SRAACC}_trinity_transcripts*.fasta" > /dev/null && [ -z "${FORCEOVERWRITE}" ]; then
if compgen -G "${OUT_DIR}"/"${SRAACC}"_trinity*.fasta > /dev/null && [ -z "${FORCEOVERWRITE}" ]; then
  if [ -n "${MULTI_TRINITY}" ]; then
    sleep 5s   # Important! Otherwise subprocesses might run out of tracking.
    echo "Trinity has already been run on SRA data ${SRAACC}, moving on to next data ..."
    exit 1
  else
    echo "Trinity has already been run on SRA data ${SRAACC}, nothing to do ..."
    exit 1
  fi
fi
shopt -u globstar
# Trinity: Generate de novo Transcriptome assembly
# option --seqType [fa|fq] is mandatory
# If paired reads:
# --left  <string>                     :left reads, one or more file names (separated by commas, no spaces)
# --right <string>                     :right reads, one or more file names (separated by commas, no spaces)
# --single <string>                    :single reads, one or more file names, comma-delimited (note, if single file contains pairs, can use flag: --run_as_paired )

# --genome_guided_bam <string>         :genome guided mode, provide path to coordinate-sorted bam file.
# --genome_guided_max_intron <int>     :mandatory:maximum allowed intron length (also maximum fragment span on genome)
# --genome_guided_min_coverage <int>              :optional:minimum read coverage for identifying an expressed region of the genome. (default: 1)
# --genome_guided_min_reads_per_partition <int>   :optional:default min of 10 reads per partition
# Note: If you have a highly fragmented draft genome, then you are likely better off performing a genome-free de novo transcriptome assembly.

# --full_cleanup                       :only retain the Trinity fasta file, rename as ${output_dir}.Trinity.fasta
# --jaccard_clip                       :option, set if you have paired reads and you expect high gene density with UTR
#                                       overlap (use FASTQ input file format for reads). (note: jaccard_clip is an expensive
#                                       operation, so avoid using it unless necessary due to finding excessive fusion transcripts w/o it.)
# --long_reads_bam <string>            :long reads to include for genome-guided Trinity
#                                       (bam file consists of error-corrected or circular consensus (CCS) pac bio read aligned to the genome)
# --long_reads <string>                :fasta file containing error-corrected or circular consensus (CCS) pac bio reads
# --min_contig_length <int>            :minimum assembled contig length to report (def=200)
# --include_supertranscripts           :yield supertranscripts fasta and gtf files as outputs.
# --no_version_check                   :dont run a network check to determine if software updates are available.
# --monitoring                         :use collectl to monitor all steps of Trinity
# --inchworm_cpu <int>                 :number of CPUs to use for Inchworm, default is min(6, --CPU option)
# --max_memory <string>                :suggested max memory to use by Trinity where limiting can be enabled. (jellyfish, sorting, etc)
#                                       provided in Gb of RAM, ie.  '--max_memory 10G'

if [ "${NRBASES}" -lt 50000 ]; then
  USE_JACCARD=" --jaccard_clip"
else
  USE_JACCARD=""
fi

if [ -n "${RNASEQ_LEFT}" ] && [ -z "${RNASEQ_RIGHT}" ]; then
  Trinity --seqType fq --trimmomatic \
  --CPU 12 --max_memory 80G"${USE_JACCARD}" \
  --genome_guided_bam "${MAIN_OUT_DIR}"/star/"${SRAACC}"/Aligned.sortedByCoord.out.bam \
  --genome_guided_max_intron "${INTRONMAX}" \
  --single "${RNASEQ_LEFT}" \
  --output "${OUT_DIR}"/"${SRAACC}"_trinity_single \
  --full_cleanup

elif [ -n "${RNASEQ_LEFT}" ] && [ -n "${RNASEQ_RIGHT}" ]; then
# For debugging:
#  echo "time Trinity --seqType fq --SS_lib_type RF --trimmomatic \
#  --CPU 24 --max_memory 100G ${USE_JACCARD} \
#  --genome_guided_bam ${MAIN_OUT_DIR}/star/${SRAACC}/Aligned.sortedByCoord.out.bam \
#  --genome_guided_max_intron ${INTRONMAX} \
#  --left ${RNASEQ_LEFT} --right ${RNASEQ_RIGHT} \
#  --output ${OUT_DIR}/${SRAACC}_trinity_transcripts_paired_${DATE_SAVED} \
#  --full_cleanup"
  Trinity --seqType fq --SS_lib_type RF --trimmomatic \
  --CPU 12 --max_memory 80G"${USE_JACCARD}" \
  --genome_guided_bam "${MAIN_OUT_DIR}"/star/"${SRAACC}"/Aligned.sortedByCoord.out.bam \
  --genome_guided_max_intron "${INTRONMAX}" \
  --left "${RNASEQ_LEFT}" --right "${RNASEQ_RIGHT}" \
  --output "${OUT_DIR}"/"${SRAACC}"_trinity_paired \
  --full_cleanup

  # --min_kmer_cov 2 \

fi

for f in "${OUT_DIR}"/"${SRAACC}"*.Trinity-GG.*; do
  mv "$f" "${f//Trinity-GG\./}"
done
# rename is not supported in docker
# rename 's/Trinity-GG.//' "${OUT_DIR}"/"${SRAACC}"*.*

end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"