#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container
# to have all software available.
################################################################################

beginning=$(date +%s)
start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

COMMANDARGS=$*

export AUGUSTUS_CONFIG_PATH=/mnt/software/augustus/config

showHelp() {
cat << EOF

Usage: 02.run.augustus.sh -g [id] -o [path] <option(s)>

mandatory
-g [id]                              genome file id (id of genome file = old structure, might be replaced by uuid )

input/output
-o [path]                            basic output data directory (will be the current directory if option not set)
                                     Results will be written into basic_output_data_dir/prediction/augustus

options (at least 1 must be given; case insensitive)
-c [rnaseq | (TSA) | pyscipio]       create hints (TSA could be StringTie, GMAP or TransAbyss)
                                     Note: - STAR hints for introns (SJ) will be filtered by min-cov = 10
                                           - TSA-data are expected to be present in a single, merged/concatenated gff-file
                                             within the GMAP directory
-i [path]                            path-to-gff-file (e.g. to utr-enriched PyScipio file)
-u                                   UTR-option (for creating hints, preparing gene set-file based on UTR-hints)
-p [existing AUGUSTUS profile]       predict genes with hints from STAR aligned reads based on existing profile,
                                     existing AUGUSTUS profile must be in the AUGUSTUS_CONFIG_PATH/species directory
-q [existing AUGUSTUS profile]       predict genes without hints based on existing profile,
                                     existing AUGUSTUS profile must be in the AUGUSTUS_CONFIG_PATH/species directory
-n [new | existing-id]               generate new profile (name will be created from genome file id),
                                     "new" will create a new profile from scratch, "existing-id" will copy an existing profile
                                     this will also prepare a gene set-file (gb-format) for training (use -u option to include UTRs)
-t [etraining | optimize | autoaug | utr]  AUGUSTUS training according to parameter
-z [nohints | withhints | utr | alternative]  final prediction !
-f                                   force overwrite
-x [number]                          set translation table
                                     (allowed numbers: 1 2 3 4 5 6 9 10 11 12 13 14 16 21 22 23 24 25 26 27 28 29 30 31 33)
                                     for adding new genetic codes, extend src/geneticcode.cc file

-h   Show this help

examples:
./02.run.augustus.sh -g JAATWF010000000 -o /mnt/customer/self -c rnaseq
./02.run.augustus.sh -g AACU03000000 -o /mnt/customer/self -z nohints

EOF
exit 0
}

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fo:c:i:up:q:n:t:z:x:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG      # ID of GenomeFile e.g. 'AACU03000000'
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    c )
      CREATEHINTS="true"
      HINTTYPE=$OPTARG
      ;;
    i )
      GFFFILE_PATH=$OPTARG
      ;;
    u )
      UTROPTION="true"
      ;;
    p )
      AUGUSTUS_PROFILE=$OPTARG
      PREDICTWITHHINTS="true"
      ;;
    q )
      AUGUSTUS_PROFILE=$OPTARG
      PREDICTNOHINTS="true"
      ;;
    n )
      AUGUSTUS_PROFILE=$OPTARG
      PREPAREDATA="true"
      ;;
    t )
      TRAINING="true"
      TRAININGTYPE=$OPTARG
      ;;
    z )
      FINALPREDICTION="true"
      PREDICTIONTYPE=$OPTARG
      ;;
    x )
      TRANSTABLE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

################################################################################

# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="augustus"
. config.sh

ALLOWEDHINTTYPES="rnaseq stringtie gmap transabyss pyscipio"
ALLOWEDHINTTYPES=$( tr '[:upper:]' '[:lower:]' <<<"$ALLOWEDHINTTYPES" )
[[ -n ${HINTTYPE} ]] && HINTTYPE=$( tr '[:upper:]' '[:lower:]' <<<"${HINTTYPE}" )
if [[ -n ${HINTTYPE} ]] && ! [[ ${ALLOWEDHINTTYPES} =~ (^|[[:space:]])$HINTTYPE($|[[:space:]]) ]]; then
  echo "Please provide an allowed argument for hint-type!"
  exit 1
fi
ALLOWEDTRAININGTYPES="etraining optimize autoaug utr"
ALLOWEDTRAININGTYPES=$( tr '[:upper:]' '[:lower:]' <<<"${ALLOWEDTRAININGTYPES}" )
[[ -n ${TRAININGTYPE} ]] && TRAININGTYPE=$( tr '[:upper:]' '[:lower:]' <<<"${TRAININGTYPE}" )
if [[ -n ${TRAININGTYPE} ]] && ! [[ ${ALLOWEDTRAININGTYPES} =~ (^|[[:space:]])$TRAININGTYPE($|[[:space:]]) ]]; then
  echo "Please provide an allowed argument for training!"
  exit 1
fi
ALLOWEDPREDICTIONTYPES="nohints withhints utr alternative"
ALLOWEDPREDICTIONTYPES=$( tr '[:upper:]' '[:lower:]' <<<"${ALLOWEDPREDICTIONTYPES}" )
[[ -n ${PREDICTIONTYPE} ]] && PREDICTIONTYPE=$( tr '[:upper:]' '[:lower:]' <<<"${PREDICTIONTYPE}" )
if [[ -n ${PREDICTIONTYPE} ]] && ! [[ $ALLOWEDPREDICTIONTYPES =~ (^|[[:space:]])$PREDICTIONTYPE($|[[:space:]]) ]]; then
  echo "Please provide an allowed argument for predicting genes!"
  exit 1
fi
if [[ -n "${AUGUSTUS_PROFILE}" ]] && [ ! -d "${AUGUSTUS_CONFIG_PATH}"/species/"${AUGUSTUS_PROFILE}" ]; then
  echo "Augustus profile for ${AUGUSTUS_PROFILE} not found. Please check!"
  exit 1  # die with error code 1
fi
ALLOWEDTRANSTABLES="1 2 3 4 5 6 9 10 11 12 13 14 16 21 22 23 24 25 26 27 28 29 30 31 33"
if [[ -n ${TRANSTABLE} ]] && ! [[ $ALLOWEDTRANSTABLES =~ (^|[[:space:]])$TRANSTABLE($|[[:space:]]) ]]; then
  echo "Please provide an allowed genetic code number!"
  exit 1
fi
NEW_AUGUSTUSPROFILE="profile_${GENOMEFILEID}"     # Name for new species profile
OLD_AUGUSTUSPROFILE="${AUGUSTUS_PROFILE}"         # Name for old species profile
AUGUSTUS_PARAMS="${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"/"${NEW_AUGUSTUSPROFILE}"_parameters.cfg

STATUSFILE="02.run.Augustus"_"${DATE_SAVED}".out
LOGFILE="02.run.Augustus"_"${DATE_SAVED}".log

#METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

run_time () {
  end=$(date +%s)
  runtime=$((end-start))
  hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
  echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
  echo ""
  start=$(date +%s)
}

set_stop_codons () {
  stop_codon_line="^${TRANSTABLE}\s+([0-9\.]{1,4})\s+([0-9\.]{1,4})\s+([0-9\.]{1,4}).*$"
  while IFS= read -r line || [[ -n $line ]]; do
    if [[ "$line" =~ ${stop_codon_line} ]]; then
      AMBER="${BASH_REMATCH[1]}"   # tag
      OCHRE="${BASH_REMATCH[2]}"   # taa
      OPAL="${BASH_REMATCH[3]}"    # tga
      sed -i -E "s/^(.*amberprob\s*)[0-9.]*(\s*.*)$/\1${AMBER}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*ochreprob\s*)[0-9.]*(\s*.*)$/\1${OCHRE}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*opalprob\s*)[0-9.]*(\s*.*)$/\1${OPAL}\2/" "${AUGUSTUS_PARAMS}"
    fi
  done < /mnt/software/hpc-cluster/scripts/prediction/genetic_codes.txt
}

################################################################################

echo "#####################################################################################"
echo ""
echo "02.run.augustus.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

if [[ ${CREATEHINTS} == "true" ]]; then
  # Make file to add all hints-gff files to.
  if [ ! -f "${OUT_DIR}"/hint_files.lst ]; then
    touch "${OUT_DIR}"/hint_files.lst
  fi

  if [[ ${HINTTYPE} == "rnaseq" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Creating hints from RNAseq data, required input are SJ and .wig files from STAR output ..."
    echo "We do not use bam2hints from AUGUSTUS, which generates a gff-hints file, because this script"
    echo "has no option to filter introns by coverage. Instead, the SJ-file from STAR is parsed directly."
    echo ""
    # Create hints from RNA-Seq data
    # STAR: SJ + .wig files required (.bam file is ignored)
    if [ ! -f "${OUT_DIR}"/hints.STAR.concatenated.gff ] || [ -n "${FORCEOVERWRITE}" ]; then
      # find -L "${MAIN_OUT_DIR}"/star -maxdepth 2 -mindepth 1 -type f -name 'Aligned.sortedByCoord.out.bam' > "${METADATAFOLDER}"/star_bamfile_list_"${DATE_SAVED}".txt
      find -L "${MAIN_OUT_DIR}"/star -maxdepth 2 -mindepth 1 -type f -name '*SJ.out.tab' > "${METADATAFOLDER}"/star_SJfile_list_"${DATE_SAVED}".txt
      if [ -s "${METADATAFOLDER}"/star_SJfile_list_"${DATE_SAVED}".txt ]; then
        # MAX_PARALLEL=5
        # The following line might better be de-commented when script is run manually. The line
        # ensures that all processes started within the loop will be killed if the script is
        # terminated prematurely. This is not needed within the pipeline, where it might lead
        # to an unwanted EXIT because of an error within one of the executed scripts in the loop.
        # trap 'echo killing all children; kill 0' EXIT
        while IFS= read -r line || [[ -n $line ]]
          do
            DIRECTORY=$( dirname "${line}" )
            SRAACC=$( basename "${DIRECTORY}" )
            # 2 Options to generate an AUGUSTUS hints file with introns for training/prediction
            # a) Parsing the SJ-output file from STAR directly
            #    => In this case, the number of reads supporting an intron junction can be used for filtering
            # b) Using bam2hints from AUGUSTUS, which parses the .bam file from the STAR output
            #    => bam2hints results in other counting than STAR, no filtering of the introns
            #       (e.g. introns found by multi-mapping reads are not ignored)
            #
            # Parsing the star_alignedSJ.out.tab file and generating a gff within the SRRxxx folder
            if [ ! -f "${DIRECTORY}"/hints.STAR.intron.gff ] || [ -n "${FORCEOVERWRITE}" ]; then
              touch "${DIRECTORY}"/hints.STAR.intron.gff
              # column 1: chromosome
              # column 2: first base of the intron (1-based)
              # column 3: last base of the intron (1-based)
              # column 4: strand (0: undefined, 1: +, 2: -)
              # column 5: intron motif: 0: non-canonical; 1: GT/AG, 2: CT/AC, 3: GC/AG, 4: CT/GC, 5: AT/AC, 6: GT/AT
              # column 6: 0: unannotated, 1: annotated (only if splice junctions database is used)
              # column 7: number of uniquely mapping reads crossing the junction
              # column 8: number of multi-mapping reads crossing the junction
              # column 9: maximum spliced alignment overhang
              while IFS= read -r line2 || [[ -n $line2 ]]; do
                full_line="^([^\s]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)$"
                if [[ "$line2" =~ ${full_line} ]]; then
                  acc="${BASH_REMATCH[1]}"
                  start="${BASH_REMATCH[2]}"
                  end="${BASH_REMATCH[3]}"
                  strand="${BASH_REMATCH[4]}"
                  if [ "${strand}" -eq 1 ]; then
                    strand="+"
                  elif [ "${strand}" -eq 2 ]; then
                    strand="-"
                  else
                    strand="."
                  fi
                  unique="${BASH_REMATCH[7]}"
                  # multi="${BASH_REMATCH[8]}"
                  if [ ${unique} -gt 10 ]; then
                    echo "$acc"$'\t'goenomics$'\t'intron$'\t'"$start"$'\t'"$end"$'\t'.$'\t'"$strand"$'\t'.$'\t'"mult=${unique};pri=4;src=E" >> "${DIRECTORY}"/hints.STAR.intron.gff
                  fi
                fi
              done < "${line}"
              # This sets the source to E (EST/cDNA) if not specified.
              # time bam2hints --intronsonly --in="${line}" --out="${DIRECTORY}"/hints.STAR.intron.gff
            else
              echo "SJ-hint file for ${SRAACC} already created, moving on to next STAR output file ..."
            fi
        done < "${METADATAFOLDER}"/star_SJfile_list_"${DATE_SAVED}".txt
      else
        echo "SJ-output files of STAR aligner not found where they should be. Please check!"
        # echo "Bam-output files of STAR aligner not found where they should be. Please check!"
        exit 1
      fi
      find -L "${MAIN_OUT_DIR}"/star -maxdepth 2 -mindepth 1 -type f -name '*Signal.Unique.str1.out.wig' > "${METADATAFOLDER}"/star_wigfile_list_"${DATE_SAVED}".txt
      if [ -s "${METADATAFOLDER}"/star_wigfile_list_"${DATE_SAVED}".txt ]; then
        MAX_PARALLEL=5
        # The following line might better be de-commented when script is run manually. The line
        # ensures that all processes started within the loop will be killed if the script is
        # terminated prematurely. This is not needed within the pipeline, where it might lead
        # to an unwanted EXIT because of an error within one of the executed scripts in the loop.
        # trap 'echo killing all children; kill 0' EXIT

        while IFS= read -r line || [[ -n $line ]]
          do
            DIRECTORY=$( dirname "${line}" )
            SRAACC=$( basename "${DIRECTORY}" )
            # This will set source to W (RNAseq data, weighting hints only for exonpart)
            if [ ! -f "${DIRECTORY}"/hints.STAR.ep.gff ] || [ -n "${FORCEOVERWRITE}" ]; then
              wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 \
              --src=W --type=ep --radius=4.5 --pri=4 --strand="." < "${line}" > "${DIRECTORY}"/hints.STAR.ep.gff && \
              cat "${DIRECTORY}"/hints.STAR.intron.gff "${DIRECTORY}"/hints.STAR.ep.gff > "${DIRECTORY}"/hints.STAR.gff &
            else
              echo "EP-hint file for ${SRAACC} already created, moving on to next STAR output file ..."
            fi
            # track job ids to wait for later
            pids[${SRAACC}]=$!
            # Wait here for any job to be finished, so there is a place to start next one.
            if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi

        done < "${METADATAFOLDER}"/star_wigfile_list_"${DATE_SAVED}".txt
        # wait for all pids
        for pid in ${pids[*]}; do
            wait $pid
        done
      else
        echo "Wig-output files of STAR aligner not found where they should be. Please check!"
        exit 1
      fi
      # Concatenate hints files.
      touch "${OUT_DIR}"/hints.STAR.concatenated.gff
      for dir in "${MAIN_OUT_DIR}"/star/SRR*; do
        cat "${DIRECTORY}"/hints.STAR.intron.gff "${DIRECTORY}"/hints.STAR.ep.gff >> "${OUT_DIR}"/hints.STAR.concatenated.gff
      done
      # Simplify headers
      sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\t[[:upper:][:lower:]].*)$/\1\2/' "${OUT_DIR}"/hints.STAR.concatenated.gff
    else
      echo "Hint files for STAR-aligned RNAseq data have already been prepared. Nothing to do."
    fi
    # Add hints file to hint list file
    if [ ! -f "${OUT_DIR}"/hint_files.lst ] || ( ! grep -q "hints.STAR.merged.gff" "${OUT_DIR}"/hint_files.lst ); then
      [ ! -f "${OUT_DIR}"/hint_files.lst ] && touch "${OUT_DIR}"/hint_files.lst
      echo "${OUT_DIR}"/hints.STAR.concatenated.gff >> "${OUT_DIR}"/hint_files.lst
    fi
  fi

  if [[ ${HINTTYPE} == "stringtie" ]] || [[ ${HINTTYPE} == "gmap" ]] || [[ ${HINTTYPE} == "transabyss" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Creating hints from ${HINTTYPE} data (essentially only reformatting) ..."
    echo ""
    # Prepare TSA data to be used in AUGUSTUS
    HINTTYPE_UPPER=$( echo "${HINTTYPE}" | tr '[:lower:]' '[:upper:]' )
    LATESTHINTFILE=$( find -L "${MAIN_OUT_DIR}"/stringtie_merge -type f -regex ".*${HINTTYPE}_[^/]*\.g.f" )
    GTFGFF="${LATESTHINTFILE##*.}"
    HINTTYPEBASENAME="${OUT_DIR}"/hints."${HINTTYPE_UPPER}"
    if [ ! -f "${OUT_DIR}"/hints."${HINTTYPE_UPPER}"."${GTFGFF}" ]; then
#      if [ -f "${BASE_DIR}"/"${OUT_DIR}"/stringtie/transcripts_merged.gff ]; then
#        MERGED="_merged"
#      else
#        MERGED=""
#      fi
      if [ -f "${LATESTHINTFILE}" ]; then
        if ! ( grep -q -E '^.*;src=E$' "${LATESTHINTFILE}" ); then
          # Append src=E to gff-attributes for correct weighting of hints (in extrinsic.MPEW.cfg file)
          sed -n -E 's/^([^#].*)$/\1;src=E/p' "${LATESTHINTFILE}" > "${HINTTYPEBASENAME}"."${GTFGFF}"
        else
          cp "${LATESTHINTFILE}" "${HINTTYPEBASENAME}"."${GTFGFF}"
        fi
        # Optional: StringTie output from a single dataset does not contain a source specifier
        # Exchange "." in 2nd column by "StringTie"
        if [[ ${HINTTYPE} == "stringtie" ]]; then
          sed -i -E 's/^([^\t]*\t)\.(\t.*)$/\1StringTie\2/' "${HINTTYPEBASENAME}"."${GTFGFF}"
        fi
        # The TSA regions mapped onto the genome are named "exons". For better training the terminal exons
        # will be renamed to "exonpart". "exons" and "exonpart" get different weightings during training and prediction.
        # 1. Change first exon of gene/mRNA
        sed -i -E '/transcript\s/{n;s/exon/exonpart/}' "${HINTTYPEBASENAME}"."${GTFGFF}"
        # 2. Change last exon of gene/mRNA
        sed -i -E 'N;s/exon(\s.*\n.*transcript\s)/exonpart\1/;P;D' "${HINTTYPEBASENAME}"."${GTFGFF}"
        # Simplify contig headers in hints file
        cp "${HINTTYPEBASENAME}"."${GTFGFF}" "${HINTTYPEBASENAME}".min."${GTFGFF}"
        sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\t[[:upper:][:lower:]].*)$/\1\2/' "${HINTTYPEBASENAME}".min."${GTFGFF}"
        # Adjust PyScipio SO-types (GFF3 conform) so that they comply to extrinsic.MPEW.cfg file terms
        sed -i '/transcript/d' "${HINTTYPEBASENAME}".min."${GTFGFF}"
      else
        echo "${HINTTYPE_UPPER} transcript gff-file not found where it should be or AUGUSTUS-formatted file already generated. Please check!"
        exit 1
      fi
    else
      echo "${HINTTYPE_UPPER} hints file already prepared. Nothing to do."
    fi
    # Add hints file to hint list file
    if [ ! -f "${OUT_DIR}"/hint_files.lst ] || ( ! grep -q -E ".*${HINTTYPE_UPPER}.*\.g.f" "${OUT_DIR}"/hint_files.lst ); then
      [ ! -f "${OUT_DIR}"/hint_files.lst ] && touch "${OUT_DIR}"/hint_files.lst
      echo "${HINTTYPEBASENAME}".min."${GTFGFF}" >> "${OUT_DIR}"/hint_files.lst
    fi
  fi


  if [[ ${HINTTYPE} == "pyscipio" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Creating hints from PyScipio genes (essentially only reformatting) ..."
    echo ""
    PYSCIPIO_UPPER=$( echo "${HINTTYPE}" | tr '[:lower:]' '[:upper:]' )
    if [ -z "${GFFFILE_PATH}" ]; then
      PYSCIPIOFILE="${MAIN_OUT_DIR}"/"${HINTTYPE}"/allGenesFirstPrediction.complete.gff
    elif [ -n "${GFFFILE_PATH}" ] && [ -f "${GFFFILE_PATH}" ]; then
      PYSCIPIOFILE="${GFFFILE_PATH}"
    else
      echo "GFF-file option chosen and path given but file was not found where it should be. Please check!"
      exit 1
    fi
    if [ -n "${UTROPTION}" ]; then
      UTR=".utr"
    else
      UTR=""
    fi
    PYSCIPIOBASENAME="${OUT_DIR}"/hints."${PYSCIPIO_UPPER}""${UTR}"
    if [ -f "${PYSCIPIOFILE}" ] && [ ! -f "${PYSCIPIOBASENAME}".min.sorted.gff ]; then
      # Prepare PyScipio data to be used in AUGUSTUS
      cp "${PYSCIPIOFILE}" "${PYSCIPIOBASENAME}".gff
      sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\tPyS.*)$/\1\2/' "${PYSCIPIOBASENAME}".gff
      sed -i '/##.*/d' "${PYSCIPIOBASENAME}".gff
      # Append src=P to gff-attributes for correct weighting of hints (in extrinsic.MPEW.cfg file)
      if ! ( grep -q -E '^.*;src=P$' "${PYSCIPIOBASENAME}".gff ); then
        sed -i -E 's/^([^#].*)$/\1;src=P/' "${PYSCIPIOBASENAME}".gff
      fi
      # Simplify contig headers in hints file
      cp "${PYSCIPIOBASENAME}".gff "${PYSCIPIOBASENAME}".min.gff
      sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\t[[:upper:][:lower:]].*)$/\1\2/' "${PYSCIPIOBASENAME}".min.gff
      # Adjust PyScipio SO-types (GFF3 conform) so that they comply to extrinsic.MPEW.cfg file terms
      sed -i 's/three_prime_UTR/UTR/' "${PYSCIPIOBASENAME}".min.gff
      sed -i 's/five_prime_UTR/UTR/' "${PYSCIPIOBASENAME}".min.gff
      sed -i 's/gene/genicpart/' "${PYSCIPIOBASENAME}".min.gff
      sed -i 's/start_codon/start/' "${PYSCIPIOBASENAME}".min.gff
      sed -i 's/stop_codon/stop/' "${PYSCIPIOBASENAME}".min.gff
      sed -i '/mRNA/d' "${PYSCIPIOBASENAME}".min.gff
      # TODO: why sort lines???
      gt gff3 -sortlines -tidy -retainids "${PYSCIPIOBASENAME}".min.gff > "${PYSCIPIOBASENAME}".min.sorted.gff
    else
      echo "PyScipio gene gff-file not found where it should be or AUGUSTUS-formatted file already generated. Please check!"
      exit 1
    fi
    # Add hints file to hint list file
    if [ ! -f "${OUT_DIR}"/hint_files.lst ] || ( ! grep -q "${PYSCIPIOBASENAME}.min.sorted.gff" "${OUT_DIR}"/hint_files.lst ); then
      echo "${PYSCIPIOBASENAME}".min.sorted.gff >> "${OUT_DIR}"/hint_files.lst
    fi
  fi

  # Concatenate all hints into a single hints file to be used during gene prediction
  if [ -f "${OUT_DIR}"/hints.EXTRINSIC.concatenated.gff ]; then
    : > "${OUT_DIR}"/hints.EXTRINSIC.concatenated.gff
    while IFS= read -r line || [[ -n $line ]]; do
      cat $line >> "${OUT_DIR}"/hints.EXTRINSIC.concatenated.gff
    done < "${OUT_DIR}"/hint_files.lst
  fi
fi

################################################################################

# Iterative training
# Read: https://vcru.wisc.edu/simonlab/bioinformatics/programs/augustus/docs/tutorial2015/ittrain.html
# BUT: Why should "iterative" work at all?
# Assumption: "Iterative" in the sense of AUGUSTUS means:
#                a) training genes in the target genome do NOT exist
#                b) start iteration by predicting genes with a related profile (optionally use RNAseq hints)
#                c) use predicted genes as hints in further round of training
# However: If training genes are available, ALL genes/hints should be used in 1 training step, otherwise
#          species parameters will be overwritten by last training run

# AUGUSTUS options explained
# --softmasking=on     The genome assembly must have been soft-masked (e.g. by RepeatMasker).
#                      If not soft-masked this option does not have any effect.
# --predictionEnd=#    Prediction will only be done in up to nucleotide # of the genome assembly
# --extrinsicCfgFile=  The file specified will be used to weight the hints (e.g. manual vs rnaseq vs protein vs est)

################################################################################

# Option 1: NO hints available, train species profile by gene prediction with related species profile
# species list under /software/augustus/config/species

################################################################################

# Option 2: Predict genes using STAR hints (RNA-Seq evidence)
# In this case, a "wrong" species profile from a related species will be used.
# The resulting gene predictions should ONLY be used for training a new profile.
# The extrinsic.MPEW.cfg-file is a GOENOMICS-generated weighting scheme containing
# weights for hints/evidence from protein, EST (RNA-seq generated TSAs), RNA-seq (STAR-aligned reads) and AUGUSTUS predictions.

if [[ ${PREDICTWITHHINTS} == "true" ]] || [[ ${PREDICTNOHINTS} == "true" ]]; then
  if [[ ${PREDICTNOHINTS} == "true" ]]; then
    text="without using any hints"
    H=".nohints"
    # no parameter to set, using minimal run options
    PARAMS=""
  fi
  if [[ ${PREDICTWITHHINTS} == "true" ]]; then
    if [ ! -f "${OUT_DIR}"/hints.STAR.concatenated.gff ]; then
      echo "A hints file has not been found where it should be. Please check!"
      exit 1
    fi
    # Simplify contig headers in hints file
    sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\t[[:upper:][:lower:]].*)/\1\2/' "${OUT_DIR}"/hints.STAR.concatenated.gff
    cp "${OUT_DIR}"/hints.STAR.concatenated.gff "${HOME}"/hints.STAR.concatenated.gff
    text="with hints from RNAseq"
    H=".withhints"
    # set parameters to use hints
    PARAMS="--extrinsicCfgFile=extrinsic.MPEW.cfg --hintsfile="${HOME}"/hints.STAR.concatenated.gff "
  fi
  echo "-------------------------------------------------------------------------------------"
  echo "Predicting genes $text data. The resulting genes are used to"
  echo "generate training/test genes for the training process."
  echo ""

  if [ ! -f "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff ] || [ -n "${FORCEOVERWRITE}" ]; then
    # Parallelize prediction in dependence of genome file size
    MAXSIZE=50000000
    FILESIZE=$(stat -c%s "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta)
    if (( FILESIZE < MAXSIZE )); then
      # Case 1: Small genomefiles 50Mb ~50 min
      # for debugging add parameters --predictionStart=1 --predictionEnd=750000
      augustus --species="${OLD_AUGUSTUSPROFILE}" "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
        ${PARAMS}--genemodel=complete --outfile="${HOME}"/hints.AUGUSTUS.predicted"${H}".gff --errfile="${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".err
      mv "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
#      sourcefile=$( stat --printf="%s" "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff )
#      targetfile=$( stat --printf="%s" "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff )
#      echo "${sourcefile}" "${targetfile}"
#      if [ "${sourcefile}" -eq "${targetfile}" ]; then
#        echo "files identical"
#      fi
#      until [ "${sourcefile}" -eq "${targetfile}" ]; do
#        echo "rsync from kubernetes pod failed, re-trying ..."
#        rsync "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
#        sourcefile=$( stat --printf="%s" "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff )
#        targetfile=$( stat --printf="%s" "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff )
#      done
#      rm "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff
    else
      # Case 2: Big genomefiles
      MAX_PARALLEL=10
      echo "${MAX_PARALLEL} jobs in parallel ..."
      echo ""
      /mnt/software/pyscipio3/scripts/splitfile.py "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta 50M '>'
      i=0
      for f in "${GENOME_DIR}"/*.min_*; do
        augustus --species="${OLD_AUGUSTUSPROFILE}" "${f}" ${PARAMS}--genemodel=complete \
        --outfile="${HOME}"/hints.AUGUSTUS.predicted"${H}".$i.gff --errfile="${HOME}"/hints.AUGUSTUS.predicted"${H}".$i.err &
        ((i++))
        # track job ids to wait for later
        pids[${SRAACC}]=$!
        # Wait here for any job to be finished, so there is a place to start next one.
        if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi
      done
      # wait for all pids
      for pid in ${pids[*]}; do
          wait $pid
      done
      cat "${HOME}"/hints.AUGUSTUS.predicted"${H}".*.gff > "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff
      mv "${HOME}"/hints.AUGUSTUS.predicted"${H}".gff "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
      cat "${HOME}"/hints.AUGUSTUS.predicted"${H}".*.err >> "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".err
      rm "${HOME}"/hints.AUGUSTUS.predicted"${H}".*.gff "${HOME}"/hints.AUGUSTUS.predicted"${H}".*.err
    fi
    [ -f "${HOME}"/hints.STAR.concatenated.gff ] && rm "${HOME}"/hints.STAR.concatenated.gff
  else
    echo "An AUGUSTUS prediction based on a related species profile has already been generated. Please check."
    echo ""
  fi

  if [[ ${PREDICTWITHHINTS} == "true" ]]; then
    if [ ! -f "${OUT_DIR}"/star_supported.lst ] || [ -n "${FORCEOVERWRITE}" ]; then
      # Generate list of genes that are 100% supported by STAR hints (introns and exonpart)
      # Be aware: We are likely to introduce a bias when we restrict ourselves to this set, e.g. to the highly
      # expressed genes. Also, we are introducing a bias towards genes that fit the previous model well.
      # We apply the following very simple filter: Select all transcripts that are fully supported by evidence.
      #
      # The list will be used later when generating the GenBank gb-file.
      echo "Generating a list of genes that are 100% supported by STAR hints (introns and exonpart) ..."
      echo ""
      perl -ne 'if (/\ttranscript\t.*\t(\S+)/){$tx=$1;} if (/transcript supported.*100/) {print "$tx\n";}' < "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff \
        | tee "${OUT_DIR}"/star_supported.lst
    else
      echo "List of genes 100% supported by STAR hints already prepared. Nothing to do."
      echo ""
    fi
  fi

  # Append src=A to gff-attributes for correct weighting of hints (in extrinsic.MPEW.cfg file)
  # make AUGUSTUS hints file gtf conform
  if ! ( grep -q -E '^.*;src=A$' "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff ); then
    sed -i -E 's/^([^#].*[^;])$/\1;src=A/' "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
    sed -i -E 's/^([^#].*);$/\1;src=A/' "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
    sed -i -E 's/(.*\t)([g0-9]*\.[t0-9]*)$/\1transcript_id "\2";/' "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
    sed -i -E 's/(.*\t)([g0-9]*)$/\1gene_id "\2";/' "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff
  fi

  # Add hints file to hint list file
  if [ ! -f "${OUT_DIR}"/hint_files.lst ] || ( ! grep -q hints.AUGUSTUS.predicted"${H}".gff "${OUT_DIR}"/hint_files.lst ); then
    echo "Adding AUGUSTUS predictions to hint list file"
    echo ""
    echo "${OUT_DIR}"/hints.AUGUSTUS.predicted"${H}".gff >> "${OUT_DIR}"/hint_files.lst
  else
    echo "File with AUGUSTUS predictions already added to hint list file. Please check."
    echo ""
  fi
fi

################################################################################

# Generate a new species profile (written automatically in AUGUSTUS-CONFIG-PATH)
# Prepare gene set for training:  a) get genes (gtf/gff3)
#                                 b) merge gene sets (optional)
#                                 c) add flanking DNA and convert to gb format
#                                 d) random slit into training and test genes

if [[ ${PREPAREDATA} == "true" ]]; then
  echo "-------------------------------------------------------------------------------------"
  echo "Generating a new species profile ..."
  echo ""

  if [ ! -d "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}" ] || [ -n "${FORCEOVERWRITE}" ]; then
    # Create new species profile (meta parameters) for training.
    if [[ -n ${AUGUSTUS_PROFILE} ]] && [[ ${AUGUSTUS_PROFILE} == "new" ]]; then
      new_species.pl --species="${NEW_AUGUSTUSPROFILE}"
    # Copy existing profile from AUGUSTUS folder structure
    elif [[ -n ${AUGUSTUS_PROFILE} ]] && [ -d "${AUGUSTUS_CONFIG_PATH}"/species/"${OLD_AUGUSTUSPROFILE}" ]; then
      [ ! -d "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}" ] && mkdir "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"
      cp "${AUGUSTUS_CONFIG_PATH}"/species/"${OLD_AUGUSTUSPROFILE}"/*.cfg "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"
      cp "${AUGUSTUS_CONFIG_PATH}"/species/"${OLD_AUGUSTUSPROFILE}"/*.pbl "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"
      cp "${AUGUSTUS_CONFIG_PATH}"/species/"${OLD_AUGUSTUSPROFILE}"/*.txt "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"
      # 1. find files to rename
      # 2. replace pattern, output "file_old file_new"
      # 3. mv old to new
      find "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}" -type f | \
         sed -n -E "s/^(.*)${OLD_AUGUSTUSPROFILE}(.*)\$/& \1${NEW_AUGUSTUSPROFILE}\2/p" | xargs -n 2 mv
      # requires rename command to be installed, e.g. sudo apt-get rename
      # rename 's/"${OLD_AUGUSTUSPROFILE}"/"${NEW_AUGUSTUSPROFILE}"/' "${AUGUSTUS_CONFIG_PATH}"/species/"${NEW_AUGUSTUSPROFILE}"/*.*
      # 4. rename file mentions in parameters.cfg file
      sed -i -E "s/${OLD_AUGUSTUSPROFILE}/${NEW_AUGUSTUSPROFILE}/" "${AUGUSTUS_PARAMS}"
    else
      echo "The specified profile of ${AUGUSTUS_PROFILE} does not exist. Please check."
      exit 1
    fi
  else
    echo "The specified profile of ${NEW_AUGUSTUSPROFILE} does already exist. Using the existing profile from now."
    echo ""
  fi
  # Edit new species config
  # set stopCodonExcludedFromCDS to true (required for etraining and augustus)
  sed -i "s/stopCodonExcludedFromCDS false/stopCodonExcludedFromCDS true/" "${AUGUSTUS_PARAMS}"
  # add line with translation table before the lines with the stop codon probabilities
  sed -i -E '/^\/Constant\/amberprob.*/i translation_table 1' "${AUGUSTUS_PARAMS}"
  if [[ -n ${TRANSTABLE} ]]; then
    echo "Setting translation table for ${GENOMEFILEID} to ${TRANSTABLE} ..."
    echo ""
    sed -i -E "s/^(translation_table\s)1\$/\1$TRANSTABLE/" "${AUGUSTUS_PARAMS}"
    set_stop_codons
  fi

  echo "-------------------------------------------------------------------------------------"
  echo "Merge all gff files containing hints into a single gff file ..."
  echo ""

  if [ -n "${UTROPTION}" ]; then
    UTR=".utr"
  else
    UTR=""
  fi

  # 1) Simplify contig headers in hints file
  # Make file to add all hints-gff files to.
  if [ -f "${OUT_DIR}"/hint_files.lst ] && [ ! -f "${OUT_DIR}"/hint_files_new.lst ]; then
    touch "${OUT_DIR}"/hint_files_new.lst
  fi
  if [ -f "${OUT_DIR}"/hint_files_new.lst ]; then
    while IFS= read -r line || [[ -n $line ]]
      do
        # DIRNAME=$( dirname "$line" )
        FULLFILENAME=$( basename "$line" )
        BASEFILENAME="${FULLFILENAME%.gff}"
        # Only hints from PyScipio and preliminary predictions from AUGUSTUS contain
        # complete genes (start, stop, CDS), which are necessary for generating a gene-set for training
        # sed -i '/.*stringtie.*/d' "${OUT_DIR}"/hint_files.lst
        HINTTYPE=$( echo "${BASEFILENAME}" | sed -E 's/hints\.([A-Z]*)\..*/\1/' )
        ALLOWEDHINTTYPES="PYSCIPIO AUGUSTUS"
        if ! [[ ${ALLOWEDHINTTYPES} =~ (^|[[:space:]])$HINTTYPE($|[[:space:]]) ]]; then
          continue
        else
          FILENAME="${OUT_DIR}"/"${BASEFILENAME}"
          # Add hints file to new hint list file
          if [ ! -f "${OUT_DIR}"/hint_files_new.lst ] || ( ! grep -q "${FILENAME}" "${OUT_DIR}"/hint_files_new.lst ); then
            echo "${FILENAME}"_simplified.gff >> "${OUT_DIR}"/hint_files_new.lst
          fi
          cp "${line}" "${FILENAME}"_simplified.gff
          sed -i -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[^\t]+(\t[[:upper:][:lower:]].*)/\1\2/' "${FILENAME}"_simplified.gff
          sed -i '/##.*/d' "${FILENAME}"_simplified.gff
        fi
    done < "${OUT_DIR}"/hint_files.lst
    # 2) Merge gff files
    FILENR=$( wc -l < "${OUT_DIR}"/hint_files_new.lst )
    if [ "${FILENR}" -gt 1 ]; then
      # TODO: Do not use gffcompare! This removes the CDS. Use gffread -merge instead!
      # gffcompare [options]* {-i <input_gtf_list> | <input1.gtf> [<input2.gtf> .. <inputN.gtf>]}
      # -M	discard (ignore) single-exon transfrags and reference transcripts (i.e. consider only multi-exon transcripts)
      # -C	Discard the “contained” transfrags from the .combined.gtf output. By default, without this option, gffcompare
      #     writes in that file isoforms that were found to be fully contained/covered (with the same compatible intron
      #     structure) by other transfrags in the same locus, with the attribute “contained_in” showing the first container
      #     transfrag found. (Note: this behavior is the opposite of Cuffcompare's -C option).
      # -A	Like -C but will not discard intron-redundant transfrags if they start on a different 5' exon (keep alternate transcript start sites)
      # -X	Like -C but also discard contained transfrags if transfrag ends stick out within the container's introns
      # -o <outprefix>	All output files created by gffcompare will have this prefix (e.g. .loci, .tracking, etc.).
      #                 If this option is not provided the default output prefix being used is: “gffcmp”
      gffcompare -XD -i "${OUT_DIR}"/hint_files_new.lst -o "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged
      # Use "*.combined.gtf" file generated by gffcompare for further processing.
      cp "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.combined.gtf "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf
    else
      GFFTOCOPY=$( head -1 "${OUT_DIR}"/hint_files_new.lst )
      cp "${GFFTOCOPY}" "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf
    fi
    # 3) Append src= to gff-attributes for correct weighting of hints (in extrinsic.MPEW.cfg file)
    sed -i -E 's/^.*PyScipio.*$/&;src=P/' "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf
    # sed -i -E 's/^.*StringTie.*$/&;src=E/' "${OUT_DIR}"/hints.AUG.merged.gtf
    # hints from augustus gene prediction based on STAR-hints
    sed -i -E 's/^.*AUGUSTUS.*$/&;src=A/' "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf
  else
    echo "Hint files not found, skipping gff merging. Please re-run later to include hint file preparation."
    echo ""
  fi

  echo "-------------------------------------------------------------------------------------"
  echo "Selecting genes with introns from merged gff ..."
  echo ""
  # Select genes with introns
  # gffread <input_gff> [-g <genomic_seqs_fasta> | <dir>][-s <seq_info.fsize>]
  #              [-o    <outfile.gff>]    [-t    <tname>]    [-r   [[<strand>]<chr>:]<start>..<end>]
  #              [-CTVNMAFGRUVBHSZWTOE] [-w <spl_exons.fa>] [-x <spl_cds.fa>] [-y  <tr_cds.fa>]  [-i
  #              <maxintron>]
  # -U     discard single-exon transcripts
  # -N     only show multi-exon mRNAs if all their introns have the typical splice site
  #        consensus ( GT-AG, GC-AG or AT-AC )  => this option might be useful if gff with hints from RNAseq are included
  # -g     full path to a multi-fasta file with the genomic sequences
  #        for all input mappings, OR a directory with single-fasta files
  #        (one per genomic sequence, with file names matching sequence names)
  if [ -f "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf ]; then
    cp "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.gtf "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_filtering.gtf
    # StringTie hints cannot distinguish between coding and non-coding in exons, but AUGUSTUS needs
    # clear coding (CDS) to train "exon" parameters (which are interpreted as coding only)
    # sed -i '/*StringTie*/d' "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_filtering.gtf
    gffread "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_filtering.gtf -o "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf -UNMKQY -g "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta
    # gff2gbSmallDNA.pl, which is needed to generate gene sets for training, doesn't understand "exon", expects "CDS"
    #sed -i -E 's/\texon\t/\tCDS\t/' "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf
  else
    echo "Because hint files are missing, no selection possible. Please re-run later again."
    echo ""
  fi

  echo "-------------------------------------------------------------------------------------"
  echo "Computing size of flanking DNA (up- and downstream DNA) which should be added to genes ..."
  echo ""
  FILESIZE=$(stat -L -c%s "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta)
  min_size_of_gene_flanking_DNA=1000
  max_size_of_gene_flanking_DNA=10000
  GENE_FLANKING_DNA=$(( FILESIZE / 50000 ))
  if (( GENE_FLANKING_DNA < min_size_of_gene_flanking_DNA )); then
    size_of_gene_flanking_DNA=${min_size_of_gene_flanking_DNA}
  elif (( max_size_of_gene_flanking_DNA < GENE_FLANKING_DNA )); then
    size_of_gene_flanking_DNA=${max_size_of_gene_flanking_DNA}
  else
    size_of_gene_flanking_DNA=${GENE_FLANKING_DNA}
  fi
  echo "Size of flanking DNA: ${size_of_gene_flanking_DNA} bp"
  echo ""

  echo "-------------------------------------------------------------------------------------"
  echo "Generating gene set for AUGUSTUS training ..."
  echo "(selecting supported=good genes from gene set, adding flanking DNA)"
  echo ""
  # Create a training set
  # Usage: gff2gbSmallDNA.pl gff-file seq-file max-size-of-gene-flanking-DNA output-file [options]
  # options:
  # --bad=badfile    Specify a file with gene names. All except these are included in the output.
  # --good=goodfile  Specify a file with gene names. Only these genes are included in the output.
  # --overlap        Overlap filtering turned off.

  if [ -f "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf ]; then
    if [ -f "${OUT_DIR}"/star_supported.lst ] && ( grep -q AUGUSTUS "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf ); then
      grep -P "\tAUGUSTUS\t" "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf > "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.aug.gtf
      gff2gbSmallDNA.pl --good="${OUT_DIR}"/star_supported.lst "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.aug.gtf "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
         ${size_of_gene_flanking_DNA} "${OUT_DIR}"/genes"${UTR}".aug.gb
      grep -P "\tPyScipio\t" "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf > "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.pys.gtf
      gff2gbSmallDNA.pl "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.pys.gtf "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
         ${size_of_gene_flanking_DNA} "${OUT_DIR}"/genes"${UTR}".pys.gb
      cat "${OUT_DIR}"/genes"${UTR}".*.gb > "${OUT_DIR}"/genes"${UTR}".gb
      rm "${OUT_DIR}"/genes"${UTR}".*.gb "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.*.gtf
    else
      # TODO: this script fails if a gene starts with coordinate "1". Remove all genes from gff that start with coordinate "1".
      gff2gbSmallDNA.pl "${OUT_DIR}"/hints.AUGUSTUS"${UTR}".merged.for_training.gtf "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
         ${size_of_gene_flanking_DNA} "${OUT_DIR}"/genes"${UTR}".gb
    fi
  else
    echo "Because hint files are missing, no gene set creation possible. Please re-run later again."
    echo ""
  fi

  echo "-------------------------------------------------------------------------------------"
  echo "Removing genes from gene set that lead to error messages in etraining ..."
  echo ""
  # Do a training, look for errors, filter all genes resulting in errors from gene set
  if [ -f "${OUT_DIR}"/genes_before_filtering"${UTR}".gb ]; then
    mv "${OUT_DIR}"/genes"${UTR}".gb "${OUT_DIR}"/genes_before_filtering"${UTR}".gb
    etraining --species="${NEW_AUGUSTUSPROFILE}" "${OUT_DIR}"/genes_before_filtering"${UTR}".gb 2>&1 | grep "in sequence" | \
      perl -pe 's/.*n sequence (\S+):.*/$1/' | sort -u > "${OUT_DIR}"/bad"${UTR}".lst

    filterGenes.pl "${OUT_DIR}"/bad"${UTR}".lst "${OUT_DIR}"/genes_before_filtering"${UTR}".gb > "${OUT_DIR}"/genes"${UTR}".gb
  else
    echo "Because hint files are missing, no gene set filtering possible. Please re-run later again."
    echo ""
  fi

  echo "-------------------------------------------------------------------------------------"
  echo "Splitting gene set into training and test set ..."
  echo ""

  if [ -f "${OUT_DIR}"/genes"${UTR}".gb ]; then
    NUMBERGENES=$( grep -c "LOCUS" "${OUT_DIR}"/genes"${UTR}".gb )
    NUMBERTRAININGGENES=$(( NUMBERGENES * 4 / 5 ))
    # NUMBERTRAININGGENES=${float%.*}
    if [ ${NUMBERTRAININGGENES} -gt 400 ]; then
      # Usage: randomSplit dbfile test_size
      randomSplit.pl "${OUT_DIR}"/genes"${UTR}".gb ${NUMBERTRAININGGENES}
      echo "Number of genes in training set: ${NUMBERTRAININGGENES}"
      echo "Number of genes in test set    :" $(( NUMBERGENES - NUMBERTRAININGGENES ))
    else
      echo "Not enough genes in training set!"
      exit 1
    fi
  else
    echo "GenBank-file for training AUGUSTUS not found."
    exit 1
  fi
fi

################################################################################

if [[ ${TRAINING} == "true" ]]; then
  echo "-------------------------------------------------------------------------------------"
  echo "Training AUGUSTUS with hints ..."
  echo ""

  PARAMSTRANSTABLE=$( sed -n -E 's/^translation_table\s([0-9]+)$/\1/p' "${AUGUSTUS_PARAMS}" )
  if [[ ${PARAMSTRANSTABLE} == "1" ]] && [[ -n "${TRANSTABLE}" ]] && [[ ${TRANSTABLE} != "1" ]]; then
    echo "Translation table has not been set yet, changing translation table to #${TRANSTABLE} ..."
    echo ""
    if ( ! grep -q "translation_table" "${AUGUSTUS_PARAMS}" ); then
      sed -i -E '/^\/Constant\/amberprob.*/i translation_table 1' "${AUGUSTUS_PARAMS}"
    fi
    sed -i -E "s/^(translation_table\s)1\$/\1$TRANSTABLE/" "${AUGUSTUS_PARAMS}"
    set_stop_codons
  elif [[ -n "${TRANSTABLE}" ]] && [[ ${TRANSTABLE} != "${PARAMSTRANSTABLE}" ]]; then
    echo "Translation table in species config file (= #${PARAMSTRANSTABLE}) contradicts input translation table (= #${TRANSTABLE})! Please check!"
    exit 1
  fi

  # 1. Step: Make initial training with RNAseq-hints
  # Note: Dependent on initially predicted genes

  # AUGUSTUS uses 2 sets of params: "meta parameters" and "parameters"
  # "meta parameters" are params that set the environment to determine the "parameters"
  # Example: the "meta parameters" set the window size where the splice site model (=> "parameter") is determined
  # or the weighting scheme for using GC content across the genome.
  # => The "etraining" uses the "meta parameters" from the standard (or better: the initial species configs) and
  #    based on these computes all "new profile" parameters.
  # => The "optimize_augustus" iterates between "etraining" and "augustus" and iteratively optimizes the "meta parameters"
  # As consequence, etraining is fast and optimize_augustus is very slow. optimize_augustus is said to optimize the
  # overall prediction by only a few percent. Maybe the more efficient way would be to get appropriate "meta parameters"
  # out of "complete PyScipio" hits. Maybe do etraining with "manually" set meta parameters and subsequently check the
  # accuracy with the quick augustus prediction in the small test set, and decide on the output whether to do a time-consuming
  # long optimize_augustus.
  # => The "autoAug" script is an intermediate between etraining and optimize. It starts with the gff-file containing
  #    the hints and subsequently does all the hints reformatting and filtering, adding of flanking DNA and splitting genes,
  #    and finally does an etraining on the training gene set. autoAug also does "meta parameter" optimization (default: 1 round).
  #    Principally, autoAug could also take hints and align them to the genome, and could generate a new species profile from
  #    an existing and subsequently do at least 2 rounds of iterative training (1st step train a new profile with predictions
  #    based on existing (old) species profile, 2nd step train the new profile based on predictions based on this new profile).
  #    autoAug does not optimize UTR parameters, except if cDNA hint data are given!

  # Training of UTR regions should only be done if UTRs are annotated in the training genes
  if [ -n "${UTROPTION}" ]; then
    if [ -f "${OUT_DIR}"/genes.utr.gb.train ]; then
      UTR=".utr"
      WITHUTR=".utr"
      echo "Training genes contain UTR regions ..."
      echo ""
    else
      echo "UTR option set but file with training genes containing UTRs not found. Please check!"
      exit 1
    fi
  elif [ -f "${OUT_DIR}"/genes.utr.gb.train ] && [[ ${TRAININGTYPE} == "utr" ]]; then
    UTR=".utr"
    WITHUTR=".onlyutr"
    echo "Training genes contain UTR regions, UTR training enabled ..."
    echo ""
  elif [ -f "${OUT_DIR}"/genes.utr.gb.train ] && [ -z "${UTROPTION}" ]; then
    UTR=""
    WITHUTR=""
    echo "Training genes contain UTR regions but UTR training was not enabled ..."
    echo ""
  elif [ -f "${OUT_DIR}"/genes.gb.train ] && [ -z "${UTROPTION}" ]; then
    UTR=""
    WITHUTR=""
    echo "Training genes do NOT contain UTR regions ..."
    echo ""
  else
    echo "GB-file for training not found where it should be. Please run -n (prepare data) first!"
    exit 1
  fi

  # Optimize species meta parameters with GenBank training gene set
  # usage: optimize_augustus.pl --species=myspecies train.gb [parameters]
  # myspecies                prefix of the species name
  # train.gb                 genbank file for training with bona fide gene structures
  # --rounds=r               r is the number of rounds (default: 5)
  # --cpus=n                 n is the number of CPUs to use (default: 1)
  #                          The perl module Parallel::ForkManager is required to run optimize_augustus.pl in parallel.
  #                          sudo apt-get install libparallel-forkmanager-perl
  # --UTR=on                 turn untranslated region model on for training and prediction
  # --trainOnlyUtr=1         Use this option, if the exon, intron and intergenic models need not be trained. (default: 0)
  # --onlytrain=onlytrain.gb an optional genbank file that is used in addition to train.gb
  #                          but only for etrain not for intermediate evaluation of accuracy.
  # --metapars=metapars.cfg  metapars.cfg contains the names and their ranges of the
  #                          meta parameters that are subject to optimization. (default: generic_metapars.cfg)
  # --translation_table=n    TODO: Is this option used at all??? Anyway, we set the correct translation table with own option.
  if { [[ ${TRAININGTYPE} == "optimize" ]] || [[ ${TRAININGTYPE} == "utr" ]]; } && [ -f "${OUT_DIR}"/genes"${UTR}".gb.train ]; then
    if [[ ${TRAININGTYPE} == "utr" ]]; then
      echo "Starting AUGUSTUS 'optimize' doing only UTR-training ..."
      echo ""
      NRGENES="200"
      UTRON="--UTR=on --metapars=${AUGUSTUS_CONFIG_PATH}/species/${NEW_AUGUSTUSPROFILE}/${NEW_AUGUSTUSPROFILE}_metapars.utr.cfg --trainOnlyUtr=1 "
      ROUNDS="--rounds=3 "
    else
      NRGENES="300"
      ROUNDS=""
      if [ -z "${UTROPTION}" ]; then
        echo "Starting AUGUSTUS 'optimize' (meta parameter optimization), UTR-training disabled ..."
        echo ""
        UTRON=""
      elif [ -n "${UTROPTION}" ]; then
        echo "Starting AUGUSTUS 'optimize' (meta parameter optimization), UTR-training enabled ..."
        echo ""
        UTRON="--UTR=on "
      fi
    fi
    # optimize_augustus takes very long, thus reduce the gene set for optimization
    NUMBERGENES=$( grep -c "LOCUS" "${OUT_DIR}"/genes"${UTR}".gb )
    if [ "${NUMBERGENES}" -gt "${NRGENES}" ]; then
      echo "Generating a smaller gene set of ${NRGENES} genes for training the most time-consuming parameters ..."
      echo ""
      cp "${OUT_DIR}"/genes"${UTR}".gb "${OUT_DIR}"/genes"${UTR}".split"${NRGENES}".gb
      randomSplit.pl "${OUT_DIR}"/genes"${UTR}".split"${NRGENES}".gb "${NRGENES}"
      rm "${OUT_DIR}"/genes"${UTR}".split"${NRGENES}".gb
      run_time
    fi
    # The most time-consuming steps are performed on the test-set only, which is usually much smaller than the training-set
    echo "Starting optimize_augustus.pl ..."
    echo ""
    optimize_augustus.pl --species="${NEW_AUGUSTUSPROFILE}" "${OUT_DIR}"/genes"${UTR}".split"${NRGENES}".gb.test \
    "${ROUNDS}""${UTRON}"--onlytrain="${OUT_DIR}"/genes"${UTR}".split"${NRGENES}".gb.train > "${OUT_DIR}"/aug.optimize"${WITHUTR}".trainSet.result.out
    run_time
    # IMPORTANT: After optimization has finished, one final round of etraining is required!
  elif { [[ ${TRAININGTYPE} == "optimize" ]] || [[ ${TRAININGTYPE} == "utr" ]]; } && [ ! -f "${OUT_DIR}"/genes"${UTR}".gb.train ]; then
    echo "GenBank file with training genes not found! Please check."
  fi

  # The "etraining" is a quick-and-dirty optimization of the parameters
  if { [[ ${TRAININGTYPE} == "etraining" ]] || [[ ${TRAININGTYPE} == "optimize" ]] || [[ ${TRAININGTYPE} == "utr" ]]; } && [ -f "${OUT_DIR}"/genes"${UTR}".gb.train ]; then
    echo "Starting AUGUSTUS 'etraining' (species profile optimization) ..."
    echo ""
    if [ -z "${UTROPTION}" ]; then
      UTRON=""
    elif [ -n "${UTROPTION}" ]; then
      UTRON="--UTR=on "
    fi
    if [[ ${TRAININGTYPE} == "etraining" ]]; then
      TRAINTYPE="etraining${WITHUTR}"
    elif [[ ${TRAININGTYPE} == "optimize" ]]; then
      TRAINTYPE="optimize_etraining${WITHUTR}"
    elif [[ ${TRAININGTYPE} == "utr" ]]; then
      TRAINTYPE="optimize_etraining${WITHUTR}"
      UTRON="--UTR=on "
    fi
    etraining --species="${NEW_AUGUSTUSPROFILE}" ${UTRON}"${OUT_DIR}"/genes"${UTR}".gb.train > "${OUT_DIR}"/aug."${TRAINTYPE}".trainSet.result.out
    run_time
    # Extract stop codon frequencies from etraining output and update parameters.cfg file
    AMBER=$( sed -n -E 's/^tag\:[[:space:]]+[0-9]*[[:space:]]\(([0-9\.]*)\)/\1/p' "${OUT_DIR}"/aug."${TRAINTYPE}".trainSet.result.out | head -1 )
    OCHRE=$( sed -n -E 's/^taa\:[[:space:]]+[0-9]*[[:space:]]\(([0-9\.]*)\)/\1/p' "${OUT_DIR}"/aug."${TRAINTYPE}".trainSet.result.out | head -1 )
    OPAL=$( sed -n -E 's/^tga\:[[:space:]]+[0-9]*[[:space:]]\(([0-9\.]*)\)/\1/p' "${OUT_DIR}"/aug."${TRAINTYPE}".trainSet.result.out | head -1 )
    OPALCALC=$( echo "1" "${AMBER}" "${OCHRE}" | awk '{print $1*1000 - $2*1000 - $3*1000}' )
    STOPCODONS=$( echo "${AMBER}" "${OCHRE}" "${OPAL}" | awk '{print $1 + $2 + $3}' )
    OPALDIFF=$( echo "${OPAL}" "${OPALCALC}" | awk '{print $1*1000 - $2}' )
    OPALDIFFABS=${OPALDIFF#-}
    if [ "${STOPCODONS}" == 1 ]; then
      echo "Changing stop codon probabilities in parameters file based on etraining results ..."
      echo ""
      sed -i -E "s/^(.*amberprob\s*)[0-9.]*(\s*.*)$/\1${AMBER}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*ochreprob\s*)[0-9.]*(\s*.*)$/\1${OCHRE}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*opalprob\s*)[0-9.]*(\s*.*)$/\1${OPAL}\2/" "${AUGUSTUS_PARAMS}"
    elif [ "${STOPCODONS}" != 1 ] && [ "${OPALDIFFABS}" -eq 1 ]; then
      echo "Small rounding error in AUGUSTUS, nevertheless changing stop codon probabilities in parameters file based on etraining results ..."
      echo ""
      sed -i -E "s/^(.*amberprob\s*)[0-9.]*(\s*.*)$/\1${AMBER}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*ochreprob\s*)[0-9.]*(\s*.*)$/\1${OCHRE}\2/" "${AUGUSTUS_PARAMS}"
      sed -i -E "s/^(.*opalprob\s*)[0-9.]*(\s*.*)$/\1${OPALCALC}\2/" "${AUGUSTUS_PARAMS}"
    else
      echo "Sum of stop codon probabilities is not equal 1.000! Please check."
      echo ""
    fi

    # IMPORTANT: The following is ONLY a test, it does not change the parameters! Thus not needed in a pipeline!
#    echo "Ab initio prediction on test-gene regions (accuracy report, only a small subset of genome) ..."
#    echo ""
#    augustus --species="${NEW_AUGUSTUSPROFILE}" "${OUT_DIR}"/genes"${UTR}".gb.test \
#    --outfile="${OUT_DIR}"/aug."${TRAINTYPE}".testSet.result.gff --errfile="${OUT_DIR}"/aug."${TRAINTYPE}".testSet.result.err
#    csplit "${OUT_DIR}"/aug."${TRAINTYPE}".testSet.result.gff /a-posteriori/
#    mv xx00 "${OUT_DIR}"/aug."${TRAINTYPE}".testSet.result.gff
#    mv xx01 "${OUT_DIR}"/aug."${TRAINTYPE}".testSet.result.stats
#    run_time
#    echo "Ab initio prediction on whole genome (no hints) using the newly trained profile ..."
#    echo ""
#    # gff3 format requires the stop codon to be part of the CDS
#    sed -i "s/stopCodonExcludedFromCDS true/stopCodonExcludedFromCDS false/" "${AUGUSTUS_PARAMS}"
#    augustus --gff3=on --species="${NEW_AUGUSTUSPROFILE}" "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
#    --outfile="${OUT_DIR}"/aug."${TRAINTYPE}".genome.result.gff --errfile="${OUT_DIR}"/aug."${TRAINTYPE}".genome.result.err
#    sed -i "s/stopCodonExcludedFromCDS false/stopCodonExcludedFromCDS true/" "${AUGUSTUS_PARAMS}"
#    run_time
  elif [[ ${TRAININGTYPE} == "etraining" ]] && [ ! -f "${OUT_DIR}"/genes"${UTR}".gb.train ]; then
    echo "GenBank file with training genes not found! Please check."
  fi

  # Automatically train species parameter
  # AUGUSTUS requires a genome sequence and a gff-file with hints and is supposed to perform
  # all the hint formatting, hint filtering and training automatically
  # Usage:
  # autoAug.pl [OPTIONS] -g genome.fasta --species=sname
  #
  # Options:
  # --genome=fasta [-g]                 fasta file with genome DNA sequence
  # --trainingset=genesfile [-t]        genesfile contains training genes in Genbank, GFF or protein FASTA format
  # --species=sname                     species name as used by AUGUSTUS
  # --useexisting                       use and change the present config and parameter files if they exist for 'species'
  # --verbose [-v]                      print more status info. Cumulative option, e.g. use -v -v -v to make this script very verbose
  # --noutr                             do not train and predict UTRs (OPTION does NOT exist!!!)
  # --workingdir=/path/to/wd/           working directory to store results and temporary files (default: current directory)
  # --optrounds=n                       optimization rounds - each meta parameter is optimized this often (default 1)
  if [[ ${TRAININGTYPE} == "autoaug" ]] && [ -f "${OUT_DIR}"/genes"${UTR}".gb ]; then
    echo "Starting AUGUSTUS 'autoAug' ..."
    echo ""
    autoAug.pl -g "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta -t "${OUT_DIR}"/genes"${UTR}".gb --species="${NEW_AUGUSTUSPROFILE}" -v --useexisting --workingdir="${OUT_DIR}"
    # autoAugTrain within autoAug automatically updates stop codon frequencies in parameter.cfg file
    run_time
    # autoAug performs an AUGUSTUS prediction on the test-set to get prediction quality stats
    mv "${OUT_DIR}"/autoAug/autoAugTrain/training/train.out "${OUT_DIR}"/aug.autoaug.trainSet.result.out
    mv "${OUT_DIR}"/autoAug/autoAugTrain/training/optimize.out "${OUT_DIR}"/aug.autoaug.optimize.trainSet.result.out
    mv "${OUT_DIR}"/autoAug/autoAugTrain/training/test/augustus.1.out "${OUT_DIR}"/aug.autoaug.testSet.result.tmp
    csplit "${OUT_DIR}"/aug.autoaug.testSet.result.tmp /a-posteriori/
    mv xx00 "${OUT_DIR}"/aug."${TRAININGTYPE}".testSet.result.gff
    mv xx01 "${OUT_DIR}"/aug."${TRAININGTYPE}".testSet.result.stats
    rm "${OUT_DIR}"/aug.autoaug.testSet.result.tmp
    rm -rf "${OUT_DIR}"/autoAug

    # IMPORTANT: The following is ONLY a test, it does not change the parameters! Thus not needed in a pipeline!
#    echo "Ab initio prediction on whole genome (no hints) using the newly trained profile"
#    echo ""
#    # gff3 format requires the stop codon to be part of the CDS
#    sed -i "s/stopCodonExcludedFromCDS true/stopCodonExcludedFromCDS false/" "${AUGUSTUS_PARAMS}"
#    augustus --gff3=on --species="${NEW_AUGUSTUSPROFILE}" "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
#    --outfile="${OUT_DIR}"/aug.autoaug.genome.result.gff --errfile="${OUT_DIR}"/aug.autoaug.genome.result.err
#    sed -i "s/stopCodonExcludedFromCDS false/stopCodonExcludedFromCDS true/" "${AUGUSTUS_PARAMS}"
#    run_time
  elif [[ ${TRAININGTYPE} == "autoaug" ]] && [ ! -f "${OUT_DIR}"/genes"${UTR}".gb ]; then
    echo "gff file with training genes not found! Please check."
  fi

fi

################################################################################

if [[ ${FINALPREDICTION} == "true" ]]; then

  PARAMSTRANSTABLE=$( sed -n -E 's/^translation_table\s([0-9]+)$/\1/p' "${AUGUSTUS_PARAMS}" )
  if [[ ${PARAMSTRANSTABLE} == "1" ]] && [[ -n "${TRANSTABLE}" ]] && [[ ${TRANSTABLE} != "1" ]]; then
    echo "Translation table has not been set yet, changing translation table to #${TRANSTABLE} ..."
    echo ""
    if ( ! grep -q "translation_table" "${AUGUSTUS_PARAMS}" ); then
      sed -i -E '/^\/Constant\/amberprob.*/i translation_table 1' "${AUGUSTUS_PARAMS}"
    fi
    sed -i -E "s/^(translation_table\s)1\$/\1$TRANSTABLE/" "${AUGUSTUS_PARAMS}"
    set_stop_codons
  elif [[ -n "${TRANSTABLE}" ]] && [[ ${TRANSTABLE} != "${PARAMSTRANSTABLE}" ]]; then
    echo "Translation table in species config file (= #${PARAMSTRANSTABLE}) contradicts input translation table (= #${TRANSTABLE})! Please check!"
    exit 1
  fi
  # RUNNING AUGUSTUS
  # augustus [parameters] --species=SPECIES queryfilename
  #
  # Parameters:
  # --strand=[both | forward | backward]       report predicted genes on both strands,
  #                                            just the forward or just the backward strand (default: 'both')
  # --genemodel=[partial | intronless | complete | atleastone | exactlyone]
  #                                            partial: allow prediction of incomplete genes at the sequence boundaries (default)
  #                                            intronless: only predict single-exon genes like in prokaryotes and some eukaryotes
  #                                            complete: only predict complete genes
  #                                            atleastone: predict at least one complete gene
  #                                            exactlyone: predict exactly one complete gene
  # --singlestrand=true                        predict genes independently on each strand, allow overlapping genes on opposite strands
  #                                            This option is turned off by default.
  # --hintsfile=hintsfilename                  When this option is used the prediction considering hints (extrinsic information) is turned on.
  #                                            hintsfilename contains the hints in gff format.
  # --extrinsicCfgFile=cfgfilename             This file contains the list of used sources for the hints and their boni and mali.
  #                                            If not specified the file "extrinsic.cfg" in the config directory $AUGUSTUS_CONFIG_PATH is used.
  # --maxDNAPieceSize=n                        This value specifies the maximal length of the pieces that the sequence is cut into for the
  #                                            core algorithm (Viterbi) to be run. Default is --maxDNAPieceSize=200000. AUGUSTUS tries to
  #                                            place the boundaries of these pieces in the intergenic region, which is inferred by a
  #                                            preliminary prediction. GC-content dependent parameters are chosen for each piece of DNA if
  #                                            /Constant/decomp_num_steps > 1 for that species. This is why this value should not be set
  #                                            very large, even if you have plenty of memory.
  # --alternatives-from-evidence=true/false    report alternative transcripts when they are suggested by hints
  # --alternatives-from-sampling=true/false    report alternative transcripts generated through probabilistic sampling
  # --proteinprofile=filename                  Read a protein profile from file filename. See section on PPX below.
  # --gff3=on/off                              output in gff3 format
  # --UTR=on/off                               predict the untranslated regions in addition to the coding sequence.
  # --outfile=filename                         print output to filename instead to standard output. This is useful for computing environments,
  #                                            e.g. parasol jobs, which do not allow shell redirection.

  if [[ ${PREDICTIONTYPE} == "nohints" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Predicting genes with AUGUSTUS based on trained profile not using hints ..."
    echo ""

    # no parameter to set, using minimal run options
    PARAMS=""
  fi

  if [[ ${PREDICTIONTYPE} == "withhints" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Predicting genes with AUGUSTUS based on trained profile using hints ..."
    echo ""

    # set parameters to use hints
    PARAMS="--allow_hinted_splicesites=atac --softmasking=false --extrinsicCfgFile=extrinsic.MPEW.cfg --hintsfile="${HOME}"/hints.EXTRINSIC.concatenated.gff "
  fi

  if [[ ${PREDICTIONTYPE} == "utr" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Predicting genes including UTRs with AUGUSTUS based on trained profile ..."
    echo ""

    # Final prediction including UTRs
    # the options "--UTR=on --print_utr=on" will set the respective parameters in the
    # "${AUGUSTUS_PARAMS}" file
    PARAMS="--allow_hinted_splicesites=atac --softmasking=false --extrinsicCfgFile=extrinsic.MPEW.cfg --hintsfile="${HOME}"/hints.EXTRINSIC.concatenated.gff --UTR=on --print_utr=on "
  fi

  if [[ ${PREDICTIONTYPE} == "alt_splicing" ]]; then
    echo "-------------------------------------------------------------------------------------"
    echo "Predicting genes including alternative transcripts with AUGUSTUS based on trained profile ..."
    echo ""

    # add option to augustus command: --alternatives-from-evidence=on
    PARAMS="--allow_hinted_splicesites=atac --softmasking=false --extrinsicCfgFile=extrinsic.MPEW.cfg --hintsfile="${HOME}"/hints.EXTRINSIC.concatenated.gff --UTR=on --print_utr=on --alternatives-from-evidence=on "
  fi

  if [[ ${PREDICTIONTYPE} != "nohints" ]]; then
    rsync ${OUT_DIR}/hints.EXTRINSIC.concatenated.gff "${HOME}"/hints.EXTRINSIC.concatenated.gff
  fi
  touch "${HOME}"/aug.final."${PREDICTIONTYPE}".gff
  # gff3 format requires the stop codon to be part of the CDS
  sed -i "s/stopCodonExcludedFromCDS true/stopCodonExcludedFromCDS false/" "${AUGUSTUS_PARAMS}"
  # Single-file prediction
  # augustus --species="${NEW_AUGUSTUSPROFILE}" "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
  # ${PARAMS}--gff3=on --outfile="${HOME}"/aug.final."${PREDICTIONTYPE}".gff --errfile="${OUT_DIR}"/aug.final."${PREDICTIONTYPE}".err
  # Parallelize prediction in dependence of genome file size
  MAXSIZE=50000000
  FILESIZE=$(stat -c%s "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta)
  if (( FILESIZE < MAXSIZE )); then
    # Case 1: Small genomefiles 50Mb ~50 min
    augustus --species="${NEW_AUGUSTUSPROFILE}" "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta \
    ${PARAMS}--gff3=on --outfile="${HOME}"/aug.final."${PREDICTIONTYPE}".gff --errfile="${OUT_DIR}"/aug.final."${PREDICTIONTYPE}".err
    mv "${HOME}"/aug.final."${PREDICTIONTYPE}".gff "${OUT_DIR}"/aug.final."${PREDICTIONTYPE}".gff
  else
    # Case 2: Big genomefiles
    MAX_PARALLEL=10
    echo "${MAX_PARALLEL} jobs in parallel ..."
    echo ""
    # usage:  splitfile.py file size delim
    /mnt/software/pyscipio3/scripts/splitfile.py "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta 50M '>'
    i=0
    for f in "${GENOME_DIR}"/*.min_*; do
      augustus --species="${NEW_AUGUSTUSPROFILE}" "${f}" \
      ${PARAMS}--gff3=on --outfile="${HOME}"/aug.final."${PREDICTIONTYPE}".$i.gff --errfile="${HOME}"/aug.final."${PREDICTIONTYPE}".$i.err &
      ((i++))
      # track job ids to wait for later
      pids[${SRAACC}]=$!
      # Wait here for any job to be finished, so there is a place to start next one.
      if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi
    done
    # wait for all pids
    for pid in ${pids[*]}; do
        wait $pid
    done
    cat "${HOME}"/aug.final."${PREDICTIONTYPE}".*.gff > "${HOME}"/aug.final."${PREDICTIONTYPE}".gff
    mv "${HOME}"/aug.final."${PREDICTIONTYPE}".gff "${OUT_DIR}"/aug.final."${PREDICTIONTYPE}".gff
    cat "${HOME}"/aug.final."${PREDICTIONTYPE}".*.err >> "${OUT_DIR}"/aug.final."${PREDICTIONTYPE}".err
    rm "${HOME}"/aug.final."${PREDICTIONTYPE}".*.gff "${HOME}"/aug.final."${PREDICTIONTYPE}".*.err
  fi
  run_time
  [ -f "${HOME}"/hints.EXTRINSIC.concatenated.gff ] && rm "${HOME}"/hints.EXTRINSIC.concatenated.gff
  # set parameter file back to initial (standard) values
  sed -i "s/stopCodonExcludedFromCDS false/stopCodonExcludedFromCDS true/" "${AUGUSTUS_PARAMS}"
  sed -i -E "s/(print_utr\s*)on(.*)/\1off\2/" "${AUGUSTUS_PARAMS}"
  sed -i -E "s/(UTR\s*)on(.*)/\1off\2/" "${AUGUSTUS_PARAMS}"

fi



################################################################################
# Postprocessing

# gffcompare
# sed -E 's/^[[:lower:]0-9|]*([[:upper:]]{4,6}[0-9]{8,11})\.[0-9].*(\tAUG.*)/\1\2/' aug.3rd.autoAug.genome.result.gff > aug.3rd.autoAug.genome.result.min.gff
# sed -i '/##.*/d' aug.3rd.autoAug.genome.result.min.gff
#gffcompare -o gffcmp_pyscipio_augustus -r allGenesFirstPrediction.complete.min.gff aug.3rd.autoAug.genome.result.min.gff
#gffcompare -o gffcmp_augustus_pyscipio -r aug.3rd.autoAug.genome.result.min.gff allGenesFirstPrediction.complete.min.gff

end_date=$(date +%T)
end=$(date +%s)
runtime=$((end-beginning))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"
