#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

# config
SRA_DATA_PATH="/mnt/ncbi_sra"

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: download_sra.sh -i [id] OR -r [first-last] <optional -f>

choice
-i [id]          SRA-file-ID (format SRR14672191)
-r [first-last]  range of SRA-file-IDs (format SRR14672191-SRR14672195)

-f               force overwrite

example:
./download_sra.sh -f SRR14672191

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp

OPTIND=1
while getopts ":i:r:fh" opt; do
  case ${opt} in
    i )
      SRAFILEID=$OPTARG
      ;;
    r )
      SRAFILERANGE=$OPTARG
      FILERANGE="true"
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

echo "#####################################################################################"
echo ""
echo "download_sra.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""
echo "Downloading sequence data files using SRA Toolkit"
echo ""
# Read: https://www.ncbi.nlm.nih.gov/sra/docs/sradownload/ ...

cd "${SRA_DATA_PATH}" || exit
vdb-config --restore-defaults

if [[ -n "${FILERANGE}" ]]; then
  FIRST=$( echo "${SRAFILERANGE}" | cut -d "-" -f 1 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  LAST=$( echo "${SRAFILERANGE}" | cut -d "-" -f 2 | sed -E -n 's/SRR([0-9]*)/\1/p' )
else
  FIRST=$( echo "${SRAFILEID}" | cut -d "-" -f 1 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  LAST="${FIRST}"
fi

for i in $(seq "${FIRST}" "${LAST}"); do
  if [ ! -d "SRR${i}" ] || [ -n "${FORCEOVERWRITE}" ]; then
    prefetch "SRR${i}"
  fi
done


echo "-------------------------------------------------------------------------------------"
echo ""
echo "Unpack SRA files using fasterq-dump"
echo ""

# fasterq-dump <path> [options]
# -e|--threads           how many thread dflt=6
# -p|--progress          show progress
# -S|--split-files       write reads into different files
# -O|--outdir            output-dir
for i in $(seq "${FIRST}" "${LAST}"); do
  if ! ( ls "SRR${i}" | grep -q "fastq" ) || [ -n "${FORCEOVERWRITE}" ]; then
    echo "Extracting reads from ${i}"
    fasterq-dump -p -S "SRR${i}" -e 8 -O "SRR${i}"
  fi
done

end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"