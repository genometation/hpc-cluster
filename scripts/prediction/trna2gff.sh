#!/bin/bash

showHelp() {
cat << EOF

Usage: trna2gff.sh -i [path]

-i [path]         path-to-tRNAscan-SE-file (e.g. [file-name].fasta.output)

EOF
exit 0
}

OPTIND=1
while getopts ":i:h" opt; do
  case ${opt} in
    i )
      TRNAFILE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))


echo "#####################################################################################"
echo ""
echo "Converting tRNAscan-SE output into gff3 ..."
echo ""

FILENAME=$( basename "${TRNAFILE}" )
PREFIX="${FILENAME%.*}"

# column 1: seqID
# column 3: first base of the exon (1-based)
# column 4: last base of the exon (1-based)
# column 5: tRNA type          strand (0: undefined, 1: +, 2: -)
# column 6: anticodon
# column 7: first base of the intron (1-based)
# column 8: last base of the intron (1-based)
# column 12: isotype
# column 14: Note, e.g. pseudo
while IFS= read -r line || [[ -n $line ]]; do
  trna_line="^(\S+)\s+[0-9]+\s+([0-9]+)\s+([0-9]+)\s+([A-Za-z]{3,5})\s+([A-Z]{3})\s+([0-9]+)\s+([0-9]+)\s+[-0-9\.]+\s+[-0-9\.]+\s+[-0-9\.]+\s+([A-Za-z]{3,4})\s+[-0-9\.]+($|\s+.*$)"
  if [[ "$line" =~ ${trna_line} ]]; then
    gnxid=$( sed -n -E '0,/^[0-9ACGTNU:]{15}$/{s/^([0-9ACGTNU:]{15})$/\1/p}' gnx_gene_ids.txt )
    seqid="${BASH_REMATCH[1]}"
    start="${BASH_REMATCH[2]}"
    end="${BASH_REMATCH[3]}"
    type="${BASH_REMATCH[4]}"
    anticodon="${BASH_REMATCH[5]}"
    intron_start="${BASH_REMATCH[6]}"
    intron_end="${BASH_REMATCH[7]}"
    isotype="${BASH_REMATCH[8]}"
    note="${BASH_REMATCH[9]}"
    if [ "${start}" -lt "${end}" ]; then
      strand="+"
      cstart=${start}
      cend=${end}
    elif [ "${end}" -lt "${start}" ]; then
      strand="-"
      cstart=${end}
      cend=${start}
    else
      strand="."
    fi
    if [ "${type}" != "${isotype}" ]; then
      info=";Note=anticodon does not match isotype"
    else
      info=""
    fi
    if [[ "$note" =~ pseudo ]]; then
      gene_type="Pseudo_tRNA"
      echo "${seqid}"$'\t'GOENOMICS$'\t'pseudogene$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=gene-${gnxid};gene_biotype=${gene_type};Note=${gene_type} isotype:${isotype}_tRNA (anticodon ${anticodon})" >> "${PREFIX}".gff
    else
      gene_type="${isotype}_tRNA"
      {
        echo "${seqid}"$'\t'GOENOMICS$'\t'gene$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=gene-${gnxid};gene_biotype=tRNA;Note=isotype:${gene_type} (anticodon ${anticodon})" >> "${PREFIX}".gff
        echo "${seqid}"$'\t'GOENOMICS$'\t'trna$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=rna-${gnxid};Parent=gene-${gnxid};product=${gene_type}" >> "${PREFIX}".gff
      } >> "${PREFIX}".gff
      if [ "${intron_start}" == "0" ]; then
        echo "${seqid}"$'\t'GOENOMICS$'\t'exon$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=exon-${gnxid};Parent=rna-${gnxid};isotype=${gene_type} (anticodon ${anticodon})${info}" >> "${PREFIX}".gff
      else
        if [ "${intron_start}" -lt "${intron_end}" ]; then
          cstart1=${start}
          cend1=${intron_start}
          cstart2=${intron_end}
          cend2=${end}
        elif [ "${end}" -lt "${start}" ]; then
          cstart1=${end}
          cend1=${intron_end}
          cstart2=${intron_start}
          cend2=${start}
        fi
        {
          echo "${seqid}"$'\t'GOENOMICS$'\t'exon$'\t'"${cstart1}"$'\t'"${cend1}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=exon-${gnxid}-1;Parent=rna-${gnxid};isotype=${gene_type} (anticodon ${anticodon})${info}" >> "${PREFIX}".gff
          echo "${seqid}"$'\t'GOENOMICS$'\t'exon$'\t'"${cstart2}"$'\t'"${cend2}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=exon-${gnxid}-2;Parent=rna-${gnxid};isotype=${gene_type} (anticodon ${anticodon})${info}" >> "${PREFIX}".gff
        } >> "${PREFIX}".gff
      fi
    fi
    sed -i -E "s/^(${gnxid})$/\1 taken/" gnx_gene_ids.txt
  fi
done < "${TRNAFILE}"
