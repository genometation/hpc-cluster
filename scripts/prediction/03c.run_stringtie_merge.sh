#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

ext_start=$(date +%s)
ext_start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE
unset FULLFOLDER
unset SELECTEDFOLDER

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 03c.run_stringtie_merge.sh -g [id] -i [path] (-a OR -r [first-last]) <optional -o [path] -f>

Assumption: Script is run with absolute paths, or outside the "prediction" directory.
            If the data structure has correctly been set up, the output-dir == input-dir.

mandatory
-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]        input data directory (where the GTF/GFF-files are located)

choice
-a               all, get all GTF/GFF-files in the input directory
-r [first-last]  range of GTF/GFF-files (format SRR14672191-SRR14672195)

optional
-o [path]        basic output data directory (will be the current directory if option not set)
                 Results will be written into basic_output_data_dir/prediction/stringtie_merge

example:
./03c.run_stringtie_merge.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/gmap -a -o /mnt/customer/self

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')
REF_ANNOT=""

OPTIND=1
while getopts ":g:i:o:ar:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    i )
      GTFGFF_DIR=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    a )
      FULLFOLDER="true"
      ;;
    r )
      INPUTFILERANGE=$OPTARG
      SELECTEDFOLDER="true"
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="stringtie_merge"
. config.sh

STATUSFILE="03c.run_stringtie_merge.sh"_"${DATE_SAVED}".out
LOGFILE="03c.run_stringtie_merge.sh"_"${DATE_SAVED}".log

#METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

echo "#####################################################################################"
echo ""
echo "!!! StringTie merge mode !!!"
echo ""
echo "03c.run_stringtie_merge.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $ext_start_date"
echo ""
echo "Merging multiple GTF/GFF-files into a single with non-redundant hits ..."
echo "Coverage values from the respective single GTF/GFF-files will be lost."
echo ""

if [[ -n "${FULLFOLDER}" ]]; then
  echo "Getting the list of GTF/GFF-files to be merged ..."
  echo ""
  [ ! -f "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt ] && touch "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt
  find -L "${GTFGFF_DIR}" -maxdepth 1 -mindepth 1 -type f -regex '.*SRR[0-9]*_.*\.g.f' | sort -n >> "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt
fi
if [[ -n "${SELECTEDFOLDER}" ]]; then
  echo "Preparing GTF/GFF-files from ${INPUTFILERANGE} ..."
  echo ""
  FIRST=$( echo "${INPUTFILERANGE}" | cut -d "-" -f 1 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  LAST=$( echo "${INPUTFILERANGE}" | cut -d "-" -f 2 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  [ ! -f "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt ] && touch "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt
  for i in $(seq "${FIRST}" "${LAST}"); do
    find -L "${GTFGFF_DIR}" -maxdepth 1 -mindepth 1 -type f -regex ".*SRR${i}_.*\.g.f" >> "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt
  done
fi

# stringtie [-o <output.gtf>] [other_options] <read_alignments.bam>

# --merge        Transcript merge mode. This is a special usage mode of StringTie, distinct from the assembly usage mode
#                described above. In the merge mode, StringTie takes as input a list of GTF/GFF files and merges/assembles
#                these transcripts into a non-redundant set of transcripts. This mode is used in the new differential analysis
#                pipeline to generate a global, unified set of transcripts (isoforms) across multiple RNA-Seq samples.
#                If the -G option (reference annotation) is provided, StringTie will assemble the transfrags from the input
#                GTF files with the reference transcripts.
# -G <guide_gff> reference annotation to include in the merging (GTF/GFF3)
# -o <out_gtf>   output file name for the merged transcripts GTF (default: stdout)
# -m <min_len>   minimum input transcript length to include in the merge (default: 50)
FILENAME=$( tail -1 "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt )
MAPTYPE=$( echo "${FILENAME}" | sed -E -n 's/.*SRR[0-9]+_(.*)\.g.f/\1/p' )
GTFGFF="${FILENAME##*.}"
echo "stringtie --merge -m 30 -o ${OUT_DIR}/${MAPTYPE}_merged.${GTFGFF} ${OUT_DIR}/tmp_stringtie_merge_file_list.txt"
stringtie --merge -m 30 -o "${OUT_DIR}"/"${MAPTYPE}"_merged."${GTFGFF}" "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt
# ln -sf "${OUT_DIR}"/"${MAPTYPE}"_merged."${GTFGFF}" "${OUT_DIR}"/"${MAPTYPE}"_merged_latest."${GTFGFF}"
CURRENTDIR=$( pwd )
[ ! -d "${MAIN_OUT_DIR}"/jbrowse ] && mkdir "${MAIN_OUT_DIR}"/jbrowse
[ ! -L "${MAIN_OUT_DIR}"/jbrowse/"${GENOMEFILEID}"_"${MAPTYPE}"_merged."${GTFGFF}" ] \
  && cd "${MAIN_OUT_DIR}"/jbrowse && ln -sf "${OUT_DIR}"/"${MAPTYPE}"_merged."${GTFGFF}" "${GENOMEFILEID}"_"${MAPTYPE}"_merged."${GTFGFF}"
mv "${OUT_DIR}"/tmp_stringtie_merge_file_list.txt "${OUT_DIR}"/"${MAPTYPE}"_file_list.txt
cd "$CURRENTDIR" || return


ext_end=$(date +%s)
ext_end_date=$(date +%T)
runtime=$((ext_end-ext_start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $ext_end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"