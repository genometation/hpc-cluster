#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: master.sh -g [id] -h [id] -i [path] -o [path] -t [number] <optional -f>

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-h [id]          genome file id of close relative to use as AUGUSTUS start (format: "profile_[id]")
-i [path]        path to input file SRAdata.lst that lists the SRA data directories, one per row
-o [path]        basic output data directory (=> e.g. /mnt/genomes/[genome_group_id]/[genome_file_id])
                 Results will be written into "basic_output_data_dir"/prediction/
-t [number]      set translation table (default: 1)
                 (allowed numbers: 1 2 3 4 5 6 9 10 11 12 13 14 16 21 22 23 24 25 26 27 28 29 30 31 33)

-f               force overwrite

-h   Show this help

EOF
exit 0
}

[ $# -eq 0 ] && showHelp

OPTIND=1
while getopts ":g:h:fi:o:t:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    h )
      OLDGENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      SRAINPUTFILE=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    t )
      TRANSTABLE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

. config.sh

if [ -n "${FORCEOVERWRITE}" ]; then
  FORCEOVERWRITEOPTION=" -f"
else
  FORCEOVERWRITEOPTION=""
fi

STATUSFILE="master"_"${DATE_SAVED}".out
LOGFILE="master"_"${DATE_SAVED}".log

#  METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#  [ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"


STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)


echo "#####################################################################################"
echo ""
echo "master.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

############  STAR  ########################################################################

# ./01b.run_multiple.star.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/SRAdata.lst -o /mnt/customer/self
# MAX_PARALLEL=5
. ./01b.run_multiple.star.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -i "${SRAINPUTFILE}" -o "${BASE_DIR}"

# ./03b.run_multiple.stringtie.sh -g JAATWF010000000 -a -o /mnt/customer/self
# MAX_PARALLEL=5
. ./03b.run_multiple.stringtie.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -a -o "${BASE_DIR}"

# ./04b.run.trinity_on_multiple_datasets.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/SRAdata.lst -o /mnt/customer/self
# MAX_PARALLEL=2
. ./04b.run.trinity_on_multiple_datasets.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -i "${SRAINPUTFILE}" -o "${BASE_DIR}"

# ./06b.run_multiple.gmap.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/trinity -o /mnt/customer/self
# MAX_PARALLEL=5
. ./06b.run_multiple.gmap.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -i "${MAIN_OUT_DIR}"/trinity -o "${BASE_DIR}" -t "${TRANSTABLE}"

# ./03c.run_stringtie_merge.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/gmap -a -o /mnt/customer/self
. ./03c.run_stringtie_merge.sh -g "${GENOMEFILEID}" -i "${MAIN_OUT_DIR}"/gmap -a -o "${BASE_DIR}"

# ./02.run.augustus.sh -g JAATWF010000000 -o /mnt/customer/self -c rnaseq
# create hint files
. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -c rnaseq
. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -c gmap
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -c pyscipio
## generate new profile, prepare data
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -n "${OLDGENOMEFILEID}" -u -x "${TRANSTABLE}"
## train profile
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -t etraining -u -x "${TRANSTABLE}"
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -t optimize -u -x "${TRANSTABLE}"
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -t etraining -u -x "${TRANSTABLE}"
## final prediction
#. ./02.run.augustus.sh -g "${GENOMEFILEID}" -o "${BASE_DIR}" -z utr -x "${TRANSTABLE}"



end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"
