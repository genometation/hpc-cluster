#!/usr/bin/env bash

# Bash settings
# set -e
# set -u
# set -o pipefail
# Repeat executed commands
# set -x

# NOTE:
# Need to be run in AUGUSTUS Docker container (or pod)
# to have all software available (transabyss, gmap).

# Set variables
genomefile_id=${1:-""}                             # ID of GenomeFile e.g. 'AACU03000000'
out_dir="prediction_${genomefile_id}"              # Output directory
rnaseq_left=${2:-""}                               # RNA-seq: Full path e.g. path/to/RNAseq.2_1.fastq
rnaseq_right=${3:-""}                              # RNA-seq: Full path e.g. path/to/RNAseq.2_2.fastq
rnaseq_left_first=$(cut -d',' -f1 <<< "${rnaseq_left}")   # First left file to obtain BASE_DIR
RNASEQ_DIR=$(dirname "${rnaseq_left_first}")
BASE_DIR=$(dirname "${RNASEQ_DIR}")
SRR_BASENAME=$(cut -d'_' -f1 <<< $(basename "${rnaseq_left_first}"))
DATE_SAVED=$(date +"%Y%m%d")

# Check passed parameter
if [ "$#" -lt 2 ] # are there less than 3 arguments?
then
    echo "error: too few arguments, you provided $#, 3 required"
    echo "usage: script.sh genomefile_id rnaseq_left rnaseq_right"
    exit 1
fi
# Only check existence of files if 05.run.transabyss is not started via transabyss_on_multiple_datasets
#if { [[ ! "${rnaseq_left}" =~ , ]] && [ -n "${rnaseq_right}" ]; } || { [[ ! "${rnaseq_left}" =~ , ]] && [[ ! "${rnaseq_right}" =~ , ]]; }; then
#  if { [ ! -f "${rnaseq_left}" ] && [ -z "${rnaseq_right}" ]; } || { [ -n "${rnaseq_right}" ] && [ ! -f "${rnaseq_left}" ] && [ ! -f "${rnaseq_right}" ]; }
#  then
#      echo "RNA-Seq file ${rnaseq_left} or ${rnaseq_right} DOES NOT exist."
#      exit 1  # die with error code 1
#  fi
#fi

# Trans-ABySS: Generate de novo Transcriptome assembly
# usage: transabyss [-h] [--version] [--se PATH [PATH ...]] [--pe PATH [PATH ...]] [--SS] [--outdir PATH] [--name STR]
#                   [--stage {dbg,unitigs,contigs,references,junctions,final}] [--length INT] [--cleanup {0,1,2,3}] [--threads INT]
#                   [--mpi INT] [-k INT] [-c INT] [-e INT] [-E INT] [-q INT] [-Q INT] [-n INT] [-s INT] [--gsim INT] [--indel INT]
#                   [--island INT] [--useblat] [--pid FLOAT] [--walk FLOAT] [--noref]
# Important options:
# --outdir PATH         output directory [dir-where-the-command-is-invoked]
# --name STR            assembly name [transabyss]
# --se PATH [PATH ...]  single-end read files
# --pe PATH [PATH ...]  paired-end read files
# --SS                  input reads are strand-specific
# --threads INT         number of threads

TRANSABYSS_OUT="${RNASEQ_DIR}/${SRR_BASENAME}_transabyss"
# Only left files / single SRR file(s) specified
if [ -n "${rnaseq_left}" ] && [ -z "${rnaseq_right}" ]; then
  time transabyss --threads 24 --pe "${rnaseq_left}" --outdir "${TRANSABYSS_OUT}"
  ln -s "$(basename "${TRANSABYSS_OUT}")/transabyss-final.fa" "${RNASEQ_DIR}/${SRR_BASENAME}_transcript_transabyss.fa"

  # GMAP: Align Trinity transcripts to reference genome
  time /software/trinityrnaseq-v2.13.2/util/misc/process_GMAP_alignments_gff3_chimeras_ok.pl \
  --genome "${BASE_DIR}/target_${genomefile_id}/${genomefile_id}".min.fasta \
  --transcripts "${RNASEQ_DIR}/${SRR_BASENAME}_transcript_transabyss.fa" > "${BASE_DIR}/${out_dir}/${SRR_BASENAME}"_gmap_transcripts_transabyss_single.gff

  # Postprocessing: Adjust headers to minify
  # Extract NCBI accession numbers
  sed -E 's/^[a-z0-9|]*([A-Z]{4,6}[0-9]{8,11})\.[0-9].*(gmap.*)/\1\t\2/' "${BASE_DIR}/${out_dir}/${SRR_BASENAME}"_gmap_transcript_transabyss_single.gff > "${BASE_DIR}"/"${out_dir}"/"${SRR_BASENAME}"_gmap_transcript_transabyss_single.min.gff

# Both left and right SRR files specified
elif [ -n "${rnaseq_left}" ] && [ -n "${rnaseq_right}" ]; then
  time transabyss --threads 24 --pe "${rnaseq_left}" "${rnaseq_right}" --outdir "${TRANSABYSS_OUT}"
  ln -s "$(basename "${TRANSABYSS_OUT}")/transabyss-final.fa" "${RNASEQ_DIR}/${SRR_BASENAME}_transcript_transabyss.fa"

  # GMAP: Align Trinity transcripts to reference genome
  time /software/trinityrnaseq-v2.13.2/util/misc/process_GMAP_alignments_gff3_chimeras_ok.pl \
  --genome "${BASE_DIR}/target_${genomefile_id}/${genomefile_id}".min.fasta \
  --transcripts "${RNASEQ_DIR}/${SRR_BASENAME}_transcript_transabyss.fa" > "${BASE_DIR}/${out_dir}/${SRR_BASENAME}"_gmap_transcript_transabyss_paired.gff

  # Postprocessing: Adjust headers to minify
  # Extract NCBI accession numbers
  sed -E 's/^[a-z0-9|]*([A-Z]{4,6}[0-9]{8,11})\.[0-9].*(gmap.*)/\1\t\2/' "${BASE_DIR}/${out_dir}/${SRR_BASENAME}"_gmap_transcript_transabyss_paired.gff > "${BASE_DIR}"/"${out_dir}"/"${SRR_BASENAME}_"gmap_transcript_transabyss_paired.min.gff
fi

# Stringtie or transabyss-merge
find -L "${out_dir}" -maxdepth 1 -mindepth 1 -type f -regex '.*SRR[0-9]*.*transabyss.*\.min\.gff' > "${out_dir}"/stringtie_file_list_"${DATE_SAVED}".txt
echo "stringtie --merge -o ${out_dir}/stringtie_transabyss_transcripts_merged.gtf ${out_dir}/stringtie_file_list_${DATE_SAVED}.txt"
stringtie --merge -o "${out_dir}/stringtie_transabyss_transcripts_merged.gtf" "${out_dir}/stringtie_file_list_${DATE_SAVED}.txt"
[ ! -d "${out_dir}/jbrowse" ] && mkdir "${out_dir}"/jbrowse
[ ! -L "${out_dir}/jbrowse/stringtie_transabyss_transcripts_merged.gtf" ] && ln -sf "../stringtie_transabyss_transcripts_merged.gtf" "${out_dir}/jbrowse/stringtie_transabyss_transcripts_merged.gtf"
