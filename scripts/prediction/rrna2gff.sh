#!/bin/bash


showHelp() {
cat << EOF

Usage: rrna2gff.sh -i [path]

-i [path]         path to folder with result files from Rfam search, will reformat all files in folder

EOF
exit 0
}

OPTIND=1
while getopts ":i:h" opt; do
  case ${opt} in
    i )
      RRNAFILEPATH=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))


echo "#####################################################################################"
echo ""
echo "Converting Rfam search output into gff3 ..."
echo ""


for RRNAFILE in "${RRNAFILEPATH}"/*.tblout; do
  DIRNAME=$( dirname "${RRNAFILE}" )
  FILENAME=$( basename "${RRNAFILE}" )
  PREFIX="${FILENAME%.*}"
  echo "Parsing ${RRNAFILE} ..."
  # column 1: seqID
  # column 3: query name
  # column 4: Rfam accession
  # column 6: first base of sequence (with respect to search sequence)
  # column 7: last base of sequence (with respect to search sequence)
  # column 8: first base of the exon (1-based)
  # column 9: last base of the exon (1-based)
  # column 10: strand
  while IFS= read -r line || [[ -n $line ]]; do
    rrna_line="^(\S+)\s+[-]\s+(\S+)\s+(RF[0-9]+)\s+[A-Za-z]+\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([\+-])\s+.*$"
    if [[ "$line" =~ ${rrna_line} ]]; then
      gnxid=$( sed -n -E '0,/^[0-9ACGTNU:]{15}$/{s/^([0-9ACGTNU:]{15})$/\1/p}' gnx_gene_ids.txt )
      seqid="${BASH_REMATCH[1]}"
      queryname="${BASH_REMATCH[2]}"
      rfamacc="${BASH_REMATCH[3]}"
      seqstart="${BASH_REMATCH[4]}"
      seqend="${BASH_REMATCH[5]}"
      start="${BASH_REMATCH[6]}"
      end="${BASH_REMATCH[7]}"
      strand="${BASH_REMATCH[8]}"
      if [ "${start}" -lt "${end}" ] && [ "${strand}" == "+" ]; then
        cstart=${start}
        cend=${end}
      elif [ "${end}" -lt "${start}" ] && [ "${strand}" == "-" ]; then
        cstart=${end}
        cend=${start}
      else
        echo "something wrong with strand assignment ..."
        strand="."
      fi
      # ribosomal RNA genes
      RIBOSOMAL="5S_rRNA SSU_rRNA_eukarya LSU_rRNA_eukarya 5_8S_rRNA SSU_rRNA_microsporidia LSU_rRNA_bacteria SSU_rRNA_bacteria LSU_rRNA_archaea SSU_rRNA_archaea"
      # spliceosomal RNA genes
      SPLICEOSOMAL="U1 U2 U4 U5 U6 U4atac U6atac U11 U12 SmY U1_yeast"
      # RNase P RNA genes: RNaseP_nuc, RNaseP_bact_a, RNaseP_bact_b, RNase_MRP, RNaseP_arch, RNase_P, RNaseP-T
      RNASEP="RNaseP_nuc RNaseP_bact_a RNaseP_bact_b RNaseP_arch RNase_P RNaseP-T"
      # RNA component of telomerase
      TELOMERASE="Sacc_telomerase Telomerase-cil Telomerase-vert Telomerase_Asco"
      if [[ ${RIBOSOMAL} =~ (^|[[:space:]])$queryname($|[[:space:]]) ]]; then
        soterm="rRNA"
      elif [[ ${SPLICEOSOMAL} =~ (^|[[:space:]])$queryname($|[[:space:]]) ]]; then
        soterm="${queryname}_snRNA"
      elif [[ ${queryname} == "SmY" ]]; then
        soterm="snRNA"
      elif [[ ${queryname} == "U1_yeast" ]]; then
        soterm="U1_snRNA"
      # snoRNA genes, here: only U3
      elif [[ ${queryname} =~ U3 ]]; then
        soterm="snoRNA"
      elif [[ ${RNASEP} =~ (^|[[:space:]])$queryname($|[[:space:]]) ]]; then
        soterm="RNase_P_RNA"
      elif [[ ${TELOMERASE} =~ (^|[[:space:]])$queryname($|[[:space:]]) ]]; then
        soterm="telomerase_RNA"
      elif [[ ${queryname} =~ RNase_MRP ]]; then
        soterm="RNase_MRP_RNA"
      else
        soterm="ncRNA"
      fi

      querylen=$( grep -E "Query:\s+${queryname}\s" "${PREFIX}".out | sed -n -E 's/.*\[CLEN\=([0-9]+).*/\1/p' )
      description=$( grep -A 2 -E "Query:\s+${queryname}\s" "${PREFIX}".out | tail -1 | sed -n -E 's/^Description:\s+(\S.*)$/\1/p' )
      hitlen=$((cend-cstart+1))
      # 1-base counting
      seqlen=$((seqend-seqstart+1))
      if [ "${hitlen}" -gt "${seqlen}" ]; then
        difflen=$((hitlen-seqlen))
      else
        difflen=$((seqlen-hitlen))
      fi
      if [ "${seqstart}" == "1" ] && [ "${seqend}" == "${querylen}" ]; then
        seqtype="complete"
      # if more than 20% are missing, term it "fragment"
      elif [ ${difflen} -gt $((hitlen*20/100)) ]; then
        seqtype="fragment"
      else
        seqtype="partial_sequence"
      fi
      if { [ "${seqstart}" == "1" ] && [ "${seqend}" != "${querylen}" ]; } && ! [ ${seqtype} == "fragment" ]; then
        threeprimetype=",3'-incomplete"
      else
        threeprimetype=""
      fi
      if { [ "${seqstart}" != "1" ] && [ "${seqend}" == "${querylen}" ]; } && ! [ ${seqtype} == "fragment" ]; then
        fiveprimetype=",5'-incomplete"
      else
        fiveprimetype=""
      fi
      if { [ "${seqstart}" != "1" ] && [ "${seqend}" != "${querylen}" ]; } && ! [ ${seqtype} == "fragment" ]; then
        threeprimetype=",3'-incomplete"
        fiveprimetype=",5'-incomplete"
      fi
      {
        echo "${seqid}"$'\t'GOENOMICS$'\t'gene$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=gene-${gnxid};gene_biotype=${queryname};Rfam-accession=${rfamacc};qlen=${querylen};genelen=${hitlen};sequence=${seqtype}${threeprimetype}${fiveprimetype};Note=${description}, ${seqtype}${threeprimetype}${fiveprimetype}"
        echo "${seqid}"$'\t'GOENOMICS$'\t'"${soterm}"$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=rna-${gnxid};Parent=gene-${gnxid};product=${description}"
        echo "${seqid}"$'\t'GOENOMICS$'\t'exon$'\t'"${cstart}"$'\t'"${cend}"$'\t'.$'\t'"${strand}"$'\t'.$'\t'"ID=exon-${gnxid};Parent=rna-${gnxid};product=${description}"
      } >> "${DIRNAME}"/"${PREFIX}".gff
      sed -i -E "s/^(${gnxid})$/\1 taken/" gnx_gene_ids.txt
    fi
  done < "${RRNAFILE}"
done