#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

ext_start=$(date +%s)
ext_start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE
unset FULLFOLDER
unset SELECTEDFOLDER
unset REF_ANNOT

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 03b.run_multiple.stringtie.sh -g [id] (-a OR -r [first-last]) <optional -o [path] -b [path] -f>

Assumption: Script is run with absolute paths, or outside the "prediction" directory.
            If the data structure has correctly been set up, the output-dir == input-dir.
            STAR-aligner needs to be run first.

mandatory
-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )

choice
-a               all, get all SRA-files in the input directory
-r [first-last]  range of SRA-files (format SRR14672191-SRR14672195)

optional
-b [path]        reference annotation (optional; full path)
-o [path]        basic output data directory (will be the current directory if option not set)
                 Results will be written into basic_output_data_dir/prediction/stringtie
-f               force overwrite

example:
./03b.run_multiple.stringtie.sh -g JAATWF010000000 -a -o /mnt/customer/self

-h   Show this help

EOF
exit 0
}
#-i [path]        input data directory (where the SRRnnn directories with the sorted bam-files from e.g. STAR are located)

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')
REF_ANNOT=""

OPTIND=1
while getopts ":b:g:fo:ar:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    a )
      FULLFOLDER="true"
      ;;
    r )
      INPUTFILERANGE=$OPTARG
      SELECTEDFOLDER="true"
      ;;
    b )
      REF_ANNOT=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

# Setting "multi-STRINGTIE" variable so that config-script is not invoked twice.
MULTI_STRINGTIE="true"
# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="stringtie"
. config.sh

if [ -n "${FORCEOVERWRITE}" ]; then
  FORCEOVERWRITEOPTION=" -f"
else
  FORCEOVERWRITEOPTION=""
fi

if [[ -z ${REF_ANNOT} ]]; then
  USE_REF_ANNOT=""
elif [[ -n ${REF_ANNOT} ]] && [ ! -f "${REF_ANNOT}" ]; then
  echo "The reference annotation ${REF_ANNOT} file cannot be found, please provide a correct path!"
  exit 1
elif [[ -n ${REF_ANNOT} ]] && [ -f "${REF_ANNOT}" ]; then
  USE_REF_ANNOT="-r ${REF_ANNOT}"
fi

STATUSFILE="03b.run_multiple.stringtie"_"${DATE_SAVED}".out
LOGFILE="03b.run_multiple.stringtie"_"${DATE_SAVED}".log

#METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

echo "#####################################################################################"
echo ""
echo "03b.run_multiple.stringtie.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $ext_start_date"
echo ""
echo "Getting the list of directories with STAR output ..."
echo ""

if [[ -n "${FULLFOLDER}" ]]; then
  echo "Getting the list of STAR-files to assemble ..."
  echo ""
  find -L "${MAIN_OUT_DIR}"/star -maxdepth 1 -mindepth 1 -type d -name 'SRR*' | sort -n > "$METADATAFOLDER"/star_dir_list_"${DATE_SAVED}".txt
fi
if [[ -n "${SELECTEDFOLDER}" ]]; then
  echo "Preparing STAR-files from ${INPUTFILERANGE} ..."
  echo ""
  FIRST=$( echo "${INPUTFILERANGE}" | cut -d "-" -f 1 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  LAST=$( echo "${INPUTFILERANGE}" | cut -d "-" -f 2 | sed -E -n 's/SRR([0-9]*)/\1/p' )
  [ ! -f "$METADATAFOLDER"/star_dir_list_"${DATE_SAVED}".txt ] && touch "$METADATAFOLDER"/star_dir_list_"${DATE_SAVED}".txt
  for i in $(seq "${FIRST}" "${LAST}"); do
    find -L "${MAIN_OUT_DIR}"/star -maxdepth 1 -mindepth 1 -type d -name "SRR${i}" >> "$METADATAFOLDER"/star_dir_list_"${DATE_SAVED}".txt
  done
fi

echo "-------------------------------------------------------------------------------------"
echo ""
echo "Running StringTie on all/selected STAR output files ..."
echo ""
MAX_PARALLEL=5
echo "Max ${MAX_PARALLEL} jobs in parallel ..."
echo ""
# The following line might better be de-commented when script is run manually. The line
# ensures that all processes started within the loop will be killed if the script is
# terminated prematurely. This is not needed within the pipeline, where it might lead
# to an unwanted EXIT because of an error within one of the executed scripts in the loop.
# trap 'echo killing all children; kill 0' EXIT

while IFS= read -r line || [[ -n $line ]]
  do
    sleep 10   # Important! Otherwise subprocesses might run out of tracking.
    SRAACC=$( basename "${line}" )
    . ./03.run.stringtie.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -i "${line}" -o "${BASE_DIR}" "${USE_REF_ANNOT}" &
    # track job ids to wait for later
    pids[${SRAACC}]=$!
    # Wait here for any job to be finished, so there is a place to start next one.
    if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi

done < "$METADATAFOLDER"/star_dir_list_"${DATE_SAVED}".txt

# wait for all pids
for pid in ${pids[*]}; do
  wait $pid
done


ext_end=$(date +%s)
ext_end_date=$(date +%T)
runtime=$((ext_end-ext_start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $ext_end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"