#!/usr/bin/env bash
################################################################################
# NOTE:
# Need to be run in AUGUSTUS Docker container to have all software available.
################################################################################

start=$(date +%s)
start_date=$(date '+%T, %A %d-%B, %Y')

unset FORCEOVERWRITE
unset TRANSTABLE

COMMANDARGS=$*

showHelp() {
cat << EOF

Usage: 06b.run_multiple.gmap.sh -g [id] -i [path] <optional -o [path] -t [num] -f>

-g [id]          genome file id (id of genome file = old structure, might be replaced by uuid )
-i [path]        input data directory/file (where the TSA-files to be mapped are located)
-o [path]        basic output data directory (will be the current directory if option not set)
                 If genome-file-id and basic-output-data-dir are given, results will be written
                 into basic_output_data_dir/prediction/gmap
-t [num]         genetic code translation table (default: 1; numbers as of NCBI list)
-f               force overwrite

-h   Show this help

example:
./06b.run_multiple.gmap.sh -g JAATWF010000000 -i /mnt/customer/self/prediction_JAATWF010000000/trinity -o /mnt/customer/self

EOF
exit 0
}

[ $# -eq 0 ] && showHelp
DATE_SAVED=$(date +'%y%m%d_%H%M')

OPTIND=1
while getopts ":g:fi:o:t:h" opt; do
  case ${opt} in
    g )
      GENOMEFILEID=$OPTARG
      ;;
    f )
      FORCEOVERWRITE="true"
      ;;
    i )
      INPUTFILES=$OPTARG
      ;;
    o )
      BASE_DIR=$OPTARG
      ;;
    t )
      TRANSTABLE=$OPTARG
      ;;
    h|\? )
      showHelp
      ;;
    : )
      echo "Compulsory argument missing for option: -$OPTARG"
      showHelp
      ;;
  esac
done
shift $((OPTIND-1))

# Setting "multi-GMAP" variable so that config-script is not invoked twice.
MULTI_GMAP="true"
# The config-script will set all PATH-variables, check for presence or mkdir subfolders.
# Turn debugging on within the config-script.
SCRIPT="gmap"
. config.sh

if [ ! -d "${INPUTFILES}" ]; then
  echo "The input data folder ${INPUTFILES} does not exist, please provide a correct path!"
  exit 1
fi

if [ -n "${FORCEOVERWRITE}" ]; then
  FORCEOVERWRITEOPTION=" -f"
else
  FORCEOVERWRITEOPTION=""
fi

if [ -z "${TRANSTABLE}" ]; then
  TRANSTABLE=1
fi

STATUSFILE="06b.run_multiple.gmap"_"${DATE_SAVED}".out
LOGFILE="06b.run_multiple.gmap"_"${DATE_SAVED}".log

#METADATAFOLDER="${MAIN_OUT_DIR}/status_log"
#[ ! -d "${METADATAFOLDER}" ] && mkdir -p "${METADATAFOLDER}"

STATUSFILEPATH="${METADATAFOLDER}/${STATUSFILE}"
LOGFILEPATH="${METADATAFOLDER}/${LOGFILE}"
### STDOUT to STATUSFILEPATH, stderr to LOGFILEPATH and all output to the screen
exec > >(tee "${STATUSFILEPATH}") 2> >(tee "${LOGFILEPATH}" >&2)

echo "#####################################################################################"
echo ""
echo "06b.run_multiple.gmap.sh ${COMMANDARGS}"
echo ""
echo "Start of script: $start_date"
echo ""

################################################################################
# Build the genome index once
echo "-------------------------------------------------------------------------------------"
echo ""
echo "Building the database genome index for GMAP ..."
echo ""
# Directory for gmap_build - Tool for genome database creation for GMAP or GSNAP
# ../target_"${GENOMEFILEID}"/"${GENOMEGMAP_DIR}"/
GENOMEFILEIDMIN=$( head -1 "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta | sed 's/>//' )
GENOMEGMAP_DIR="${GENOMEFILEIDMIN}".gmap
# GENOMEGMAP_DIR="${GENOMEFILEID}".gmap

if { [ ! -s "${GENOME_DIR}"/"${GENOMEGMAP_DIR}"/"${GENOMEFILEIDMIN}".gmap.chromosome ] && \
   [ ! -s "${GENOME_DIR}"/"${GENOMEGMAP_DIR}"/"${GENOMEFILEIDMIN}".gmap.genomecomp ]; } || [ -n "${FORCEOVERWRITE}" ]; then
  gmap_build -D "${GENOME_DIR}" -d "${GENOMEGMAP_DIR}" -k 13 "${GENOME_DIR}"/"${GENOMEFILEID}".min.fasta >&2
else
  echo "Genome index for ${GENOMEFILEID}.min.fasta already created ..."
  echo ""
fi

if [ ! -d "${GENOMEMAPS_DIR}" ]; then
  mkdir -p "${GENOME_DIR}"/"${GENOMEMAPS_DIR}"
fi

################################################################################
# Running GMAP on full directory
echo "-------------------------------------------------------------------------------------"
echo ""
echo "Start parallel mapping of files in ${INPUTFILES} against ${GENOMEFILEID}.min.fasta with GMAP ..."
echo ""

if [ ! -f "${OUT_DIR}"/tsa_file_list_"${DATE_SAVED}".txt ]; then
  find -L "${INPUTFILES}" -maxdepth 1 -mindepth 1 -type f -name '*.fasta' | sort -n > "${OUT_DIR}"/tsa_file_list_"${DATE_SAVED}".txt
fi
MAX_PARALLEL=5
echo "Max ${MAX_PARALLEL} jobs in parallel ..."
echo ""
# The following line might better be de-commented when script is run manually. The line
# ensures that all processes started within the loop will be killed if the script is
# terminated prematurely. This is not needed within the pipeline, where it might lead
# to an unwanted EXIT because of an error within one of the executed scripts in the loop.
trap 'echo killing all children; kill 0' EXIT

while IFS= read -r line || [[ -n $line ]]
  do
    echo ". ./06.run.gmap.sh -g ${GENOMEFILEID} -i ${line} -o ${BASE_DIR} -t ${TRANSTABLE}${FORCEOVERWRITEOPTION} &"
    . ./06.run.gmap.sh -g "${GENOMEFILEID}"${FORCEOVERWRITEOPTION} -i "${line}" -o "${BASE_DIR}" -t ${TRANSTABLE} &
    # track job ids to wait for later
    pids[${SRAACC}]=$!
    # Wait here for any job to be finished, so there is a place to start next one.
    if [[ $(jobs -r -p | wc -l) -ge ${MAX_PARALLEL} ]]; then wait -n; fi
done < "${OUT_DIR}"/tsa_file_list_"${DATE_SAVED}".txt

# wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done


end=$(date +%s)
end_date=$(date +%T)
runtime=$((end-start))
hours=$((runtime / 3600)); minutes=$(( (runtime % 3600) / 60 )); seconds=$(( (runtime % 3600) % 60 ));
echo ""
echo "Time now: $end_date"
echo ""
echo "Runtime: $hours:$minutes:$seconds (hh:mm:ss)"
echo ""
echo "#####################################################################################"