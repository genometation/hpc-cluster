#!/bin/bash

path=/mnt/databases/taxdump
server=/home/genwork/server/genomebank

cd $server
source venv/bin/activate
##  Initial Import of TaxNames and TaxNodes
time python manage.py stage_import_ncbi_tax_names --mode=initial $path/taxdmp_2014-08-01 2014-08-01
time python manage.py stage_import_ncbi_tax_nodes --mode=initial $path/taxdmp_2014-08-01 2014-08-01
for f in $path/*.zip;
do
    dir=$(dirname "$f")
    bf=$(basename "$f" | cut -d. -f1)
    ts=$(basename "$bf" | cut -d_ -f2)
    ##  Import TaxNames with Timestamp as Release-Date
    time python manage.py stage_import_ncbi_tax_names --mode=update $dir/$bf $ts
    time python manage.py stage_import_ncbi_tax_nodes --mode=update $dir/$bf $ts
done
time python manage.py stage_import_ncbi_tax_names --mode=add_constraints
time python manage.py stage_import_ncbi_tax_nodes --mode=add_constraints
time python manage.py stage_import_ncbi_tax_nodes --mode=generate_lineages
