#!/bin/bash

## Incremental/Mirror Backup: StorAdenin to backup storage StorThymin

_src_base="/nfs/zfs_adenin"
_dst_base="/srv/zfspool"
_logfile_sym_base="/home/genwork/rsync.log"
_logfile_base="/home/genwork/backup_logs/rsync.log"
_dow="$(date +'%w')"

## Directories to include in backup
declare -a dirs=("assemblies"
                "backup/databases"
                "databases"
                "genomes"
                "publications"
                "sources"
                "species"
                "test"
                )
# Run backup with rsync (keep logs of past 7 days)
for dir in "${dirs[@]}"
do
   echo "Backup folder ${_src_base}/$dir to ${_dst_base}"
   # Incremental: Keep all former files of StorAdenin
   echo "rsync -ahuv --stats ${_src_base}/${dir} ${_dst_base} > ${_logfile_base}.${dir}.${_dow} 2>&1"
   rsync -ahuv --stats ${_src_base}/${dir} ${_dst_base} > ${_logfile_base}.${dir}.${_dow} 2>&1
#   # Mirror state of StorAdenin (--delete removed files in backup)
#   echo "rsync -ahuv --stats --delete ${_src_base}/${dir} ${_dst_base} > ${_logfile_base}.${dir}.${_dow} 2>&1"
#   rsync -ahuv --stats --delete ${_src_base}/${dir} ${_dst_base} > ${_logfile_base}.${dir}.${_dow} 2>&1

   # Link latest logs
   rm -f ${_logfile_sym_base}.${dir}.latest
   ln -s ${_logfile_base}.${dir}.${_dow} ${_logfile_sym_base}.${dir}.latest
done
